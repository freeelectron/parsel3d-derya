#pragma once
#include "resource.h"
#include "NpPreviewCtrl.h"

// CDlgKesitGor dialog

class CDlgKesitGor : public CAcUiDialog
{
	DECLARE_DYNAMIC(CDlgKesitGor)

public:
	CDlgKesitGor(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgKesitGor();

// Dialog Data
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_KESITGOR };
//#endif
public:
	//////////////////////////////////////////////////////////////////////////////
	// takes a drawing and updates the GsView with it
	Acad::ErrorStatus InitDrawingControl(AcDbDatabase *pDb, const TCHAR *space = ACDB_MODEL_SPACE);
	// resets the view to the passed AcDbDatabase
	// util function to toggle the tick for a specific menu item
	bool toggleCheckMenuItem(UINT resId);
	// util functions for setting the tick for a specific menu item
	void setMenuCheck(UINT resId, UINT nCheck = MF_CHECKED);
	void setMenuUnCheck(UINT resId);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);

public:
	CNpPreviewCtrl mPreviewCtrl;
	AcDbDatabase *mCurrentDwg;

	AcGeMatrix3d m_viewMatrix;

	// - take into account previous temporary entities
	AcDbExtents m_tempExt;
};
