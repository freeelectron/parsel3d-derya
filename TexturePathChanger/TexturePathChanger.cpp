// (C) Copyright 2002-2007 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.
//

//-----------------------------------------------------------------------------
//- TexturePathChagner.cpp : Initialization functions
//-----------------------------------------------------------------------------
#include "StdAfx.h"
#include "resource.h"
#include <afxdllx.h>

//-----------------------------------------------------------------------------
//- Define the sole extension module object.
AC_IMPLEMENT_EXTENSION_MODULE(TexturePathChagnerDLL)
//- Please do not remove the 3 following lines. These are here to make .NET MFC Wizards
//- running properly. The object will not compile but is require by .NET to recognize
//- this project as being an MFC project
#ifdef NEVER
AFX_EXTENSION_MODULE TexturePathChagnerExtDLL ={ NULL, NULL } ;
#endif

//- Now you can use the CAcModuleResourceOverride class in
//- your application to switch to the correct resource instance.
//- Please see the ObjectARX Documentation for more details

//-----------------------------------------------------------------------------
//- DLL Entry Point
extern "C"
BOOL WINAPI DllMain (HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved) {
	//- Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved) ;

	if ( dwReason == DLL_PROCESS_ATTACH ) {
        _hdllInstance =hInstance ;
		TexturePathChagnerDLL.AttachInstance (hInstance) ;
		InitAcUiDLL () ;
	} else if ( dwReason == DLL_PROCESS_DETACH ) {
		TexturePathChagnerDLL.DetachInstance () ;
	}
	return (TRUE) ;
}


CString GetFilePath(const CString str)
{
	int r = str.ReverseFind('\\');
	if (r == -1)
		r = str.ReverseFind('/');
	return str.Left(r);
}

CString GetFileName(const CString str)
{
	int r = str.ReverseFind('\\');
	if (r == -1)
		r = str.ReverseFind('/');
	return str.Right(str.GetLength() - r - 1);
}

void CreateMaterial(const ACHAR* name, AcDbDatabase *pDb)
{
	AcDbDictionary *pMaterialDict;
	Acad::ErrorStatus es;
	es = pDb->getMaterialDictionary(pMaterialDict, AcDb::kForWrite);
	if (es == Acad::eOk)
	{
		AcGiImageFileTexture tex;
		tex.setSourceFileName(_T("D:\\sil\\100x200.png"));

		double uScale = 1.0;
		double vScale = 1.0;
		double uOffset = 0;
		double vOffset = 0;

		AcGeMatrix3d mx;
		mx(0, 0) = uScale;
		mx(0, 1) = 0;
		mx(0, 2) = 0;
		mx(0, 3) = uScale * uOffset;

		mx(1, 0) = 0;
		mx(1, 1) = vScale;
		mx(1, 2) = 0;
		mx(1, 3) = vScale * vOffset;

		mx(2, 0) = 0;
		mx(2, 1) = 0;
		mx(2, 2) = 1;
		mx(2, 3) = 0;

		mx(3, 0) = 0;
		mx(3, 1) = 0;
		mx(3, 2) = 0;
		mx(3, 3) = 1;

		AcGiMapper mapper;
		mapper.setProjection(AcGiMapper::Projection::kBox);
		mapper.setUTiling(AcGiMapper::Tiling::kCrop);
		mapper.setVTiling(AcGiMapper::Tiling::kCrop);
		mapper.setAutoTransform(AcGiMapper::AutoTransform::kNone);
		mapper.setTransform(mx);

		AcGiMaterialMap map;
		map.setSourceFileName(name);
		map.setTexture(&tex);
		map.setBlendFactor(1.0);
		map.setMapper(mapper);

		AcDbMaterial *pMaterialObj	= new AcDbMaterial();
		pMaterialObj->setName(name);

		AcDbObjectId materialId;
		es = pMaterialDict->setAt(name, pMaterialObj, materialId);

		AcGiMaterialColor diffuseColor;
		pMaterialObj->setDiffuse(diffuseColor, map);
		pMaterialObj->setMode(AcGiMaterialTraits::kRealistic);

		pMaterialObj->close();
		pMaterialDict->close();
	}
}

void ChangeTexturePath()
{
	AcDbDictionary *pMaterialDict;
	AcDbDatabase *pDb = acdbHostApplicationServices()->workingDatabase();
	Acad::ErrorStatus es;
	//es = acdbHostApplicationServices()->workingDatabase()->getNamedObjectsDictionary(pDict, AcDb::kForRead);
	es = pDb->getMaterialDictionary(pMaterialDict, AcDb::kForWrite);
	CString dwgname = acdbHostApplicationServices()->getEnv(_T("DWGNAME"));
	if (es == Acad::eOk)
	{
		AcDbDictionaryIterator *pIter;

		AcDbObject *pObj;
		pIter = pMaterialDict->newIterator();

		AcDbMaterial *pMaterialObj;

		int i = 0;
		for (; !pIter->done(); pIter->next())
		{
			pIter->getObject(pObj, AcDb::kForWrite);

			pMaterialObj = AcDbMaterial::cast(pObj);

			if (NULL != pMaterialObj)
			{
				AcGiMaterialMap map;
				AcGiMaterialColor diffuseColor;

				pMaterialObj->diffuse(diffuseColor, map);

				const AcGiMaterialTexture* mattex = map.texture();

				CString sfn = map.sourceFileName();
				if (!sfn.IsEmpty())
				{
					ACHAR drive[_MAX_PATH];
					ACHAR dir[_MAX_PATH];
					ACHAR fname[_MAX_PATH];
					ACHAR ext[_MAX_PATH];

					// get dwg file path
					_tsplitpath(curDoc()->fileName(), drive, dir, fname, ext);
					CString dwgpath = CString(drive) + CString(dir);
					
					_tsplitpath(sfn, drive, dir, fname, ext);
					CString newname = dwgpath + fname + ext;

					acutPrintf(_T("\n---[%d]----\nTexture Eski Dosya Ad�: %s\nTexture Yeni Dosya Ad�: %s\n"), ++i, sfn, newname);
					AcGiImageFileTexture tex;
					tex.setSourceFileName(newname);
					map.setTexture(&tex);
					pMaterialObj->setDiffuse(diffuseColor, map);
				}
			}
			pMaterialObj->close();
		}
		delete pIter;
		pMaterialDict->close();

	}
}

void command_ChangeTexturePath()
{
	ChangeTexturePath();
}