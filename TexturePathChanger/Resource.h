//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TexturePathChagner.rc
//
#define IDS_PROJNAME                    100
#define IDI_ORBIT                       159
#define IDD_BLOCKVIEW                   160
#define IDC_LOADDRAWING                 210
#define IDR_MAINMENU                    297
#define ID_TOOLS_PRINT                  298
#define ID_FILE_ACGSCONFIG              299
#define ID_FILE_OPEN300                 300
#define IDD_YOLTANIM                    302
#define IDD_KESITGOR                    304
#define IDC_EDIT1                       400
#define IDC_VIEW_YOLTANIM               401
#define IDI_CROSS                       1194
#define IDI_PAN                         1478
#define ID_VIEW_RESET                   32768
#define ID_VIEW_SETTINGS                32769
#define ID_SHOWLINETYPES                32770
#define ID_SHOWMATERIALS                32771
#define ID_SHOWSECTIONING               32772
#define ID_VIEW_RENDERTYPE              32773
#define ID_RENDERTYPE_KHIGHLIGHT        32779
#define ID_RENDERERTYPE_KDEFAULT        32780
#define ID_RENDERERTYPE_KSOFTWARE       32781
#define ID_RENDERERTYPE_KSOFTWARENEWVIEWSONLY 32782
#define ID_RENDERERTYPE_KFULLRENDER     32783
#define ID_RENDERERTYPE_KSELECTIONRENDER 32784
#define ID_VIEW_ZOOM                    32785
#define ID_ZOOM_WINDOW                  32786
#define ID_ZOOM_EXTENTS                 32787
#define ID_VIEW_RESETVIEW               32788
#define ID_VIEW_SETTINGS32789           32789
#define ID_SETTINGS_VISUALSTYLE         32790
#define IDC_VIEW_KESITGOR               32791

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        306
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         402
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
