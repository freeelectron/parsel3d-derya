#pragma once
#include "GsPreviewCtrl.h"

// CNpDrawingCtrl

class CNpPreviewCtrl : public CGsPreviewCtrl
{
	DECLARE_DYNAMIC(CNpPreviewCtrl)

public:
	CNpPreviewCtrl();
	virtual ~CNpPreviewCtrl();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
//	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
//	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};


