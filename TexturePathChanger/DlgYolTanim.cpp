// DlgYolTanim.cpp : implementation file
//

#include "stdafx.h"
#include "DlgYolTanim.h"
//#include "afxdialogex.h"


// CDlgYolTanim dialog

IMPLEMENT_DYNAMIC(CDlgYolTanim, CAcUiDialog)

CDlgYolTanim::CDlgYolTanim(CWnd* pParent /*=NULL*/)
	: CAcUiDialog(CDlgYolTanim::IDD, pParent)
{

//	EnableAutomation();

}

CDlgYolTanim::~CDlgYolTanim()
{
}
/*
void CDlgYolTanim::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CAcUiDialog::OnFinalRelease();
}
*/
void CDlgYolTanim::DoDataExchange(CDataExchange* pDX)
{
	CAcUiDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgYolTanim, CAcUiDialog)
END_MESSAGE_MAP()

//BEGIN_DISPATCH_MAP(CDlgYolTanim, CAcUiDialog)
//END_DISPATCH_MAP()

// Note: we add support for IID_IDlgYolTanim to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {44CA46A4-6C7C-4B9E-8291-17553A45FED7}
//static const IID IID_IDlgYolTanim =
//{ 0x44CA46A4, 0x6C7C, 0x4B9E, { 0x82, 0x91, 0x17, 0x55, 0x3A, 0x45, 0xFE, 0xD7 } };

//BEGIN_INTERFACE_MAP(CDlgYolTanim, CAcUiDialog)
//	INTERFACE_PART(CDlgYolTanim, IID_IDlgYolTanim, Dispatch)
//END_INTERFACE_MAP()


// CDlgYolTanim message handlers
