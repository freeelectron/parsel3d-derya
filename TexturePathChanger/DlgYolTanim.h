#pragma once
#include "resource.h"

// CDlgYolTanim dialog

class CDlgYolTanim : public CAcUiDialog
{
	DECLARE_DYNAMIC(CDlgYolTanim)

public:
	CDlgYolTanim(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgYolTanim();

//	virtual void OnFinalRelease();

// Dialog Data
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_YOLTANIM };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
//	DECLARE_DISPATCH_MAP()
//	DECLARE_INTERFACE_MAP()
};
