﻿using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsel3D
{
    class GeoCalc
    {
        const double PI = 3.1415926535897932384626433832795;
        const double FOURTHPI = 3.1415926535897932384626433832795 / 4;
        const double deg2rad = 3.1415926535897932384626433832795 / 180;
        const double rad2deg = 180.0 / 3.1415926535897932384626433832795;
        const int ELP_GRS80 = 11;
        const int ELP_HAYFORD = 14;
        const int ELP_WGS84 = 23;

        class Ellipsoid
        {
            public int id;
            public string ellipsoidName;
            public double EquatorialRadius;
            public double eccentricitySquared;

            public Ellipsoid(int Id, string name, double radius, double ecc)
            {
                id = Id;
                ellipsoidName = name;
                EquatorialRadius = radius;
                eccentricitySquared = ecc;
            }
        }

        static Ellipsoid[] ellipsoid =
                                {//  id, Ellipsoid name, Equatorial Radius, square of eccentricity	
                                new Ellipsoid(-1, "Placeholder", 0, 0),//placeholder only, To allow array indices to match id numbers
								new Ellipsoid(1, "Airy", 6377563, 0.00667054),
                                new Ellipsoid(2, "Australian National", 6378160, 0.006694542),
                                new Ellipsoid(3, "Bessel 1841", 6377397, 0.006674372),
                                new Ellipsoid(4, "Bessel 1841 (Nambia) ", 6377484, 0.006674372),
                                new Ellipsoid(5, "Clarke 1866", 6378206, 0.006768658),
                                new Ellipsoid(6, "Clarke 1880", 6378249, 0.006803511),
                                new Ellipsoid(7, "Everest", 6377276, 0.006637847),
                                new Ellipsoid(8, "Fischer 1960 (Mercury) ", 6378166, 0.006693422),
                                new Ellipsoid(9, "Fischer 1968", 6378150, 0.006693422),
                                new Ellipsoid(10, "GRS 1967", 6378160, 0.006694605),
                                new Ellipsoid(ELP_GRS80, "GRS 1980", 6378137, 0.00669438),
                                new Ellipsoid(12, "Helmert 1906", 6378200, 0.006693422),
                                new Ellipsoid(13, "Hough", 6378270, 0.00672267),
                                new Ellipsoid(ELP_HAYFORD, "International (Hayford)", 6378388, 0.006722670022333),
                                new Ellipsoid(15, "Krassovsky", 6378245, 0.006693422),
                                new Ellipsoid(16, "Modified Airy", 6377340, 0.00667054),
                                new Ellipsoid(17, "Modified Everest", 6377304, 0.006637847),
                                new Ellipsoid(18, "Modified Fischer 1960", 6378155, 0.006693422),
                                new Ellipsoid(19, "South American 1969", 6378160, 0.006694542),
                                new Ellipsoid(20, "WGS 60", 6378165, 0.006693422),
                                new Ellipsoid(21, "WGS 66", 6378145, 0.006694542),
                                new Ellipsoid(22, "WGS-72", 6378135, 0.006694318),
                                new Ellipsoid(ELP_WGS84, "WGS-84", 6378137, 0.00669437999013)
                                };

        public static void UTMtoLL(int ReferenceEllipsoid, double UTMNorthing, double UTMEasting, int dob, out double Lat, out double Long)
        {
            //converts UTM coords to lat/long.  Equations from USGS Bulletin 1532 
            //East Longitudes are positive, West longitudes are negative. 
            //North latitudes are positive, South latitudes are negative
            //Lat and Long are in decimal degrees. 
            //Written by Chuck Gantz- chuck.gantz@globalstar.com
            //Converted from c++ by Derya KILIÇ 2017.07.12

            double k0 = 1.0;//0.9996;
            double a = ellipsoid.ElementAt<Ellipsoid>(ReferenceEllipsoid).EquatorialRadius;
            double eccSquared = ellipsoid[ReferenceEllipsoid].eccentricitySquared;
            double eccPrimeSquared;
            double e1 = (1 - Math.Sqrt(1 - eccSquared)) / (1 + Math.Sqrt(1 - eccSquared));
            double N1, T1, C1, R1, D, M;
            double mu, phi1, phi1Rad;
            double x, y, LongOrigin;

            x = UTMEasting - 500000.0; //remove 500,000 meter offset for longitude
            y = UTMNorthing;

            LongOrigin = dob;
            int ZoneNumber = (int)((LongOrigin + 180) / 3) + 1;

            eccPrimeSquared = (eccSquared) / (1 - eccSquared);

            M = y / k0;
            mu = M / (a * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256));

            phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.Sin(2 * mu)
                + (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32) * Math.Sin(4 * mu)
                + (151 * e1 * e1 * e1 / 96) * Math.Sin(6 * mu);
            phi1 = phi1Rad * rad2deg;

            N1 = a / Math.Sqrt(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad));
            T1 = Math.Tan(phi1Rad) * Math.Tan(phi1Rad);
            C1 = eccPrimeSquared * Math.Cos(phi1Rad) * Math.Cos(phi1Rad);
            R1 = a * (1 - eccSquared) / Math.Pow(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad), 1.5);
            D = x / (N1 * k0);

            Lat = phi1Rad - (N1 * Math.Tan(phi1Rad) / R1) * (D * D / 2 - (5 + 3 * T1 + 10 * C1 - 4 * C1 * C1 - 9 * eccPrimeSquared) * D * D * D * D / 24
                + (61 + 90 * T1 + 298 * C1 + 45 * T1 * T1 - 252 * eccPrimeSquared - 3 * C1 * C1) * D * D * D * D * D * D / 720);
            Lat = Lat * rad2deg;

            Long = (D - (1 + 2 * T1 + C1) * D * D * D / 6 + (5 - 2 * C1 + 28 * T1 - 3 * C1 * C1 + 8 * eccPrimeSquared + 24 * T1 * T1)
                * D * D * D * D * D / 120) / Math.Cos(phi1Rad);
            Long = LongOrigin + Long * rad2deg;
        }
        public static void LLtoUTM(int ReferenceEllipsoid, double Lat, double Long, double referansdob, out double UTMNorthing, out double UTMEasting, ref double hesaplanandob)
        {
            UTMNorthing = 0;
            UTMEasting = 0;
        }

        public static List<Point3dCollection> ConvertGridToLatLongListPColl(List<Point3dCollection> listpcoll, int dob)
        {
            var ret = new List<Point3dCollection>();
            double latitude;
            foreach (var list in listpcoll)
            {
                var p3dcoll = new Point3dCollection();
                foreach (Point3d po in list)
                {
                    UTMtoLL(GeoCalc.ELP_WGS84, po.Y, po.X, dob, out latitude, out double longitude);
                    //p3dcoll.Add(new Point3d(latitude, longitude, po.Z));
                    p3dcoll.Add(new Point3d(longitude, latitude, po.Z));
                }
                ret.Add(p3dcoll);
            }
            return ret;
        }
        public static List<List<Point3dCollection>> ConvertGridToLatLongListListPColl(List<List<Point3dCollection>> listlistpcoll, int dob)
        {
            var ret = new List<List<Point3dCollection>>();
            foreach (var listpcoll in listlistpcoll)
            {
                ret.Add(ConvertGridToLatLongListPColl(listpcoll, dob));
            }
            return ret;
        }
        public static Point3dCollection ConvertGridToLatLong(Point3dCollection pcoll, int dob)
        {
            Point3dCollection ret = new Point3dCollection();
            double latitude;
            foreach (Point3d po in pcoll)
            {
                UTMtoLL(GeoCalc.ELP_WGS84, po.Y, po.X, dob, out latitude, out double longitude);
                //ret.Add(new Point3d(latitude, longitude, po.Z));
                ret.Add(new Point3d(longitude, latitude, po.Z));
            }
            return ret;
        }
    }
}
