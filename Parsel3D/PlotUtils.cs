﻿using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.PlottingServices;

using Autodesk.AutoCAD.Interop.Common;
using Autodesk.AutoCAD.Interop;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using System.Collections.Specialized;


// Plots the current layout to a DWF file
namespace Parsel3D
{
    public partial class PlanExport
    {
       // [CommandMethod("PlotLayout")]
        public static void PlotWindowAlternative(string filename, string devname, string medname, string stylename, Extents2d window, bool backgroundplot)
        {
            // Get the current document and database, and start a transaction
            Document acDoc = Application.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;

            AcadApplication app = (AcadApplication)Autodesk.AutoCAD.ApplicationServices.Application.AcadApplication;
            app.ActiveDocument.SetVariable("backgroundplot", backgroundplot ? 1 : 0);

            if (devname.Contains("PDF"))
                filename += ".pdf";
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {

                BlockTableRecord btr = (BlockTableRecord)acTrans.GetObject(acCurDb.CurrentSpaceId, OpenMode.ForRead);

                // Reference the Layout Manager
                LayoutManager acLayoutMgr = LayoutManager.Current;

                // Get the current layout and output its name in the Command Line window
                //Layout acLayout = (Layout)acTrans.GetObject(btr.LayoutId, OpenMode.ForRead);
                Layout acLayout = acTrans.GetObject(acLayoutMgr.GetLayoutId(acLayoutMgr.CurrentLayout), OpenMode.ForRead) as Layout;

                // Get the PlotInfo from the layout
                using (PlotInfo acPlInfo = new PlotInfo())
                {
                    acPlInfo.Layout = acLayout.ObjectId;

                    // Get a copy of the PlotSettings from the layout
                    using (PlotSettings acPlSet = new PlotSettings(acLayout.ModelType))
                    {
                        acPlSet.CopyFrom(acLayout);

                        // Update the PlotSettings object
                        PlotSettingsValidator acPlSetVdr = PlotSettingsValidator.Current;

                        // Let's first select the device
                        StringCollection devicelist = acPlSetVdr.GetPlotDeviceList();
                        StringCollection medialist = acPlSetVdr.GetCanonicalMediaNameList(acPlSet);
                        StringCollection pstyles = acPlSetVdr.GetPlotStyleSheetList();

                        // Set the plot type
                        acPlSetVdr.SetPlotWindowArea(acPlSet, window);
                        acPlSetVdr.SetPlotType(acPlSet, Autodesk.AutoCAD.DatabaseServices.PlotType.Window);

                        // Set the plot scale
                        acPlSetVdr.SetUseStandardScale(acPlSet, true);
                        acPlSetVdr.SetStdScaleType(acPlSet, StdScaleType.ScaleToFit);
                        // Center the plot
                        acPlSetVdr.SetPlotCentered(acPlSet, true);

                        // Set the plot device to use
                        acPlSetVdr.SetPlotConfigurationName(acPlSet, devname, medname);
                        acPlSetVdr.SetCurrentStyleSheet(acPlSet, stylename);

                        // Set the plot info as an override since it will
                        // not be saved back to the layout
                        acPlInfo.OverrideSettings = acPlSet;

                        // Validate the plot info
                        using (PlotInfoValidator acPlInfoVdr = new PlotInfoValidator())
                        {
                            acPlInfoVdr.MediaMatchingPolicy = MatchingPolicy.MatchEnabled;
                            acPlInfoVdr.Validate(acPlInfo);

                            // Check to see if a plot is already in progress
                            if (PlotFactory.ProcessPlotState == ProcessPlotState.NotPlotting)
                            {
                                using (PlotEngine acPlEng = PlotFactory.CreatePublishEngine())
                                {
                                    // Track the plot progress with a Progress dialog
                                    using (PlotProgressDialog acPlProgDlg = new PlotProgressDialog(false, 1, true))
                                    {
                                        using (acPlProgDlg)
                                        {
                                            // Define the status messages to display 
                                            // when plotting starts
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.DialogTitle, "Plot Progress");
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.CancelJobButtonMessage, "Cancel Job");
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.CancelSheetButtonMessage, "Cancel Sheet");
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.SheetSetProgressCaption, "Sheet Set Progress");
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.SheetProgressCaption, "Sheet Progress");

                                            // Set the plot progress range
                                            acPlProgDlg.LowerPlotProgressRange = 0;
                                            acPlProgDlg.UpperPlotProgressRange = 100;
                                            acPlProgDlg.PlotProgressPos = 0;

                                            // Display the Progress dialog
                                            acPlProgDlg.OnBeginPlot();
                                            acPlProgDlg.IsVisible = true;

                                            // Start to plot the layout
                                            acPlEng.BeginPlot(acPlProgDlg, null);

                                            // Define the plot output
                                            acPlEng.BeginDocument(acPlInfo, acDoc.Name, null, 1, true, filename);

                                            // Display information about the current plot
                                            //acPlProgDlg.set_PlotMsgString(PlotMessageIndex.Status, "Plotting: " + acDoc.Name + " - " + acLayout.LayoutName);
                                            acPlProgDlg.set_PlotMsgString(PlotMessageIndex.Status, "Plotting: " + filename);

                                            // Set the sheet progress range
                                            acPlProgDlg.OnBeginSheet();
                                            acPlProgDlg.LowerSheetProgressRange = 0;
                                            acPlProgDlg.UpperSheetProgressRange = 100;
                                            acPlProgDlg.SheetProgressPos = 0;

                                            // Plot the first sheet/layout
                                            using (PlotPageInfo acPlPageInfo = new PlotPageInfo())
                                            {
                                                acPlEng.BeginPage(acPlPageInfo, acPlInfo, true, null);
                                            }

                                            acPlEng.BeginGenerateGraphics(null);
                                            acPlEng.EndGenerateGraphics(null);

                                            // Finish plotting the sheet/layout
                                            acPlEng.EndPage(null);
                                            acPlProgDlg.SheetProgressPos = 100;
                                            acPlProgDlg.OnEndSheet();

                                            // Finish plotting the document
                                            acPlEng.EndDocument(null);

                                            // Finish the plot
                                            acPlProgDlg.PlotProgressPos = 100;
                                            acPlProgDlg.OnEndPlot();
                                            acPlEng.EndPlot(null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void PlotPDF(string filename, string devname, string medname, string stylename, Extents2d window, bool onizleme)
        {
            AcadApplication app = (AcadApplication)Autodesk.AutoCAD.ApplicationServices.Application.AcadApplication;
//            AcadLayouts LayoutsCOLL = app.ActiveDocument.Layouts;
            //AcadLayout cadLayout;
            //string strModel;

            //foreach (AcadLayout cadlayout in LayoutsCOLL)
            //{
            app.ActiveDocument.SetVariable("backgroundplot", 0);
            //if (app.ActiveDocument.GetVariable("tilemode") == 1)
            //{
            //    app.ActiveDocument.SetVariable("tilemode", 0);
            //}

            app.ActiveDocument.ModelSpace.Layout.RefreshPlotDeviceInfo();
            app.ActiveDocument.Regen(AcRegenType.acAllViewports);
            app.ActiveDocument.ActiveLayout.ConfigName = devname; // "DWG To PDF.pc3";
            app.ActiveDocument.ActiveLayout.PlotWithPlotStyles = true;
            app.ActiveDocument.ActiveLayout.ScaleLineweights = false;
            app.ActiveDocument.ActiveLayout.StyleSheet = stylename; // "FINAL-BK.ctb";
            app.ActiveDocument.ActiveLayout.CanonicalMediaName = medname; //  "ISO_A0_(841.00_x_1189.00_MM)";
            app.ActiveDocument.ActiveLayout.PaperUnits = AcPlotPaperUnits.acMillimeters;
            //app.ActiveDocument.ActiveLayout.PlotRotation = AcPlotRotation.ac90degrees;
            app.ActiveDocument.ActiveLayout.StandardScale = AcPlotScale.acScaleToFit;
            //app.ActiveDocument.ActiveLayout.SetWindowToPlot(window.MinPoint, window.MaxPoint);
            app.ActiveDocument.ActiveLayout.SetWindowToPlot(
            //LowerLeft: new Point3d(window.MinPoint.X, window.MinPoint.Y, 0),
            LowerLeft:  new double[] { window.MinPoint.X, window.MinPoint.Y },
            //UpperRight: new Point3d(window.MaxPoint.X, window.MaxPoint.Y, 0));
            UpperRight: new double[] { window.MaxPoint.X, window.MaxPoint.Y });

            app.ActiveDocument.ActiveLayout.PlotType = AcPlotType.acWindow;
            app.ActiveDocument.ActiveLayout.CenterPlot = true;
            if (onizleme)
            {
                app.ActiveDocument.Plot.DisplayPlotPreview(AcPreviewMode.acFullPreview);
            }


            //app.ActiveDocument.Application.ZoomExtents();
            //app.ActiveDocument.Plot.PlotToDevice();

            //app.ActiveDocument.Plot.PlotToFile(filename, app.ActiveDocument.PlotConfigurations);

            //app.ActiveDocument.Plot.PlotToFile(filename);
            PlotPDF_(filename, devname, medname, stylename, window, "Pdf");
            //} // foreach
        }
        public void PlotPDF_(string filename, string devname, string medname, string stylename, Extents2d window, string plotto)
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var first  = System.String.Format("{0},{1}", window.MinPoint.X, window.MinPoint.Y); ;
            var second = System.String.Format("{0},{1}", window.MaxPoint.X, window.MaxPoint.Y); ;

            Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("FILEDIA", 0);
            ed.Command("-export",
                plotto,
                "Window",
                first,
                second,
                "No", // Detailed plot configuration? [Yes/No] <No>:
                //medname,
                //"Millimeters",
                //"Landscape",
                //"Fit",
                //"Yes", // Plot with plot styles?
                //stylename, // Enter plot style table name or [?] (enter . for none) <acad.ctb>:
                //"Yes", // Plot with lineweights?[Yes / No] < Yes >:
                filename);
            Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("FILEDIA", 1);
        }
    }
}
