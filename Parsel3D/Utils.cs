﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.BoundaryRepresentation;
using cadPolyline = Autodesk.AutoCAD.DatabaseServices.Polyline;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Autodesk.AutoCAD.GraphicsInterface;
//using Microsoft.WindowsAPICodePack.Dialogs;

namespace Parsel3D
{
    public class Utils
    {
        //public enum SolidOperation { SolidUnion, SolidSubtract };
        public enum SolidTuru { KatSolid, BagimsizSolid };
        public class SolidFaceMaterialInfo
        {
            public int faceNumber = 0;
            public Point3dCollection points;
            public string materialName;
            public TransformationParameters materialMappingParameters; // texture'ın o yüzeydeki MaterialMap parametreleri
            public Matrix3d transformationMatrix;
        }


        public class TransformationParameters
        {
            public TransformationParameters() {}
            public TransformationParameters(double rot, double uscale, double vscale, double uoffset, double voffset, double uskew, double vskew, int autoTransformMethodOfDiffuseMapMapper)
            {
                Rotation = rot;
                UScale = uscale;
                VScale = vscale;
                UOffset = uoffset;
                VOffset = voffset;
                USkew = uskew;
                VSkew = vskew;
                AutoTransformMethodOfDiffuseMapMapper = autoTransformMethodOfDiffuseMapMapper;
            }
            public double Rotation = 0.0, UScale = 1.0, VScale = 1.0, UOffset = 0.0, VOffset = 0.0, USkew = 0.0, VSkew = 0.0;
            public int AutoTransformMethodOfDiffuseMapMapper = 1; /* None */
        }

        public const string LAYERADI_BAGIMSIZBOLUM = "Parsel3d_BagimsizBölüm";
        public const string LAYERADI_KAT = "Parsel3d_Kat";
        public const string LAYERADI_BINA = "Parsel3d_Bina";
        public const string LAYERADI_PARSEL = "Parsel3d_Parsel";
        public const string LAYERADI_KATPLANI = "Parsel3d_KatPlani";
        public const string POLYGON_GEOMETRY_TEXT = "POLYGON";

        public enum BarkodTuru { BagimsizBolumBarkodu, BinaBarkodu, KatBarkodu, ParselBarkodu };
        public static TransformationParameters ReadFromTransformationMatrix_(Matrix3d m3d)
        {
            // __                ___ 
            // |  a    c    0   e  | // aşağıdakinden farklı
            // |                   |
            // |  b    d    0   f  |
            // |                   |
            // |  0    0    1   0  |
            // |                   |
            // |  0    0    0   1  |
            // ___               ___

            double a = m3d[0, 0], b = m3d[1, 0];
            double c = m3d[0, 1], d = m3d[1, 1];
            double e = m3d[0, 3], f = m3d[1, 3];

            double delta = a * d - b * c;

            TransformationParameters result = new TransformationParameters();

            result.UOffset = e;
            result.VOffset = f;

            // Apply the QR-like decomposition.
            if (a != 0 || b != 0)
            {
                double r = Math.Sqrt(a * a + b * b);
                result.Rotation = b > 0 ? Math.Acos(a / r) : -Math.Acos(a / r);
                result.UScale = r;
                result.VScale = delta / r;
                result.USkew = Math.Atan((a * c + b * d) / (r * r));
                result.VSkew = 0.0;
            }
            else if (c != 0 || d != 0)
            {
                var s = Math.Sqrt(c * c + d * d);
                result.Rotation = Math.PI / 2 - (d > 0 ? Math.Acos(-c / s) : -Math.Acos(c / s));
                result.UScale = delta / s;
                result.VScale = s;
                result.USkew = 0.0;
                result.VSkew = Math.Atan((a * c + b * d) / (s * s));
            }
            else
            {
                // a = b = c = d = 0
            }
            result.Rotation *= 180.0 / Math.PI; // radyandan dereceye çeviriliyor
            return result;
        }
        public static TransformationParameters ReadFromTransformationMatrix(Matrix3d m3d) // çalışan
        {
            // XY düzleminde (Z ekseni etrafında) döndürme matrisi:
            // __                 ___        __                                        ___ 
            // |  a    b    0   tx  |        |  Sx*cos(rot)   -Sx*sin(rot)    0      tx  |
            // |                    |        |                                           |
            // |  c    d    0   ty  |        |  Sy*sin(rot)    Sy*cos(rot)    0      ty  |
            // |                    |   =>   |                                           |
            // |  0    0    1   tz  |        |  0               0             1      tz  |
            // |                    |        |                                           |
            // |  0    0    0   1   |        |  0               0             0      1   |
            // ___                ___        ___                                       ___

            double a = m3d[0, 0], b = m3d[0, 1], c = m3d[1, 0], d = m3d[1, 1];
            double degree = 180.0 / Math.PI;
            //double radian = Math.PI / 180.0;

            // matristen dönüşüm parametrelerinin eldesi..
            //double uscale  = Math.Sign(a) * Math.Sqrt(a * a + b * b);
            //double vscale  = Math.Sign(d) * Math.Sqrt(c * c + d * d);
            double uscale = Math.Sqrt(a * a + b * b);
            double vscale = Math.Sqrt(c * c + d * d);

            double rad = Math.Acos(a / uscale);
            double deg = rad * degree;

            //double rot = Math.Atan2(Math.Abs(b), Math.Abs(a));
            //double rot = Math.Atan2(b, a);
            double rot = Math.Atan2(c, d);
            double rotationInDegree = rot * degree;


            //if (c > 0 && d > 0) // açı 1. bölgede 
            //    rotationInDegree = rotationInDegree;
            if (c > 0 && d < 0) // açı 2. bölgede 
                rotationInDegree = 180.0 - rotationInDegree;
            else if (c < 0 && d < 0) // açı 3. bölgede 
                rotationInDegree = 180.0 + rotationInDegree;
            else if (c < 0 && d > 0) // açı 4. bölgede 
                rotationInDegree = 360.0 - rotationInDegree;

            if (rotationInDegree > 360)
                rotationInDegree -= 360;

            if (uscale < 0.0001)
                uscale = 1.0;
            if (vscale < 0.0001)
                vscale = 1.0;

            //double uoffset = m3d[0, 3] / (Math.Sign(a) * uscale);
            //double voffset = m3d[1, 3] / (Math.Sign(d) * vscale);

            double uoffset = m3d[0, 3];// / uscale;
            double voffset = m3d[1, 3];// / vscale;

            //return new TransformationParameters(rotationInDegree, 
            //    uscale < 0.000001 ? 1.0 : 1.0 / uscale, 
            //    vscale < 0.000001 ? 1.0 : 1.0 / vscale,
            return new TransformationParameters(rotationInDegree, 1/uscale, 1/vscale, uoffset, voffset, 0, 0, 1);
        }

        public static TransformationParameters ReadFromTransformationMatrix___(Matrix3d m3d) // decompose çalışmıyor..
        {
            System.Numerics.Matrix4x4 mat4x4 = new System.Numerics.Matrix4x4(
                (float)m3d[0, 0], (float)m3d[0, 1], (float)m3d[0, 2], (float)m3d[0, 3],
                (float)m3d[1, 0], (float)m3d[1, 1], (float)m3d[1, 2], (float)m3d[1, 3],
                (float)m3d[2, 0], (float)m3d[2, 1], (float)m3d[2, 2], (float)m3d[2, 3],
                (float)m3d[3, 0], (float)m3d[3, 1], (float)m3d[3, 2], (float)m3d[3, 3]);
            System.Numerics.Vector3 vecScale = new System.Numerics.Vector3();
            System.Numerics.Quaternion quaRotation = new System.Numerics.Quaternion();
            System.Numerics.Vector3 vecTranslation = new System.Numerics.Vector3();
            if (!System.Numerics.Matrix4x4.Decompose(mat4x4, out vecScale, out quaRotation, out vecTranslation))
                System.Windows.MessageBox.Show("Matrix decompose cort");

            return new TransformationParameters(quaRotation.Z * 180f / Math.PI, vecScale.X, vecScale.Y, vecTranslation.X, vecTranslation.Y, 0, 0, 1);
        }

        public static TransformationParameters ReadFromTransformationMatrix__(Matrix3d m3d) // 
        //************* HATALI ÇALIŞIYOR ****************
        {
            // __                 ___ 
            // |  a    b    0   tx  |
            // |                    |
            // |  c    d    0   ty  |
            // |                    |
            // |  0    0    1   tz  |
            // |                    |
            // |  0    0    0   1   |
            // ___                ___

            double a = m3d[0, 0], b = m3d[0, 1], c = m3d[1, 0], d = m3d[1, 1];
            double degree = 180.0 / Math.PI;
            double radian = Math.PI / 180.0;

            // matristen dönüşüm parametrelerinin eldesi..
            double rot = Math.Atan2(-b, a);
            //double uscale  = Math.Sign(a) * Math.Sqrt(a * a + b * b);
            //double vscale  = Math.Sign(d) * Math.Sqrt(c * c + d * d);
            double uscale = Math.Sqrt(a * a + b * b);
            double vscale = Math.Sqrt(c * c + d * d);

            double sign = Math.Atan(-b / a);
            double rad = Math.Acos(a / uscale);
            double deg = rad * degree;

            if (deg > 90 && sign > 0)
            {
                rot = (360 - deg) * radian;
            }
            else if (deg < 90 && sign < 0)
            {
                rot = (360 - deg) * radian;
            }
            else
            {
                rot = rad;
            }
            double rotationInDegree = rot * degree;

            if (uscale < 0.0001)
                uscale = 1.0;
            if (vscale < 0.0001)
                vscale = 1.0;

            //double uoffset = m3d[0, 3] / (Math.Sign(a) * uscale);
            //double voffset = m3d[1, 3] / (Math.Sign(d) * vscale);
            double uoffset = m3d[0, 3] / uscale;
            double voffset = m3d[1, 3] / vscale;

            //return new TransformationParameters(rotationInDegree, 
            //    uscale < 0.000001 ? 1.0 : 1.0 / uscale, 
            //    vscale < 0.000001 ? 1.0 : 1.0 / vscale,
            return new TransformationParameters(rotationInDegree, uscale,  vscale, uoffset, voffset, 0 , 0, 1);
        }


        /// <summary>
        /// https://stackoverflow.com/questions/275689/how-to-get-relative-path-from-absolute-path
        /// Creates a relative path from one file or folder to another.
        /// </summary>
        /// <param name="fromPath">Contains the directory that defines the start of the relative path.</param>
        /// <param name="toPath">Contains the path that defines the endpoint of the relative path.</param>
        /// <returns>The relative path from the start directory to the end path or <c>toPath</c> if the paths are not related.</returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="UriFormatException"></exception>
        /// <exception cref="InvalidOperationException"></exception>
        public static String MakeRelativePath(String fromPath, String toPath)
        {
            //if (String.IsNullOrEmpty(fromPath)) throw new ArgumentNullException("fromPath");
            //if (String.IsNullOrEmpty(toPath)) throw new ArgumentNullException("toPath");
            if (String.IsNullOrEmpty(fromPath))
                return "";
            if (String.IsNullOrEmpty(toPath))
                return "";

            Uri fromUri, toUri;
            try
            {
                fromUri = new Uri(fromPath);
            }
            catch { return ""; }
            try
            {
                toUri = new Uri(toPath);
            }
            catch { return ""; }

            if (fromUri.Scheme != toUri.Scheme) { return toPath; } // path can't be made relative.

            Uri relativeUri = fromUri.MakeRelativeUri(toUri);
            String relativePath = Uri.UnescapeDataString(relativeUri.ToString());

            if (toUri.Scheme.Equals("file", StringComparison.InvariantCultureIgnoreCase))
            {
                relativePath = relativePath.Replace(System.IO.Path.AltDirectorySeparatorChar, System.IO.Path.DirectorySeparatorChar);
            }

            return relativePath;
        }

        public static string RemoveNonNumberDigitsAndCharacters(string text)
        {
            var numericChars = "0123456789,+-".ToCharArray();
            return new String(text.Where(c => numericChars.Any(n => n == c)).ToArray());
        }

        public static string getStringFromDBTextOrMText(Transaction tr, ObjectId oid)
        {
            var entity = (Entity)tr.GetObject(oid, OpenMode.ForRead);
            if (entity is DBText)
            {
                var dbtext = entity as DBText;
                return dbtext.TextString;
            }
            else if (entity is MText)
            {
                var mtext = entity as MText;
                //return mtext.Contents;
                return mtext.Text;
            }
            return null;
        }

        public static ObjectIdCollection SelectFloorPlanObjects()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;

            PromptSelectionOptions pso = new PromptSelectionOptions();
            pso.AllowDuplicates = false;
            pso.MessageForAdding = "\nParsel yapılacak 'Polyline'ları ve 'Text'leri seçiniz: ";

            TypedValue[] tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Operator,"<or"),
                        new TypedValue((int)DxfCode.Operator,"<and"),
                            new TypedValue((int)DxfCode.Start,"LWPOLYLINE"),
                            new TypedValue((int)DxfCode.Operator,"<or"),
                                new TypedValue((int)DxfCode.LayerName,Utils.LAYERADI_BAGIMSIZBOLUM),
                                new TypedValue((int)DxfCode.LayerName,Utils.LAYERADI_KAT),
                            new TypedValue((int)DxfCode.Operator,"or>"),
                        new TypedValue((int)DxfCode.Operator,"and>"),
                        new TypedValue((int)DxfCode.Start,"TEXT"),
                        new TypedValue((int)DxfCode.Start,"MTEXT"),
                    new TypedValue((int)DxfCode.Operator,"or>")
                };

            ObjectIdCollection objectIdCollection = new ObjectIdCollection();

            SelectionFilter selectionfilter = new SelectionFilter(tvs);
            PromptSelectionResult promptSelectionResult = doc.Editor.GetSelection(pso, selectionfilter);
            // Seçilen polyline'ların daha önce sayısallaştırılıp sayısallaştırılmadığına bakılıyor.
            if (promptSelectionResult.Status == PromptStatus.OK)
            {
                Transaction tr = db.TransactionManager.StartTransaction();
                using (tr)
                {
                    // Loop through the selected objects
                    bool soruldu = false;
                    foreach (SelectedObject o in promptSelectionResult.Value)
                    {
                        List<string> parcelInf = new List<string>();
                        //DBObject obj = tr.GetObject(o.ObjectId, OpenMode.ForRead);
                        parcelInf = XDataHelpers.ParcelInfoForObject(tr, o.ObjectId);
                        if (parcelInf.Count > 0 && parcelInf[0] != "" && !soruldu)
                        {
                            var page = new Autodesk.Windows.TaskDialog()
                            {
                                // Şuradan apartıldı -> https://github.com/dotnet/winforms/issues/146
                                // aynı zamanda : https://github.com/kpreisser/TaskDialog
                                // TaskDialogPage çalışmadı ben düzenledim.
                                MainIcon = Autodesk.Windows.TaskDialogIcon.Warning,
                                WindowTitle = "Uyarı",
                                MainInstruction = "Seçtiğiniz katta daha önceden sayısallaştırılmış polyline(lar) var",
                                ContentText = "Ne yapmak istersiniz?\n",
                                UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                                FooterText =
                                    "Not: Bu uyarıyı almamak için 'XData Sil' komutu ile polyline'lardan sayısallaştırma bilgisini temizleyebilirsiniz. ",
                                FooterIcon = Autodesk.Windows.TaskDialogIcon.Information,
                                Buttons =
                                {
                                    new Autodesk.Windows.TaskDialogButton(1, "&Devam et\nÖnceki sayısallaştırma bilgisi silinir, yenisi yazılır."),
                                    new Autodesk.Windows.TaskDialogButton(2, "&Vazgeç"),
                                }
                            };
                            var result = page.Show(Application.MainWindow.Handle);
                            if (result != 1)
                            {
                                objectIdCollection.Clear();
                                break; // foreach
                            }
                            // Taskdialog Microsoft versiyonu (çalışma anında sıçıyor) 
                            //var page1 = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog()
                            //{
                            //    // Şuradan apartıldı -> https://github.com/dotnet/winforms/issues/146
                            //    // aynı zamanda : https://github.com/kpreisser/TaskDialog
                            //    // TaskDialogPage çalışmadı ben düzenledim.
                            //    Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Warning,
                            //    Caption = "Uyarı",
                            //    InstructionText = "Seçtiğiniz katta daha önceden sayısallaştırılmış polyline(lar) var",
                            //    Text = "Ne yapmak istersiniz?\n",
                            //    //UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                            //    FooterText =
                            //        "Not: XData Sil komutu ile polyline'lardan sayısallaştırma bilgisini temizleyebilirsiniz. ",
                            //    FooterIcon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Information,
                            //    Controls =
                            //    {
                            //        new Microsoft.WindowsAPICodePack.Dialogs.TaskDialogButton("&Devam", "Bekleme yapma"),
                            //        new Microsoft.WindowsAPICodePack.Dialogs.TaskDialogButton("&Vazgeç", "Neyse.."),
                            //    }
                            //};
                            //var result1 = page1.Show();
                            //if (result1 == Microsoft.WindowsAPICodePack.Dialogs.TaskDialogResult.Cancel)
                            //{
                            //    objectIdCollection.Clear();
                            //    break;
                            //}
                            soruldu = true;
                        } // if parcelInf
                        objectIdCollection.Add(o.ObjectId);
                    } // foreach
                }
            }
            return objectIdCollection;
        }
        public static void ShowMessage(string title, string msg, string expandedText)
        {
            Autodesk.Windows.TaskDialog td = new Autodesk.Windows.TaskDialog();
            td.WindowTitle = title;
            td.ContentText = msg;
            td.EnableHyperlinks = true;
            if (expandedText != "")
            {
                td.ExpandedText = expandedText;
                td.ExpandFooterArea = true;
            }
//            td.UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
//            td.ShowProgressBar = true;
            td.AllowDialogCancellation = true;
            td.CommonButtons = Autodesk.Windows.TaskDialogCommonButtons.Ok;
            td.Show();
        }
        public static void ShowMessage(string msg)
        {
            ShowMessage("Nova", msg, "");
        }
        public static void ShowMessageExample(string msg)
        {
            var taskDialog = new Microsoft.WindowsAPICodePack.Dialogs.TaskDialog();
            taskDialog.Caption = "McMD";
            taskDialog.InstructionText = "McMDK";
            taskDialog.Text = "havalar nasıl" + Environment.NewLine +
                              "yağışlı mı？";
            taskDialog.DetailsExpandedText = String.Format(
                "gereksiz　　：{0}" + Environment.NewLine +
                "laf kalabalığı ：{1}",
                1,
                "version");
            taskDialog.ExpansionMode = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogExpandedDetailsLocation.ExpandContent;
            taskDialog.DetailsExpanded = false;
            taskDialog.Icon = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardIcon.Information;
            taskDialog.StandardButtons = Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.Yes | Microsoft.WindowsAPICodePack.Dialogs.TaskDialogStandardButtons.No;
            taskDialog.Opened += (_sender, _e) =>
            {
                ((Microsoft.WindowsAPICodePack.Dialogs.TaskDialog)_sender).Icon = ((Microsoft.WindowsAPICodePack.Dialogs.TaskDialog)_sender).Icon;
            };
            if (taskDialog.Show() == Microsoft.WindowsAPICodePack.Dialogs.TaskDialogResult.Yes)
            {
                //duruma göre...
            }
        }
        public static bool BinaBilgileriFormuGoster()
        {
            BinaBilgiForm ef = new BinaBilgiForm();
            if (ef.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //ef.Update();
                Application.DocumentManager.MdiActiveDocument.SetLispSymbol("BINABILGILERIKONTROLEDILDI", "1");
                Utils.WriteToDictionary(ef.tb);
                return true;
            }
            else
                return false;
        }
        public static string BarkodOlustur(ref ParselBilgileri parselbilgileri, BarkodTuru barkodturu)
        {
            string bk;

            try
            {
                bk = String.Format("{0,4:0000}{1,5:00000}{2,5:00000}",
                    Convert.ToInt32(IlceKoduAyikla(parselbilgileri.TapuBilgileri.ilceKodu)),
                    Convert.ToInt32(parselbilgileri.TapuBilgileri.AdaNo),
                    Convert.ToInt32(parselbilgileri.TapuBilgileri.ParselNo));
            }
            catch
            {
                return "HATALIBARKOD";
            }
            if (barkodturu == BarkodTuru.ParselBarkodu)
            { return bk; }
            else if (barkodturu == BarkodTuru.BinaBarkodu)
            {
                bk += String.Format("{0,3:000}", Convert.ToInt32(parselbilgileri.TapuBilgileri.BinaNo));
                return bk;
            }
            else if (barkodturu == BarkodTuru.KatBarkodu)
            {
                bk += String.Format("{0,3:000}K{1,3:000}",
                    Convert.ToInt32(parselbilgileri.TapuBilgileri.BinaNo),
                    parselbilgileri.BagimsizBolumBulunduguKat);
                //                System.Windows.MessageBox.Show("Kat barkodu:" + bk);
                return bk;
            }
            else // bağımsız bölüm barkodu
            {
                bk += String.Format("{0,3:000}", Convert.ToInt32(parselbilgileri.TapuBilgileri.BinaNo));

                if (parselbilgileri.BagimsizBolumNo.Contains("+") ||  // + veya - varsa ortak alandır.
                    parselbilgileri.BagimsizBolumNo.Contains("-"))
                {

                    bool giris = false;
                    var bbno = parselbilgileri.BagimsizBolumNo;
                    if (bbno.ToUpper().Contains("G")) // G varsa giriş demektir.
                    {
                        bbno = bbno.Split('G')[1];
                        giris = true;
                    }
                    //bk += parselbilgileri.BagimsizBolumNo;
                    bbno = bbno.Replace('+', '1');
                    bbno = bbno.Replace('-', '0');
                    if (giris)
                    {
                        bk += string.Format("G{0,5:00000}", Convert.ToInt32(bbno));
                        parselbilgileri.BagimsizTuru = "Giriş";
                    }
                    else
                    {
                        bk += string.Format("{0,5:00000}", Convert.ToInt32(bbno));
                        parselbilgileri.BagimsizTuru = "Ortak Alan";
                    }
                }
                else if (parselbilgileri.BagimsizBolumNo.Contains("E")) // E varsa eklentidir. örn: 011E1 : 11 nolu b. bölümün 1 numaralı eklentisi
                {
                    string[] ayikla = parselbilgileri.BagimsizBolumNo.Split('E');
                    bk += string.Format("{0,3:000}", Convert.ToInt32(ayikla[0]));
                    //bk += "E" + ayikla[1];
                    bk += ayikla[1];
                    parselbilgileri.BagimsizTuru = "Eklenti";
                }
                //else if (parselbilgileri.BagimsizBolumNo.Contains("G")) // G varsa giriş demektir.
                //{
                //    string[] ayikla = parselbilgileri.BagimsizBolumNo.Split('G');
                //    bk += string.Format("G{0,4:+0000}", Convert.ToInt32(ayikla[0]));
                //    bk += "G" + ayikla[1];
                //    parselbilgileri.BagimsizTuru = "Giriş";
                //}
                else
                {
                    //System.Windows.MessageBox.Show(pb.BagimsizBolumNo);
                    bk += string.Format("{0,3:000}", Convert.ToInt32(parselbilgileri.BagimsizBolumNo));
                    parselbilgileri.BagimsizTuru = "Bağımsız Bölüm";
                }
            }
            return bk;
        }

        // işlenecek barkodun daha önce işleme girip girmediğinin kontrolü 
        // aynı zamanda dubleks katların tespiti.
        // aynı zamanda o katın daha önce sayısallaştırmasının yapılıp yapılmadığının kontrolü
        internal static bool SearchBarcodeDuplication(Transaction tr, ref ParselBilgileri parselBilgileri)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            ObjectId duplicatedObjectId = ObjectId.Null;
            TypedValue[] tvs =
                new TypedValue[] {
                    //new TypedValue((int)DxfCode.Start,"REGION"),
                    new TypedValue((int)DxfCode.Start,"3DSOLID"),
                    new TypedValue((int)DxfCode.ExtendedDataRegAppName,ParselBilgileri.regAppName)
                };

            SelectionFilter selectionfilter = new SelectionFilter(tvs);
            PromptSelectionResult promptSelectionResult = doc.Editor.SelectAll(selectionfilter);
            if (promptSelectionResult.Status == PromptStatus.OK)
            {
                //using (Transaction tr = db.TransactionManager.StartOpenCloseTransaction())
                //using (tr)
                //{
                foreach (SelectedObject o in promptSelectionResult.Value)
                {
                    List<string> parcelInf = new List<string>();
                    DBObject obj = tr.GetObject(o.ObjectId, OpenMode.ForRead);
                    parcelInf = XDataHelpers.ParcelInfoForObject(obj);
                    if (parcelInf.Count > 0 && parcelInf[(int)XDataTable.BARKODNO] == parselBilgileri.BarkodNo)
                    {
                        if (parcelInf[(int)XDataTable.BAGIMSIZBOLUMBULUNDUGUKAT] == parselBilgileri.BagimsizBolumBulunduguKat.ToString()) // o objenin bulunduğu kat daha önce işleme girmiş.
                        {
                            var page = new Autodesk.Windows.TaskDialog()
                            {
                                MainIcon = Autodesk.Windows.TaskDialogIcon.Error,
                                WindowTitle = "Hata",
                                MainInstruction = "Bu kat planı zaten sayısallaştırılmış görünüyor.",
                                ContentText = "Lütfen diğer katlara geçiniz\n",
                                UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                                Buttons =
                                {
                                new Autodesk.Windows.TaskDialogButton(1, "&Tamam"),
                                }
                            };
                            page.Show(Application.MainWindow.Handle);
                            return false;
                        }
                        //ShowMessage("mesaj", "parcelInf :"+parcelInf+"\nbarcodestr:"+barcodeStr, "ha babam de babam");
                        else // barkod numarası aynı fakat farklı kattaysa dubleks olup olmadığının uyarısı veriliyor
                        {
                            var page = new Autodesk.Windows.TaskDialog()
                            {
                                MainIcon = Autodesk.Windows.TaskDialogIcon.Warning,
                                WindowTitle = "Uyarı",
                                MainInstruction = string.Format("{0} ({1})\nBu barkoda sahip bir bağımsız bölüm zaten var",
                                parselBilgileri.BagimsizBolumNo, parselBilgileri.BarkodNo),
                                ContentText = "Ne yapmak istersiniz?\n",
                                UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                                FooterText = "Not: Bağımsız bölüm barkod numaraları benzersiz (unique) olmalıdır. "
                                + "\nDubleks, tripleks vb. bağımsız bölümlerde barkod numarası aynı olmak zorundadır."
                                + "\nBağımsız bölüm bilgilerinin birleştirilebilmesi için bu gereklidir.",

                                FooterIcon = Autodesk.Windows.TaskDialogIcon.Information,
                                //ExpandFooterArea = true,
                                //ExpandedText = "\nDubleks, tripleks vb. bağımsız bölümlerde barkod numarası aynı olmak zorundadır."
                                //+ "\nBağımsız bölüm geometrilerinin birleştirilebilmesi için bu gereklidir.",
                                Buttons =
                            {
                                new Autodesk.Windows.TaskDialogButton(1, "&Devam et \nDubleks, tripleks vb. durumu. Veritabanında birleştirilir."),
                                new Autodesk.Windows.TaskDialogButton(2, "&Vazgeç"),
                            }
                            };
                            var result = page.Show(Application.MainWindow.Handle);
                            if (result != 1)
                            {
                                //break; // foreach
                                return false;
                            }
                            else
                            {
                                //parselBilgileri.BagimsizBolumNotlar += "[Çok Katlı B. Bölüm]"; // Bu şekilde yapılmış olması anlamsız gelebilir ama anlamsız şeyler oluyor hayatta. Burada verilen notlar stringi diğer b. bölümlere de bulaşıyor anlamsız bir şekilde.
                                if (!parselBilgileri.BagimsizBolumNotlar.Contains("Çok Katlı")) // önceden [Çok Katlı B. Bölüm] yazılıyordu. Önceden sayısallaştırılmış ibr bağımsız bölümü işlemek için.
                                {
                                    string note;
                                    if (parselBilgileri.BagimsizBolumNotlar.Contains("Dubleks"))
                                    {
                                        note = "Tripleks";
                                    }
                                    else if (parselBilgileri.BagimsizBolumNotlar.Contains("Tripleks"))
                                    {
                                        note = "4 Katlı";
                                    }
                                    else if (parselBilgileri.BagimsizBolumNotlar.Contains("4 Katlı"))
                                    {
                                        note = "5 Katlı";
                                    }
                                    else
                                    {
                                        note = "Dubleks";
                                    }
                                    parselBilgileri.BagimsizBolumNotlar = note; // Bu şekilde yapılmış olması anlamsız gelebilir ama anlamsız şeyler oluyor hayatta. Burada verilen notlar stringi diğer b. bölümlere de bulaşıyor anlamsız bir şekilde.
                                }
                                return true;
                            }
                        } // else
                    }
                }
            }
            return true;
        } // SearchBarcodeDuplication

        // Programda kullanılacak layerlari oluşturur.
        // layer'lardan bir veya daha fazlası mevcut değilse false döndürür. 
        public static bool LayerlariOlustur()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            bool ret = true;
            // gerekli layer'lar oluşturuluyor.
            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);
                ObjectId ltId;

                if (!lt.Has(Utils.LAYERADI_BAGIMSIZBOLUM))
                {
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = Utils.LAYERADI_BAGIMSIZBOLUM;
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 4); // cyan
                    // Add the new layer to the layer table

                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                    ret = false;
                }
                if (!lt.Has(Utils.LAYERADI_KAT))
                {
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = Utils.LAYERADI_KAT;
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 3); // green
                    // Add the new layer to the layer table

                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                    // Set the layer to be current for this drawing
                    //db.Clayer = ltId;
                    ret = false;
                }
                if (!lt.Has(Utils.LAYERADI_BINA))
                {
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = Utils.LAYERADI_BINA;
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 2); // yellow
                    // Add the new layer to the layer table

                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                    // Set the layer to be current for this drawing
                    //db.Clayer = ltId;
                    ret = false;
                }
                if (!lt.Has(Utils.LAYERADI_PARSEL))
                {
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = Utils.LAYERADI_PARSEL;
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 1); // red
                    // Add the new layer to the layer table

                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                    // Set the layer to be current for this drawing
                    //db.Clayer = ltId;
                    ret = false;
                }
                if (!lt.Has(Utils.LAYERADI_KATPLANI))
                {
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = Utils.LAYERADI_KATPLANI;
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 4); // cyan
                    // Add the new layer to the layer table

                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                    // Set the layer to be current for this drawing
                    //db.Clayer = ltId;
                    ret = false;
                }
                // Commit the transaction
                tr.Commit();
            } // using tr
            return ret;
        }

        public static void KoordinatEkle(ref Point3dCollection pts, Point3d p)
        {
            foreach (Point3d po in pts)
            {
                if (po.DistanceTo(p) < 0.0005)
                    return;
            }
            pts.Add(p);
        }
        // Bağımsız bölümlerden Binayı oluştururken, Union komutu direkt çalışmadığından, sıralarsak çalışır belki diye.
        //        public static List<ObjectId> SolidleriZyeGoreSirala(List<ObjectId> solidList)
        //        {
        ////            Document doc = Application.DocumentManager.MdiActiveDocument;
        ////            Database db = doc.Database;
        //            // gerekli layer'lar oluşturuluyor.
        ////            Transaction tr = db.TransactionManager.StartTransaction();
        //            var sortedList = from Solid3d solid in solidList
        //                             orderby solid.GeometricExtents ascending
        //                             select solid;

        //            //           using (tr)
        //            //           {

        //            //           }
        //            return sortedList;
        //        }

        // Bağımsız bölümler ayrık olamayacağından, yalnızca delikli vs olabildiğinden subtract yapılıyor
        public static Solid3d BagimsizSolidleriniBirlestir(List<ObjectId> objectIdArray, string layerName)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            Transaction tr = db.TransactionManager.StartTransaction();

            using (tr)
            {
                ObjectId[] id0 = new ObjectId[] { objectIdArray[0] };
                SelectionSet mainSolid = SelectionSet.FromObjectIds(id0);
                objectIdArray.RemoveAt(0);
                SelectionSet sele = SelectionSet.FromObjectIds(objectIdArray.ToArray());

                //ed.Command("subtract", mainSolid, "", sele, "");
                //BooleanSolids.Commands.UniteSolidsWithCopy(selRes);
                BooleanSolids.Commands.BooleanSolids(
                    tr, objectIdArray[0], objectIdArray.ToArray(),
                    BooleanOperationType.BoolSubtract);

                Solid3d katSolid = (Solid3d)
                    tr.GetObject(ed.SelectLast().Value[0].ObjectId,
                        OpenMode.ForWrite);
                LayerTable lt = (LayerTable)tr.GetObject(
                    db.LayerTableId,
                    OpenMode.ForRead);
                katSolid.SetLayerId(lt[layerName], false);
                katSolid.DowngradeOpen();
                tr.Commit();
                return katSolid;
            }
        }
        // Kat Solidi'i ayrık olabildiğinden oluşturmak için union kullanılıyor
        public static Solid3d KatSolidleriniBirlestir(List<ObjectId> objectIdArray, string layerAdi)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            Transaction tr = db.TransactionManager.StartTransaction();

            var newObjectIdArray = new List<ObjectId>(); // kat solidini oluşturacak solidlerin kopyası için.
            using (tr)
            {
                foreach (ObjectId oid in objectIdArray)
                {
                    var entity = (Entity)tr.GetObject(oid, OpenMode.ForRead);
                    // kat solidini oluşturacak solidlerin kopyası alınıyor.
                    // copy and transform the clone
                    var clone = entity.GetTransformedCopy(Matrix3d.Displacement(new Vector3d(0, 0, 0)));
                    // add the clone to the current space
                    var space = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                    space.AppendEntity(clone);
                    tr.AddNewlyCreatedDBObject(clone, true);
                    newObjectIdArray.Add(clone.Id);
                }
                var sortedList = from oid in newObjectIdArray
                                 orderby ((Solid3d)oid.GetObject(OpenMode.ForRead)).GeometricExtents.MinPoint.Z ascending
                                 select oid;

                var pb = new ParselBilgileri();
                pb.ReadFromAutoCadObject(tr.GetObject(sortedList.First(), OpenMode.ForRead)); // gerçek taban kotunu merak ediyoruz
                //var sele = SelectionSet.FromObjectIds(newObjectIdArray.ToArray());
                var sele = SelectionSet.FromObjectIds(sortedList.ToArray());

                ed.Command("union", sele, "");
                //BooleanSolids.Commands.UniteSolidsWithCopy(selRes);
                Solid3d binaSolid = (Solid3d)
                    tr.GetObject(ed.SelectLast().Value[0].ObjectId,
                        OpenMode.ForWrite);
                LayerTable lt = (LayerTable)tr.GetObject(
                    db.LayerTableId,
                    OpenMode.ForRead);
                binaSolid.SetLayerId(lt[layerAdi], false);
                pb.WriteToAutoCadObject(tr, binaSolid);
                binaSolid.DowngradeOpen();
                tr.Commit();
                return binaSolid;
            }
        }

        public static bool isEqual(double a, double b, double tolerance = 0.0001)
        {
            return (Math.Abs(a - b) < tolerance);
        }
        public static bool isEqual(Point3d a, Point3d b, double tolerance = 0.0001)
        {
            return ((Math.Abs(a.X - b.X) < tolerance)
                 && (Math.Abs(a.Y - b.Y) < tolerance)
                 && (Math.Abs(a.Z - b.Z) < tolerance));
        }
        public static Point3dCollection GetFloorCoordinatesFromSolid3d_(Solid3d solid)
        {
            using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep = new Autodesk.AutoCAD.BoundaryRepresentation.Brep(solid))
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                Database db = doc.Database;
                Editor ed = doc.Editor;


                int i = 0;
                double floorZ = solid.GeometricExtents.MinPoint.Z; //brep.Edges.ElementAt(0).Vertex1.Point.Z;
                double ceilingZ = solid.GeometricExtents.MaxPoint.Z;
                Autodesk.AutoCAD.Geometry.Point3dCollection floorPts = new Autodesk.AutoCAD.Geometry.Point3dCollection();
                //foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge edge in brep.Edges)
                List<Edge> belist = brep.Edges.ToList();

                for (i = 0; i < belist.Count && belist.Count > 0; i++)
                {
                    Autodesk.AutoCAD.BoundaryRepresentation.Edge edge = belist[i];
                    if (edge.Vertex1.Point.Z != edge.Vertex2.Point.Z)
                    {
                        i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                        belist.Remove(edge);
                    }
                    else if (edge.Vertex1.Point.Z == floorZ)
                    {
                        if (floorPts.Count == 0) // ilk köşe daima ekleniyor.
                        {
                            floorPts.Add(edge.Vertex1.Point);
                            //floorPts.Add(edge.Vertex2.Point); sadee 1 noktasını ekleyeceğiz yoksa başı sonu sıkıntı yaratıyor.
                            //  ed.WriteMessage("\nilk nokta => i = {0} floor[0]= \t xy={1}", i, floorPts[0]);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                                   //                    belist.Remove(edge);
                        }
                        else if (edge.Vertex1.Point == floorPts[0] || // kenarın baştaki veya sondaki noktası daha önce eklenen noktalarla tutuyorsa diğer noktası sepete ekleniyor.
                            edge.Vertex1.Point == floorPts[floorPts.Count - 1])
                        {
                            if (edge.Vertex1.Point == floorPts[0])
                                floorPts.Insert(0, edge.Vertex2.Point); // başa ekleniyor
                            else
                                floorPts.Add(edge.Vertex2.Point); // sona ekleniyor.
                                                                  //  ed.WriteMessage("\nfloor[i = {0}]= \t xy={1}", i, edge.Vertex2.Point);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                            belist.Remove(edge);
                        }
                        else if (edge.Vertex2.Point == floorPts[0] || // kenarın baştaki veya sondaki noktası daha önce eklenen noktalarla tutuyorsa diğer noktası sepete ekleniyor.
                            edge.Vertex2.Point == floorPts[floorPts.Count - 1])
                        {
                            if (edge.Vertex2.Point == floorPts[0])
                                floorPts.Insert(0, edge.Vertex1.Point);// başa ekleniyor.
                            else
                                floorPts.Add(edge.Vertex1.Point); // sona ekleniyor.
                                                                  // ed.WriteMessage("\nfloor[i = {0}]= \t xy={1}", i, edge.Vertex1.Point);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                            belist.Remove(edge);
                        }
                    }
                    else // kot tabana ait değil.
                    {
                        //i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                        ed.WriteMessage("\nedge({0})\tvertex1: {1},{2},{3} \t\t vertex2: {4},{5},{6}", i, edge.Vertex1.Point.X, edge.Vertex1.Point.Y, edge.Vertex1.Point.Z,
                            edge.Vertex2.Point.X, edge.Vertex2.Point.Y, edge.Vertex2.Point.Z);
                        belist.Remove(edge);
                    }
                } // for i = 0 ..
#if DEBUG
                i = 0;
                //                System.Windows.MessageBox.Show("floorpts : " + floorPts.Count.ToString() + " adet nokta");
                foreach (Point3d p in floorPts)
                {
                    ed.WriteMessage("\nfloor({0})\t{1}\t{2}\t{3}", i++, p.X, p.Y, p.Z);
                }
                if (floorPts.Count < 1)
                    System.Windows.MessageBox.Show("hatalarrr.." + floorPts.Count.ToString() + " adet nokta");
#endif
                return floorPts;
            } // using
        } // GetFloorCoordinatesFromSolid3d
        public static Point3dCollection GetFloorCoordinatesFromSolid3d__(Solid3d solid)
        {
            using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep = new Autodesk.AutoCAD.BoundaryRepresentation.Brep(solid))
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                Database db = doc.Database;
                Editor ed = doc.Editor;


                int i = 0;
                double floorZ = solid.GeometricExtents.MinPoint.Z; //brep.Edges.ElementAt(0).Vertex1.Point.Z;
                double ceilingZ = solid.GeometricExtents.MaxPoint.Z;
                Autodesk.AutoCAD.Geometry.Point3dCollection floorPts = new Autodesk.AutoCAD.Geometry.Point3dCollection();
                //foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge edge in brep.Edges)
                List<Edge> belist = brep.Edges.ToList();
                //// ilk önce tabana ait noktalar ayıklanıyor.
                //var floorlist = new List<Point3d>();
                //for (i = 0; i < belist.Count && belist.Count > 0; i++)
                //{
                //    Edge edge = belist[i];
                //    if (Math.Abs(edge.Vertex1.Point.Z - floorZ) < 0.0001)
                //    {
                //        floorlist.Add(edge.Vertex1.Point);
                //    }
                //    if (Math.Abs(edge.Vertex2.Point.Z - floorZ) < 0.0001)
                //    {
                //        floorlist.Add(edge.Vertex2.Point);
                //    }
                //}
                for (i = 0; i < belist.Count && belist.Count > 0; i++)
                {
                    Autodesk.AutoCAD.BoundaryRepresentation.Edge edge = belist[i];
                    //if (edge.Vertex1.Point.Z != edge.Vertex2.Point.Z)
                    if (!isEqual(edge.Vertex1.Point.Z, edge.Vertex2.Point.Z))
                    {
                        i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                        belist.Remove(edge);
                    }
                    // else if (edge.Vertex1.Point.Z == floorZ)
                    else if (isEqual(edge.Vertex1.Point.Z, floorZ))
                    {
                        if (floorPts.Count == 0) // ilk köşe daima ekleniyor.
                        {
                            floorPts.Add(edge.Vertex1.Point);
                            //floorPts.Add(edge.Vertex2.Point); sadee 1 noktasını ekleyeceğiz yoksa başı sonu sıkıntı yaratıyor.
                            //  ed.WriteMessage("\nilk nokta => i = {0} floor[0]= \t xy={1}", i, floorPts[0]);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                                   //                    belist.Remove(edge);
                        }
                        //else if (edge.Vertex1.Point == floorPts[0] || // kenarın baştaki veya sondaki noktası daha önce eklenen noktalarla tutuyorsa diğer noktası sepete ekleniyor.
                        // edge.Vertex1.Point == floorPts[floorPts.Count - 1])
                        else if (isEqual(edge.Vertex1.Point, floorPts[0]) ||
                                 isEqual(edge.Vertex1.Point, floorPts[floorPts.Count - 1]))

                        {
                            //if (edge.Vertex1.Point == floorPts[0])
                            if (isEqual(edge.Vertex1.Point, floorPts[0]))
                                floorPts.Insert(0, edge.Vertex2.Point); // başa ekleniyor
                            else
                                floorPts.Add(edge.Vertex2.Point); // sona ekleniyor.
                                                                  //  ed.WriteMessage("\nfloor[i = {0}]= \t xy={1}", i, edge.Vertex2.Point);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                            belist.Remove(edge);
                        }
                        //                        else if (edge.Vertex2.Point == floorPts[0] || // kenarın baştaki veya sondaki noktası daha önce eklenen noktalarla tutuyorsa diğer noktası sepete ekleniyor.
                        //                            edge.Vertex2.Point == floorPts[floorPts.Count - 1])
                        else if (isEqual(edge.Vertex2.Point, floorPts[0]) || // kenarın baştaki veya sondaki noktası daha önce eklenen noktalarla tutuyorsa diğer noktası sepete ekleniyor.
                            isEqual(edge.Vertex2.Point, floorPts[floorPts.Count - 1]))
                        {
                            if (edge.Vertex2.Point == floorPts[0])
                                floorPts.Insert(0, edge.Vertex1.Point);// başa ekleniyor.
                            else
                                floorPts.Add(edge.Vertex1.Point); // sona ekleniyor.
                                                                  // ed.WriteMessage("\nfloor[i = {0}]= \t xy={1}", i, edge.Vertex1.Point);
                            i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
                            belist.Remove(edge);
                        }
                    }
                    else // kot tabana ait değil.
                    {
                        //i = 0; // sayaç sıfırlanıyor. tüm kenarlar yeniden taranacak.
#if DEBUG
                        //ed.WriteMessage("\nedge({0})\tvertex1: {1},{2},{3} \t\t vertex2: {4},{5},{6}", i, edge.Vertex1.Point.X, edge.Vertex1.Point.Y, edge.Vertex1.Point.Z,
                        //    edge.Vertex2.Point.X, edge.Vertex2.Point.Y, edge.Vertex2.Point.Z);
#endif
                        belist.Remove(edge);
                    }
                } // for i = 0 ..
#if DEBUG
                i = 0;
                //                System.Windows.MessageBox.Show("floorpts : " + floorPts.Count.ToString() + " adet nokta");
                //foreach (Point3d p in floorPts)
                //{
                //    ed.WriteMessage("\nfloor({0})\t{1}\t{2}\t{3}", i++, p.X, p.Y, p.Z);
                //}
                if (floorPts.Count < 1)
                    System.Windows.MessageBox.Show("Hata! Zemin koordinatları çıkartılamadı" + floorPts.Count.ToString() + " adet nokta");
#endif
                return floorPts;
            } // using
        } // GetFloorCoordinatesFromSolid3d
        public static List<Point3dCollection> GetFloorCoordinatesFromSolid3d___(Solid3d solid) // aynı katta ayrık yapılar olduğunda (multipolygon) oluşan sıkıntıyı giderir.
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            double floorZ = solid.GeometricExtents.MinPoint.Z; //brep.Edges.ElementAt(0).Vertex1.Point.Z;
            double ceilingZ = solid.GeometricExtents.MaxPoint.Z;
            Autodesk.AutoCAD.Geometry.Point3dCollection floorPts = new Autodesk.AutoCAD.Geometry.Point3dCollection();
            List<Point3dCollection> solidAllfaceCoordinates = Utils.GetSolidFaceCoordinates(solid.ObjectId); // burada bütün zemin, tavan ve cephe koordinatları geliyor. buradan sadece zemine ait olanları ayıklayacağız.
            List<Point3dCollection> floorFaceAllCoordinates = new List<Point3dCollection>();

            foreach (Point3dCollection facePointCollection in solidAllfaceCoordinates)
            {
                bool breyk = false;
                foreach (Point3d point in facePointCollection)
                {
                    if(!isEqual(floorZ, point.Z))
                    {
                        breyk = true;
                        break; // bir noktanın bile kotu zeminden farklıysa o yüzeyi pas geçiyoruz.
                    }
                }
                if(!breyk)
                {
                    facePointCollection.Add(new Point3d(facePointCollection[0].X, facePointCollection[0].Y, facePointCollection[0].Z)); // ilk noktayı sona da ekliyoruz.
                    floorFaceAllCoordinates.Add(facePointCollection);
                }
 //               else
 //                  break;
            }// foreach pointCollection
            // Bütün zemin koordinatları elde edildi. Şimdi delikler ayıklanıyor.
            // List içinde List içinde PointCollection
            // ilk Point3dCollection en dıştaki sınırları, diğerleri ise delikleri belirleyecek.
            //List<List<Point3dCollection>> floorFaceCoordinatesCollection = new List<List<Point3dCollection>>();
            List<Point3dCollection> floorFaceCoordinatesCollection = new List<Point3dCollection>();
            foreach (Point3dCollection pointCollection in floorFaceAllCoordinates)
            {
                Point3d onceki = pointCollection[0];
                pointCollection.RemoveAt(0); 
                foreach (Point3d point in pointCollection)
                {
                    if (!isEqual(floorZ, point.Z))
                    {
                        break; // bir noktanın bile kotu zeminden farklıysa o yüzeyi pas geçiyoruz.
                    }
                }
            }
            return floorFaceAllCoordinates;
        } // GetFloorCoordinatesFromSolid3d

        public static Point3dCollection GetAllCoordinatesFromSolid3d(Solid3d solid)
        {
            using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep = new Autodesk.AutoCAD.BoundaryRepresentation.Brep(solid))
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                Database db = doc.Database;
                Editor ed = doc.Editor;

                int i = 0;
                Autodesk.AutoCAD.Geometry.Point3dCollection pts = new Autodesk.AutoCAD.Geometry.Point3dCollection();
                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge edge in brep.Edges)
                {
                    KoordinatEkle(ref pts, edge.Vertex1.Point);
                    //ed.WriteMessage("\nVertex1: i = {0} xy={1}", i, edge.Vertex1.Point);
                    KoordinatEkle(ref pts, edge.Vertex2.Point);
                    //ed.WriteMessage("\nvertex2: i = {0} xy={1}", i, edge.Vertex2.Point);
                    i++;
                }
                return pts;
            } // using
        }
        public static string ConvertToStringEnlemBoylam2d(Point3dCollection pcoll, char ayrac)
        {
            string str = "";
            foreach (Point3d po in pcoll)
            {
                str += po.Y.ToString("0.00000000") + ayrac + po.X.ToString("0.00000000") + ayrac;
            }
            str = str.TrimEnd(ayrac);
            return "[" + str + "]";
        }
        public static Point3dCollection ReplaceZOfPoint3dCollection(Point3dCollection pcoll, double newZ)
        {
            var newPts = new Point3dCollection();
            foreach (Point3d p in pcoll)
            {
                newPts.Add(new Point3d(p.X, p.Y, newZ));
            }
            return newPts;
        }
        public static List<Point3dCollection> ReplaceZOfListPoint3dCollection(List<Point3dCollection> pList, double newZ)
        {
            var newList = new List<Point3dCollection>();
            foreach (var pcoll in pList)
            {
                newList.Add(ReplaceZOfPoint3dCollection(pcoll, newZ));
            }
            return newList;
        }        
        public static List<List<Point3dCollection>> ReplaceZOfListOfListPoint3dCollection(List<List<Point3dCollection>> pListList, double newZ)
        {
            var newListList = new List<List<Point3dCollection>>();
            foreach (var pList in pListList)
            {
                newListList.Add(ReplaceZOfListPoint3dCollection(pList, newZ));
            }
            return newListList;
        }
        public static Point3dCollection KotEkle(Point3dCollection pcoll, double kot)
        {
            var ret = new Point3dCollection();
            foreach(Point3d p in pcoll)
                ret.Add(new Point3d(p.X, p.Y, p.Z + kot));
            return ret;
        }

        public static string ConvertToStringEnlemBoylam3d(Point3dCollection pcoll, char ayrac)
        {
            string str = "";
            foreach (Point3d po in pcoll)
            {
                str += po.Y.ToString("0.000000000") + ayrac + po.X.ToString("0.000000000") + ayrac + po.Z.ToString("0.000000000") + ayrac;
            }
            str = str.TrimEnd(ayrac);
            return str;
        }
        public static string ConvertToString3d(Point3dCollection pcoll, char ayrac)
        {
            string str = "";
            foreach (Point3d po in pcoll)
            {
                str += po.X.ToString("0.000000000") + "," + po.Y.ToString("0.000000000") + "," + po.Z.ToString("0.000") + ayrac;
            }
            str = str.TrimEnd(ayrac);
            return str;
        }

        public static string ConvertPoint3dCollectionToString(Point3dCollection pcoll, char ayrac1, char ayrac2, int decimalplaces)
        {
            string str = "";
            foreach (Point3d po in pcoll)
            {
                //str += po.X.ToString("0.000000000") + ayrac1 + po.Y.ToString("0.00000000") + ayrac1 + po.Z.ToString("0.000") + ayrac2;
                str += string.Format(
                      "{0:F" + decimalplaces + "}"
                    + ayrac1
                    + "{1:F" + decimalplaces + "}"
                    + ayrac1
                    + "{2:F" + decimalplaces + "}"
                    + ayrac2,
                    po.X, po.Y, po.Z);
            }
            str = str.TrimEnd(ayrac2);
            return str;
        }

        internal static string Convert3dPointCollectionToWktPolygon(Point3dCollection point3dCollection, char v)
            => "PolygonZ((" + ConvertPoint3dCollectionToString(point3dCollection, v, ',', 3) + "))";

        internal static string Convert3dPointCollectionToWktMultiPolygon(Point3dCollection point3dCollection, char v, int ondalikbasamaksay)
            => (Utils.POLYGON_GEOMETRY_TEXT +
            (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? "(((" : "((") + // POLYGON'da ((  MULTIPOLYGON'da ((( olüyör.
            ConvertPoint3dCollectionToString(point3dCollection, v, ',', ondalikbasamaksay) +
            (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? ")))" : "))")); // POLYGON'da ))  MULTIPOLYGON'da ))) olüyör.


        // içi içe halka durumunda ve/veya ayrık polyline'ları string'e çevirir.
        internal static string ConvertListOfListOf3dPointCollectionToString(List<List<Point3dCollection>> listlistpcoll, int ondalikbasamaksay)
        {
            string str = (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? "(" : "");

            //string str = "";
            foreach (List<Point3dCollection> listpcoll in listlistpcoll)
            {
                str += "(";
                foreach (Point3dCollection pcoll in listpcoll)
                {
                    //Point3d ilk = new Point3d(pcoll[0].X, pcoll[0].Y, pcoll[0].Z); // ilk noktayı sona da ekliyoruz. yoksa FME'de kapatmıyor. Gerek yok çünkü pointlist'te o iş yapılıyor zaten
                    //pcoll.Add(ilk);
                    str += "(" + ConvertPoint3dCollectionToString(pcoll, ' ', ',', ondalikbasamaksay) + "),";
                }
                str = str.TrimEnd(',');
                str += "),";
            }
            str = str.TrimEnd(',');
            return (str +
                (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? ")" : ""));
            //return str;
        }

        // içi içe halka durumunda ve/veya ayrık polyline'ları wkt formatına çevirir.
        internal static string ConvertListOfListOf3dPointCollectionToWktMultiPolygon(List<List<Point3dCollection>> listlistpcoll, int ondalikbasamaksay)
        {
            string str = (Utils.POLYGON_GEOMETRY_TEXT
                + ConvertListOfListOf3dPointCollectionToString(listlistpcoll, ondalikbasamaksay));
            return str;
        }
        internal static string ConvertEnlemBoylam3dCollectionToWktString(Point3dCollection latlongs, char v)
        {
            // return Convert3dPointCollectionToWktPolygon(latlongs, v);
            string str = "PolygonZ((";
            foreach (Point3d po in latlongs)
            {
                str += po.Y.ToString("0.000000000") + v + po.X.ToString("0.00000000") + v + po.Z.ToString("0.000") + ',';
            }
            str = str.TrimEnd(',');
            return str + "))";
        }

        internal static string ConvertEnlemBoylam3dCollectionToWktMultiPolygon(Point3dCollection latlongs, char v)
            => (Utils.POLYGON_GEOMETRY_TEXT +
            (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? "(((" : "((") + // POLYGON'da ((  MULTIPOLYGON'da ((( olüyör.
            ConvertPoint3dCollectionToString(latlongs, v, ',', 8) +
            (Utils.POLYGON_GEOMETRY_TEXT == "MULTIPOLYGON" ? ")))" : "))")); // POLYGON'da ))  MULTIPOLYGON'da ))) olüyör.

        public static Extents3d GetExtents3dFromCoordinates(ref Point3dCollection pcoll)
        {
            if (pcoll.Count < 1)
            {
                System.Windows.MessageBox.Show("HATA! pcoll.count < 1");
            }

            var pl = new Autodesk.AutoCAD.DatabaseServices.Polyline();
            int i = 0;
            foreach (Point3d p in pcoll)
            {
                pl.AddVertexAt(i, new Point2d(p.X, p.Y), 0, 0, 0);
                i++;
            }
            Extents3d ext = pl.GeometricExtents;

            return ext;
        }
        public static int GetDOB(string ilcekodu)
        {
            var doblist = new Dictionary<string, int>() {
            { "Adana",36 },
            { "Adıyaman",39 },
            { "Afyonkarahisar",30 },
            { "Ağrı",42 },
            { "Aksaray",33 },
            { "Amasya",36 },
            { "Ankara",33 },
            { "Antalya",30 },
            { "Ardahan",42 },
            { "Artvin",42 },
            { "Aydın",27 },
            { "Balıkesir",27 },
            { "Bartın",33 },
            { "Batman",42 },
            { "Bayburt",39 },
            { "Bilecik",30 },
            { "Bingöl",39 },
            { "Bitlis",42 },
            { "Bolu",33 },
            { "Burdur",30 },
            { "Bursa",30 },
            { "Çanakkale",27 },
            { "Çankırı",33 },
            { "Çorum",36 },
            { "Denizli",30 },
            { "Diyarbakır",39 },
            { "Düzce",30 },
            { "Edirne",27 },
            { "Elazığ",39 },
            { "Erzincan",39 },
            { "Erzurum",42 },
            { "Eskişehir",30 },
            { "Gaziantep",36 },
            { "Giresun",39 },
            { "Gümüşhane",39 },
            { "Hakkari",45 },
            { "Hatay",36 },
            { "Iğdır",45 },
            { "Isparta",30 },
            { "İstanbul",30 },
            { "İzmir",27 },
            { "Kahramanmaraş",36 },
            { "Karabük",33 },
            { "Karaman",33 },
            { "Kars",42 },
            { "Kastamonu",33 },
            { "Kayseri",36 },
            { "Kırıkkale",33 },
            { "Kırklareli",27 },
            { "Kırşehir",33 },
            { "Kilis",36 },
            { "Kocaeli",30 },
            { "Konya",33 },
            { "Kütahya",30 },
            { "Malatya",39 },
            { "Manisa",27 },
            { "Mardin",42 },
            { "Mersin",36 },
            { "Muğla",27 },
            { "Muş",42 },
            { "Nevşehir",36 },
            { "Niğde",36 },
            { "Ordu",39 },
            { "Osmaniye",36 },
            { "Rize",42 },
            { "Sakarya",30 },
            { "Samsun",36 },
            { "Siirt",42 },
            { "Sinop",36 },
            { "Sivas",36 },
            { "Şanlıurfa",39 },
            { "Şırnak",42 },
            { "Tekirdağ",27 },
            { "Tokat",36 },
            { "Trabzon",39 },
            { "Tunceli",39 },
            { "Uşak",30 },
            { "Van",42 },
            { "Yalova",30 },
            { "Yozgat",36 },
            { "Zonguldak",33 }};

            string il = ilcekodu.Split(' ')[1];
            il = il.Split('-')[0];

            return doblist[il];
            //int dob;
            //doblist.TryGetValue(il, out dob);
            //return dob;
        }
        // mahalle kodu stringinden kod numarasını çeker
        // Örn: "Bostancı (148129)" => 148129
        public static string MahalleKoduAyikla(string mahallekodu)
        {
            string kod = "";
            if (mahallekodu != "")
            {
                int i = mahallekodu.Length - 1;
                while (!Char.IsNumber(mahallekodu[i]))
                    i--;
                while (Char.IsNumber(mahallekodu[i]))
                    kod += mahallekodu[i--];
            }
            else
                kod = "0";
            char[] charArray = kod.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
        // ilçe kodu stringinden kod numarasını çeker
        // Örn: "34 istanbul-Sarıyer(1604)" => 1604
        public static string IlceKoduAyikla(string ilcekodu)
        {
            //if (ilcekodu != "")
            //{
            //    string[] s = ilcekodu.Split('(');
            //    ilcekodu = (s.Length > 1) ? s[1] : s[0];
            //    ilcekodu = ilcekodu.Trim(')');
            //}
            //else
            //    ilcekodu = "0";
            //return ilcekodu;
            return MahalleKoduAyikla(ilcekodu);
        }
        //    Convert.ToInt64($"1327{Right($"0000{item.properties.AdaNo}", 4)}{Right($"00000{item.properties.ParselNo}", 5)}{Right($"000{i}", 3)}")
        public static string Right(string sValue, int iMaxLength)

        {
            //Check if the value is valid
            if (string.IsNullOrEmpty(sValue))
            {
                //Set valid empty string as string could be null
                sValue = string.Empty;
            }
            else if (sValue.Length > iMaxLength)
            {
                //Make the string no longer than the max length
                sValue = sValue.Substring(sValue.Length - iMaxLength, iMaxLength);
            }
            //Return the string
            return sValue;
        }

        public static bool PointOnPolylineEdge(double px, double py, cadPolyline polyline, float tolerans)
        {

            Point3d pointoncurve;
            Point2d p2d = new Point2d(px, py);
            pointoncurve = polyline.GetClosestPointTo(new Point3d(px, py, 0), false);
            return (p2d.GetDistanceTo(new Point2d(pointoncurve.X, pointoncurve.Y)) <= tolerans);
        }

        public static bool PointInExtents(Point3d p, Extents3d ext)
        {
            var min = ext.MinPoint;
            var max = ext.MaxPoint;
            return (p.X >= min.X &&
                     p.X <= max.X &&
                     p.Y >= min.Y &&
                     p.Y <= max.Y);
        }

        public static bool PointInPoly(double x, double y, cadPolyline poly)
        {
            if (PointOnPolylineEdge(x, y, poly, 0.001f)) // kenarlardan birisi üzerinde mi?
                return true;

            int n = poly.NumberOfVertices;
            int i = 0, j = n - 1;
            Point2d pi, pj;
            bool sonuc = false;

            while (i < n)
            {
                pi = poly.GetPoint2dAt(i);
                pj = poly.GetPoint2dAt(j);

                if (((pi.Y <= y && y < pj.Y) || (pj.Y <= y && y < pi.Y)) &&
                    (x < (pi.X + ((pj.X - pi.X) * (y - pi.Y)) / (pj.Y - pi.Y))))
                {
                    sonuc = !sonuc;
                }
                j = i;
                i++;
            }
            return sonuc;
        }
        public static bool IsPolylinesSame(cadPolyline poly1, cadPolyline poly2)
        {
            int n = poly1.NumberOfVertices;
            if (n != poly2.NumberOfVertices)
                return false;
            int i = 0;
            while (i < n)
            {
                if (!poly1.GetPoint2dAt(i).IsEqualTo(poly2.GetPoint2dAt(i)))
                    return false;
                i++;
            }
            return true;
        }
        public static bool PolylineInPolyline(cadPolyline inPoly, cadPolyline outPoly)
        {
            int n = inPoly.NumberOfVertices;
            int i = 0;
            Point2d pi;
            while (i < n)
            {
                pi = inPoly.GetPoint2dAt(i);
                if (!PointInPoly(pi.X, pi.Y, outPoly))
                    return false;
                i++;
            }
            return true;
        }
        public static List<cadPolyline> BagimsizBolumNoBul(cadPolyline poly, List<cadPolyline> _polylines, List<Tuple<string, Point3d>> _numberTexts)
        {
            List<cadPolyline> listBagimsizBolumPolylines = new List<cadPolyline>(); // dubleks, ortası boşluklu vb ayrık bağımsız bölümleri tanımlayan polyline'ları tutan liste. 

            // polyine listesinin array kopyasını alıyoruz.
            List<cadPolyline> copyOf_polylines = new List<cadPolyline>(_polylines.Count);
            _polylines.ForEach((item) =>
            {
                cadPolyline newitem = new cadPolyline();
                newitem = item.Clone() as cadPolyline;
                copyOf_polylines.Add(newitem);
            });
            // numara yazıları tek tek hangi polyline'ın içine giriyor ona bakılıyor.
            foreach (Tuple<string, Point3d> tup in _numberTexts)
            {
                string numberStr = tup.Item1;
                cadPolyline ilgiliPoly = null;

                //DBObject obj = tr.GetObject(pl.ObjectId, OpenMode.ForWrite);
                //cadPolyline poly = obj as cadPolyline;
                double x = tup.Item2.X;
                double y = tup.Item2.Y;
                if (Utils.PointInPoly(x, y, poly))
                {
                    if (ilgiliPoly != null)
                    {
                        if (Utils.PolylineInPolyline(poly, ilgiliPoly))
                        {
                            ilgiliPoly = poly;
                        }
                    }
                    else
                    {
                        ilgiliPoly = poly;
                    }
                    listBagimsizBolumPolylines.Add(ilgiliPoly); // .Add yapınca sonuna ekliyor ve listeyi değiştiriyor, .Append yapınca listeyi değiştirmiyor

                    //System.Windows.MessageBox.Show("numberstr:" + numberStr + "\n" + ilgiliPoly.ToString());
                    //yazının içinde bulunduğu polyline bulundu, bu polyline'ın içinde başka polyline'lar var mı diye bakılıyor.

                    foreach (cadPolyline pl2 in copyOf_polylines)
                    {
                        if (Utils.PolylineInPolyline(pl2, ilgiliPoly) &&
                            !Utils.IsPolylinesSame(pl2, ilgiliPoly))
                        {
                            Entity polyEnt = Utils.CopyObject(pl2 as Entity, new Point3d(0, 0, 0), new Point3d(0, 0, 0), "0");
                            listBagimsizBolumPolylines.Add(polyEnt as cadPolyline);
                        }
                    }
                    //break;
                }
            }// foreach  (Tuple<string, Point3d> tup 
            return listBagimsizBolumPolylines;
        }
        public static bool PointInArea(double x, double y, Point3dCollection pcoll)
        {
            bool sonuc = false;
            var pl = new cadPolyline(pcoll.Count);
            int i = 0;
            foreach (Point3d p in pcoll)
            {
                pl.AddVertexAt(i, new Point2d(p.X, p.Y), 0.0, 0.0, 0.0);
                i++;
            }
            sonuc = PointInPoly(x, y, pl);
            pl.Dispose();
            return sonuc;
        }
        /// <summary>
        /// Checks if there are boundaryreps that are marked as elliptical or circular arcs
        /// returns true if we found at least 2 of those points
        /// also stores the points in a referenced Point3dCollection
        /// </summary>
        /// <param name="solid"></param>
        /// <param name="pts"></param>
        /// <returns></returns>
        public static bool GetSweepPathPoints(Entity solid, ref Point3dCollection pts)
        {
            System.Windows.MessageBox.Show("eee");

            try
            {
                using (Transaction Tx = solid.Database.TransactionManager.StartTransaction())
                {
                    Solid3d sld = Tx.GetObject(solid.ObjectId, OpenMode.ForWrite) as Solid3d;
                    // create boundary rep for the solid
                    using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brep = new Autodesk.AutoCAD.BoundaryRepresentation.Brep(sld))
                    {
                        // get edges of the boundary rep
                        if (brep != null)
                        {
                            BrepEdgeCollection edges = brep.Edges;

                            foreach (Edge edge in edges)
                            {
                                // get the nativ curve geometry of the edges and then 
                                // check if it is a circle
                                // for more info look at:
                                // http://adndevblog.typepad.com/autocad/2012/08/retrieving-native-curve-geometry-using-brep-api.html

                                Curve3d curv = ((ExternalCurve3d)edge.Curve).NativeCurve;
                                if (curv is CircularArc3d)
                                {
                                    // transform curved arch into circle and add it to the colecction 
                                    // (if not in it alreadz)
                                    CircularArc3d circle = curv as CircularArc3d;
                                    if (!pts.Contains(circle.Center)) pts.Add(circle.Center);
                                }
                            }
                        }
                    }
                    Tx.Commit();
                }
            }
            catch (System.Exception ex)
            {
                Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("Exception: {0}", ex.Message);
            }
            return (pts.Count > 1) ? true : false;
        }
        public static Entity CopyObject(Entity obj, Point3d basePoint, Point3d destPoint, string layerName)
        {
            /*
                        Database db = poly.Database;

                        //make model space as owner for new entity
                        ObjectId ModelSpaceId = SymbolUtilityServices.GetBlockModelSpaceId(db);
                        IdMapping mapping = new IdMapping();
                        ObjectIdCollection coll = new ObjectIdCollection();
                        coll.Add(poly.ObjectId);
                        db.DeepCloneObjects(coll, ModelSpaceId, mapping, false);

                        //now open the new entity and move to 0,0 point...
                        using (Transaction Tx = db.TransactionManager.StartTransaction())
                        {
                            //get the map.
                            IdPair pair1 = mapping[poly.ObjectId];
                            //new object
                            Entity ent = Tx.GetObject(pair1.Value,OpenMode.ForWrite) as Entity;
                            Tx.Commit();
                            // move to 0,0
                            // buraya birşeyler gelecek..
                        }
            */
            Database db = obj.Database;
            //            Point3d destPoint = new Point3d(0, 0, 0);
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Entity newEntity = null;

            var disp = basePoint.GetVectorTo(destPoint).TransformBy(ed.CurrentUserCoordinateSystem);

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                // open the source entity
                var entity = (Entity)tr.GetObject(obj.ObjectId, OpenMode.ForRead);
                // copy and transform the clone
                var clone = entity.GetTransformedCopy(Matrix3d.Displacement(disp));
                // add the clone to the current space
                var space = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                space.AppendEntity(clone);
                tr.AddNewlyCreatedDBObject(clone, true);
                if (layerName != "")
                    clone.Layer = layerName;
                newEntity = clone;
                tr.Commit();
            }
            if (newEntity == null)
                System.Windows.MessageBox.Show("copyobject null");
            return newEntity;
        }
        public static void ExtrudePlines(List<ObjectId> polylineIdList, double height, string layerName)
        {
            foreach (ObjectId id in polylineIdList)
            {
                ExtrudePline(id.GetObject(OpenMode.ForRead) as cadPolyline, height, layerName);
            }
        }
        public static ObjectId ExtrudePline_iptal(cadPolyline poly, double height)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            ObjectId[] ids = new ObjectId[1] { poly.ObjectId };
            var sele = SelectionSet.FromObjectIds(ids);
            //ed.Command("extrude", "mode", "solid", sele, "", height);
            ed.Command("extrude", sele, "", height);

            //Point3d pickPoint = poly.GetPoint3dAt(0);
            //ResultBuffer rb = new ResultBuffer();
            //rb.Add(new TypedValue((int)Autodesk.AutoCAD.Runtime.LispDataType.ListBegin));
            //rb.Add(new TypedValue((int)Autodesk.AutoCAD.Runtime.LispDataType.ObjectId, poly.ObjectId));
            //rb.Add(new TypedValue((int)Autodesk.AutoCAD.Runtime.LispDataType.Point3d, pickPoint));
            //rb.Add(new TypedValue((int)Autodesk.AutoCAD.Runtime.LispDataType.ListEnd));
            //ed.Command("extrude", "mode", "solid", rb, "", height);
            return ed.SelectLast().Value[0].ObjectId;
        }
        public static ObjectId ExtrudePline(cadPolyline pline, double height, string layerName)
        {
            // Never dispose Document nor Document.Editor
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            ObjectId oid;
            // Never dispose Database when the drawing is opened in the Editor
            // On the other hand, always dispose Database created with the constructor, i.e. new Database()
            Database db = doc.Database;

            // Always dispose DocumentLock
            using (doc.LockDocument())
            // Always dispose Transaction
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                // No need to dispose a DBObject opened from a transaction
                BlockTableRecord currentSpace =
                    (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                // Always dispose a new DBObject which may not be added to the database
                using (pline)
                {
                    short delobj = (short)Application.GetSystemVariable("DELOBJ");
                    Application.SetSystemVariable("DELOBJ", 2);
                    // Dispose DBObjectCollection in case there're some objects left 
                    // for which no managed wrapper have been created
                    //System.Windows.MessageBox.Show("debug -1");
                    using (DBObjectCollection plineCollection = new DBObjectCollection())
                    {
                        plineCollection.Add(pline);

                        // Dispose DBObjectCollection in case there're some objects left 
                        // for which no managed wrapper have been created
                        using (DBObjectCollection regionCollection = Region.CreateFromCurves(plineCollection))
                        {
                            // Always dispose an object contained in a DBObjectCollection
                            // for which a managed wrapper is created and isn't added to the database
                            using (Region region = (Region)regionCollection[0])
                            {
                                // Use Dispose to insure the new DBObject will be disposed 
                                // if an exception occurs before it is added to the Database
                                using (Solid3d solid = new Solid3d())
                                {
                                    solid.Extrude(region, height, 0.0);
                                    currentSpace.AppendEntity(solid);
                                    tr.AddNewlyCreatedDBObject(solid, true);
                                    solid.Layer = layerName;
                                    oid = solid.ObjectId;
                                }
                            }
                        }
                    }
                    Application.SetSystemVariable("DELOBJ", delobj);

                    //if ((short)Application.GetSystemVariable("DELOBJ") == 0)
                    //{
                    //    currentSpace.AppendEntity(pline);
                    //    tr.AddNewlyCreatedDBObject(pline, true);
                    //}
                }
                tr.Commit();
            }
            return oid;
        } // ExtrudePline

        public static ObjectId ExtrudePlines(List<cadPolyline> listPolylines, double height, string layerName, SolidTuru st)
        {
            // Never dispose Document nor Document.Editor
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            List<ObjectId> solidIds = new List<ObjectId>();

            // Never dispose Database when the drawing is opened in the Editor
            // On the other hand, always dispose Database created with the constructor, i.e. new Database()
            Database db = doc.Database;

            // Always dispose DocumentLock
            using (doc.LockDocument())
            // Always dispose Transaction
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                // No need to dispose a DBObject opened from a transaction
                BlockTableRecord currentSpace =
                    (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                // Always dispose a new DBObject which may not be added to the database
                using (DBObjectCollection plineCollection = new DBObjectCollection())
                {
                    foreach (var poly in listPolylines)
                    {
                        plineCollection.Add(poly);
                    }
                    short sassoc = (short)Application.GetSystemVariable("SURFACEASSOCIATIVITY"); // If the SURFACEASSOCIATIVITY system variable is set to 1, the DELOBJ setting is ignored. 
                    short delobj = (short)Application.GetSystemVariable("DELOBJ");
                    Application.SetSystemVariable("DELOBJ", 2);
                    // Dispose DBObjectCollection in case there're some objects left 
                    // for which no managed wrapper have been created
                    using (DBObjectCollection regionCollection = Region.CreateFromCurves(plineCollection))
                    {
                        for (int i = 0; i < regionCollection.Count; i++)
                        {
                            // for which a managed wrapper is created and isn't added to the database
                            using (Region region = (Region)regionCollection[i])
                            {
                                // Use Dispose to insure the new DBObject will be disposed 
                                // if an exception occurs before it is added to the Database
                                using (Solid3d solid = new Solid3d())
                                {
                                    solid.Extrude(region, height, 0.0);
                                    currentSpace.AppendEntity(solid);
                                    tr.AddNewlyCreatedDBObject(solid, true);
                                    solid.Layer = layerName;
                                    solidIds.Add(solid.ObjectId);
                                }
                            }
                        }
                    }
                    Application.SetSystemVariable("DELOBJ", delobj);

                    //if ((short)Application.GetSystemVariable("DELOBJ") == 0)
                    //{
                    //    currentSpace.AppendEntity(pline);
                    //    tr.AddNewlyCreatedDBObject(pline, true);
                    //}
                } // using DBCollection
                tr.Commit();
            }
            Solid3d resultSolid;
            if(st == SolidTuru.KatSolid)
            {
                resultSolid = KatSolidleriniBirlestir(solidIds, layerName);
            }
            else
            {
                resultSolid = BagimsizSolidleriniBirlestir(solidIds, layerName);
            }
            return resultSolid.ObjectId;
        } // ExtrudePlines



        public static ObjectId CreateSolidFromPolyline(cadPolyline poly, Point3d basePoint, double floorHeight, string layerName)
        {
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;

            // Polyline 0,0 noktasına kopyalanıyor
            Entity polyEnt = CopyObject(poly as Entity, basePoint, new Point3d(0, 0, 0), layerName);
            //polyEnt.Layer = layerName;

            // kopyalanan polyline "extrude" işlemiyle 3DSOLID haline getiriliyor.
            return ExtrudePline(polyEnt as cadPolyline, floorHeight, layerName);
        } // CreateSolidFromPolyline

        public static ObjectId CreateSolidFromPolylines(List<cadPolyline> listPolyline, Point3d basePoint, double floorHeight, string layerName, SolidTuru st)
        {
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            var copiedPolylines = new List<cadPolyline>();

            foreach (var poly in listPolyline)
            {
                // Polyline 0,0 noktasına kopyalanıyor
                Entity polyEnt = CopyObject(poly as Entity, basePoint, new Point3d(0, 0, 0), layerName);
                //polyEnt.Layer = layerName;
                copiedPolylines.Add(polyEnt as cadPolyline);
            }
            // kopyalanan polyline "extrude" işlemiyle 3DSOLID haline getiriliyor.
            return ExtrudePlines(copiedPolylines, floorHeight, layerName, st);
        } // CreateSolidFromPolyline

        public static ObjectId PolyFaceMeshFromPline(Point3dCollection pts, double height, Point3d basePoint)
        {
            // Never dispose Document nor Document.Editor
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            ObjectId id;

            // Never dispose Database when the drawing is opened in the Editor
            // On the other hand, always dispose Database created with the constructor, i.e. new Database()
            Database db = doc.Database;

            // Always dispose DocumentLock
            using (doc.LockDocument())
            {
                // Always dispose Transaction
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    // No need to dispose a DBObject opened from a transaction
                    BlockTableRecord currentSpace =
                        (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                    PolyFaceMesh pfm = new PolyFaceMesh();
                    currentSpace.AppendEntity(pfm);
                    tr.AddNewlyCreatedDBObject(pfm, true);
                    //                    if (pline.IsErased) System.Windows.MessageBox.Show("silik");
                    //                   System.Windows.MessageBox.Show("-1-numverts:" + pts.Count.ToString());

                    // Always dispose a new DBObject which may not be added to the database
                    //using (pline)
                    //{
                    int numVerts = pts.Count;

                    for (int i = 0; i < numVerts; i++)
                    {
                        Point3d ptx = pts[i];
                        PolyFaceMeshVertex vert = new PolyFaceMeshVertex(new Point3d(ptx.X - basePoint.X, ptx.Y - basePoint.Y, ptx.Z));
                        pfm.AppendVertex(vert);
                        tr.AddNewlyCreatedDBObject(vert, true);

                        vert = new PolyFaceMeshVertex(new Point3d(ptx.X - basePoint.X, ptx.Y - basePoint.Y, ptx.Z + height));
                        pfm.AppendVertex(vert);
                        tr.AddNewlyCreatedDBObject(vert, true);
                    }

                    //bool ilk = true;

                    //for (short i = 0; i < numVerts - 4; i++)
                    //{
                    //    short v1, v2, v3, v4;
                    //    v1 = i;
                    //    v2 = (short) (i + 1);
                    //    v3 = (short) (i + 3);
                    //    v4 = (short) (i + 2);

                    //    using (FaceRecord acFaceRec1 = new FaceRecord(v1, v2, v3, v4))
                    //    {
                    //        pfm.AppendFaceRecord(acFaceRec1);
                    //        tr.AddNewlyCreatedDBObject(acFaceRec1, true);
                    //    }
                    //}
                    //using (FaceRecord acFaceRec1 = new FaceRecord((short)(numVerts - 5), (short)(numVerts - 4), 3, 2))
                    //{
                    //    pfm.AppendFaceRecord(acFaceRec1);
                    //    tr.AddNewlyCreatedDBObject(acFaceRec1, true);
                    //}

                    //for (int i = 0; i < numVerts; i++)
                    //{
                    //    Point3d ptx = pts[i];
                    //    PolyFaceMeshVertex vert = new PolyFaceMeshVertex(
                    //        new Point3d(ptx.X - basePoint.X, ptx.Y - basePoint.Y, ptx.Z));
                    //    pfm.AppendVertex(vert);
                    //    tr.AddNewlyCreatedDBObject(vert, true);
                    //}
                    //for (int i = numVerts - 1; i <= 0; i--)
                    //{
                    //    Point3d ptx = pts[i];
                    //    PolyFaceMeshVertex vert = new PolyFaceMeshVertex(
                    //        new Point3d(ptx.X - basePoint.X, ptx.Y - basePoint.Y, ptx.Z + height));
                    //    pfm.AppendVertex(vert);
                    //    tr.AddNewlyCreatedDBObject(vert, true);
                    //}

                    //for (short i = 0, j = (short)(numVerts - 1); i < numVerts; i++, j--)
                    //{
                    //    short v1, v2, v3, v4;
                    //    v1 = i;
                    //    v2 = (short)(v1 + 1);
                    //    if (v2 > numVerts - 1) v2 = 0;
                    //    v3 = j;
                    //    v4 = (short)(v3 - 1);
                    //    if (v4 < 0) v4 = (short)(numVerts - 1);
                    //    //v3 = (short)(v2 + numVerts);
                    //    //v4 = (short)(v1 + numVerts);
                    //    using (FaceRecord acFaceRec1 = new FaceRecord(v1, v2, v3, v4))
                    //    {
                    //        pfm.AppendFaceRecord(acFaceRec1);
                    //        tr.AddNewlyCreatedDBObject(acFaceRec1, true);
                    //    }
                    //}
                    id = pfm.ObjectId;

                    //if ((short)Application.GetSystemVariable("DELOBJ") == 0)
                    //{
                    //    currentSpace.AppendEntity(pline);
                    //    tr.AddNewlyCreatedDBObject(pline, true);
                    //}
                    tr.Commit();
                } // using pline

            } // using (doc.LockDocument())
            return id;
        } //PolyFaceMeshFromPline
        public static ObjectId Create3DFloor(cadPolyline poly, Point3d basePoint, double floorHeight)
        {
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;

            // Polyline 0,0 noktasına kopyalanıyor
            cadPolyline polyEnt = (cadPolyline)CopyObject(poly as Entity, basePoint, new Point3d(0, 0, 0), "");
            int n = polyEnt.NumberOfVertices;
            Point3dCollection pts = new Point3dCollection();
            for (int i = 0; i < n; i++)
            {
                pts.Add(polyEnt.GetPoint3dAt(i));
            }

            // kopyalanan polyline ile 3 Boyutlu PolyFaceMesh oluşturuluyor.
            return PolyFaceMeshFromPline(pts, floorHeight, basePoint);
        } // Create3DFloor

        public static ClassTapuBilgileri ReadFromDictionary()
        {
            ClassTapuBilgileri tb       = new ClassTapuBilgileri();
            tb.ProjeID                  = CustomDictionary.GetValue("TapuBilgileri", "ProjeID");
            tb.ilceKodu                 = CustomDictionary.GetValue("TapuBilgileri", "ilceKodu");
            tb.MahalleKoduTapu          = CustomDictionary.GetValue("TapuBilgileri", "MahalleKoduTapu");
            tb.MahalleKoduIdari         = CustomDictionary.GetValue("TapuBilgileri", "MahalleKoduIdari");
            tb.AdaNo                    = CustomDictionary.GetValue("TapuBilgileri", "AdaNo");
            tb.ParselNo                 = CustomDictionary.GetValue("TapuBilgileri", "ParselNo");
            tb.SiteAdi                  = CustomDictionary.GetValue("TapuBilgileri", "SiteAdi");
            tb.BinaNo                   = CustomDictionary.GetValue("TapuBilgileri", "BinaNo");
            tb.BlokAdi                  = CustomDictionary.GetValue("TapuBilgileri", "BlokAdi");
            tb.BinaBarkodNo             = CustomDictionary.GetValue("TapuBilgileri", "BinaBarkodNo");
            tb.BinaSifirKotu            = CustomDictionary.GetValue("TapuBilgileri", "BinaSifirKotu");
            tb.Ondulasyon               = CustomDictionary.GetValue("TapuBilgileri", "Ondulasyon");
            tb.BinaDaskNo               = CustomDictionary.GetValue("TapuBilgileri", "BinaDaskNo");
            tb.OtelAdi                  = CustomDictionary.GetValue("TapuBilgileri", "OtelAdi");
            tb.Notlar                   = CustomDictionary.GetValue("TapuBilgileri", "Notlar");
            return tb;
        } // ReadFromDictionary
        public static void WriteToDictionary(ClassTapuBilgileri tb)
        {
            CustomDictionary.SetValue("TapuBilgileri", "ProjeID", tb.ProjeID);
            CustomDictionary.SetValue("TapuBilgileri", "ilceKodu", tb.ilceKodu);
            CustomDictionary.SetValue("TapuBilgileri", "MahalleKoduTapu", tb.MahalleKoduTapu);
            CustomDictionary.SetValue("TapuBilgileri", "MahalleKoduIdari", tb.MahalleKoduIdari);
            CustomDictionary.SetValue("TapuBilgileri", "AdaNo", tb.AdaNo);
            CustomDictionary.SetValue("TapuBilgileri", "ParselNo", tb.ParselNo);
            CustomDictionary.SetValue("TapuBilgileri", "SiteAdi", tb.SiteAdi);
            CustomDictionary.SetValue("TapuBilgileri", "BinaNo", tb.BinaNo);
            CustomDictionary.SetValue("TapuBilgileri", "BlokAdi", tb.BlokAdi);
            CustomDictionary.SetValue("TapuBilgileri", "BinaBarkodNo", tb.BinaBarkodNo);
            CustomDictionary.SetValue("TapuBilgileri", "BinaSifirKotu", tb.BinaSifirKotu);
            CustomDictionary.SetValue("TapuBilgileri", "Ondulasyon", tb.Ondulasyon);
            CustomDictionary.SetValue("TapuBilgileri", "BinaDaskNo", tb.BinaDaskNo);
            CustomDictionary.SetValue("TapuBilgileri", "OtelAdi", tb.OtelAdi);
            CustomDictionary.SetValue("TapuBilgileri", "Notlar", tb.Notlar);
        } // WriteToDictionary

        public static List<Point3dCollection> GetSolidFaceCoordinates(ObjectId solidId)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var faceCoordinates = new List<Point3dCollection>();
            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid = Tx.GetObject(solidId, OpenMode.ForWrite) as Solid3d;

                ObjectId[] ids = new ObjectId[] { solid.ObjectId };

                FullSubentityPath path = new FullSubentityPath(ids, new SubentityId(SubentityType.Null, IntPtr.Zero));

                List<SubentityId> subEntIds = new List<SubentityId>();

                using (Brep brep = new Brep(solid))
                {
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                    {
                        foreach (BoundaryLoop boundaryloop in face.Loops)
                        {
                            var p3d = new Point3dCollection();
                            foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                            {
                                p3d.Add(vertex.Point);
                            }
                            faceCoordinates.Add(p3d);
                            //p3d.Clear();
                        } // foreach BoundaryLoop
                    }
                } // using new Brep(solid)
                Tx.Commit();
            }
            return faceCoordinates;
        } // GetSolidFaceCoordinates

//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
// yeni eklenen
        //public static List<Point3dCollection> GetFloorCoordinatesFromSolid3d(ObjectId solidId)
        public static List<Point3dCollection> GetFloorCoordinatesFromSolid3d(Solid3d solid)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Autodesk.AutoCAD.BoundaryRepresentation.Face floorFace = null;
            Point3dCollection outerCoordinates = new Point3dCollection();
            List<Point3dCollection> innerCoordinatesList = new List<Point3dCollection>();
            List<Point3dCollection> retList = new List<Point3dCollection>();

            //   using (Transaction Tx = db.TransactionManager.StartTransaction())
            // {
            //                Solid3d solid = Tx.GetObject(solidId, OpenMode.ForWrite) as Solid3d;
            double floorZ = solid.GeometricExtents.MinPoint.Z; //brep.Edges.ElementAt(0).Vertex1.Point.Z;
           // Utils.ShowMessage("floorZ=" + floorZ.ToString());
            using (Brep brep = new Brep(solid))
            {
                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                {
                    bool breyk = false;
                    foreach (BoundaryLoop boundaryloop in face.Loops)
                    {
                        breyk = false;
                        foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                        {
                            if (!isEqual(floorZ, vertex.Point.Z))
                            {
                                breyk = true;
                                break;
                            }
                        }
                        if (!breyk)
                        {
                            floorFace = face;
                            break;
                        }
                    }// foreach BoundaryLoop

                    if (floorFace != null) // floorFace bulundu şimdi dış ve iç (delik) koordinatları ayıklanıyor.
                    {
                        foreach (BoundaryLoop boundaryloop in floorFace.Loops)
                        {
                        //Utils.ShowMessage("boundaryloop:" + boundaryloop.LoopType.ToString());
                            if (boundaryloop.LoopType == LoopType.LoopInterior) // iç (delik) boundary
                            {
                                Point3dCollection innerCoordinates = new Point3dCollection();
                                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                                {
                                    innerCoordinates.Add(vertex.Point);
                                }
                                innerCoordinates.Add(new Point3d(innerCoordinates[0].X, innerCoordinates[0].Y, innerCoordinates[0].Z)); // ilk noktayı sona da ekliyoruz.
                                innerCoordinatesList.Add(innerCoordinates);
                            }
                            else // dış boundary
                            {
                                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                                {
                                    outerCoordinates.Add(vertex.Point);
                                }
                                outerCoordinates.Add(new Point3d(outerCoordinates[0].X, outerCoordinates[0].Y, outerCoordinates[0].Z)); // ilk noktayı sona da ekliyoruz.
                            }
                        } // foreach BoundaryLoop
                        retList.Add(outerCoordinates); // dış koordinatlar ilk listeye konuyor
                        if(innerCoordinatesList.Count > 0)
                            retList.AddRange(innerCoordinatesList); // varsa iç koordinatlar arkaya ekleniyor.
                        return retList;
                    } // if floorFace
                } // foreach face
            } // using new Brep(solid)
                //Tx.Commit();
           // }
            return retList;
        } // GetSolidFloorFaceCoordinates

        //-------------------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------------------------------------------------

        public static List<SolidFaceMaterialInfo> GetSolidFaceInfos___(ObjectId solidId)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var solidMaterialInfos = new List<SolidFaceMaterialInfo>();

            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = Tx.GetObject(solidId, OpenMode.ForWrite) as Solid3d;

                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                IntPtr pSubentityIdPE = solid3d.QueryX(
                    AssocPersSubentityIdPE.GetClass(
                    typeof(AssocPersSubentityIdPE)));
                if (pSubentityIdPE == IntPtr.Zero)
                    ed.WriteMessage("\nEntity doesn't support the subentityPE");
                AssocPersSubentityIdPE subentityIdPE =
                    AssocPersSubentityIdPE.Create(pSubentityIdPE, false)
                        as AssocPersSubentityIdPE;

                //FullSubentityPath path = new FullSubentityPath(ids, new SubentityId(SubentityType.Null, IntPtr.Zero));
                SubentityId[] faceIds = subentityIdPE.GetAllSubentities(solid3d, SubentityType.Face);
                ed.WriteMessage("\n- Face Subentities: ");
                foreach (SubentityId subentId in faceIds)
                {
                    FullSubentityPath path = new FullSubentityPath(ids, subentId);
                    Entity faceEntity = solid3d.GetSubentity(path);

                    if (faceEntity != null)
                    {
                        ed.WriteMessage("\n . " + faceEntity.ToString());
                        // faceEntity.Dispose();
                    }

                    if (faceEntity != null)
                    {
                        string matName = "xxx";
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(path.SubentId);
                            Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                            matName = material.Name;

                            Mapper mapper = solid3d.GetSubentityMaterialMapper(path.SubentId);
                            Matrix3d matris = mapper.Transform;
                            Utils.TransformationParameters trparam = Utils.ReadFromTransformationMatrix(matris);
                            trparam.AutoTransformMethodOfDiffuseMapMapper = (int)mapper.AutoTransform;

                            ed.WriteMessage("\n-------------------Transformation parameters:---------------------" +
                               "\nUOffset:" + trparam.UOffset.ToString("0.000") +
                               "\nVOffset:" + trparam.VOffset.ToString("0.000") +
                               "\nUScale:" + trparam.UScale.ToString("0.000") +
                               "\nVScale:" + trparam.VScale.ToString("0.000") +
                               "\nRotation:" + trparam.Rotation.ToString("0.000") +
                               "\nMaterial" + matName +
                               "\n----------------------------------------------------------------------------------\n");
                        }
                        catch (System.Exception ex)
                        {
                            matName = "----Yok-----" + ex.ToString();
                        }
                    }
                }
                Tx.Commit();
            }
            return solidMaterialInfos;
        } // GetSolidFaceInfos

        public static List<SolidFaceMaterialInfo> GetSolidFacesInfo___(ObjectId solidId)
        {
            var solidFaceMaterialInfos = new List<SolidFaceMaterialInfo>();
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = Tx.GetObject(solidId, OpenMode.ForWrite) as Solid3d;

                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                IntPtr pSubentityIdPE = solid3d.QueryX(
                    AssocPersSubentityIdPE.GetClass(
                    typeof(AssocPersSubentityIdPE)));
                if (pSubentityIdPE == IntPtr.Zero)
                    ed.WriteMessage("\nEntity doesn't support the subentityPE");
                AssocPersSubentityIdPE subentityIdPE =
                    AssocPersSubentityIdPE.Create(pSubentityIdPE, false)
                        as AssocPersSubentityIdPE;

                //FullSubentityPath path = new FullSubentityPath(ids, new SubentityId(SubentityType.Null, IntPtr.Zero));
                SubentityId[] faceIds = subentityIdPE.GetAllSubentities(solid3d, SubentityType.Face);
                ed.WriteMessage("\n- Face Subentities: ");
                foreach (SubentityId subentId in faceIds)
                {
                    FullSubentityPath path = new FullSubentityPath(ids, subentId);
                    Entity faceEntity = solid3d.GetSubentity(path);

                    if (faceEntity != null)
                    {
                        ed.WriteMessage("\n . " + faceEntity.ToString());
                        // faceEntity.Dispose();
                    }

                    if (faceEntity != null)
                    {
                        var sfmi = new SolidFaceMaterialInfo();
                        string matName = "xxx";
                        try
                        {
                            System.Windows.MessageBox.Show("---1----");

                            ObjectId materialId = solid3d.GetSubentityMaterial(path.SubentId);
                            System.Windows.MessageBox.Show("---2----");

                            Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                            matName = material.Name;

                            Mapper mapper = solid3d.GetSubentityMaterialMapper(path.SubentId);
                            Matrix3d matris = mapper.Transform;
                            Utils.TransformationParameters trparam = Utils.ReadFromTransformationMatrix(matris);
                            ed.WriteMessage("\n-------------------Transformation parameters:---------------------" +
                               "\nUOffset:" + trparam.UOffset.ToString("0.000") +
                               "\nVOffset:" + trparam.VOffset.ToString("0.000") +
                               "\nUScale:" + trparam.UScale.ToString("0.000") +
                               "\nVScale:" + trparam.VScale.ToString("0.000") +
                               "\nRotation:" + trparam.Rotation.ToString("0.000") +
                               "\nMaterial" + matName +
                               "\n----------------------------------------------------------------------------------\n");
                        }
                        catch (System.Exception ex)
                        {
                            matName = ex.ToString() + ":----Yok-----";
                        }

                        if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Surface))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Surface sEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Surface;
                        }
                        else if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Region))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Region rEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Region;
                            using (Brep brep = new Brep(rEnt))
                            {
                                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                                {
                                    foreach (BoundaryLoop boundaryloop in face.Loops)
                                    {
                                        foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                                        {
                                            sfmi.points.Add(vertex.Point);
                                            System.Windows.MessageBox.Show("---3----");
                                        }
                                    } // foreach BoundaryLoop
                                }
                            } // using new Brep(rEnt)
                        }
                        faceEntity.Dispose();
                        solidFaceMaterialInfos.Add(sfmi);
                    }
                } // foreach
            } // using
            return solidFaceMaterialInfos;
        }

        public static List<SolidFaceMaterialInfo> GetSolidFaceMaterialInfoList(ObjectId solidId)
        {
            var solidFaceMaterialInfos = new List<SolidFaceMaterialInfo>();
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Entity entity = Tx.GetObject(solidId, OpenMode.ForRead)
                    as Entity;
                ObjectId[] entId = new ObjectId[] { entity.ObjectId };
                IntPtr pSubentityIdPE = entity.QueryX(
                    AssocPersSubentityIdPE.GetClass(
                    typeof(AssocPersSubentityIdPE)));
                if (pSubentityIdPE == IntPtr.Zero)
                    ed.WriteMessage("\nEntity doesn't support the subentityPE");
                AssocPersSubentityIdPE subentityIdPE =
                    AssocPersSubentityIdPE.Create(pSubentityIdPE, false)
                        as AssocPersSubentityIdPE;

                SubentityId[] faceIds = subentityIdPE.GetAllSubentities(entity, SubentityType.Face);
                ed.WriteMessage("\n- Face Subentities: ");
                Solid3d solid3d = entity as Solid3d;
                int i = 0;
                foreach (SubentityId subentId in faceIds)
                {
                    i++;
                    FullSubentityPath path = new FullSubentityPath(entId, subentId);
                    Entity faceEntity = entity.GetSubentity(path);

                    if (faceEntity != null)
                    {
                        ed.WriteMessage("\n" + i +". " + faceEntity.ToString());
                        // faceEntity.Dispose();
                    }

                    if (faceEntity != null)
                    {
                        SolidFaceMaterialInfo sfmi = new SolidFaceMaterialInfo();
                        sfmi.materialMappingParameters = new TransformationParameters();
                        sfmi.faceNumber = subentId.IndexPtr.ToInt32();

                        string matName = "xxx";
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(path.SubentId);

                            Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                            matName = material.Name;

                            Mapper mapper = solid3d.GetSubentityMaterialMapper(path.SubentId);
                            Matrix3d matris = mapper.Transform;
                            string msg =
                                "\nfaceNum:" + sfmi.faceNumber.ToString() +
                                "  matName:" + matName + "\n" +
                                " m00 =" + matris[0, 0].ToString("0.0000") +
                                " m01 =" + matris[0, 1].ToString("0.0000") +
                                " m02 =" + matris[0, 2].ToString("0.0000") +
                                " m03 =" + matris[0, 3].ToString("0.0000") + "\n" +
                                " m10 =" + matris[1, 0].ToString("0.0000") +
                                " m11 =" + matris[1, 1].ToString("0.0000") +
                                " m12 =" + matris[1, 2].ToString("0.0000") +
                                " m13 =" + matris[1, 3].ToString("0.0000") + "\n" +
                                " m20 =" + matris[2, 0].ToString("0.0000") +
                                " m21 =" + matris[2, 1].ToString("0.0000") +
                                " m22 =" + matris[2, 2].ToString("0.0000") +
                                " m23 =" + matris[2, 3].ToString("0.0000") + "\n" +
                                " m30 =" + matris[3, 0].ToString("0.0000") +
                                " m31 =" + matris[3, 1].ToString("0.0000") +
                                " m32 =" + matris[3, 2].ToString("0.0000") +
                                " m33 =" + matris[3, 3].ToString("0.0000");

//                            System.Windows.MessageBox.Show(msg);
                            ed.WriteMessage("\n------------------------------------------------------------\n" + msg);

                            Utils.TransformationParameters trparam = Utils.ReadFromTransformationMatrix(matris);
                            //ed.WriteMessage("\n-------------------Transformation parameters:---------------------" +
                            //   "\nSubentId IndexPtr : " + subentId.IndexPtr +
                            //   "\nUOffset:" + trparam.UOffset.ToString("0.000") +
                            //   "\nVOffset:" + trparam.VOffset.ToString("0.000") +
                            //   "\nUScale:" + trparam.UScale.ToString("0.000") +
                            //   "\nVScale:" + trparam.VScale.ToString("0.000") +
                            //   "\nRotation:" + trparam.Rotation.ToString("0.000") +
                            //   "\nMaterial" + matName +
                            //   "\n----------------------------------------------------------------------------------\n");
                            trparam.AutoTransformMethodOfDiffuseMapMapper = (int)mapper.AutoTransform;
                            sfmi.materialMappingParameters = trparam;
                            sfmi.transformationMatrix = matris;
                        }
                        catch (System.Exception ex)
                        {
                            matName = "----Yok-----" + ex.ToString();
                        }

                        sfmi.materialName = matName;

                        if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Region))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Region rEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Region;
                            //ed.WriteMessage(
                            //"\n *** Region ***" +
                            //" SubentId IndexPtr : {0}" +
                            //" Face Perimeter : {1}" +
                            //" Face Area : {2}" +
                            //" Material : {3}",

                            //subentId.IndexPtr,
                            //rEnt.Perimeter,
                            //rEnt.Area,
                            //matName
                            //);
                            using (Brep brep = new Brep(rEnt))
                            //using (Brep brep = new Brep(path))
                            {
                                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                                {
                                    //ed.WriteMessage("\nface.SubentityPath.SubentId.IndexPtr:" + face.ToString());
                                    foreach (BoundaryLoop boundaryloop in face.Loops)
                                    {
                                        var points = new Point3dCollection();
                                        foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                                        {
                                            points.Add(vertex.Point);
                                            //ed.WriteMessage("\nvertex(" + i.ToString() + "):" +
                                            //    vertex.Point.X.ToString("0.000") + "," +
                                            //    vertex.Point.Y.ToString("0.000") + "," +
                                            //    vertex.Point.Z.ToString("0.000")
                                            //    );
                                        }
                                        sfmi.points = points;
                                    } // foreach BoundaryLoop
                                }
                            } // using new Brep(rEnt)

                        }
                        solidFaceMaterialInfos.Add(sfmi);
                        faceEntity.Dispose();
                    }
                }
            }
            return solidFaceMaterialInfos;
        } // GetSolidFaceMaterialInfoList

        static public void SetVisualStyle(string visualstylename)
        {
            /// Uzun yoldan..
            //Document doc =
            //  Application.DocumentManager.MdiActiveDocument;
            //Editor ed = doc.Editor;
            //Database db = doc.Database;
            //Transaction tr =
            //  db.TransactionManager.StartTransaction();
            //using (tr)
            //{
            //    ViewportTable vt =
            //      (ViewportTable)tr.GetObject(
            //        db.ViewportTableId, OpenMode.ForRead
            //      );
            //    ViewportTableRecord vtr =
            //      (ViewportTableRecord)tr.GetObject(
            //        vt["*Active"], OpenMode.ForWrite
            //      );
            //    DBDictionary dict =
            //      (DBDictionary)tr.GetObject(
            //        db.VisualStyleDictionaryId, OpenMode.ForRead
            //      );
            //    vtr.VisualStyleId = dict.GetAt(visualstylename);
            //    tr.Commit();
            //}
            //ed.UpdateTiledViewportsFromDatabase();
            //ed.WriteMessage("\nConceptual ayarlandı..");

            // Kısa yoldan..
            Autodesk.AutoCAD.Internal.Utils.SetCurrentViewportVisualStyle(Autodesk.AutoCAD.Internal.Utils.visualStyleId(visualstylename));
        }

        // code from:
        //https://adndevblog.typepad.com/autocad/2018/10/pattern-generation-of-linetype-with-text-on-polyline-entity.html

        public void CreateComplexLinetype()
        {
            Document doc =
                Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            Transaction tr =
                db.TransactionManager.StartTransaction();
            using (tr)
            {
                TextStyleTable tt =
                    (TextStyleTable)tr.GetObject(
                    db.TextStyleTableId,
                    OpenMode.ForRead
                    );
                LinetypeTable lt =
                    (LinetypeTable)tr.GetObject(
                    db.LinetypeTableId,
                    OpenMode.ForWrite
                    );
                LinetypeTableRecord ltr =
                    new LinetypeTableRecord();

                ltr.Name = "COLD_WATER_SUPPLY";
                ltr.AsciiDescription =
                    "Cold water supply ---- CW ---- CW ---- CW ----";
                ltr.PatternLength = 0.9;
                ltr.NumDashes = 3;
                // Dash #1
                ltr.SetDashLengthAt(0, 0.5);
                // Dash #2
                ltr.SetDashLengthAt(1, -0.2);
                ltr.SetShapeStyleAt(1, tt["Standard"]);
                ltr.SetShapeNumberAt(1, 0);
                ltr.SetShapeScaleAt(1, 0.1);
                ltr.SetTextAt(1, "CW");
                ltr.SetShapeRotationAt(1, 0);
                ltr.SetShapeOffsetAt(1, new Vector2d(0, -0.05));
                // Dash #3
                ltr.SetDashLengthAt(2, -0.2);

                // Add the new linetype to the linetype table
                ObjectId ltId = lt.Add(ltr);
                tr.AddNewlyCreatedDBObject(ltr, true);
                // Create a test line with this linetype
                BlockTable bt =
                    (BlockTable)tr.GetObject(
                    db.BlockTableId,
                    OpenMode.ForRead
                    );
                BlockTableRecord btr =
                    (BlockTableRecord)tr.GetObject(
                    bt[BlockTableRecord.ModelSpace],
            OpenMode.ForWrite
            );

                using (cadPolyline acPoly = new cadPolyline())
                {
                    acPoly.SetDatabaseDefaults(db);
                    acPoly.AddVertexAt(0, new Point2d(0, 0), 0, 0, 0);
                    acPoly.AddVertexAt(1, new Point2d(0, 2), 0, 0, 0);
                    acPoly.AddVertexAt(2, new Point2d(2, 2), 0, 0, 0);
                    acPoly.AddVertexAt(3, new Point2d(2, 0), 0, 0, 0);
                    acPoly.Closed = true;
                    btr.AppendEntity(acPoly);
                    tr.AddNewlyCreatedDBObject(acPoly, false);
                    Polyline2d poly2 = acPoly.ConvertTo(true);
                    poly2.LinetypeGenerationOn = false;
                    poly2.LinetypeId = ltId;
                    tr.AddNewlyCreatedDBObject(poly2, true);
                }
                tr.Commit();
            }
        }
    } // class Utils
}

