﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Runtime.InteropServices;

namespace Parsel3D
{
    class ImportsR17
    {
        public struct ads_name // Bunun yerine direkt AdsName de kullanılabilir.
        {
            public IntPtr a;
            public IntPtr b;
        };
        [DllImport("acdb21.dll",
            CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?acdbGetAdsName@@YA?AW4ErrorStatus@Acad@@AAY01JVAcDbObjectId@@@Z")]
        public static extern int acdbGetAdsName32(
            ref ads_name name,
            ObjectId objId);
        [DllImport("acdb21.dll",
            CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?acdbGetAdsName@@YA?AW4ErrorStatus@Acad@@AEAY01_JVAcDbObjectId@@@Z")]
        public static extern int acdbGetAdsName64(
            ref ads_name name,
            ObjectId objId);
        public static int acdbGetAdsName(
            ref ads_name name,
            ObjectId objId)
        {
            if (Marshal.SizeOf(IntPtr.Zero) > 4)
                return acdbGetAdsName64(ref name, objId);
            return acdbGetAdsName32(ref name, objId);
        }
        [DllImport("accore.dll",
            CharSet = CharSet.Unicode,
            CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "acdbEntGet")]
        public static extern System.IntPtr acdbEntGet(
            ref ads_name ename);
        public static System.Collections.Generic.List<TypedValue>
            acdbEntGetTypedValues(ObjectId id)
        {
            System.Collections.Generic.List<TypedValue> result =
                new System.Collections.Generic.List<TypedValue>();
            ads_name name = new ads_name();
            int res = acdbGetAdsName(ref name, id);
            ResultBuffer rb = new ResultBuffer();
            Autodesk.AutoCAD.Runtime.Interop.AttachUnmanagedObject(
                rb,
                acdbEntGet(ref name),
                true);
            ResultBufferEnumerator iter = rb.GetEnumerator();
            while (iter.MoveNext())
            {
                result.Add((TypedValue)iter.Current);
            }
            return result;
        }
        [CommandMethod("FieldList")]
        public static void FieldList()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            using (var tx = db.TransactionManager.StartTransaction())
            {
                var nod = tx.GetObject(
                    db.NamedObjectsDictionaryId,
                    OpenMode.ForRead) as DBDictionary;
                if (!nod.Contains("ACAD_FIELDLIST"))
                {
                    ed.WriteMessage("\nDrawing has no field...");
                    return;
                }
                var id = nod.GetAt("ACAD_FIELDLIST");
                System.Collections.Generic.List<TypedValue> dxf = ImportsR17.acdbEntGetTypedValues(id);
                foreach (var entry in dxf)
                {
                    if (entry.TypeCode == 330)
                    {
                        ObjectId objId = (ObjectId)entry.Value;
                        if (objId.ObjectClass.Name == "AcDbField")
                        {
                            Field field = tx.GetObject(
                                objId,
                                OpenMode.ForWrite) as Field;
                            field.Evaluate();
                            ed.WriteMessage(
                             "\n - Format: " + field.Format +
                             " Value: " +
                             field.GetStringValue());
                        }
                    }
                }
            }
        }
    } // ImportsR17
}