﻿namespace Parsel3D
{
    partial class PlanPlotForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonTamam = new System.Windows.Forms.Button();
            this.buttonVazgec = new System.Windows.Forms.Button();
            this.textboxKlasor = new System.Windows.Forms.TextBox();
            this.buttonGozat = new System.Windows.Forms.Button();
            this.cbMediaName1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbPlotStyle1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbDeviceList1 = new System.Windows.Forms.ComboBox();
            this.pickbutton = new System.Windows.Forms.Button();
            this.buttonCizimdenOku = new System.Windows.Forms.Button();
            this.textboxKatlar = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxArkaplanda = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxOnEk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSonEk = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbMediaName2 = new System.Windows.Forms.ComboBox();
            this.cbDeviceList2 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbPlotStyle2 = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.planPlotFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.planPlotFormBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.planPlotFormBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.planPlotFormBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonTamam
            // 
            this.buttonTamam.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonTamam.Location = new System.Drawing.Point(573, 484);
            this.buttonTamam.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonTamam.Name = "buttonTamam";
            this.buttonTamam.Size = new System.Drawing.Size(107, 33);
            this.buttonTamam.TabIndex = 0;
            this.buttonTamam.Text = "&Tamam";
            this.buttonTamam.UseVisualStyleBackColor = true;
            this.buttonTamam.Click += new System.EventHandler(this.buttonTamam_Click);
            // 
            // buttonVazgec
            // 
            this.buttonVazgec.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonVazgec.Location = new System.Drawing.Point(688, 484);
            this.buttonVazgec.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonVazgec.Name = "buttonVazgec";
            this.buttonVazgec.Size = new System.Drawing.Size(107, 33);
            this.buttonVazgec.TabIndex = 1;
            this.buttonVazgec.Text = "&Vazgeç";
            this.buttonVazgec.UseVisualStyleBackColor = true;
            // 
            // textboxKlasor
            // 
            this.textboxKlasor.Location = new System.Drawing.Point(8, 23);
            this.textboxKlasor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textboxKlasor.Name = "textboxKlasor";
            this.textboxKlasor.Size = new System.Drawing.Size(647, 22);
            this.textboxKlasor.TabIndex = 1;
            // 
            // buttonGozat
            // 
            this.buttonGozat.Location = new System.Drawing.Point(664, 22);
            this.buttonGozat.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonGozat.Name = "buttonGozat";
            this.buttonGozat.Size = new System.Drawing.Size(95, 25);
            this.buttonGozat.TabIndex = 2;
            this.buttonGozat.Text = "&Gözat";
            this.buttonGozat.UseVisualStyleBackColor = true;
            this.buttonGozat.Click += new System.EventHandler(this.buttonGozat_Click);
            // 
            // cbMediaName1
            // 
            this.cbMediaName1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMediaName1.FormattingEnabled = true;
            this.cbMediaName1.Items.AddRange(new object[] {
            "ISO_A4_(210.00_x_297.00_MM)",
            "ISO_A4_(297.00_x_210.00_MM)",
            "ISO_A3_(297.00_x_420.00_MM)",
            "ISO_A3_(420.00_x_297.00_MM)",
            "ISO_A2_(420.00_x_594.00_MM)",
            "ISO_A2_(594.00_x_420.00_MM)",
            "ISO_A1_(594.00_x_841.00_MM)",
            "ISO_A1_(841.00_x_594.00_MM)",
            "ISO_A0_(841.00_x_1189.00_MM)"});
            this.cbMediaName1.Location = new System.Drawing.Point(97, 76);
            this.cbMediaName1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbMediaName1.Name = "cbMediaName1";
            this.cbMediaName1.Size = new System.Drawing.Size(264, 24);
            this.cbMediaName1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Kağıt-1:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 123);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Plot Style-1:";
            // 
            // cbPlotStyle1
            // 
            this.cbPlotStyle1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlotStyle1.FormattingEnabled = true;
            this.cbPlotStyle1.Items.AddRange(new object[] {
            "acad.stb",
            "Autodesk-Color.stb",
            "Autodesk-MONO.stb",
            "monochrome.stb",
            "acad.ctb",
            "DWF Virtual Pens.ctb",
            "Fill Patterns.ctb",
            "Grayscale.ctb",
            "Screening 100%.ctb",
            "monochrome.ctb",
            "Screening 25%.ctb",
            "Screening 50%.ctb",
            "Screening 75%.ctb"});
            this.cbPlotStyle1.Location = new System.Drawing.Point(97, 119);
            this.cbPlotStyle1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPlotStyle1.Name = "cbPlotStyle1";
            this.cbPlotStyle1.Size = new System.Drawing.Size(264, 24);
            this.cbPlotStyle1.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 34);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Plotter-1:";
            // 
            // cbDeviceList1
            // 
            this.cbDeviceList1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceList1.FormattingEnabled = true;
            this.cbDeviceList1.Items.AddRange(new object[] {
            "DWG To PDF.pc3",
            "AutoCAD PDF (General Documentation).pc3",
            "AutoCAD PDF (High Quality Print).pc3",
            "AutoCAD PDF (Smallest File).pc3",
            "AutoCAD PDF (Web and Mobile).pc3",
            "DWF6 ePlot.pc3",
            "DWFx ePlot (XPS Compatible).pc3"});
            this.cbDeviceList1.Location = new System.Drawing.Point(97, 31);
            this.cbDeviceList1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbDeviceList1.Name = "cbDeviceList1";
            this.cbDeviceList1.Size = new System.Drawing.Size(264, 24);
            this.cbDeviceList1.TabIndex = 4;
            this.cbDeviceList1.SelectedIndexChanged += new System.EventHandler(this.cbDeviceList1_SelectedIndexChanged);
            // 
            // pickbutton
            // 
            this.pickbutton.Location = new System.Drawing.Point(127, 23);
            this.pickbutton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pickbutton.Name = "pickbutton";
            this.pickbutton.Size = new System.Drawing.Size(133, 28);
            this.pickbutton.TabIndex = 5;
            this.pickbutton.Text = "Tıkla <";
            this.pickbutton.UseVisualStyleBackColor = true;
            this.pickbutton.Click += new System.EventHandler(this.Pickbutton_Click);
            // 
            // buttonCizimdenOku
            // 
            this.buttonCizimdenOku.Location = new System.Drawing.Point(264, 21);
            this.buttonCizimdenOku.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCizimdenOku.Name = "buttonCizimdenOku";
            this.buttonCizimdenOku.Size = new System.Drawing.Size(124, 31);
            this.buttonCizimdenOku.TabIndex = 7;
            this.buttonCizimdenOku.Text = "Çizimden Oku <";
            this.buttonCizimdenOku.UseVisualStyleBackColor = true;
            this.buttonCizimdenOku.Click += new System.EventHandler(this.buttonCizimdenOku_Click);
            // 
            // textboxKatlar
            // 
            this.textboxKatlar.Location = new System.Drawing.Point(12, 23);
            this.textboxKatlar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textboxKatlar.Name = "textboxKatlar";
            this.textboxKatlar.Size = new System.Drawing.Size(235, 22);
            this.textboxKatlar.TabIndex = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textboxKatlar);
            this.groupBox1.Controls.Add(this.buttonCizimdenOku);
            this.groupBox1.Location = new System.Drawing.Point(17, 188);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(400, 66);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Katlar :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textboxKlasor);
            this.groupBox2.Controls.Add(this.buttonGozat);
            this.groupBox2.Location = new System.Drawing.Point(17, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(775, 66);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Çıktıların saklanacağı klasör:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.cbMediaName1);
            this.groupBox3.Controls.Add(this.cbDeviceList1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.cbPlotStyle1);
            this.groupBox3.Location = new System.Drawing.Point(17, 282);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(376, 175);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PDF Çıktı Ayarları";
            // 
            // checkBoxArkaplanda
            // 
            this.checkBoxArkaplanda.AutoSize = true;
            this.checkBoxArkaplanda.Location = new System.Drawing.Point(16, 464);
            this.checkBoxArkaplanda.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxArkaplanda.Name = "checkBoxArkaplanda";
            this.checkBoxArkaplanda.Size = new System.Drawing.Size(135, 21);
            this.checkBoxArkaplanda.TabIndex = 6;
            this.checkBoxArkaplanda.Text = "Arka planda takıl";
            this.checkBoxArkaplanda.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.textBoxOnEk);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.textBoxSonEk);
            this.groupBox4.Location = new System.Drawing.Point(17, 87);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox4.Size = new System.Drawing.Size(775, 89);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dosya İsimlendirme :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(452, 30);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 17);
            this.label6.TabIndex = 11;
            this.label6.Text = "Son Ek";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 30);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Ön Ek";
            // 
            // textBoxOnEk
            // 
            this.textBoxOnEk.Location = new System.Drawing.Point(12, 52);
            this.textBoxOnEk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxOnEk.Name = "textBoxOnEk";
            this.textBoxOnEk.Size = new System.Drawing.Size(308, 22);
            this.textBoxOnEk.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(329, 55);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "+ Kat Barkodu +";
            // 
            // textBoxSonEk
            // 
            this.textBoxSonEk.Location = new System.Drawing.Point(456, 52);
            this.textBoxSonEk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxSonEk.Name = "textBoxSonEk";
            this.textBoxSonEk.Size = new System.Drawing.Size(301, 22);
            this.textBoxSonEk.TabIndex = 8;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.cbMediaName2);
            this.groupBox5.Controls.Add(this.cbDeviceList2);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.cbPlotStyle2);
            this.groupBox5.Location = new System.Drawing.Point(401, 282);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox5.Size = new System.Drawing.Size(391, 175);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Resim Çıktı Ayarları";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 31);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 17);
            this.label7.TabIndex = 3;
            this.label7.Text = "Plotter-2:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 76);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "Kağıt-2:";
            // 
            // cbMediaName2
            // 
            this.cbMediaName2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMediaName2.FormattingEnabled = true;
            this.cbMediaName2.Items.AddRange(new object[] {
            "Sun_Hi-Res_(1600.00_x_1280.00_Pixels)",
            "XGA_Hi-Res_(1600.00_x_1200.00_Pixels)",
            "Super_XGA_(1280.00_x_1024.00_Pixels)",
            "Sun_Standard_(1152.00_x_900.00_Pixels)",
            "XGA_(1024.00_x_768.00_Pixels)",
            "Super_VGA_(800.00_x_600.00_Pixels)",
            "VGA_(640.00_x_480.00_Pixels)",
            "Sun_Hi-Res_(1280.00_x_1600.00_Pixels)",
            "XGA_Hi-Res_(1200.00_x_1600.00_Pixels)",
            "Super_XGA_(1024.00_x_1280.00_Pixels)",
            "Sun_Standard_(900.00_x_1152.00_Pixels)",
            "XGA_(768.00_x_1024.00_Pixels)",
            "Super_VGA_(600.00_x_800.00_Pixels)",
            "VGA_(480.00_x_640.00_Pixels)"});
            this.cbMediaName2.Location = new System.Drawing.Point(100, 76);
            this.cbMediaName2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbMediaName2.Name = "cbMediaName2";
            this.cbMediaName2.Size = new System.Drawing.Size(273, 24);
            this.cbMediaName2.TabIndex = 4;
            // 
            // cbDeviceList2
            // 
            this.cbDeviceList2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.planPlotFormBindingSource, "devicename2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, "PublishToWeb JPG.pc3"));
            this.cbDeviceList2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceList2.FormattingEnabled = true;
            this.cbDeviceList2.Items.AddRange(new object[] {
            "PublishToWeb JPG.pc3",
            "PublishToWeb PNG.pc3"});
            this.cbDeviceList2.Location = new System.Drawing.Point(100, 31);
            this.cbDeviceList2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbDeviceList2.Name = "cbDeviceList2";
            this.cbDeviceList2.Size = new System.Drawing.Size(273, 24);
            this.cbDeviceList2.TabIndex = 4;
            this.cbDeviceList2.SelectedIndexChanged += new System.EventHandler(this.cbDeviceList2_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 123);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "Plot Style-2:";
            // 
            // cbPlotStyle2
            // 
            this.cbPlotStyle2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPlotStyle2.FormattingEnabled = true;
            this.cbPlotStyle2.Items.AddRange(new object[] {
            "acad.stb",
            "Autodesk-Color.stb",
            "Autodesk-MONO.stb",
            "monochrome.stb",
            "acad.ctb",
            "DWF Virtual Pens.ctb",
            "Fill Patterns.ctb",
            "Grayscale.ctb",
            "Screening 100%.ctb",
            "monochrome.ctb",
            "Screening 25%.ctb",
            "Screening 50%.ctb",
            "Screening 75%.ctb"});
            this.cbPlotStyle2.Location = new System.Drawing.Point(100, 119);
            this.cbPlotStyle2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbPlotStyle2.Name = "cbPlotStyle2";
            this.cbPlotStyle2.Size = new System.Drawing.Size(273, 24);
            this.cbPlotStyle2.TabIndex = 4;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pickbutton);
            this.groupBox6.Location = new System.Drawing.Point(425, 188);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox6.Size = new System.Drawing.Size(367, 66);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Çizim Alanı :";
            // 
            // planPlotFormBindingSource
            // 
            this.planPlotFormBindingSource.DataSource = typeof(Parsel3D.PlanPlotForm);
            // 
            // planPlotFormBindingSource1
            // 
            this.planPlotFormBindingSource1.DataSource = typeof(Parsel3D.PlanPlotForm);
            // 
            // PlanPlotForm
            // 
            this.AcceptButton = this.buttonTamam;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonVazgec;
            this.ClientSize = new System.Drawing.Size(811, 532);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.checkBoxArkaplanda);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonVazgec);
            this.Controls.Add(this.buttonTamam);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PlanPlotForm";
            this.Text = "Plan Export PDF";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.planPlotFormBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.planPlotFormBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonTamam;
        private System.Windows.Forms.Button buttonVazgec;
        private System.Windows.Forms.TextBox textboxKlasor;
        private System.Windows.Forms.Button buttonGozat;
        private System.Windows.Forms.ComboBox cbMediaName1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbPlotStyle1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbDeviceList1;
        private System.Windows.Forms.Button pickbutton;
        private System.Windows.Forms.Button buttonCizimdenOku;
        private System.Windows.Forms.TextBox textboxKatlar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox checkBoxArkaplanda;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxSonEk;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxOnEk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbMediaName2;
        private System.Windows.Forms.ComboBox cbDeviceList2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbPlotStyle2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.BindingSource planPlotFormBindingSource;
        private System.Windows.Forms.BindingSource planPlotFormBindingSource1;
    }
}