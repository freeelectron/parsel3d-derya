﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using DocumentFormat.OpenXml.Math;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsel3D
{
    class sqldenemesatiri
    {
        public string WKT;
        public string fid;
        public string ilcekodu_kisa;
    }
    class Excel_OrtakAlanTabloSatiri : Excel_BagimsizTabloSatiri
    {
        public string OrtakAlanNo { get { return base.getBolumNo(); } set { base.setBolumNo(value); } }
        public string OrtakAlanBarkodNo { get { return base.getBarkodNo(); } set { base.setBarkodNo(value); } }
    }
    class Excel_EklentiTabloSatiri : Excel_BagimsizTabloSatiri
    {
        public string EklentiNo { get { return base.getBolumNo(); } set { base.setBolumNo(value); } }
        public string EklentiBarkodNo { get { return base.getBarkodNo(); } set { base.setBarkodNo(value); } }
    }
    class Excel_BagimsizTabloSatiri
    {
        public string ProjeID { get; set; }
//        public string ilceKodu_Uzun { get; set; }
//        public string MahalleKoduTapu_Uzun { get; set; }
//        public string MahalleKoduIdari_Uzun { get; set; }
        public string ilceKodu_Kisa { get; set; }
        public string TapuMahalleKodu { get; set; } // Eski adı MahalleKoduTapu_Kısa idi
        public string IdariMahalleKodu { get; set; } // Eski adı MahalleKoduIdari_Kısa idi
        public string Ada { get; set; }
        public string Parsel { get; set; }
        public string BinaNo { get; set; }
        public string BlokAdi { get; set; }
        public string BagimsizBolumNo { get; set; }
        public void setBolumNo(string bolumNo)
        { BagimsizBolumNo = bolumNo; }
        public string getBolumNo()
        { return BagimsizBolumNo; }
        public string BagimsizBolumBarkodNo { get; set; }
        public void setBarkodNo(string barkodNo)
        { BagimsizBolumBarkodNo = barkodNo; }
        public string getBarkodNo()
        { return BagimsizBolumBarkodNo; }
        public string KatNo { get; set; }
        private string BagimsizTuru { get; set; } // excel'de çıkmasın diye private yapıldı.
        public string getBagimsizTuru() { return BagimsizTuru; }
        //public string DilimOrtaBoylami { get; set; }
        public string ZeminKoordinatlari_Grid { get; set; }
        public string ZeminKoordinatlariEnlemBoylam { get; set; }
        public string _3DKoordinatlarGrid { get; set; }
        public string _3DKoordinatlarEnlemBoylam { get; set; }
        public string TabanKotu_Mimari { get; set; }
        public string TabanKotu_DenizdenYukseklik { get; set; }
        //public string Ondulasyon { get; set; }
        public string KatYuksekligi { get; set; }
        //public string PdfDosyaAdi { get; set; }
        public string Notlar { get; set; } = "";
        public void Oku(ParselBilgileri pb)
        {
            ProjeID = pb.TapuBilgileri.ProjeID;
            var ilceKodu_Uzun = pb.TapuBilgileri.ilceKodu;
            var MahalleKoduTapu_Uzun = pb.TapuBilgileri.MahalleKoduTapu;
            var MahalleKoduIdari_Uzun = pb.TapuBilgileri.MahalleKoduIdari;
            ilceKodu_Kisa = Utils.IlceKoduAyikla(ilceKodu_Uzun);
            TapuMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduTapu_Uzun);
            IdariMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduIdari_Uzun);
            Ada = pb.TapuBilgileri.AdaNo;
            Parsel = pb.TapuBilgileri.ParselNo;
            BinaNo = pb.TapuBilgileri.BinaNo;
            BlokAdi = pb.TapuBilgileri.BlokAdi;
            Notlar = pb.BagimsizBolumNotlar;

            string str = pb.BagimsizBolumNo;
            str = str.Replace('+', '0');
            str = str.Replace('-', '1');
            BagimsizBolumNo = str;

            str = pb.BarkodNo;
            str = str.Replace('+', '0');
            str = str.Replace('-', '1');
            BagimsizBolumBarkodNo = str;

            KatNo = pb.BagimsizBolumBulunduguKat.ToString();
            BagimsizTuru = pb.BagimsizTuru;
            //DilimOrtaBoylami = Utils.GetDOB(pb.TapuBilgileri.ilceKodu).ToString();

            var zeminKoordinatlari = pb.ZeminKoordinatlari;
            var tavanKoordinatlari = pb.TavanKoordinatlari;

            ZeminKoordinatlari_Grid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(zeminKoordinatlari, 3);

            // Zemin koordinatları enlem boylam'a çevriliyor
            var listenlemboylampcoll = GeoCalc.ConvertGridToLatLongListListPColl(zeminKoordinatlari, Utils.GetDOB(pb.TapuBilgileri.ilceKodu));
            ZeminKoordinatlariEnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listenlemboylampcoll, 8);

            // Tavan koordinatları enlem boylam'a çevriliyor.
            listenlemboylampcoll = GeoCalc.ConvertGridToLatLongListListPColl(tavanKoordinatlari, Utils.GetDOB(pb.TapuBilgileri.ilceKodu));
            var tavanKoordinatlariEnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listenlemboylampcoll, 8);

            //_3DKoordinatlarGrid = pb.TumKoordinatlarString;
            _3DKoordinatlarGrid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(zeminKoordinatlari, 3) 
                + Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(tavanKoordinatlari, 3);
            _3DKoordinatlarEnlemBoylam = ZeminKoordinatlariEnlemBoylam + tavanKoordinatlariEnlemBoylam;


            TabanKotu_Mimari = pb.BagimsizBolumTabanKotu_Mimari.ToString();

            TabanKotu_DenizdenYukseklik = pb.BagimsizBolumTabanKotu_DenizdenYukseklik.ToString();
            //Ondulasyon = pb.TapuBilgileri.Ondulasyon.ToString();
            KatYuksekligi = pb.BagimsizBolumKatYuksekligi.ToString();
            //PdfDosyaAdi = "Buraya Pdf Dosya adı gelecek";
        }
        public void Birlestir(Excel_BagimsizTabloSatiri other) // dubleks vs için birleştirme gerektiğinde..
        {
            if (other == null)
            {
                throw new ArgumentNullException(nameof(other));
            }
            Notlar += (Notlar == "") ? other.Notlar : (other.Notlar == "") ? "" : "," + other.Notlar; // Notlar kısmı da birleştiriliyor..

            string mevcut = ZeminKoordinatlari_Grid.Remove(ZeminKoordinatlari_Grid.Length-1); // sondaki ')' parantez atılıyor.
            string eklenecek = other.ZeminKoordinatlari_Grid.Replace(Utils.POLYGON_GEOMETRY_TEXT + "(", "");
            ZeminKoordinatlari_Grid = mevcut + "," + eklenecek;

            mevcut = ZeminKoordinatlariEnlemBoylam.Remove(ZeminKoordinatlariEnlemBoylam.Length - 1);
            eklenecek = other.ZeminKoordinatlariEnlemBoylam.Replace(Utils.POLYGON_GEOMETRY_TEXT + "(", "");
            ZeminKoordinatlariEnlemBoylam = mevcut + "," + eklenecek;

            mevcut = _3DKoordinatlarGrid.Remove(_3DKoordinatlarGrid.Length - 1);
            eklenecek = other._3DKoordinatlarGrid.Replace("(", "");
            _3DKoordinatlarGrid = mevcut + "," + eklenecek;

            mevcut = _3DKoordinatlarEnlemBoylam.Remove(_3DKoordinatlarEnlemBoylam.Length - 1);
            eklenecek = other._3DKoordinatlarEnlemBoylam.Replace("(", "");
            _3DKoordinatlarEnlemBoylam = mevcut + "," + eklenecek;
        }
    }
    class Excel_KatlarTablosu // !
    {
        public string ProjeID { get; set; }
        //public string ilceKodu_Uzun { get; set; }
        //public string MahalleKoduTapu_Uzun { get; set; }
        //public string MahalleKoduIdari_Uzun { get; set; }
        public string ilceKodu_Kisa { get; set; }
        public string TapuMahalleKodu { get; set; }
        public string IdariMahalleKodu { get; set; }
        public string Ada { get; set; }
        public string Parsel { get; set; }
        public string BinaNo { get; set; }
        public string BlokAdi { get; set; }
        public string KatNo { get; set; }
        public string KatBarkodNo { get; set; }
        //public string DilimOrtaBoylami { get; set; }
        public string ZeminKoordinatlari_Grid { get; set; }
        public string ZeminKoordinatlari_EnlemBoylam { get; set; }
        public string _3DKoordinatlar_Grid { get; set; }
        public string _3DKoordinatlar_EnlemBoylam { get; set; }
        public string TabanKotu_Mimari { get; set; }
        public string TabanKotu_DenizdenYukseklik { get; set; }
        //public string Ondulasyon { get; set; }
        public string KatYuksekligi { get; set; }
        //public string PdfDosyaAdi { get; set; }
        //public string Notlar { get; set; } = "";
        public void Oku(ParselBilgileri pb)
        {
            ProjeID = pb.TapuBilgileri.ProjeID;
            var ilceKodu_Uzun = pb.TapuBilgileri.ilceKodu;
            var MahalleKoduTapu_Uzun = pb.TapuBilgileri.MahalleKoduTapu;
            var MahalleKoduIdari_Uzun = pb.TapuBilgileri.MahalleKoduIdari;
            ilceKodu_Kisa = Utils.IlceKoduAyikla(ilceKodu_Uzun);
            TapuMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduTapu_Uzun);
            IdariMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduIdari_Uzun);

            Ada = pb.TapuBilgileri.AdaNo;
            Parsel = pb.TapuBilgileri.ParselNo;
            BinaNo = pb.TapuBilgileri.BinaNo;
            BlokAdi = pb.TapuBilgileri.BlokAdi;
            KatNo = pb.BagimsizBolumBulunduguKat.ToString();
            //DilimOrtaBoylami = Utils.GetDOB(pb.TapuBilgileri.ilceKodu).ToString();
            //Notlar = pb.BagimsizBolumNotlar;

            ZeminKoordinatlari_Grid = "0"; // buradan gerisi KatPlanSay fonk'da yazılıyor.
            ZeminKoordinatlari_EnlemBoylam = "0";
            _3DKoordinatlar_Grid = "0";
            _3DKoordinatlar_EnlemBoylam = "0";
            TabanKotu_Mimari = pb.BagimsizBolumTabanKotu_Mimari.ToString();
            TabanKotu_DenizdenYukseklik = pb.BagimsizBolumTabanKotu_DenizdenYukseklik.ToString();
            //Ondulasyon = pb.TapuBilgileri.Ondulasyon.ToString();
            KatYuksekligi = pb.BagimsizBolumKatYuksekligi.ToString();
            //PdfDosyaAdi = "0";
            KatBarkodNo = pb.BarkodNo; // Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.KatBarkodu);
        }
    }
    class Excel_BinaTablosu
    {
        public string ProjeID { get; set; }
        //public string ilceKodu_Uzun { get; set; }
        //public string MahalleKoduTapu_Uzun { get; set; }
        //public string MahalleKoduIdari_Uzun { get; set; }
        public string ilceKodu_Kisa { get; set; }
        public string TapuMahalleKodu { get; set; }
        public string IdariMahalleKodu { get; set; }
        public string Ada { get; set; }
        public string Parsel { get; set; }
        public string BinaNo { get; set; }
        public string BlokAdi { get; set; }
        public string BinaBarkodNo { get; set; }
        public string BinaZeminKoordinatlari_Grid { get; set; }

        public string BinaCatiKotu { get; set; }
        public string BinaTemelKotu { get; set; }
        public string BinaZeminKoordinatlariEnlemBoylam { get; set; }
        //public string BinaZeminOrtaNoktaVeDonukluk_Grid { get; set; }
        //public string BinaZeminOrtaNoktaVeDonukluk_EnlemBoylam { get; set; }
        public string _3DKoordinatlar_Grid { get; set; }
        public string _3DKoordinatlar_EnlemBoylam { get; set; }
        //public string BinaYuksekligi { get; set; }
        public string BinaKirmiziKotu { get; set; } // eski adı BinaSifirKotu idi
        //public string Ondulasyon { get; set; }
        //public string BinaDaskNo { get; set; }
        //public string OtelAdi { get; set; }
        //public string Notlar { get; set; } = "";

        public void Oku(ParselBilgileri pb)
        {
            ProjeID = pb.TapuBilgileri.ProjeID;
            var ilceKodu_Uzun = pb.TapuBilgileri.ilceKodu;
            var MahalleKoduTapu_Uzun = pb.TapuBilgileri.MahalleKoduTapu;
            var MahalleKoduIdari_Uzun = pb.TapuBilgileri.MahalleKoduIdari;
            ilceKodu_Kisa = Utils.IlceKoduAyikla(ilceKodu_Uzun);
            TapuMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduTapu_Uzun);
            IdariMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduIdari_Uzun);

            Ada = pb.TapuBilgileri.AdaNo;
            Parsel = pb.TapuBilgileri.ParselNo;
            BinaNo = pb.TapuBilgileri.BinaNo;
            BlokAdi = pb.TapuBilgileri.BlokAdi;
            BinaBarkodNo = pb.TapuBilgileri.BinaBarkodNo;
            BinaKirmiziKotu = pb.TapuBilgileri.BinaSifirKotu;
            //Ondulasyon = pb.TapuBilgileri.Ondulasyon;
            //BinaDaskNo = pb.TapuBilgileri.BinaDaskNo;
            //OtelAdi = pb.TapuBilgileri.OtelAdi;
            //Notlar = pb.TapuBilgileri.Notlar;

            BinaZeminKoordinatlari_Grid = "0"; // bu ikisi KatPlanSay fonk'da yazılıyor.
            _3DKoordinatlar_Grid = "0";
            BinaZeminKoordinatlariEnlemBoylam = "0";
            //BinaZeminOrtaNoktaVeDonukluk_Grid = "0";
            //BinaZeminOrtaNoktaVeDonukluk_EnlemBoylam = "0";
            //BinaYuksekligi = "0";
        }
    }
    class Excel_AdaParselTablosu
    {
        public string ProjeID { get; set; }
        //public string ilceKodu_Uzun { get; set; }
        //public string MahalleKoduTapu_Uzun { get; set; }
        //public string MahalleKoduIdari_Uzun { get; set; }
        public string ilceKodu_Kisa { get; set; }
        public string TapuMahalleKodu { get; set; }
        public string IdariMahalleKodu { get; set; }
        public string Ada { get; set; }
        public string Parsel { get; set; }
        public string AdaParselBarkodNo { get; set; }
        public string ParselKoordinatlari_Grid { get; set; }
        public string ParselKoordinatlari_EnlemBoylam { get; set; }
        //public string Notlar { get; set; } = "";
        public void Oku(ParselBilgileri pb)
        {
            ProjeID = pb.TapuBilgileri.ProjeID;
            var ilceKodu_Uzun = pb.TapuBilgileri.ilceKodu;
            var MahalleKoduTapu_Uzun = pb.TapuBilgileri.MahalleKoduTapu;
            var MahalleKoduIdari_Uzun = pb.TapuBilgileri.MahalleKoduIdari;
            ilceKodu_Kisa = Utils.IlceKoduAyikla(ilceKodu_Uzun);
            TapuMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduTapu_Uzun);
            IdariMahalleKodu = Utils.MahalleKoduAyikla(MahalleKoduIdari_Uzun);
            Ada = pb.TapuBilgileri.AdaNo;
            Parsel = pb.TapuBilgileri.ParselNo;
            AdaParselBarkodNo = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.ParselBarkodu);
            pb.BagimsizTuru = "Ada_Parsel";
            ParselKoordinatlari_Grid = "_grid_koordinatlar_";
            ParselKoordinatlari_EnlemBoylam = "_cografi_koordinatlar_";
            //Notlar = pb.BagimsizBolumNotlar;
        }
    }
    class Excel_YuzeyTablosu
    {
        //private string _uoffset;
        //private string _voffset;
        //private string _uscale;
        //private string _vscale;
        private int _autoTransformMethodOfDiffuseMapMapper;

        protected string[] AutoTransformMethodOfDiffuseMapMapperStr = {
                "1 = No auto transform",
                "2 = Scale mapper to current entity extents; translate mapper to entity origin",
                "4 = Include current block transform in mapper transform"
            };

        public string BinaBarkodNo { get; set; }
        public string SiteAdi { get; set; }
        public string BlokAdi { get; set; }
        public string Face_Number { get; set; }
        public string MaterialName { get; set; }
        public string AutoTransformMethodOfDiffuseMapMapper
        {
            get { return AutoTransformMethodOfDiffuseMapMapperStr[_autoTransformMethodOfDiffuseMapMapper]; }
            set { _autoTransformMethodOfDiffuseMapMapper = Convert.ToInt32(value); }
        }
        public string Koordinatlar_Grid { get; set; }
        public string Koordinatlar_EnlemBoylam { get; set; }

        public string mat00 { get; set; }
        public string mat01 { get; set; }
        public string mat02 { get; set; }
        public string mat03 { get; set; }

        public string mat10 { get; set; }
        public string mat11 { get; set; }
        public string mat12 { get; set; }
        public string mat13 { get; set; }

        public string mat20 { get; set; }
        public string mat21 { get; set; }
        public string mat22 { get; set; }
        public string mat23 { get; set; }

        public string mat30 { get; set; }
        public string mat31 { get; set; }
        public string mat32 { get; set; }
        public string mat33 { get; set; }

        //public string UOffset
        //{
        //    set { _uoffset = (value == "Infinity") ? "0" : value;}
        //    get { return _uoffset; }
        //}
        //public string VOffset
        //{
        //    set { _voffset = (value == "Infinity") ? "0" : value; }
        //    get { return _voffset; }
        //}
        public string UOffset { get; set; }
        public string VOffset { get; set; }
        public string UScale { get; set; }
        public string VScale { get; set; }
        public string Rotation { get; set; }

        public void Oku(ClassTapuBilgileri tb)
        {
            SiteAdi = tb.SiteAdi;
            BlokAdi = tb.BlokAdi;
            BinaBarkodNo = tb.BinaBarkodNo;
        }
    }
    // https://knowledge.autodesk.com/search-result/caas/CloudHelp/cloudhelp/2017/ENU/AutoCAD-DXF/files/GUID-E540C5BB-E166-44FA-B36C-5C739878B272-htm.html
    public class AutoCadMaterialRawProperties
    {
        public string Name { get; set; } // 1 Material name (string)
        public string Description { get; set; } // 2 Description (string, default null string)
        public string AmbientColorMethod { get; set; } // 70 Ambient color method(default = 0) :
        //	0 = Use current color
        //	1 = Override current color

        public string AmbientColorFactor { get; set; } // 40 Ambient color factor(real, default = 1.0; valid range is 0.0 to 1.0)
        public string AmbientColorValue { get; set; } // 90 Ambient color value(unsigned 32-bit integer representing an AcCmEntityColor)
        public string DiffuseColorMethod { get; set; } // 71 Diffuse color method(default = 0) :
                                                        //	0 = Use current color
                                                        //	1 = Override current color
        public string DiffuseColorFactor { get; set; } //41 Diffuse color factor(real, default = 1.0; valid range is 0.0 to 1.0)
        public string DiffuseColorValue { get; set; } // 91 Diffuse color value(unsigned 32-bit integer representing an AcCmEntityColor)
        public string DiffuseMapBlendFactor { get; set; } // 42 Diffuse map blend factor(real, default = 1.0)
        public string DiffuseMapSource { get; set; } // 72 Diffuse map source(default = 1) :
                                                     //0 = Use current scene
                                                     //1 = Use image file(specified by file name; null file name specifies no map)
        public string DiffuseMapFileName { get; set; } //3 Diffuse map file name(string, default = null string)
        public string ProjectionMethodOfDiffuseMapMapper { get; set; } // 73 Projection method of diffuse map mapper(default = 1):
                                                                       //1 = Planar
                                                                       //2 = Box
                                                                       //3 = Cylinder
                                                                       //4 = Sphere
        public string TilingMethodOfDiffuseMapMapper { get; set; } // 74 Tiling method of diffuse map mapper(default = 1):
                                                                   //1 = Tile
                                                                   //2 = Crop
                                                                   //3 = Clamp
        public string AutoTransformMethodOfDiffuseMapMapper { get; set; } // 75 Auto transform method of diffuse map mapper(bitset, default = 1) :
                                                                          //1= No auto transform
                                                                          //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                          //4 = Include current block transform in mapper transform
        public string TransformMatrixOfDiffuseMapMapper { get; set; } // 43 Transform matrix of diffuse map mapper(16 reals; row major format; default = identity matrix)
        public string SpecularGlossFactor { get; set; } // 44 Specular gloss factor(real, default = 0.5)
        public string SpecularColorMethod { get; set; } // 76 Specular color method(default = 0) :
                                                        //0 = Use current color
                                                        //1 = Override current color
        public string SpecularColorFactor { get; set; } // 45 Specular color factor(real, default = 1.0; valid range is 0.0 to 1.0)
        public string SpecularColorValue { get; set; } // 92 Specular color value(unsigned 32-bit integer representing an AcCmEntityColor)
        public string SpecularMapBlendFactor { get; set; } // 46 Specular map blend factor(real; default = 1.0)
        public string SpecularMapSource { get; set; }  // 77 Specular map source(default = 1) :
                                                       //0 = Use current scene
                                                       //1 = Use image file(specified by file name; null file name specifies no map)
        public string SpecularMapFileName { get; set; } // 4 Specular map file name(string; default = null string)
        public string ProjectionMethodOfSpecularMapMapper { get; set; } // 78 Projection method of specular map mapper(default = 1):
                                                                        //1 = Planar
                                                                        //2 = Box
                                                                        //3 = Cylinder
                                                                        //4 = Sphere
        public string TilingMethodOfSpecularMapMapper { get; set; } // 79 Tiling method of specular map mapper(default = 1):
                                                                    //1 = Tile
                                                                    //2 = Crop
                                                                    //3 = Clamp
        public string AutoTransformMethodOfSpecularMapMapper { get; set; } // 170 Auto transform method of specular map mapper(bitset; default = 1):
                                                                           //1 = No auto transform
                                                                           //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                           //4 = Include current block transform in mapper transform
        public string TransformMatrixOfSpecularMapMapper { get; set; } // 47 Transform matrix of specular map mapper(16 reals; row major format; default = identity matrix)
        public string BlendFactorOfReflectionMap { get; set; } // 48 Blend factor of reflection map(real, default = 1.0)
        public string ReflectionMapSource { get; set; } // 171 Reflection map source(default = 1) :
                                                        //0 = Use current scene
                                                        //1 = Use image file(specified by file name; null file name specifies no map)
        public string ReflectionMapFileName { get; set; } // 6 Reflection map file name(string; default = null string)
        public string ProjectionMethodOfReflectionMapMapper { get; set; } // 172 Projection method of reflection map mapper(default = 1):
                                                                          //1 = Planar
                                                                          //2 = Box
                                                                          //3 = Cylinder
                                                                          //4 = Sphere
        public string TilingMethodOfReflectionMapMapper { get; set; }// 173 Tiling method of reflection map mapper(default = 1):
                                                                     //1 = Tile
                                                                     //2 = Crop
                                                                     //3 = Clamp
        public string AutoTransformMethodOfReflectionMapMapper { get; set; } // 174 Auto transform method of reflection map mapper(bitset; default = 1):
                                                                             //1 = No auto transform
                                                                             //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                             //4 = Include current block transform in mapper transform
        public string TransformMatrixOfReflectionMapMapper { get; set; } // 49 Transform matrix of reflection map mapper(16 reals; row major format; default = identity matrix)
        public string OpacityPercent { get; set; } // 140 Opacity percent(real; default = 1.0)
        public string BlendFactorOfOpacityMap { get; set; } // 141 Blend factor of opacity map(real; default = 1.0)
        public string OpacityMapSource { get; set; }  //175 Opacity map source(default = 1) :
                                                      //0 = Use current scene
                                                      //1 = Use image file(specified by file name; null file name specifies no map)
        public string OpacityMapFileName { get; set; } // 7 Opacity map file name(string; default = null string)
        public string ProjectionMethodOfOpacityMapMapper { get; set; }  // 176 Projection method of opacity map mapper(default = 1):
                                                                        //1 = Planar
                                                                        //2 = Box
                                                                        //3 = Cylinder
                                                                        //4 = Sphere
        public string TilingMethodOfOpacityMapMapper { get; set; } // 177 Tiling method of opacity map mapper(default = 1):
                                                                   //1 = Tile
                                                                   //2 = Crop
                                                                   //3 = Clamp
        public string AutoTransformMethodOfOpacityMapMapper { get; set; } // 178 Auto transform method of opacity map mapper(bitset; default = 1):
                                                                          //1 = No auto transform
                                                                          //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                          //4 = Include current block transform in mapper transform
        public string TransformMatrixOfOpacityMapMapper { get; set; }  // 142 Transform matrix of opacity map mapper(16 reals; row major format; default = identity matrix)
        public string BlendFactorOfBumpMap { get; set; } // 143 Blend factor of bump map(real; default = 1.0)
        public string BumpMapSource { get; set; }  // 179 Bump map source(default = 1) :
                                                   //0 = Use current scene
                                                   //1 = Use image file(specified by file name; null file name specifies no map)
        public string BumpMapFileName { get; set; } // 8 Bump map file name(string; default = null string)
        public string ProjectionMethodOfBumpMapMapper { get; set; } //270 Projection method of bump map mapper(default = 1):
                                                                    //1 = Planar
                                                                    //2 = Box
                                                                    //3 = Cylinder
                                                                    //4 = Sphere
        public string TilingMethodOfBumpMapMapper { get; set; } // 271 Tiling method of bump map mapper(default = 1):
                                                                //1 = Tile
                                                                //2 = Crop
                                                                //3 = Clamp
        public string AutoTransformMethodOfBumpMapMapper { get; set; } // 272	Auto transform method of bump map mapper(bitset; default = 1):
                                                                       //1 = No auto transform
                                                                       //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                       //4 = Include current block transform in mapper transform
        public string TransformMatrixOfBumpMapMapper { get; set; } //144 Transform matrix of bump map mapper(16 reals; row major format; default = identity matrix)
        public string RefractionIndex { get; set; } // 145 Refraction index(real; default = 1.0)
        public string BlendFactorOfRefractionMap { get; set; } // 146 Blend factor of refraction map(real; default = 1.0)
        public string RefractionMapSource { get; set; }  //273	Refraction map source(default = 1) :
                                                         //0 = Use current scene
                                                         //1 = Use image file(specified by file name; null file name specifies no map)
        public string RefractionMapFileName { get; set; }  // 9 Refraction map file name(string; default = null string)
        public string ProjectionMethodOfRefractionMapMapper { get; set; } // 274	Projection method of refraction map mapper(default = 1):
                                                                          //1 = Planar
                                                                          //2 = Box
                                                                          //3 = Cylinder
                                                                          //4 = Sphere
        public string TilingMethodOfRefractionMapMapper { get; set; } // 275	Tiling method of refraction map mapper(default = 1):
                                                                      //1 = Tile
                                                                      //2 = Crop
                                                                      //3 = Clamp
        public string AutoTransformMethodOfRefractionMapMapper { get; set; } //276	Auto transform method of refraction map mapper(bitset; default = 1):
                                                                             //1 = No auto transform
                                                                             //2 = Scale mapper to current entity extents; translate mapper to entity origin
                                                                             //4 = Include current block transform in mapper transform
        public string TransformMatrixOfRefractionMapMapper { get; set; } // 147 Transform matrix of refraction map mapper(16 reals; row major format; default = identity matrix)
        public string ColorBleedScale { get; set; } // 460	Color Bleed Scale
        public string IndirectDumpScale { get; set; } // 461 Indirect Dump Scale
        public string ReflectanceScale { get; set; } // 462 Reflectance Scale
        public string TransmittanceScale { get; set; } // 463 Transmittance Scale
        public string TwoSidedMaterial { get; set; } // 290 Two-sided Material
        public string Luminance { get; set; } // 464 Luminance
        public string LuminanceMode { get; set; } // 270 Luminance Mode
        public string NormalMapMethod { get; set; } // 271 Normal Map Method
        public string NormalMapStrength { get; set; } // 465 Normal Map Strength
        public string NormalMapBlendFactor { get; set; } // 42  Normal Map Blend Factor
        public string NormalMapsource { get; set; } // 72	Normal Map Source
        public string NormalMapsourceFileName { get; set; } // 3 Normal Map Source File Name
        public string NormalMapperProjection { get; set; } // 73 Normal Mapper Projection
        public string NormalMapperTiling { get; set; } // 74 Normal Mapper Tiling
        public string NormalMapperAutoTransform { get; set; } // 75	Normal Mapper Auto Transform
        public string NormalMapperTransform { get; set; } // 43 Normal Mapper Transform
        public string MaterialsAnonymous { get; set; } // 293 Materials Anonymous
        public string GlobalIlluminationMode { get; set; } // 272 Global Illumination Mode
        public string FinalGatherMode { get; set; } // 273 Final Gather Mode
        public string GenProcName { get; set; } // 300	GenProcName
        public string GenProcValBool { get; set; } // 291	GenProcValBool
        public string GenProcValInt { get; set; } // 271	GenProcValInt
        public string GenProcValReal { get; set; } // 469	GenProcValReal
        public string GenProcValText { get; set; } // 301	GenProcValText
        public string GenProcTableEnd { get; set; } // 292	GenProcTableEnd
        public string GenProcValColorIndex { get; set; } // 62 	GenProcValColorIndex
        public string GenProcValColorRGB { get; set; } // 420	GenProcValColorRGB
        public string GenProcValColorName { get; set; } // 430	GenProcValColorName
        public string MapUTile { get; set; } // 270	Map UTile
        public string Translucence { get; set; } // 148 Translucence
        public string SelfIlluminaton { get; set; } // 90 	Self-Illuminaton
        public string Reflectivity { get; set; } // 468 Reflectivity
        public string IlluminationModel { get; set; } // 93 	Illumination Model
        public string ChannelFlags { get; set; } // 94 	Channel Flags
        public void ReadFromTypedValues(Material mat)
        {
            List<TypedValue> dxf = ImportsR17.acdbEntGetTypedValues(mat.ObjectId);
            foreach (var entry in dxf)
            {
                if (entry.TypeCode == 1) Name = entry.Value.ToString();
                else if (entry.TypeCode == 2) Description = entry.Value.ToString();
                else if (entry.TypeCode == 70) AmbientColorMethod = entry.Value.ToString();
                else if (entry.TypeCode == 40) AmbientColorFactor = entry.Value.ToString();
                else if (entry.TypeCode == 90)
                {
                    AmbientColorValue = entry.Value.ToString();

                    //attr3.ColorIndex = 0;
                    //attr3.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 0);
                    //var val = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, entry.Value as uint);

                }
                else if (entry.TypeCode == 71) DiffuseColorMethod = entry.Value.ToString();
                else if (entry.TypeCode == 41) DiffuseColorFactor = entry.Value.ToString();
                else if (entry.TypeCode == 91) DiffuseColorValue = entry.Value.ToString();
                else if (entry.TypeCode == 42) DiffuseMapBlendFactor = entry.Value.ToString();
                else if (entry.TypeCode == 72) DiffuseMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 3) DiffuseMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 73) ProjectionMethodOfDiffuseMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 74) TilingMethodOfDiffuseMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 75) AutoTransformMethodOfDiffuseMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 43) TransformMatrixOfDiffuseMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 44) SpecularGlossFactor = entry.Value.ToString();
                else if (entry.TypeCode == 76) SpecularColorMethod = entry.Value.ToString();
                else if (entry.TypeCode == 45) SpecularColorFactor = entry.Value.ToString();
                else if (entry.TypeCode == 92) SpecularColorValue = entry.Value.ToString();
                else if (entry.TypeCode == 46) SpecularMapBlendFactor = entry.Value.ToString();
                else if (entry.TypeCode == 77) SpecularMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 4) SpecularMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 78) ProjectionMethodOfSpecularMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 79) TilingMethodOfSpecularMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 170) AutoTransformMethodOfSpecularMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 47) TransformMatrixOfSpecularMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 48) BlendFactorOfReflectionMap = entry.Value.ToString();
                else if (entry.TypeCode == 171) ReflectionMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 6) ReflectionMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 172) ProjectionMethodOfReflectionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 173) TilingMethodOfReflectionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 174) AutoTransformMethodOfReflectionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 49) TransformMatrixOfReflectionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 140) OpacityPercent = entry.Value.ToString();
                else if (entry.TypeCode == 141) BlendFactorOfOpacityMap = entry.Value.ToString();
                else if (entry.TypeCode == 175) OpacityMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 7) OpacityMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 176) ProjectionMethodOfOpacityMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 177) TilingMethodOfOpacityMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 178) AutoTransformMethodOfOpacityMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 142) TransformMatrixOfOpacityMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 143) BlendFactorOfBumpMap = entry.Value.ToString();
                else if (entry.TypeCode == 179) BumpMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 8) BumpMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 270) ProjectionMethodOfBumpMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 271) TilingMethodOfBumpMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 272) AutoTransformMethodOfBumpMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 144) TransformMatrixOfBumpMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 145) RefractionIndex = entry.Value.ToString();
                else if (entry.TypeCode == 146) BlendFactorOfRefractionMap = entry.Value.ToString();
                else if (entry.TypeCode == 273) RefractionMapSource = entry.Value.ToString();
                else if (entry.TypeCode == 9) RefractionMapFileName = entry.Value.ToString();
                else if (entry.TypeCode == 274) ProjectionMethodOfRefractionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 275) TilingMethodOfRefractionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 276) AutoTransformMethodOfRefractionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 147) TransformMatrixOfRefractionMapMapper = entry.Value.ToString();
                else if (entry.TypeCode == 460) ColorBleedScale = entry.Value.ToString();
                else if (entry.TypeCode == 461) IndirectDumpScale = entry.Value.ToString();
                else if (entry.TypeCode == 462) ReflectanceScale = entry.Value.ToString();
                else if (entry.TypeCode == 463) TransmittanceScale = entry.Value.ToString();
                else if (entry.TypeCode == 290) TwoSidedMaterial = entry.Value.ToString();
                else if (entry.TypeCode == 464) Luminance = entry.Value.ToString();
                else if (entry.TypeCode == 270) LuminanceMode = entry.Value.ToString();
                else if (entry.TypeCode == 271) NormalMapMethod = entry.Value.ToString();
                else if (entry.TypeCode == 465) NormalMapStrength = entry.Value.ToString();
                else if (entry.TypeCode == 42) NormalMapBlendFactor = entry.Value.ToString();
                else if (entry.TypeCode == 72) NormalMapsource = entry.Value.ToString();
                else if (entry.TypeCode == 3) NormalMapsourceFileName = entry.Value.ToString();
                else if (entry.TypeCode == 73) NormalMapperProjection = entry.Value.ToString();
                else if (entry.TypeCode == 74) NormalMapperTiling = entry.Value.ToString();
                else if (entry.TypeCode == 75) NormalMapperAutoTransform = entry.Value.ToString();
                else if (entry.TypeCode == 43) NormalMapperTransform = entry.Value.ToString();
                else if (entry.TypeCode == 293) MaterialsAnonymous = entry.Value.ToString();
                else if (entry.TypeCode == 272) GlobalIlluminationMode = entry.Value.ToString();
                else if (entry.TypeCode == 273) FinalGatherMode = entry.Value.ToString();
                else if (entry.TypeCode == 300) GenProcName = entry.Value.ToString();
                else if (entry.TypeCode == 291) GenProcValBool = entry.Value.ToString();
                else if (entry.TypeCode == 271) GenProcValInt = entry.Value.ToString();
                else if (entry.TypeCode == 469) GenProcValReal = entry.Value.ToString();
                else if (entry.TypeCode == 301) GenProcValText = entry.Value.ToString();
                else if (entry.TypeCode == 292) GenProcTableEnd = entry.Value.ToString();
                else if (entry.TypeCode == 62) GenProcValColorIndex = entry.Value.ToString();
                else if (entry.TypeCode == 420) GenProcValColorRGB = entry.Value.ToString();
                else if (entry.TypeCode == 430) GenProcValColorName = entry.Value.ToString();
                else if (entry.TypeCode == 270) MapUTile = entry.Value.ToString();
                else if (entry.TypeCode == 148) Translucence = entry.Value.ToString();
                else if (entry.TypeCode == 90) SelfIlluminaton = entry.Value.ToString();
                else if (entry.TypeCode == 468) Reflectivity = entry.Value.ToString();
                else if (entry.TypeCode == 93) IlluminationModel = entry.Value.ToString();
                else if (entry.TypeCode == 94) ChannelFlags = entry.Value.ToString();
            } // foreach
        }
    } // class AutoCadMaterialRawProperties
}

public class AutoCadMaterialExportProperties
{
    public AutoCadMaterialExportProperties()
    {
//        Name = Description = Mode = Ambient = ColorRed = Col
    }

    public string Name { get; set; }
    public string Description { get; set; }
    public string Mode { get; set; } // Realistic = 0, Advanced = 1
    public string Ambient { get; set; }
    public string ColorRed { get; set; }
    public string ColorGreen { get; set; }
    public string ColorBlue { get; set; }
    public string Reflectivity { get; set; }
    public string BlendFactor { get; set; } // Image Fade
    public string OpacityPercentage { get; set; }
    public string Gloss { get; set; } // Glossiness
    public string IlluminationModel { get; set; }
    public string Source { get; set; } // Scene = 0, File = 1, Procedural = 2
    public string SourceFileFullPath { get; set; }
    public string SourceFileRelativePath { get; set; }
    public string SourceFileName { get; set; }
    public string ChannelFlags { get; set; }
    public string Projection { get; set; } // InheritProjection = 0, Planar = 1, Box = 2, Cylinder = 3, Sphere = 4
    public string AutoTransform { get; set; } // InheritAutoTransform = 0, None = 1, TransformObject = 2, Model = 4
    public string UTiling { get; set; }    // InheritTiling = 0, Tile = 1, Crop = 2, Clamp = 3, Mirror = 4
    public string VTiling { get; set; }

    public string Rotation { get; set; }
    public string UScale { get; set; } // Texture Editor'deki Sample size'dan 1/Width
    public string VScale { get; set; } // Texture Editor'deki Sample size'dan 1/Height
    public string UOffset { get; set; }
    public string VOffset { get; set; }

    public string mat00 { get; set; }
    public string mat01 { get; set; }
    public string mat02 { get; set; }
    public string mat03 { get; set; }

    public string mat10 { get; set; }
    public string mat11 { get; set; }
    public string mat12 { get; set; }
    public string mat13 { get; set; }

    public string mat20 { get; set; }
    public string mat21 { get; set; }
    public string mat22 { get; set; }
    public string mat23 { get; set; }

    public string mat30 { get; set; }
    public string mat31 { get; set; }
    public string mat32 { get; set; }
    public string mat33 { get; set; }

    protected string[] ProjectionStr = { "InheritProjection", "Planar", "Box", "Cylinder", "Sphere" }; 
    protected string[] AutoTransformStr = { "InheritAutoTransform", "None", "TransformObject", "Model" };
    protected string[] TilingStr = { "InheritTiling", "Tile", "Crop", "Clamp", "Mirror" };
    protected string[] SourceStr = { "Scene", "File", "Procedural" };
    protected string[] ChannelFlagStr = { "None", "UseDiffuse", "UseSpecular", "UseReflection", "UseOpacity", "UseBump", "UseRefraction", "UseAll" };
    public void ReadFromMaterial(Material mat)
    {
        this.Name = mat.Name;
        this.Description = mat.Description;
        this.Mode = mat.Mode == Autodesk.AutoCAD.GraphicsInterface.Mode.Realistic ? "Realistic" : "Advanced";
        this.SourceFileName = "-";
        MaterialColor matcolor = mat.Ambient;

        this.Ambient = matcolor.Factor.ToString("0.0000");

        MaterialOpacityComponent moc = mat.Opacity;
        this.OpacityPercentage = moc.Percentage.ToString("0.0000");

        var cf = (int)mat.ChannelFlags;
        int cfi = 
            (cf == 63 ? 7 : 
             cf == 32 ? 6 : 
             cf == 16 ? 5 : 
             cf ==  8 ? 4 : 
             cf ==  4 ? 3 : 
             cf ==  2 ? 2 : 
             cf ==  1 ? 1 : 0); 
        this.ChannelFlags = ChannelFlagStr[(int)cfi];

        MaterialDiffuseComponent mdc = mat.Diffuse;
        ColorRed = mdc.Color.Color.Red.ToString();
        ColorGreen = mdc.Color.Color.Green.ToString();
        ColorBlue = mdc.Color.Color.Blue.ToString();
        Reflectivity = mat.Reflectivity.ToString("0.0000");
        IlluminationModel illuminationModel = mat.IlluminationModel; // BlinnShader = 0, MetalShader = 1
        this.IlluminationModel = illuminationModel == Autodesk.AutoCAD.GraphicsInterface.IlluminationModel.BlinnShader ? "BlinnShader" : "MetalShader";

        //System.Windows.MessageBox.Show(Name);

        //MaterialRefractionComponent mrc = mat.Refraction; // Işığın kırılması

        //MaterialNormalMapComponent mmc = new MaterialNormalMapComponent(); // burada sıçıyor (fatal error)..
        //mmc = mat.NormalMap;
        //if (mmc != null)
        //{
        //    MaterialMap normalmap = mmc.Map;
        //    System.Windows.MessageBox.Show("Name:" + Name + "\nMaterialNormalMapComponent.Strength:" + mmc.Strength.ToString() +
        //    "\nMaterialNormalMapComponent.Map.Mapper.Transform:\n" + normalmap.Mapper.Transform.ToString());
        //}
        MaterialSpecularComponent msc = mat.Specular; // yansıtıcı
        if(msc != null)
        {
            this.Gloss = msc.Gloss.ToString("0.0000");
            //System.Windows.MessageBox.Show("msc.color:" + msc.Color.ToString() + " msc.gloss:" + msc.Gloss.ToString("0.0000"));
            //MaterialMap mm = msc.Map;
            //System.Windows.MessageBox.Show("msc.map:" + mm.Mapper.Transform.ToString());
        }

        MaterialMap map = mdc.Map;
        if( map == null && this.ChannelFlags == "UseBump")
        {
            map = mat.Bump;
        } 

        if (map != null)
        {
            Source source = map.Source;
            this.Source = SourceStr[(int)source];

            this.BlendFactor = map.BlendFactor.ToString("0.0000");

            Mapper mapper = map.Mapper;
            if (mapper != null)
            {
                try
                {
                    Projection projection = mapper.Projection;
                    this.Projection = ProjectionStr[(int)mapper.Projection];
                    //System.Windows.MessageBox.Show(Projection);
                }
                catch (System.Exception ex)
                {
                    System.Windows.MessageBox.Show("hata oluştu..\r\nException: " + ex.Message);
                }
                if (source == Autodesk.AutoCAD.GraphicsInterface.Source.File)
                {
                    ImageFileTexture tex = map.Texture as ImageFileTexture;
                    //System.Windows.MessageBox.Show("tex.SourceFileName: " + tex.SourceFileName);

                    this.SourceFileFullPath = tex.SourceFileName;
                    this.SourceFileName = Path.GetFileName(tex.SourceFileName);
                    //this.SourceFileRelativePath = // Bu alan fonksiyonun çağrıldığüı yerde konuyor.


                    //  MaterialTexture texture = map.Texture;
                }
                Tiling utiling = mapper.UTiling;
                Tiling vtiling = mapper.VTiling;
                AutoTransform autoTransform = mapper.AutoTransform;

                this.AutoTransform = AutoTransformStr[(int)autoTransform];

                this.UTiling = TilingStr[(int)utiling];
                this.VTiling = TilingStr[(int)vtiling];

                Matrix3d m3d = mapper.Transform;

                mat00 = m3d[0, 0].ToString(); mat01 = m3d[0, 1].ToString(); mat02 = m3d[0, 2].ToString(); mat03 = m3d[0, 3].ToString();
                mat10 = m3d[1, 0].ToString(); mat11 = m3d[1, 1].ToString(); mat12 = m3d[1, 2].ToString(); mat13 = m3d[1, 3].ToString();
                mat20 = m3d[2, 0].ToString(); mat21 = m3d[2, 1].ToString(); mat22 = m3d[2, 2].ToString(); mat23 = m3d[2, 3].ToString();
                mat30 = m3d[3, 0].ToString(); mat31 = m3d[3, 1].ToString(); mat32 = m3d[3, 2].ToString(); mat33 = m3d[3, 3].ToString();

                var tp = Parsel3D.Utils.ReadFromTransformationMatrix(m3d);

                this.Rotation = tp.Rotation.ToString("0.0000");
                this.UScale   = tp.UScale.ToString("0.0000");
                this.VScale   = tp.VScale.ToString("0.0000");
                this.UOffset  = tp.UOffset.ToString("0.0000");
                this.VOffset  = tp.VOffset.ToString("0.0000");

                if (this.Rotation == "NaN") this.Rotation = "0.0000";
                if (this.UScale == "Infinity") this.UScale = "0.0000";
                if (this.VScale == "Infinity") this.VScale = "0.0000";
                if (this.UOffset == "Infinity") this.UOffset = "0.0000";
                if (this.VOffset == "Infinity") this.VOffset = "0.0000";
            }
        }
    }
}
