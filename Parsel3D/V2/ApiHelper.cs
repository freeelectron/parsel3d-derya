﻿using Newtonsoft.Json;
using RealEstateExpo.WebApi.Models.Dtos.Crud;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Parsel3D.V2
{
    public static class ApiHelper
    {
        //http://10.0.5.19:1984/api/tr-TR/Counties?isIncludeDistricts=true&isIncludeGeom=true
        public static string rootUrl = "http://10.0.5.19:1984/api";
        public static string language = "tr-TR";
        public static LanguageDto languageDto = new LanguageDto() { Name = "tr-TR" };

        public static T Get<T>(string actionName, params Parameter[] parameters) where T : new()
        {
            string apiUrl = $"{rootUrl}/{language}";
            RestClient client = new RestClient(apiUrl);
            RestRequest request = new RestRequest(actionName);
            //request.Parameters.AddRange(parameters);
            foreach (var item in parameters)
            {
                request.AddParameter(item);
            }

            var response = client.Get<T>(request);

            bool isValid = ValidateResponse(response);

            T result = response.Data;

            return result;
        }



        //public static List<QualityOfLifeDto> GetQualityOfLifeList()//string actionName, params Parameter[] parameters)
        //{
        //    //System.Windows.MessageBox.Show("aha");

        //    RestClient client = new RestClient(apiUrl + languageDto.Name);

        //    RestRequest request = new RestRequest("qualityoflifeList");

        //    Parameter param = new Parameter("qualityIds", 2, ParameterType.UrlSegment);

        //    var restResponse = client.Get(request);

        //    string str = restResponse.Content;
        //    if (!restResponse.IsSuccessful)
        //        System.Windows.MessageBox.Show("Yaşam kalitesi listesi server'dan çekilemedi\n ErrorMessage:\n"
        //            + restResponse.StatusCode.ToString()
        //            + ":\n" + restResponse.StatusDescription);
        //    List<QualityOfLifeDto> qol = JsonConvert.DeserializeObject<List<QualityOfLifeDto>>(str);
        //    return qol;
        //}
        //public static List<CountyDto> GetCountyList()
        //{
        //    RestClient client = new RestClient(apiUrl + languageDto.Name);

        //    RestRequest request = new RestRequest("Counties");
        //    Parameter param = new Parameter("isIncludeDistricts", true, ParameterType.UrlSegment);
        //    request.AddParameter(param);
        //    param = new Parameter("isIncludeGeom", true, ParameterType.UrlSegment);
        //    request.AddParameter(param);

        //    var restResponse = client.Get(request);

        //    if (!restResponse.IsSuccessful)
        //        System.Windows.MessageBox.Show("İlçe listesi server'dan çekilemedi\n ErrorMessage:\n"
        //            + restResponse.StatusCode.ToString()
        //            + ":\n" + restResponse.StatusDescription);

        //    string str = restResponse.Content;
        //    List<CountyDto> countyList = JsonConvert.DeserializeObject<List<CountyDto>>(str);
        //    return countyList;
        //}

        private static bool ValidateResponse(IRestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.Created:
                case HttpStatusCode.NoContent:
                case HttpStatusCode.Accepted:
                case HttpStatusCode.OK:
                    return true;

                case HttpStatusCode.Continue:
                case HttpStatusCode.SwitchingProtocols:
                case HttpStatusCode.NonAuthoritativeInformation:
                case HttpStatusCode.ResetContent:
                case HttpStatusCode.PartialContent:
                case HttpStatusCode.MultipleChoices:
                case HttpStatusCode.MovedPermanently:
                case HttpStatusCode.Found:
                case HttpStatusCode.SeeOther:
                case HttpStatusCode.NotModified:
                case HttpStatusCode.UseProxy:
                case HttpStatusCode.Unused:
                case HttpStatusCode.TemporaryRedirect:
                case HttpStatusCode.BadRequest:
                case HttpStatusCode.Unauthorized:
                case HttpStatusCode.PaymentRequired:
                case HttpStatusCode.Forbidden:
                case HttpStatusCode.NotFound:
                case HttpStatusCode.MethodNotAllowed:
                case HttpStatusCode.NotAcceptable:
                case HttpStatusCode.ProxyAuthenticationRequired:
                case HttpStatusCode.RequestTimeout:
                case HttpStatusCode.Conflict:
                case HttpStatusCode.Gone:
                case HttpStatusCode.LengthRequired:
                case HttpStatusCode.PreconditionFailed:
                case HttpStatusCode.RequestEntityTooLarge:
                case HttpStatusCode.RequestUriTooLong:
                case HttpStatusCode.UnsupportedMediaType:
                case HttpStatusCode.RequestedRangeNotSatisfiable:
                case HttpStatusCode.ExpectationFailed:
                case HttpStatusCode.UpgradeRequired:
                case HttpStatusCode.InternalServerError:
                case HttpStatusCode.NotImplemented:
                case HttpStatusCode.BadGateway:
                case HttpStatusCode.ServiceUnavailable:
                case HttpStatusCode.GatewayTimeout:
                case HttpStatusCode.HttpVersionNotSupported:
                    {
                        string message = $"bilinmeyen bir hata oluştu {Environment.NewLine}{response.ResponseUri.AbsoluteUri}";
                        MessageBox.Show(message);
                        return false;
                    }
                default:
                    return false;
            }
        }
    }

}
