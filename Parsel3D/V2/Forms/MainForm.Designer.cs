﻿namespace Parsel3D.V2.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewProject = new System.Windows.Forms.Button();
            this.btnChangeProject = new System.Windows.Forms.Button();
            this.btnUpdateProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnNewProject
            // 
            this.btnNewProject.Location = new System.Drawing.Point(64, 42);
            this.btnNewProject.Name = "btnNewProject";
            this.btnNewProject.Size = new System.Drawing.Size(180, 23);
            this.btnNewProject.TabIndex = 3;
            this.btnNewProject.Text = "Yeni Proje";
            this.btnNewProject.UseVisualStyleBackColor = true;
            this.btnNewProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // btnChangeProject
            // 
            this.btnChangeProject.Location = new System.Drawing.Point(64, 71);
            this.btnChangeProject.Name = "btnChangeProject";
            this.btnChangeProject.Size = new System.Drawing.Size(180, 23);
            this.btnChangeProject.TabIndex = 3;
            this.btnChangeProject.Text = "Proje Düzeltme";
            this.btnChangeProject.UseVisualStyleBackColor = true;
            this.btnChangeProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // btnUpdateProject
            // 
            this.btnUpdateProject.Location = new System.Drawing.Point(64, 100);
            this.btnUpdateProject.Name = "btnUpdateProject";
            this.btnUpdateProject.Size = new System.Drawing.Size(180, 23);
            this.btnUpdateProject.TabIndex = 3;
            this.btnUpdateProject.Text = "Proje Güncelleme";
            this.btnUpdateProject.UseVisualStyleBackColor = true;
            this.btnUpdateProject.Click += new System.EventHandler(this.btnNewProject_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 166);
            this.Controls.Add(this.btnUpdateProject);
            this.Controls.Add(this.btnChangeProject);
            this.Controls.Add(this.btnNewProject);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "MainForm";
            this.Text = "Ana Ekran";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnNewProject;
        private System.Windows.Forms.Button btnChangeProject;
        private System.Windows.Forms.Button btnUpdateProject;
    }
}