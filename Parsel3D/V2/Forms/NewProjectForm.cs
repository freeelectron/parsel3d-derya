﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parsel3D.V2.Forms
{
    public partial class NewProjectForm : Form
    {
        public NewProjectForm()
        {
            InitializeComponent();
        }

        private void labelProjectInfo_Click(object sender, EventArgs e)
        {
            ProjectInfoForm pif = new ProjectInfoForm();
            pif.ShowDialog();
        }

        private void labelBuildingName_Click(object sender, EventArgs e)
        {
            BuildingInfoForm bif = new BuildingInfoForm();
                bif.ShowDialog();
        }
    }
}
