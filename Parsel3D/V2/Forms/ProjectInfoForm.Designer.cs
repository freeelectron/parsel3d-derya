﻿namespace Parsel3D.V2.Forms
{
    partial class ProjectInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelQualityOfLifeId = new System.Windows.Forms.Label();
            this.cmbQualityOfLives = new System.Windows.Forms.ComboBox();
            this.labelCompanyId = new System.Windows.Forms.Label();
            this.cmbCompanyId = new System.Windows.Forms.ComboBox();
            this.labelName = new System.Windows.Forms.Label();
            this.cmbName = new System.Windows.Forms.ComboBox();
            this.labelProjectArea = new System.Windows.Forms.Label();
            this.cmbProjectArea = new System.Windows.Forms.ComboBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.cmbProjectDescription = new System.Windows.Forms.ComboBox();
            this.labelImagePreview = new System.Windows.Forms.Label();
            this.cmbImagePreview = new System.Windows.Forms.ComboBox();
            this.labelConstructionDate = new System.Windows.Forms.Label();
            this.labelDeliveryDate = new System.Windows.Forms.Label();
            this.labelArchitectName = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.textBoxArchitectName = new System.Windows.Forms.TextBox();
            this.textBoxHouseCount = new System.Windows.Forms.TextBox();
            this.labelHouseCount = new System.Windows.Forms.Label();
            this.textBoxOfficeCount = new System.Windows.Forms.TextBox();
            this.labelOfficeCount = new System.Windows.Forms.Label();
            this.textStoreCount = new System.Windows.Forms.TextBox();
            this.labelStoreCount = new System.Windows.Forms.Label();
            this.textBoxGeom = new System.Windows.Forms.TextBox();
            this.labelGeom = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelQualityOfLifeId
            // 
            this.labelQualityOfLifeId.AutoSize = true;
            this.labelQualityOfLifeId.Location = new System.Drawing.Point(2, 72);
            this.labelQualityOfLifeId.Name = "labelQualityOfLifeId";
            this.labelQualityOfLifeId.Size = new System.Drawing.Size(81, 13);
            this.labelQualityOfLifeId.TabIndex = 13;
            this.labelQualityOfLifeId.Text = "Yaşam Kalitesi :";
            // 
            // cmbQualityOfLives
            // 
            this.cmbQualityOfLives.FormattingEnabled = true;
            this.cmbQualityOfLives.Location = new System.Drawing.Point(89, 69);
            this.cmbQualityOfLives.Name = "cmbQualityOfLives";
            this.cmbQualityOfLives.Size = new System.Drawing.Size(224, 21);
            this.cmbQualityOfLives.TabIndex = 12;
            // 
            // labelCompanyId
            // 
            this.labelCompanyId.AutoSize = true;
            this.labelCompanyId.Location = new System.Drawing.Point(45, 18);
            this.labelCompanyId.Name = "labelCompanyId";
            this.labelCompanyId.Size = new System.Drawing.Size(38, 13);
            this.labelCompanyId.TabIndex = 17;
            this.labelCompanyId.Text = "Firma :";
            // 
            // cmbCompanyId
            // 
            this.cmbCompanyId.FormattingEnabled = true;
            this.cmbCompanyId.Location = new System.Drawing.Point(89, 15);
            this.cmbCompanyId.Name = "cmbCompanyId";
            this.cmbCompanyId.Size = new System.Drawing.Size(224, 21);
            this.cmbCompanyId.TabIndex = 16;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(28, 45);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(55, 13);
            this.labelName.TabIndex = 19;
            this.labelName.Text = "Proje Adı :";
            // 
            // cmbName
            // 
            this.cmbName.FormattingEnabled = true;
            this.cmbName.Location = new System.Drawing.Point(89, 42);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(224, 21);
            this.cmbName.TabIndex = 18;
            // 
            // labelProjectArea
            // 
            this.labelProjectArea.AutoSize = true;
            this.labelProjectArea.Location = new System.Drawing.Point(20, 99);
            this.labelProjectArea.Name = "labelProjectArea";
            this.labelProjectArea.Size = new System.Drawing.Size(63, 13);
            this.labelProjectArea.TabIndex = 21;
            this.labelProjectArea.Text = "Proje Alanı :";
            // 
            // cmbProjectArea
            // 
            this.cmbProjectArea.FormattingEnabled = true;
            this.cmbProjectArea.Location = new System.Drawing.Point(89, 96);
            this.cmbProjectArea.Name = "cmbProjectArea";
            this.cmbProjectArea.Size = new System.Drawing.Size(224, 21);
            this.cmbProjectArea.TabIndex = 20;
            // 
            // labelDescription
            // 
            this.labelDescription.AutoSize = true;
            this.labelDescription.Location = new System.Drawing.Point(12, 131);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(71, 13);
            this.labelDescription.TabIndex = 23;
            this.labelDescription.Text = "Proje Tanımı :";
            // 
            // cmbProjectDescription
            // 
            this.cmbProjectDescription.FormattingEnabled = true;
            this.cmbProjectDescription.Location = new System.Drawing.Point(89, 123);
            this.cmbProjectDescription.Name = "cmbProjectDescription";
            this.cmbProjectDescription.Size = new System.Drawing.Size(224, 21);
            this.cmbProjectDescription.TabIndex = 22;
            // 
            // labelImagePreview
            // 
            this.labelImagePreview.AutoSize = true;
            this.labelImagePreview.Location = new System.Drawing.Point(0, 154);
            this.labelImagePreview.Name = "labelImagePreview";
            this.labelImagePreview.Size = new System.Drawing.Size(83, 13);
            this.labelImagePreview.TabIndex = 25;
            this.labelImagePreview.Text = "Proje Önizleme :";
            // 
            // cmbImagePreview
            // 
            this.cmbImagePreview.FormattingEnabled = true;
            this.cmbImagePreview.Location = new System.Drawing.Point(89, 151);
            this.cmbImagePreview.Name = "cmbImagePreview";
            this.cmbImagePreview.Size = new System.Drawing.Size(224, 21);
            this.cmbImagePreview.TabIndex = 24;
            // 
            // labelConstructionDate
            // 
            this.labelConstructionDate.AutoSize = true;
            this.labelConstructionDate.Location = new System.Drawing.Point(372, 22);
            this.labelConstructionDate.Name = "labelConstructionDate";
            this.labelConstructionDate.Size = new System.Drawing.Size(71, 13);
            this.labelConstructionDate.TabIndex = 27;
            this.labelConstructionDate.Text = "Yapım Tarihi :";
            // 
            // labelDeliveryDate
            // 
            this.labelDeliveryDate.AutoSize = true;
            this.labelDeliveryDate.Location = new System.Drawing.Point(371, 48);
            this.labelDeliveryDate.Name = "labelDeliveryDate";
            this.labelDeliveryDate.Size = new System.Drawing.Size(72, 13);
            this.labelDeliveryDate.TabIndex = 29;
            this.labelDeliveryDate.Text = "Teslim Tarihi :";
            // 
            // labelArchitectName
            // 
            this.labelArchitectName.AutoSize = true;
            this.labelArchitectName.Location = new System.Drawing.Point(402, 74);
            this.labelArchitectName.Name = "labelArchitectName";
            this.labelArchitectName.Size = new System.Drawing.Size(41, 13);
            this.labelArchitectName.TabIndex = 31;
            this.labelArchitectName.Text = "Mimar :";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(449, 16);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 32;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(449, 42);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 33;
            // 
            // textBoxArchitectName
            // 
            this.textBoxArchitectName.Location = new System.Drawing.Point(449, 71);
            this.textBoxArchitectName.Name = "textBoxArchitectName";
            this.textBoxArchitectName.Size = new System.Drawing.Size(223, 20);
            this.textBoxArchitectName.TabIndex = 34;
            // 
            // textBoxHouseCount
            // 
            this.textBoxHouseCount.Location = new System.Drawing.Point(449, 97);
            this.textBoxHouseCount.Name = "textBoxHouseCount";
            this.textBoxHouseCount.Size = new System.Drawing.Size(51, 20);
            this.textBoxHouseCount.TabIndex = 36;
            // 
            // labelHouseCount
            // 
            this.labelHouseCount.AutoSize = true;
            this.labelHouseCount.Location = new System.Drawing.Point(372, 100);
            this.labelHouseCount.Name = "labelHouseCount";
            this.labelHouseCount.Size = new System.Drawing.Size(71, 13);
            this.labelHouseCount.TabIndex = 35;
            this.labelHouseCount.Text = "Konut Sayısı :";
            // 
            // textBoxOfficeCount
            // 
            this.textBoxOfficeCount.Location = new System.Drawing.Point(449, 123);
            this.textBoxOfficeCount.Name = "textBoxOfficeCount";
            this.textBoxOfficeCount.Size = new System.Drawing.Size(51, 20);
            this.textBoxOfficeCount.TabIndex = 38;
            // 
            // labelOfficeCount
            // 
            this.labelOfficeCount.AutoSize = true;
            this.labelOfficeCount.Location = new System.Drawing.Point(376, 126);
            this.labelOfficeCount.Name = "labelOfficeCount";
            this.labelOfficeCount.Size = new System.Drawing.Size(67, 13);
            this.labelOfficeCount.TabIndex = 37;
            this.labelOfficeCount.Text = "İşyeri Sayısı :";
            // 
            // textStoreCount
            // 
            this.textStoreCount.Location = new System.Drawing.Point(449, 149);
            this.textStoreCount.Name = "textStoreCount";
            this.textStoreCount.Size = new System.Drawing.Size(51, 20);
            this.textStoreCount.TabIndex = 40;
            // 
            // labelStoreCount
            // 
            this.labelStoreCount.AutoSize = true;
            this.labelStoreCount.Location = new System.Drawing.Point(362, 152);
            this.labelStoreCount.Name = "labelStoreCount";
            this.labelStoreCount.Size = new System.Drawing.Size(81, 13);
            this.labelStoreCount.TabIndex = 39;
            this.labelStoreCount.Text = "Dükkan Sayısı :";
            // 
            // textBoxGeom
            // 
            this.textBoxGeom.Location = new System.Drawing.Point(449, 175);
            this.textBoxGeom.Name = "textBoxGeom";
            this.textBoxGeom.Size = new System.Drawing.Size(51, 20);
            this.textBoxGeom.TabIndex = 42;
            // 
            // labelGeom
            // 
            this.labelGeom.AutoSize = true;
            this.labelGeom.Location = new System.Drawing.Point(388, 178);
            this.labelGeom.Name = "labelGeom";
            this.labelGeom.Size = new System.Drawing.Size(55, 13);
            this.labelGeom.TabIndex = 41;
            this.labelGeom.Text = "Geometri :";
            // 
            // btnOk
            // 
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.Location = new System.Drawing.Point(608, 232);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 43;
            this.btnOk.Text = "Tamam";
            this.btnOk.UseCompatibleTextRendering = true;
            this.btnOk.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(507, 232);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 43;
            this.btnCancel.Text = "Vazgeç";
            this.btnCancel.UseCompatibleTextRendering = true;
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // ProjectInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(695, 267);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.textBoxGeom);
            this.Controls.Add(this.labelGeom);
            this.Controls.Add(this.textStoreCount);
            this.Controls.Add(this.labelStoreCount);
            this.Controls.Add(this.textBoxOfficeCount);
            this.Controls.Add(this.labelOfficeCount);
            this.Controls.Add(this.textBoxHouseCount);
            this.Controls.Add(this.labelHouseCount);
            this.Controls.Add(this.textBoxArchitectName);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.labelArchitectName);
            this.Controls.Add(this.labelDeliveryDate);
            this.Controls.Add(this.labelConstructionDate);
            this.Controls.Add(this.labelImagePreview);
            this.Controls.Add(this.cmbImagePreview);
            this.Controls.Add(this.labelDescription);
            this.Controls.Add(this.cmbProjectDescription);
            this.Controls.Add(this.labelProjectArea);
            this.Controls.Add(this.cmbProjectArea);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.cmbName);
            this.Controls.Add(this.labelCompanyId);
            this.Controls.Add(this.cmbCompanyId);
            this.Controls.Add(this.labelQualityOfLifeId);
            this.Controls.Add(this.cmbQualityOfLives);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ProjectInfoForm";
            this.Text = "Proje Bİlgileri";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelQualityOfLifeId;
        private System.Windows.Forms.ComboBox cmbQualityOfLives;
        private System.Windows.Forms.Label labelCompanyId;
        private System.Windows.Forms.ComboBox cmbCompanyId;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.ComboBox cmbName;
        private System.Windows.Forms.Label labelProjectArea;
        private System.Windows.Forms.ComboBox cmbProjectArea;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.ComboBox cmbProjectDescription;
        private System.Windows.Forms.Label labelImagePreview;
        private System.Windows.Forms.ComboBox cmbImagePreview;
        private System.Windows.Forms.Label labelConstructionDate;
        private System.Windows.Forms.Label labelDeliveryDate;
        private System.Windows.Forms.Label labelArchitectName;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.TextBox textBoxArchitectName;
        private System.Windows.Forms.TextBox textBoxHouseCount;
        private System.Windows.Forms.Label labelHouseCount;
        private System.Windows.Forms.TextBox textBoxOfficeCount;
        private System.Windows.Forms.Label labelOfficeCount;
        private System.Windows.Forms.TextBox textStoreCount;
        private System.Windows.Forms.Label labelStoreCount;
        private System.Windows.Forms.TextBox textBoxGeom;
        private System.Windows.Forms.Label labelGeom;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}