﻿namespace Parsel3D.V2.Forms
{
    partial class BuildingInfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDaskKod = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxDaskNo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxBinaKirmiziKotu = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxBinaBarkodNo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBlokAdi = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxBinaNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBoxDaskKod
            // 
            this.textBoxDaskKod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxDaskKod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxDaskKod.Location = new System.Drawing.Point(128, 72);
            this.textBoxDaskKod.Name = "textBoxDaskKod";
            this.textBoxDaskKod.Size = new System.Drawing.Size(203, 20);
            this.textBoxDaskKod.TabIndex = 90;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(36, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 13);
            this.label13.TabIndex = 91;
            this.label13.Text = "DASK Bina No :";
            // 
            // textBoxDaskNo
            // 
            this.textBoxDaskNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxDaskNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxDaskNo.Location = new System.Drawing.Point(128, 98);
            this.textBoxDaskNo.Name = "textBoxDaskNo";
            this.textBoxDaskNo.Size = new System.Drawing.Size(203, 20);
            this.textBoxDaskNo.TabIndex = 88;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(25, 75);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 13);
            this.label11.TabIndex = 89;
            this.label11.Text = "DASK Bina Kodu :";
            // 
            // textBoxBinaKirmiziKotu
            // 
            this.textBoxBinaKirmiziKotu.Location = new System.Drawing.Point(128, 150);
            this.textBoxBinaKirmiziKotu.Name = "textBoxBinaKirmiziKotu";
            this.textBoxBinaKirmiziKotu.Size = new System.Drawing.Size(203, 20);
            this.textBoxBinaKirmiziKotu.TabIndex = 87;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(16, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 26);
            this.label7.TabIndex = 86;
            this.label7.Text = "  Bina Sıfır Kotu (m) :\r\n(Mimari ±0.00 Kotu)\r\n";
            // 
            // textBoxBinaBarkodNo
            // 
            this.textBoxBinaBarkodNo.Location = new System.Drawing.Point(128, 124);
            this.textBoxBinaBarkodNo.Name = "textBoxBinaBarkodNo";
            this.textBoxBinaBarkodNo.Size = new System.Drawing.Size(203, 20);
            this.textBoxBinaBarkodNo.TabIndex = 85;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(31, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 84;
            this.label6.Text = "Bina Barkod No :";
            // 
            // textBlokAdi
            // 
            this.textBlokAdi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBlokAdi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBlokAdi.Location = new System.Drawing.Point(128, 20);
            this.textBlokAdi.Name = "textBlokAdi";
            this.textBlokAdi.Size = new System.Drawing.Size(203, 20);
            this.textBlokAdi.TabIndex = 83;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(67, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 82;
            this.label9.Text = "Blok Adı :";
            // 
            // textBoxBinaNo
            // 
            this.textBoxBinaNo.Location = new System.Drawing.Point(128, 46);
            this.textBoxBinaNo.Name = "textBoxBinaNo";
            this.textBoxBinaNo.Size = new System.Drawing.Size(203, 20);
            this.textBoxBinaNo.TabIndex = 81;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(68, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 80;
            this.label4.Text = "Bina No :";
            // 
            // Cancel
            // 
            this.Cancel.AccessibleName = "Cancel";
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(141, 212);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 93;
            this.Cancel.Text = "&Vazgeç";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // OK
            // 
            this.OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OK.Location = new System.Drawing.Point(269, 212);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 92;
            this.OK.Text = "&Tamam";
            this.OK.UseVisualStyleBackColor = true;
            // 
            // BuildingInfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 247);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.textBoxDaskKod);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxDaskNo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxBinaKirmiziKotu);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxBinaBarkodNo);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBlokAdi);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxBinaNo);
            this.Controls.Add(this.label4);
            this.Name = "BuildingInfoForm";
            this.Text = "BuildingInfoForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDaskKod;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxDaskNo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxBinaKirmiziKotu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxBinaBarkodNo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBlokAdi;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxBinaNo;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button Cancel;
        public System.Windows.Forms.Button OK;
    }
}