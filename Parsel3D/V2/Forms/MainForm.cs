﻿using RealEstateExpo.WebApi.Models.Dtos.Crud;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace Parsel3D.V2.Forms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();            
        }        

        private void btnNewProject_Click(object sender, EventArgs e)
        {
            NewProjectForm newPprojectForm = new NewProjectForm();
            newPprojectForm.ShowDialog();
        }
    }
}
