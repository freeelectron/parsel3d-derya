﻿using System;

namespace Parsel3D.V2.Forms
{
    partial class NewProjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewProjectForm));
            this.labelAdministrativeDistrict = new System.Windows.Forms.Label();
            this.cmbAdministrativeDistricts = new System.Windows.Forms.ComboBox();
            this.labelRegisteredDistrict = new System.Windows.Forms.Label();
            this.labelCounty = new System.Windows.Forms.Label();
            this.cmbRegisteredDistricts = new System.Windows.Forms.ComboBox();
            this.cmbCounties = new System.Windows.Forms.ComboBox();
            this.labelProjectInfo = new System.Windows.Forms.Label();
            this.textBoxNotlar = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxParselNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxAdaNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelBuildingName = new System.Windows.Forms.Label();
            this.labelProjectName = new System.Windows.Forms.Label();
            this.labelBuildingInfo = new System.Windows.Forms.Label();
            this.Cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelAdministrativeDistrict
            // 
            resources.ApplyResources(this.labelAdministrativeDistrict, "labelAdministrativeDistrict");
            this.labelAdministrativeDistrict.Name = "labelAdministrativeDistrict";
            // 
            // cmbAdministrativeDistricts
            // 
            this.cmbAdministrativeDistricts.FormattingEnabled = true;
            resources.ApplyResources(this.cmbAdministrativeDistricts, "cmbAdministrativeDistricts");
            this.cmbAdministrativeDistricts.Name = "cmbAdministrativeDistricts";
            // 
            // labelRegisteredDistrict
            // 
            resources.ApplyResources(this.labelRegisteredDistrict, "labelRegisteredDistrict");
            this.labelRegisteredDistrict.Name = "labelRegisteredDistrict";
            // 
            // labelCounty
            // 
            resources.ApplyResources(this.labelCounty, "labelCounty");
            this.labelCounty.Name = "labelCounty";
            // 
            // cmbRegisteredDistricts
            // 
            this.cmbRegisteredDistricts.FormattingEnabled = true;
            resources.ApplyResources(this.cmbRegisteredDistricts, "cmbRegisteredDistricts");
            this.cmbRegisteredDistricts.Name = "cmbRegisteredDistricts";
            // 
            // cmbCounties
            // 
            this.cmbCounties.FormattingEnabled = true;
            resources.ApplyResources(this.cmbCounties, "cmbCounties");
            this.cmbCounties.Name = "cmbCounties";
            // 
            // labelProjectInfo
            // 
            resources.ApplyResources(this.labelProjectInfo, "labelProjectInfo");
            this.labelProjectInfo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelProjectInfo.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelProjectInfo.Name = "labelProjectInfo";
            this.labelProjectInfo.Click += new System.EventHandler(this.labelProjectInfo_Click);
            // 
            // textBoxNotlar
            // 
            resources.ApplyResources(this.textBoxNotlar, "textBoxNotlar");
            this.textBoxNotlar.Name = "textBoxNotlar";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBoxParselNo
            // 
            this.textBoxParselNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxParselNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            resources.ApplyResources(this.textBoxParselNo, "textBoxParselNo");
            this.textBoxParselNo.Name = "textBoxParselNo";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // textBoxAdaNo
            // 
            this.textBoxAdaNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxAdaNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            resources.ApplyResources(this.textBoxAdaNo, "textBoxAdaNo");
            this.textBoxAdaNo.Name = "textBoxAdaNo";
            this.textBoxAdaNo.Tag = "";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // labelBuildingName
            // 
            resources.ApplyResources(this.labelBuildingName, "labelBuildingName");
            this.labelBuildingName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelBuildingName.ForeColor = System.Drawing.SystemColors.Desktop;
            this.labelBuildingName.Name = "labelBuildingName";
            this.labelBuildingName.Click += new System.EventHandler(this.labelBuildingName_Click);
            // 
            // labelProjectName
            // 
            resources.ApplyResources(this.labelProjectName, "labelProjectName");
            this.labelProjectName.Name = "labelProjectName";
            // 
            // labelBuildingInfo
            // 
            resources.ApplyResources(this.labelBuildingInfo, "labelBuildingInfo");
            this.labelBuildingInfo.Name = "labelBuildingInfo";
            // 
            // Cancel
            // 
            resources.ApplyResources(this.Cancel, "Cancel");
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Name = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // OK
            // 
            this.OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OK, "OK");
            this.OK.Name = "OK";
            this.OK.UseVisualStyleBackColor = true;
            // 
            // NewProjectForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.textBoxNotlar);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxParselNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxAdaNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelAdministrativeDistrict);
            this.Controls.Add(this.cmbAdministrativeDistricts);
            this.Controls.Add(this.labelRegisteredDistrict);
            this.Controls.Add(this.labelBuildingName);
            this.Controls.Add(this.labelProjectInfo);
            this.Controls.Add(this.labelBuildingInfo);
            this.Controls.Add(this.labelProjectName);
            this.Controls.Add(this.labelCounty);
            this.Controls.Add(this.cmbRegisteredDistricts);
            this.Controls.Add(this.cmbCounties);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "NewProjectForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelAdministrativeDistrict;
        private System.Windows.Forms.ComboBox cmbAdministrativeDistricts;
        private System.Windows.Forms.Label labelRegisteredDistrict;
        private System.Windows.Forms.Label labelCounty;
        private System.Windows.Forms.ComboBox cmbRegisteredDistricts;
        private System.Windows.Forms.ComboBox cmbCounties;
        private System.Windows.Forms.Label labelProjectInfo;
        private System.Windows.Forms.TextBox textBoxNotlar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxParselNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxAdaNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelBuildingName;
        private System.Windows.Forms.Label labelProjectName;
        private System.Windows.Forms.Label labelBuildingInfo;
        public System.Windows.Forms.Button Cancel;
        public System.Windows.Forms.Button OK;

        private void LoadCounties()
        {
            //Parameter isIncludeDistrict = new Parameter("isIncludeDistricts", true, ParameterType.QueryString);
            //Parameter isIncludeGeom = new Parameter("isIncludeGeom", false, ParameterType.QueryString);

            //List<CountyDto> counties = ApiHelper.Get<List<CountyDto>>("counties", isIncludeDistrict, isIncludeGeom);

            //cmbCounties.DataSource = counties.OrderBy(p => p.Name).ToList();
        }

        private void cmbCountyId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //CountyDto county = cmbCounties.SelectedItem as CountyDto;
            //cmbRegisteredDistricts.DataSource = county.Districts.OrderBy(p => p.Name).ToList();
        }
    }
}