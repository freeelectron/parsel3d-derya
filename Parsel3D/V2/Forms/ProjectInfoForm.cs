﻿using RealEstateExpo.WebApi.Models.Dtos.Crud;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parsel3D.V2.Forms
{
    public partial class ProjectInfoForm : Form
    {
        public ProjectInfoForm()
        {
            InitializeComponent();
            InitializeDataSource();
        }

        private void InitializeDataSource()
        {
            //LoadCounties();
            //LoadQualityOfLives();
        }

        private void LoadQualityOfLives()
        {
            Parameter isIncludeGeom = new Parameter("isIncludeGeom", false, ParameterType.QueryString);

            List<QualityOfLifeDto> qualityOfLives = ApiHelper.Get<List<QualityOfLifeDto>>("QualityOfLives", isIncludeGeom);

            cmbQualityOfLives.DataSource = qualityOfLives.OrderBy(p => p.Name).ToList();
        }
    }
}
