﻿using System;

namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public int? CountyId { get; set; }
        public int? DistrictId { get; set; }
        public int? QualityOfLifeId { get; set; }
        public int? ProjectAreaId { get; set; }
        public int? CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImagePreview { get; set; }
        public DateTime? ConstructionDate { get; set; }
        public DateTime? DeliveryDate { get; set; }

        public string ArchitectName { get; set; }
        public int? HouseCount { get; set; }
        public int? OfficeCount { get; set; }
        public int? StoreCount { get; set; }
        public System.Collections.Generic.List<decimal> Geom { get; set; }
    }
}