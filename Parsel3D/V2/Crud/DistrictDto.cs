﻿namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class DistrictDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountyId { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}