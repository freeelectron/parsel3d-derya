﻿using System.Collections.Generic;

namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class CountyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UavtCode { get; set; }
        public List<DistrictDto> Districts { get; set; }
        //public System.Collections.Generic.List<decimal> Geom { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}