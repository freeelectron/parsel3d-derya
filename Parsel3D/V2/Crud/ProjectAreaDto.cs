﻿namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class ProjectAreaDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public System.Collections.Generic.List<decimal> Geom { get; set; }
    }
}
