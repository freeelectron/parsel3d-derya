﻿namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class QualityOfLifeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Quality { get; set; }
        public string SubQuality { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}