﻿namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class LanguageDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}