﻿namespace RealEstateExpo.WebApi.Models.Dtos.Crud
{
    public class CompanyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string WebSite { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}