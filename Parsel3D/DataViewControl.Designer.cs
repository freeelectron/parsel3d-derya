﻿using System;
using System.Collections.Generic;
using Autodesk.AutoCAD.DatabaseServices;
using System.Windows.Forms;

namespace Parsel3D
{
    partial class DataViewControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        //DataGridViewButtonColumn colButton;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public void SetData(IEnumerable<Tuple<ObjectId, string>> polylineData)
        {
            Text = "Eklenen Parseller";
            LoadDataGrid(polylineData);

            // insert edit button into datagridview
            //colButton = new DataGridViewButtonColumn();
            //colButton.HeaderText = "Data";
            //colButton.Text = "Data";
            //colButton.UseColumnTextForButtonValue = true;
            //colButton.Width = 80;
            //dataGridViewParcelInfo.Columns.Add(colButton);
        }

        private void LoadDataGrid(IEnumerable<Tuple<ObjectId, string>> polylineData)
        {
            System.Data.DataTable tbl = CreateBindingTable(polylineData);
            dataGridViewParcelInfo.DataSource = tbl;
        }

        private System.Data.DataTable CreateBindingTable(
            IEnumerable<Tuple<ObjectId, string>> polylineData)
        {
            var tbl = new System.Data.DataTable();

            System.Data.DataColumn col;

            col = new System.Data.DataColumn("SiraNo", typeof(int));
            tbl.Columns.Add(col);

            col = new System.Data.DataColumn("AutocadId", typeof(ObjectId));
            tbl.Columns.Add(col);

            col = new System.Data.DataColumn("BarCode", typeof(string));
            tbl.Columns.Add(col);

            col = new System.Data.DataColumn("ViewZoom", typeof(string));
            tbl.Columns.Add(col);

            int i = 1;
            foreach (var tup in polylineData)
            {
                var row = tbl.NewRow();
                row["SiraNo"] = i++;
                row["AutocadId"] = tup.Item1;
                row["BarCode"] = tup.Item2;
                row["ViewZoom"] = "Gör/Zoom";

                tbl.Rows.Add(row);
            }

            return tbl;
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewParcelInfo = new System.Windows.Forms.DataGridView();
            this.columnSiraNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAutocadId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnViewZoom = new System.Windows.Forms.DataGridViewLinkColumn();
            this.propertyGrid = new System.Windows.Forms.PropertyGrid();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewParcelInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewParcelInfo
            // 
            this.dataGridViewParcelInfo.AllowUserToAddRows = false;
            this.dataGridViewParcelInfo.AllowUserToDeleteRows = false;
            this.dataGridViewParcelInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewParcelInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewParcelInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewParcelInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewParcelInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnSiraNo,
            this.columnAutocadId,
            this.columnBarcode,
            this.columnViewZoom});
            this.dataGridViewParcelInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridViewParcelInfo.Location = new System.Drawing.Point(0, 165);
            this.dataGridViewParcelInfo.Name = "dataGridViewParcelInfo";
            this.dataGridViewParcelInfo.ReadOnly = true;
            this.dataGridViewParcelInfo.RowHeadersVisible = false;
            this.dataGridViewParcelInfo.Size = new System.Drawing.Size(338, 367);
            this.dataGridViewParcelInfo.TabIndex = 0;
            this.dataGridViewParcelInfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewParcelInfo_CellContentClick);
            // 
            // columnSiraNo
            // 
            this.columnSiraNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.columnSiraNo.DataPropertyName = "SiraNo";
            this.columnSiraNo.FillWeight = 101.5228F;
            this.columnSiraNo.HeaderText = "Sıra No";
            this.columnSiraNo.Name = "columnSiraNo";
            this.columnSiraNo.ReadOnly = true;
            this.columnSiraNo.Width = 67;
            // 
            // columnAutocadId
            // 
            this.columnAutocadId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.columnAutocadId.DataPropertyName = "AutocadId";
            this.columnAutocadId.FillWeight = 229.5978F;
            this.columnAutocadId.HeaderText = "Autocad Id";
            this.columnAutocadId.Name = "columnAutocadId";
            this.columnAutocadId.ReadOnly = true;
            this.columnAutocadId.Width = 84;
            // 
            // columnBarcode
            // 
            this.columnBarcode.DataPropertyName = "BarCode";
            this.columnBarcode.FillWeight = 34.43967F;
            this.columnBarcode.HeaderText = "Barkod";
            this.columnBarcode.Name = "columnBarcode";
            this.columnBarcode.ReadOnly = true;
            // 
            // columnViewZoom
            // 
            this.columnViewZoom.DataPropertyName = "ViewZoom";
            this.columnViewZoom.FillWeight = 34.43967F;
            this.columnViewZoom.HeaderText = "Göster/Zoom";
            this.columnViewZoom.Name = "columnViewZoom";
            this.columnViewZoom.ReadOnly = true;
            this.columnViewZoom.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.columnViewZoom.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // propertyGrid
            // 
            this.propertyGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid.Enabled = false;
            this.propertyGrid.HelpVisible = false;
            this.propertyGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propertyGrid.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid.Name = "propertyGrid";
            this.propertyGrid.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid.Size = new System.Drawing.Size(338, 156);
            this.propertyGrid.TabIndex = 1;
            this.propertyGrid.ToolbarVisible = false;
            this.propertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid_PropertyValueChanged);
            // 
            // DataViewControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.propertyGrid);
            this.Controls.Add(this.dataGridViewParcelInfo);
            this.Name = "DataViewControl";
            this.Size = new System.Drawing.Size(344, 534);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewParcelInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewParcelInfo;
        internal System.Windows.Forms.PropertyGrid propertyGrid;
        private DataGridViewTextBoxColumn columnSiraNo;
        private DataGridViewTextBoxColumn columnAutocadId;
        private DataGridViewTextBoxColumn columnBarcode;
        private DataGridViewLinkColumn columnViewZoom;
    }
}
