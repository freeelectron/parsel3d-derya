﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace Parsel3D
{
    public enum Cins
    {
        [Description("Parsel")]
        Parsel,
        [Description("Bağımsız Bölüm")]
        BagimsizBolum,
        [Description("Ortak Kullanım Alanı")]
        OrtakKullanimAlani,
        [Description("Bağımsız bölüme ait depo")]
        Depo
    }

    public enum XDataTable
    {
        REGAPPNAME = 0,
        BARKODNO,
        BAGIMSIZBOLUMNOTLAR,
        ILCEKODU,
        MAHALLEKODU,
        ADANO,
        PARSELNO,
        SITEADI,
        BINANO,
        BLOKADI,
        BINASIFIRKOTU,
        BINADASKNO,
        OTELADI,
        BAGIMSIZTURU,
        BAGIMSIZBOLUMNO,
        BAGIMSIZBOLUMBULUNDUGUKAT,
        BAGIMSIZBOLUMTABANKOTU_MIMARI,
        BAGIMSIZBOLUMTABANKOTU_DENIZDENYUKSEKLIK,
        BAGIMSIBOLUMKATYUKSEKLIGI
    }


    /// enum 'da DisplayName özelliği kullanılamadığından
    /// bu sınıf vasıtasıyla Description'lar DisplayName gibi gösteriliyor.
    class CinsConverter : System.ComponentModel.EnumConverter
    {
        private Type _enumType;
        /// Initializing instance
        /// type Enum
        /// this is only one function, that you must
        /// to change. All another functions for enums
        /// you can use by Ctrl+C/Ctrl+V
        public CinsConverter(Type type)
        : base(type)
        {
            _enumType = type;
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return destType == typeof(string);
        }
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destType)
        {
            FieldInfo fi = _enumType.GetField(Enum.GetName(_enumType, value));
            DescriptionAttribute dna = (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));
            if (dna != null)
                return dna.Description;
            else
                return value.ToString();
        }
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type srcType)
        {
            return srcType == typeof(string);
        }
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            foreach (FieldInfo fi in _enumType.GetFields())
            {
                DescriptionAttribute dna =
                (DescriptionAttribute)Attribute.GetCustomAttribute(fi, typeof(DescriptionAttribute));
                if ((dna != null) && ((string)value == dna.Description))
                    return Enum.Parse(_enumType, fi.Name);
            }
            return Enum.Parse(_enumType, (string)value);
        }

        public static explicit operator CinsConverter(string v)
        {
            throw new NotImplementedException();
        }
    }

    [Serializable()]
    public class ClassTapuBilgileri
    {
        private string _ilceKodu = "1604";
        private string _mahalleKodu_tapu = "_mahalle_kodu_kadastro";
        private string _mahalleKodu_idari = "_mahalle_kodu_adres";
        private string _adaNo = "_ada_";
        private string _parselNo = "_parsel_";
        private string _siteAdi = "_site_";
        private string _binaNo = "_binano_";
        private string _blokAdi = "_blokadi_";
        private string _binaBarkodNo = "0000";
        private string _binaDaskNo = "";
        private string _projeId = "";
        private string _otelAdi = "";
        private string _notlar = "";
        private string _kirmiziKot = "0";
        private string _ondulasyon = "0";
        //        private Cins _cins = Cins.BagimsizBolum; //"Bağımsız Bölüm";

        public ClassTapuBilgileri()
        {
            //il = "__il__";
            //ilce = "__ilçe__";
            //AdaNo = "__adano__";
            //ParselNo = "__parselno__";
            //SiteAdi = "__siteadi__";
            //BlokAdi = "__blokadi__";
            //ParselCinsi = Cins.BagimsizBolum;
        }

        public ClassTapuBilgileri(ClassTapuBilgileri tb)
        {
            ProjeID = tb.ProjeID;
            ilceKodu = tb.ilceKodu;
            MahalleKoduTapu = tb.MahalleKoduTapu;
            MahalleKoduIdari = tb.MahalleKoduIdari;
            AdaNo = tb.AdaNo;
            ParselNo = tb.ParselNo;
            SiteAdi = tb.SiteAdi;
            BinaNo = tb.BinaNo;
            BlokAdi = tb.BlokAdi; 
            BinaBarkodNo = tb.BinaBarkodNo;
            BinaSifirKotu = tb.BinaSifirKotu;
            Ondulasyon = tb.Ondulasyon;
            BinaDaskNo = tb.BinaDaskNo;
        }

        [DisplayName("İlçe Kodu"),
         Description("Kadastral parselin bulunduğu ilçenin kodu"),
            Category("Parsel Bilgileri")
        ]
        public string ilceKodu
        {
            set { _ilceKodu = value; }
            get { return _ilceKodu; }
        }

        [DisplayName("Mahalle Kodu (Tapu)"),
         Description("Kadastral parselin Tapu'daki mahallesinin kodu"),
            Category("Parsel Bilgileri")
        ]
        public string MahalleKoduTapu
        {
            set { _mahalleKodu_tapu = value; }
            get { return _mahalleKodu_tapu; }
        }

        [DisplayName("Mahalle Kodu (İdari)"),
         Description("Kadastral parselin bulunduğu adresteki mahallenin kodu"),
            Category("Parsel Bilgileri")
        ]
        public string MahalleKoduIdari
        {
            set { _mahalleKodu_idari = value; }
            get { return _mahalleKodu_idari; }
        }

        [DisplayName("Ada No"),
         Description("Kadastral parselin Ada No'su"),
         Category("Parsel Bilgileri")
        ]
        public string AdaNo
        {
            set { _adaNo = value; }
            get { return _adaNo; }
        }

        [DisplayName("Parsel No"),
         Description("Kadastral parselin Parsel No'su"),
            Category("Parsel Bilgileri")
        ]
        public string ParselNo
        {
            set { _parselNo = value; }
            get { return _parselNo; }
        }


        [DisplayName("Site Adı"),
         Description("Site'nin/projenin adı"),
            Category("Parsel Bilgileri")
        ]
        public string SiteAdi
        {
            set { _siteAdi = value; }
            get { return _siteAdi; }
        }

        [DisplayName("Bina No"),
        Description("Bina numarası"),
           Category("Parsel Bilgileri")
       ]
        public string BinaNo
        {
            set { _binaNo = value; }
            get { return _binaNo; }
        }

        [DisplayName("Blok Adı"),
        Description("Blok Adı"),
           Category("Parsel Bilgileri")
       ]
        public string BlokAdi
        {
            set { _blokAdi = value; }
            get { return _blokAdi; }
        }

        [DisplayName("Bina Barkod No"),
        Description("Binaya ait benzersiz barkod numarası"),
           Category("Parsel Bilgileri")
       ]
        public string BinaBarkodNo
        {
            set { _binaBarkodNo = value; }
            get { return _binaBarkodNo; }
        }

        [DisplayName("Bina DASK No"),
        Description("Binaya verien DASK numarası"),
           Category("Parsel Bilgileri")
       ]
        public string BinaDaskNo
        {
            set { _binaDaskNo = value; }
            get { return _binaDaskNo; }
        }

        [DisplayName("Proje ID"),
        Description("Binaın ait olduğu projenin ID numarası"),
           Category("Parsel Bilgileri")
       ]
        public string ProjeID
        {
            set { _projeId = value; }
            get { return _projeId; }
        }

        [DisplayName("Notlar"),
        Description("Bina ile ilgili not, açıklama vb."),
           Category("Parsel Bilgileri")
       ]
        public string Notlar
        {
            set { _notlar = value; }
            get { return _notlar; }
        }

        [DisplayName("Otel Adı"),
        Description("Otel Binası"),
           Category("Parsel Bilgileri")
       ]
        public string OtelAdi
        {
            set { _otelAdi = value; }
            get { return _otelAdi; }
        }

        [DisplayName("Bina Kırmızı Kotu"),
        Description("Binanın mimari projedeki sıfır kotunun gerçek kotu."),
           Category("Parsel Bilgileri")
       ]
        public string BinaSifirKotu
        {
            set { _kirmiziKot = value; } // mimarlar Kırmızı Kot diyor buna.
            get { return _kirmiziKot; }
        }

        [DisplayName("Ondülasyon"),
        Description("Binanın bulunduğu yerdeki Elipsoidal kotu hesaplamak için gerekli düzeltme"),
           Category("Parsel Bilgileri")
        ]
        public string Ondulasyon
        {
            set { _ondulasyon = value; } // mimarlar Kırmızı Kot diyor buna.
            get { return _ondulasyon; }
        }

        ////[DisplayName("Parselin Cinsi")]
        ////[Description("Parselin kullanım şekli/cinsi")]
        ////[Category("Parsel Bilgileri")]
        ////[TypeConverter(typeof(CinsConverter))]
        ////[DefaultValue(typeof(Cins), "BagimsizBolum")]
        ////public Cins ParselCinsi
        ////{
        ////    set { _cins = value; }
        ////    get { return _cins; }
        ////}
    }

    [Serializable()]
//    [DefaultProperty("Name")]
    public class ParselBilgileri
    {
        // Our internal properties
        public const string regAppName = "EGEGY_PARSEL3D";

        private ClassTapuBilgileri _tapuBilgileri = new ClassTapuBilgileri();
        private string _barKodNo = "_barkod_";
        private int _bulunduguKat = 0;
        private double _tabanKotu_mimari = 0.0;
        private double _tabanKotu_denizdenyukseklik = 0.0;
        private double _katYuksekligi = 3.0;
        private string _bagimsizBolumNo = "_bolumno_";
        private string _notlar = "";
//        private Autodesk.AutoCAD.Geometry.Point3dCollection _point3dColl;
        private string _tumKoordinatlarString = "";
        private List<List<Point3dCollection>> _zeminKoordinatlari;
        private List<List<Point3dCollection>> _tavanKoordinatlari;

        // Their external exposure and categorization/description

        //[DisplayName("Tapu Bilgileri")]
        //[Description("Taşınmazın Mülkiyet/Tapu Bilgileri")]
        //[Category("Tapu Bilgileri")]
        public ClassTapuBilgileri TapuBilgileri
        {
            set { _tapuBilgileri = value; }
            get { return _tapuBilgileri; }
        }

        [DisplayName("Bar Kod Numarası")]
        [Description("Bağımsız bölümün bar kod numarası")]
        [Category("Parsel Bilgileri")]
        public string BarkodNo
        {
            set { _barKodNo = value; }
            get { return _barKodNo; }
        }

        [DisplayName("Bulunduğu Kat")]
        [Description("Bağımsız bölümün bulunduğu kat")]
        [Category("Parsel Bilgileri")]
        public int BagimsizBolumBulunduguKat
        {
            set { _bulunduguKat = value; }
            get { return _bulunduguKat; }
        }

        [DisplayName("Taban Kotu")]
        [Description("Bağımsız bölümün taban kotu")]
        [Category("Parsel Bilgileri")]
        public double BagimsizBolumTabanKotu_Mimari
        {
            set { _tabanKotu_mimari = value; }
            get { return _tabanKotu_mimari; }
        }
        [DisplayName("Taban Kotu Denizden Yukseklik")]
        [Description("Bağımsız bölümün gerçek harita kotu (denizden yüksekliği)")]
        [Category("Parsel Bilgileri")]
        public double BagimsizBolumTabanKotu_DenizdenYukseklik
        {
            set { _tabanKotu_denizdenyukseklik = value; }
            get { return _tabanKotu_denizdenyukseklik; }
        }
        [DisplayName("Kat Yüksekligi")]
        [Description("Bağımsız bölümün bulunduğu katın yüksekliği")]
        [Category("Parsel Bilgileri")]
        public double BagimsizBolumKatYuksekligi
        {
            set { _katYuksekligi = value; }
            get { return _katYuksekligi; }
        }

        [DisplayName("Bağımsız Bölüm No")]
        [Description("Bağımsız bölümün numarası")]
        [Category("Parsel Bilgileri")]
        public string BagimsizBolumNo
        {
            set { _bagimsizBolumNo = value; }
            get { return _bagimsizBolumNo; }
        }

        [DisplayName("Notlar")]
        [Description("Bağımsız bölüm ile ilgili notlar")]
        [Category("Parsel Bilgileri")]
        [DefaultValue("")]
        public string BagimsizBolumNotlar
        {
            set { _notlar = value; }
            get { return _notlar; }
        }

        [DisplayName("Zemin Koordinatları")]
        [Description("Bağımsız bölümün zemin koordinatları")]
        [Category("Parsel Bilgileri")]
        public List<List<Point3dCollection>> ZeminKoordinatlari
        {
            set { _zeminKoordinatlari = value; }
            get { return _zeminKoordinatlari; }
        }

        [DisplayName("Tavan Koordinatları")]
        [Description("Bağımsız bölümün tavan koordinatları")]
        [Category("Parsel Bilgileri")]
        public List<List<Point3dCollection>> TavanKoordinatlari
        {
            set { _tavanKoordinatlari = value; }
            get { return _tavanKoordinatlari; }
        }

        [DisplayName("Koordinatlar String")]
        [Description("Bağımsız bölümün 3 Boyutlu köşe koordinatları (Tek string halinde)")]
        [Category("Parsel Bilgileri")]
        public string TumKoordinatlarString
        {
            set { _tumKoordinatlarString = value; }                                                                                           
            get { return _tumKoordinatlarString; }
        }

        public string BagimsizTuru { get; set; }

        public Point3dCollection Koseler()
        {
            Point3dCollection pts = new Point3dCollection();
            string[] noktalar = _tumKoordinatlarString.Split(' '); ;
            foreach (string str in noktalar)
            {
                string[] degerler = str.Split(',');
                Point3d po = new Point3d(
                    double.Parse(degerler[0]),
                    double.Parse(degerler[1]),
                    double.Parse(degerler[2]));
                pts.Add(po);
            }
            return pts;
        }
        const string filename = "ParselBilgileri.xml";

        // Our methods for loading and saving the settings

        // Load needs to be static, as we don't yet have
        // an instance

        public static ParselBilgileri LoadXML()
        {
            ParselBilgileri ret = null;
            XmlSerializer xs = null;
            System.IO.StreamReader sr = null;
            try
            {
                xs = new XmlSerializer(typeof(ParselBilgileri));
                sr = new StreamReader(filename);
            }
            catch
            {
                // File not found: create default settings
                return new ParselBilgileri();
            }

            if (sr != null)
            {
                ret = (ParselBilgileri)xs.Deserialize(sr);
                sr.Close();
            }
            return ret;
        }

        // Save will be called on a specific instance

        public void SaveXML(string filepath = "")
        {
            try
            {
                string fp = filepath == "" ? filename : filepath;

                XmlSerializer xs =
                    new XmlSerializer(typeof(ParselBilgileri));
                StreamWriter sw =
                    new StreamWriter(fp, false);
                xs.Serialize(sw, this);
                sw.Close();
            }
            catch (System.Exception ex)
            {
                Editor ed =
                    Application.DocumentManager.MdiActiveDocument.Editor;
                ed.WriteMessage(
                    "\nParsel bilgileri kaydedilirken bir sorun oluştu: {0}",
                    ex
                );
            }
        }
        public void ReadFromAutoCadObject(DBObject obj)
        {
            ResultBuffer rb = obj.GetXDataForApplication(regAppName);
            if (rb != null)
            {
                TypedValue[] tv = rb.AsArray();

                BarkodNo                            = tv[1].Value.ToString();
                BagimsizBolumNotlar                 = tv[2].Value.ToString();
                TapuBilgileri.ilceKodu              = tv[3].Value.ToString();
                TapuBilgileri.MahalleKoduTapu       = tv[4].Value.ToString();
                TapuBilgileri.AdaNo                 = tv[5].Value.ToString();
                TapuBilgileri.ParselNo              = tv[6].Value.ToString();
                TapuBilgileri.SiteAdi               = tv[7].Value.ToString();
                TapuBilgileri.BinaNo                = tv[8].Value.ToString();
                TapuBilgileri.BlokAdi               = tv[9].Value.ToString();
                TapuBilgileri.BinaSifirKotu         = tv[10].Value.ToString();
                TapuBilgileri.BinaDaskNo            = tv[11].Value.ToString();
                TapuBilgileri.OtelAdi               = tv[12].Value.ToString();
                BagimsizTuru                        = tv[13].Value.ToString();
                BagimsizBolumNo                     = tv[14].Value.ToString();
                BagimsizBolumBulunduguKat           = (int)tv[15].Value;
                BagimsizBolumTabanKotu_Mimari       = (double)tv[16].Value;
                BagimsizBolumTabanKotu_DenizdenYukseklik = (double)tv[17].Value;
                BagimsizBolumKatYuksekligi          = (double)tv[18].Value;

                try // 04.09.2020'den önceki objelerde aşağıdaki kayıtlar yok.
                {
                    if (tv.Count() > 19)
                    {
                        TapuBilgileri.ProjeID = tv[19].Value.ToString();
                        TapuBilgileri.MahalleKoduIdari = tv[20].Value.ToString();
                        TapuBilgileri.Ondulasyon = tv[21].Value.ToString();
                    }
                    else
                    {
                        var tb = new ClassTapuBilgileri();
                        tb = Utils.ReadFromDictionary();
                        if(!string.IsNullOrEmpty(tb.ProjeID))
                            TapuBilgileri.ProjeID = tb.ProjeID;
                        if (!string.IsNullOrEmpty(tb.MahalleKoduIdari))
                            TapuBilgileri.MahalleKoduIdari = tb.MahalleKoduIdari;
                        if (!string.IsNullOrEmpty(tb.Ondulasyon))
                            TapuBilgileri.Ondulasyon = tb.Ondulasyon;
                    }
                }
                catch
                { }
            }
            if (obj is Solid3d)
            {
                Solid3d solid = obj as Solid3d;
                var SolidinTabanKoordinatlari = new List<List<Point3dCollection>> { Utils.GetFloorCoordinatesFromSolid3d(solid) };

                // Solid'in Autocad'deki Z değerini değil olması gerekeni yazıyoruz.
                ZeminKoordinatlari = Utils.ReplaceZOfListOfListPoint3dCollection(SolidinTabanKoordinatlari, BagimsizBolumTabanKotu_DenizdenYukseklik);

                // zemin koordinatlarına kat yüksekliği eklenerek tüm koordinatlar bulunuyor.
                TavanKoordinatlari = new List<List<Point3dCollection>>(ZeminKoordinatlari); // kopyasını aldık
                TavanKoordinatlari.ForEach(listlistp3d => 
                listlistp3d.ForEach(listp3d => 
                listp3d = Utils.KotEkle(listp3d, BagimsizBolumKatYuksekligi)));

                //foreach (List<Point3dCollection> zemlistlistpcoll in ZeminKoordinatlari)
                //{
                //    //var tavlistpcoll = new List<Point3dCollection>();
                //    foreach (Point3dCollection zemlistpcoll in zemlistlistpcoll)
                //    {
                //        //int n = zemlistpcoll.Count;
                //        //var tavpcoll = new Point3dCollection();
                //        //for (int i = 0; i < n; i++)
                //        foreach(Point3d p3d in zemlistpcoll)
                //        {
                //            //tavpcoll.Add(new Point3d(zemlistpcoll[i].X, zemlistpcoll[i].Y, zemlistpcoll[i].Z + BagimsizBolumKatYuksekligi));
                //            p3d.Z += BagimsizBolumKatYuksekligi;
                //            //p3d = new Point3d(p3d.X, p3d.Y, p3d.Z + BagimsizBolumKatYuksekligi);
                //        }
                //        tavlistpcoll.Add(tavpcoll);
                //    }
                //    TavanKoordinatlari.Add(tavlistpcoll);
                //}
                //                TumKoordinatlarString = Utils.ConvertToString3d(ZeminKoordinatlari, ' ') + Utils.ConvertToString3d(TavanKoordinatlari, ' ');
                TumKoordinatlarString = Utils.ConvertListOfListOf3dPointCollectionToString(ZeminKoordinatlari, 3) + Utils.ConvertListOfListOf3dPointCollectionToString(TavanKoordinatlari, 3);
            }
            else if (obj is Polyline)
            {
                Polyline poly = obj as Polyline;
                int n = poly.NumberOfVertices;
                int i = 0;
                while (i < n)
                {
                    var p = poly.GetPoint3dAt(i++);
                    TumKoordinatlarString += p.ToString() + " ";
                }
            }
        }
        public void WriteToAutoCadObject(Transaction tr, DBObject obj)
        {
//            ErrorStatus es;
            Database db = obj.Database;

            // Make sure the application is registered
            // (we could separate this out to be called
            // only once for a set of operations)

            RegAppTable rat =
              (RegAppTable)tr.GetObject(
                db.RegAppTableId,
                OpenMode.ForRead
              );

            if (!rat.Has(regAppName))
            {
                rat.UpgradeOpen();
                RegAppTableRecord ratr = new RegAppTableRecord();
                ratr.Name = regAppName;
                rat.Add(ratr);
                tr.AddNewlyCreatedDBObject(ratr, true);
            }

            // bağımsız bölüm solid'i için barkod numarası adıyla yeni layer oluşturma ve solidi o layer'a alma
            string strBagimsizBolumLayerAdi = BarkodNo;
            if (strBagimsizBolumLayerAdi.ToUpper().Contains("G"))
                strBagimsizBolumLayerAdi += "_giris";
            //using (tr)
            {
                LayerTable lt = (LayerTable)tr.GetObject(db.LayerTableId, OpenMode.ForRead);
                ObjectId ltId;
                if (!lt.Has(strBagimsizBolumLayerAdi))
                {
                    LayerTableRecord ltr = new LayerTableRecord
                    {
                        Name = strBagimsizBolumLayerAdi
                    };
                    Random rand = new Random(); // layer rengi olarak rastgele bir renk seçmek için.
                    //ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, 4); // cyan
                    //
                    ltr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(Autodesk.AutoCAD.Colors.ColorMethod.ByAci, (short)(rand.Next()%255)); // cyan
                    // Add the new layer to the layer table
                    lt.UpgradeOpen();
                    ltId = lt.Add(ltr);
                    tr.AddNewlyCreatedDBObject(ltr, true);
                }
            }
            Entity ent = obj as Entity;
            ent.Layer = strBagimsizBolumLayerAdi;

            // Create the XData and set it on the object

            ResultBuffer rb =
              new ResultBuffer(
           /* 0 */ new TypedValue((int)DxfCode.ExtendedDataRegAppName, regAppName),
           /* 1 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, BarkodNo),
           /* 2 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, BagimsizBolumNotlar),
           /* 3 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.ilceKodu),
           /* 4 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.MahalleKoduTapu),
           /* 5 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.AdaNo),
           /* 6 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.ParselNo),
           /* 7 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.SiteAdi),
           /* 8 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.BinaNo),
           /* 9 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.BlokAdi),
           /*10 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.BinaSifirKotu),
           /*11 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.BinaDaskNo),
           /*12 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.OtelAdi),
           /*13 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, BagimsizTuru),
           /*14 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, BagimsizBolumNo),
           /*15 */ new TypedValue((int)DxfCode.ExtendedDataInteger32, BagimsizBolumBulunduguKat),
           /*16 */ new TypedValue((int)DxfCode.ExtendedDataReal, BagimsizBolumTabanKotu_Mimari),
           /*17 */ new TypedValue((int)DxfCode.ExtendedDataReal, BagimsizBolumTabanKotu_DenizdenYukseklik),
           /*18 */ new TypedValue((int)DxfCode.ExtendedDataReal, BagimsizBolumKatYuksekligi),

           /*19 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.ProjeID),
           /*20 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.MahalleKoduIdari),
           /*21 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, TapuBilgileri.Ondulasyon)
              );
            obj.XData = rb;
            //Utils.ShowMessage("rb:" + rb.ToString());

            rb.Dispose();
        }
        public void ReadXRecord()
        {
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                //Get the named object dictionary
                DBDictionary nod = tr.GetObject
                                (
                                    db.NamedObjectsDictionaryId,
                                    OpenMode.ForRead
                                ) as DBDictionary;

                // Find the "EGEGY_PARSEL3D" dictionary
                DBDictionary standardsDict = null;
                if (!nod.Contains(regAppName))
                {// Create it if not found
                    nod.UpgradeOpen();
                    standardsDict = new DBDictionary();
                    nod.SetAt(regAppName, standardsDict);
                    tr.AddNewlyCreatedDBObject(standardsDict, true);
                }
                else
                {
                    // Get it if it already exists
                    standardsDict = tr.GetObject
                                (
                                    nod.GetAt(regAppName),
                                    OpenMode.ForWrite
                                ) as DBDictionary;
                }

                // Find the dictionary name that we will add
                int recordCount = 0;
                foreach (DBDictionaryEntry dictEntry in standardsDict)
                {
                    recordCount++;
                }

                // Create a Xrecord with the dws file path set
                // to the DXF code : 1
                string dwsPath = "dummy";
                TypedValue tv = new TypedValue((int)1, dwsPath);
                Xrecord record = new Xrecord();
                using (ResultBuffer rb = new ResultBuffer(tv))
                {
                    record.Data = rb;
                }

                // Add the Xrecord to the dictionary
                standardsDict.SetAt(recordCount.ToString(), record);
                tr.AddNewlyCreatedDBObject(record, true);

                // Done
                tr.Commit();
            }
        }
    } // ParselBilgileri
}
