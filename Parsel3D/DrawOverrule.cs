﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using cadPolyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.Colors;
using System;
using System.Collections.Generic;

namespace Parsel3D
{
    public class DrawOverrule : DrawableOverrule
    {
        static public DrawOverrule theOverrule =
          new DrawOverrule();

        private SweepOptions sweepOpts = new SweepOptions();

        // polyline'ın ağırlık merkezi.
        public Point3d GetPolylineCenterOfGravityPoint(cadPolyline pl)
        {
            int vn = -1;
            double xAvg = 0.0, yAvg = 0.0, zAvg = 0.0;
            if (pl != null)
            {
                // Use a for loop to get each vertex, one by one
                vn = pl.NumberOfVertices;
                for (int i = 0; i < vn; i++)
                {
                    Point3d pt = pl.GetPoint3dAt(i);
                    xAvg += pt.X;
                    yAvg += pt.Y;
                    zAvg += pt.Z;
                }
                xAvg /= vn;
                yAvg /= vn;
                zAvg /= vn;
            }
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            //            ed.WriteMessage("\n hello");
            //            ed.WriteMessage("\n x={0} y={1} z={2}", xAvg, yAvg, zAvg);
            return new Point3d(xAvg, yAvg, zAvg);
        }
        //
        public Point3d GetPolylineCenterPoint(cadPolyline poly)
        {
            Point3d pt1, pt2;
            pt1 = poly.GeometricExtents.MinPoint;
            pt2 = poly.GeometricExtents.MaxPoint;
            return new Point3d(0.5 * (pt1.X + pt2.X), 0.5 * (pt1.Y + pt2.Y), poly.Elevation);
        }

        public override bool WorldDraw(Drawable d, WorldDraw wd)
        {
            List<string> parcelInf = new List<string>();

            if (d is DBObject)
                parcelInf = XDataHelpers.ParcelInfoForObject((DBObject)d); // parcelInf[0] => barcode, parcelInf[1] => note

            if (parcelInf.Count > 0 && parcelInf[0] != "")
            {
                if (d is cadPolyline)
                {
                    cadPolyline pline = (cadPolyline)d;
                    base.WorldDraw(d, wd);
                    // Draw the polyline as is, with overruled attributes

                    if (!pline.Id.IsNull && pline.Length > 0.0)
                    {
                        // Draw a text in polyline

                        //EntityColor c = wd.SubEntityTraits.TrueColor;
                        //wd.SubEntityTraits.TrueColor = new EntityColor(0x00AfAfff);
                        //wd.SubEntityTraits.LineWeight = LineWeight.LineWeight000;

                        //Point3d cp = GetPolylineCenterOfGravityPoint(pline);
                        Point3d cp = GetPolylineCenterPoint(pline);
                        //double area = pline.Area;
                        //double textheight = (double)Application.GetSystemVariable("TEXTSIZE");
                        Plane plane = new Plane(Point3d.Origin, pline.Normal);
                        var maxpt = pline.GeometricExtents.MaxPoint;
                        var minpt = pline.GeometricExtents.MinPoint;
                        //double textheight = 0.01 * maxpt.Convert2d(plane).GetDistanceTo(minpt.Convert2d(plane));
                        double textheight = 0.01 * maxpt.DistanceTo(minpt);

                        //wd.Geometry.Text(cp, new Vector3d(0, 0, 1), new Vector3d(1, 0, 0), parcelInf, true, "standard");
                        wd.Geometry.Text(cp, new Vector3d(0, 0, 1), new Vector3d(1, 0, 0), textheight, 1.0, 0, parcelInf[0]);
                        //string strArea = string.Format("{0:F2}m²", area);

                        if(parcelInf.Count > 1 && parcelInf[1] != "")
                            wd.Geometry.Text(cp + new Vector3d(0, -1.5 * textheight, 0), new Vector3d(0, 0, 1), new Vector3d(1, 0, 0), textheight, 1.0, 0, parcelInf[1]);
                        //DBText txt = new DBText();
                        //txt.Position = GetPolylineCenterPoint(pline);
                        //txt.TextString = parcelInf;
                        //txt.Height = 1.0;

                        //txt.WorldDraw(wd);
                        //txt.Dispose();

                        //pline.WorldDraw(wd);
                        //pline.Dispose();
                        //wd.SubEntityTraits.TrueColor = c;
                    }
                    return true;
                }
                //else if (d is Solid3d)
                //{
                //    Solid3d solid = d as Solid3d;
                //    base.WorldDraw(solid, wd);
                //    Point3d cen = GetSolidCenterPoint(solid);
                //}
            } // if parcelInf
                return base.WorldDraw(d, wd);
        } // WorldDraw

        private Point3d GetSolidCenterPoint(Solid3d solid)
        {
            Point3d ret=Point3d.Origin;
            
            return ret;
        }

        public override int SetAttributes(Drawable d, DrawableTraits t)
        {
            int b = base.SetAttributes(d, t);

            List<string> parcelInf = new List<string>();

            if (d is DBObject)
                parcelInf = XDataHelpers.ParcelInfoForObject((DBObject)d);

            if (parcelInf.Count > 0 && parcelInf[0] != "") // has barcode str?
            {
                if (d is cadPolyline)
                {
                    // If d is LINE, set color to index 6
                    t.Color = 6;
                    // and lineweight to .40 mm
                    t.LineWeight = LineWeight.LineWeight040;
                }
            }
            return b;
        }// SetAttributes
    } // DrawOverrule
}