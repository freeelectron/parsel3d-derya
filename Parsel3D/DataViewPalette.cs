﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsel3D
{
    public class DataViewPalette
    {
        static PaletteSet mPaletteSet;
        static DataViewControl mDataViewControl;
        static ClassTapuBilgileri _tapuBilgileri = null;

        public DataViewPalette()
        {
            mDataViewControl = new DataViewControl();
        }
        public void Show()
        {
            if (mPaletteSet == null)
            {
                mPaletteSet = new PaletteSet("Parsel Verileri");
                mPaletteSet.Style =
                  //PaletteSetStyles.NameEditable |
                  PaletteSetStyles.ShowPropertiesMenu |
                  PaletteSetStyles.ShowAutoHideButton |
                  PaletteSetStyles.ShowCloseButton;
                mPaletteSet.MinimumSize = new System.Drawing.Size(300, 300);
                mPaletteSet.Size = new System.Drawing.Size(300, 800);
                mPaletteSet.Add("DataViewForm", mDataViewControl);
            }
            mPaletteSet.Visible = true;

            _tapuBilgileri = Utils.ReadFromDictionary();
            mDataViewControl.propertyGrid.SelectedObject = _tapuBilgileri;
            mDataViewControl.propertyGrid.Update();
       }
        public void AddData(Tuple<ObjectId,string> data)
        {
            mDataViewControl.AddData(data);
        }
    }
}
