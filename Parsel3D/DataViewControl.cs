﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Autodesk.AutoCAD.DatabaseServices;
using CadApp = Autodesk.AutoCAD.ApplicationServices.Application;
//using Microsoft.CSharp.dll;

using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;

namespace Parsel3D
{
    public partial class DataViewControl : UserControl
    {
        static Entity _previousHighlightedEnt = null; // Tabloda zoom yaparken en son zoom yapılanı "unhiglight" yapmak için kullanılıyor.

        public DataViewControl()
        {
            InitializeComponent();

            List<Tuple<ObjectId, string>> demoData = new List<Tuple<ObjectId, string>>();
            //demoData.Add(new Tuple<ObjectId, string>(new ObjectId(), "parsel1"));
            //demoData.Add(new Tuple<ObjectId, string>(new ObjectId(), "parsel2"));
            //demoData.Add(new Tuple<ObjectId, string>(new ObjectId(), "parsel3"));
            //demoData.Add(new Tuple<ObjectId, string>(new ObjectId(), "parsel4"));
            SetData(demoData);
        }
        public int GetRowIndex(ObjectId searchId)
        {
            int rowIndex = -1;
            foreach (DataGridViewRow row in dataGridViewParcelInfo.Rows)
            {

                if (row.Cells["columnAutocadId"].Value != null) // Need to check for null if new row is exposed
                {
                //MessageBox.Show(row.Cells["columnAutocadId"].Value.ToString());
                    if (row.Cells["columnAutocadId"].Value.ToString().Equals(searchId.ToString()))
                    {
                        rowIndex = row.Index;
                        break;
                    }
                }
            }
            return rowIndex;

        } // GetRowIndex

        public int AddData(Tuple<ObjectId, string> data)
        {
            // dataGridViewParcelInfo.Rows.Add(dataGridViewParcelInfo.RowCount+1, new ObjectId(), "parsello5", "Gör/Zoom");
            // dataGridViewParcelInfo.Rows.Add(dataGridViewParcelInfo.RowCount+1, data.Item1, data.Item2, "Gör/Zoom");
            System.Data.DataTable dataTable = (System.Data.DataTable)dataGridViewParcelInfo.DataSource;

            int rowIndex = GetRowIndex(data.Item1);

            if (rowIndex == -1) // Id bulunamadı, ekleniyor.
            {
                DataRow row = dataTable.NewRow();

                row["SiraNo"] = dataGridViewParcelInfo.RowCount + 1;
                row["AutocadId"] = data.Item1;
                row["BarCode"] = data.Item2;
                row["ViewZoom"] = "Gör/Zoom";

                dataTable.Rows.Add(row);
                dataTable.AcceptChanges();
            }
            else // Id zaten var, değiştiriliyor
            {
               // MessageBox.Show("var galiba");

                dataGridViewParcelInfo.Rows[rowIndex].Cells["columnAutocadId"].Value = data.Item1;
                dataGridViewParcelInfo.Rows[rowIndex].Cells["columnBarCode"].Value = data.Item2;
            }
            return 1;
        } // AddData

        public static void ZoomTo(ObjectId oId)
        {
            Autodesk.AutoCAD.Geometry.Point3d pt1, pt2;

            using (var tran = oId.Database.TransactionManager.StartTransaction())
            {
                var ent = (Entity)tran.GetObject(oId, OpenMode.ForRead);
                pt1 = ent.GeometricExtents.MinPoint;
                pt2 = ent.GeometricExtents.MaxPoint;
                if(_previousHighlightedEnt != null)
                    _previousHighlightedEnt.Unhighlight();
                ent.Highlight();
                _previousHighlightedEnt = ent;
                tran.Commit();
            }

            double h = pt2.Y - pt1.Y;
            double w = pt2.X - pt1.X;
            double l = h;
            if (w > h) l = w;


            double[] p1 = new double[] { pt1.X - l / 2.0, pt1.Y - l / 2.0, pt1.Z };
            double[] p2 = new double[] { pt2.X + l / 2.0, pt2.Y + l / 2.0, pt2.Z };

            dynamic comApp = CadApp.AcadApplication;
            comApp.ZoomWindow(p1, p2);
        }

        private void dataGridViewParcelInfo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                ObjectId polyId = (ObjectId)dataGridViewParcelInfo.Rows[e.RowIndex].Cells[1].Value;
                ZoomTo(polyId);
            }
        }

        private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
        {
            CustomDictionary.SetValue("TapuBilgileri", e.ChangedItem.ToString(), e.ChangedItem.Value.ToString());
        }
    }
}
