﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System.Collections.Generic;

namespace Parsel3D
{
    class GripInfo
    {
        // A single set of grips would not have worked in
        // the case where multiple objects were selected.

        static Dictionary<string, Point3dCollection> _gripDict =
          new Dictionary<string, Point3dCollection>();

        private static string GetKey(Entity e)
        {
            // Generate a key based on the name of the object's type
            // and its geometric extents

            // (We cannot use the ObjectId, as this is null during
            // grip-stretch operations.)

            return e.GetType().Name + ":" + e.GeometricExtents.ToString();
        }

        // Save the locations of the grips for a particular entity

        private void StoreGripInfo(Entity e, Point3dCollection grips)
        {
            string key = GetKey(e);
            if (_gripDict.ContainsKey(key))
            {
                // Clear the grips if any already associated

                Point3dCollection grps = _gripDict[key];
                using (grps)
                {
                    grps.Clear();
                }
                _gripDict.Remove(key);
            }

            // Now we add our grips

            Point3d[] pts = new Point3d[grips.Count];
            grips.CopyTo(pts, 0);
            Point3dCollection gps = new Point3dCollection(pts);
            _gripDict.Add(key, gps);
        }

        // Get the locations of the grips for an entity

        public static Point3dCollection RetrieveGripInfo(Solid3d e)
        {
            Solid3d solid = e;// as Solid3d;
            GripDataCollection grips = new GripDataCollection();

            solid.GetGripPoints(grips, 1.0, 1, new Vector3d(1, 0, 0), GetGripPointsFlags.GripPointsOnly);

            Point3dCollection gripPts = new Point3dCollection();
            foreach (GripData gd in grips)
            {
                gripPts.Add(gd.GripPoint);
                double x = gd.GripPoint.X;
                double y = gd.GripPoint.Y;
                double z = gd.GripPoint.Z;
                System.Windows.MessageBox.Show(x.ToString() + " " + y.ToString() + " " + z.ToString());
            }
            return gripPts;
        }
    }
}
