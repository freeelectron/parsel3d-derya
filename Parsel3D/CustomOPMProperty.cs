using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Interop.Common;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows.OPM;
using Autodesk.AutoCAD.Windows.ToolPalette;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Parsel3D
{
    #region Ege GY Parsel3D Custom Properties
    [
        Guid("F60AE3DA-0373-4d24-82D2-B2646517ABCB"),
        //ProgId("OPMNetSample.CustomProperty.1"),
        ProgId("Parsel3D.CustomProperty.1"),

        // No class interface is generated for this class and
        // no interface is marked as the default.
        // Users are expected to expose functionality through
        // interfaces that will be explicitly exposed by the object
        // This means the object can only expose interfaces we define

        ClassInterface(ClassInterfaceType.None),
        // Set the default COM interface that will be used for
        // Automation. Languages like: C#, C++ and VB allow to 
        //query for interface's we're interested in but Automation 
        // only aware languages like javascript do not allow to 
        // query interface(s) and create only the default one

        ComDefaultInterface(typeof(IDynamicProperty2)),
        ComVisible(true)
    ]
    public class XdataReadWrite
    {
        public static string _barcodestr = "";
        public static string _notestr = "";

        public static void GetXdata(DBObject obj)
        {
            ResultBuffer rb = obj.GetXDataForApplication(ParselBilgileri.regAppName);
            if (rb != null)
            {
                TypedValue[] tv = rb.AsArray();

                if(tv.Length > 1)
                    _barcodestr = tv[1].Value.ToString();
                if(tv.Length > 2)
                    _notestr = tv[2].Value.ToString();
                //TapuBilgileri.ilceKodu = tv[3].Value.ToString();
                //TapuBilgileri.MahalleKodu = tv[4].Value.ToString();
                //TapuBilgileri.AdaNo = tv[5].Value.ToString();
                //TapuBilgileri.ParselNo = tv[6].Value.ToString();
                //TapuBilgileri.SiteAdi = tv[7].Value.ToString();
                //TapuBilgileri.BinaNo = tv[8].Value.ToString();
                //TapuBilgileri.BlokAdi = tv[9].Value.ToString();
                //TapuBilgileri.BinaSifirKotu = tv[10].Value.ToString();
                //BagimsizTuru = tv[11].Value.ToString();
                //BagimsizBolumNo = tv[12].Value.ToString();
                //BagimsizBolumBulunduguKat = (int)tv[13].Value;
                //BagimsizBolumTabanKotu_Mimari = (double)tv[14].Value;
                //BagimsizBolumTabanKotu_DenizdenYukseklik = (double)tv[15].Value;
                //BagimsizBolumKatYuksekligi = (double)tv[16].Value;
            }
            else
            {
                _barcodestr = "";
                _notestr = "";
            }
        }
        public static void SetXdata(DBObject obj) // sadece barkod ve notlar k�sm�n� yazar. di�erleri varsa aynen korunur yoksa bo� olarak olu�turulur.
        {
            //GetXdata(obj); // mevcut _barcodestr ve _notestr kaydediliyor
            ResultBuffer rbr = obj.GetXDataForApplication(ParselBilgileri.regAppName);
            ResultBuffer rbw;
            TypedValue[] tv;
            if (rbr != null)
            {
                tv = rbr.AsArray();
                tv[1] = new TypedValue((int)DxfCode.ExtendedDataAsciiString, _barcodestr);
                tv[2] = new TypedValue((int)DxfCode.ExtendedDataAsciiString, _notestr);

                rbw = new ResultBuffer();
                for (int i = 0; i < tv.Length; i++)
                    rbw.Add(tv[i]);
            }
            else
            {
                rbw = new ResultBuffer(
                    /* 0 */ new TypedValue((int)DxfCode.ExtendedDataRegAppName, ParselBilgileri.regAppName),
                    /* 1 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, _barcodestr),
                    /* 2 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, _notestr),
                    /* 3 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 4 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 5 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 6 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 7 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 8 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /* 9 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /*10 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /*11 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /*12 */ new TypedValue((int)DxfCode.ExtendedDataAsciiString, ""),
                    /*13 */ new TypedValue((int)DxfCode.ExtendedDataInteger32, 0),
                    /*14 */ new TypedValue((int)DxfCode.ExtendedDataReal, 0.0),
                    /*15 */ new TypedValue((int)DxfCode.ExtendedDataReal, 0.0),
                    /*16 */ new TypedValue((int)DxfCode.ExtendedDataReal, 0.0)
                    );
            }
            //System.Windows.MessageBox.Show("rbw:" + rbw.ToString() + " bc:" + _barcodestr + " nt:" + _notestr);
            obj.XData = rbw;
            rbw.Dispose();
            //rbr.Dispose();
        }
    }
    public class BarcodeProperty : IDynamicProperty2, IAcPiCategorizeProperties//, IAcPiPropertyDisplay
    {
        private IDynamicPropertyNotify2 m_pSink =null;
        //private string _barcodestr="";

        #region IDynamicProperty2 methods
        // Unique property ID
        void IDynamicProperty2.GetGUID(out Guid propGUID)
        {
            propGUID = new Guid("77A4D7DC-75C6-4290-9AE7-95D26FB58217");
        }

        // Property display name
        void IDynamicProperty2.GetDisplayName(out string szName)
        {
            szName = "Barkod Numaras�";
        }

        // Show/Hide property in the OPM, for this object instance
        void IDynamicProperty2.IsPropertyEnabled(object pUnk, out int bEnabled)
        {
            bEnabled = 1;
        }

        // Is property showing but disabled
        void IDynamicProperty2.IsPropertyReadOnly(out int bReadonly)
        {
            bReadonly = 0;
        }

        // Get the property description string
        void IDynamicProperty2.GetDescription(out string szName)
        {
            szName = "Ba��ms�z b�l�m, kat, parsel vb. ait barkod numaras�";
        }

        // OPM will typically display these in an edit field
        // optional: meta data representing property type name,
        // ex. ACAD_ANGLE
        void IDynamicProperty2.GetCurrentValueName(out string szName)
        {
            throw new System.NotImplementedException();
        }

        // What is the property type, ex. VT_R8
        void IDynamicProperty2.GetCurrentValueType(out ushort varType)
        {
            // The Property Inspector supports the following data
            // types for dynamic properties:
            //VT_I2(2), VT_I4(3), VT_R4(4), VT_R8(5),
      // VT_BSTR (8), VT_BOOL (11), and VT_USERDEFINED (29).  

            //varType = 3; // VT_I4
            varType = 8; // VT_BSTR
        }

        // Get the property value, passes the specific object
        // we need the property value for.
        void IDynamicProperty2.GetCurrentValueData(object pUnk, ref object pVarData)
        {
            // Because we said the value type was a 32b int (VT_I4)
            //pVarData = (int)4;
            //pVarData = _barcodestr;


            AcadObject obj = (AcadObject)pUnk;

            if (obj != null)
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                using (Transaction tr = doc.TransactionManager.StartTransaction())
                {
                    ObjectId objectId = new ObjectId((IntPtr)obj.ObjectID);
                    DBObject dbobject = tr.GetObject(objectId, OpenMode.ForRead);
                    XdataReadWrite.GetXdata(dbobject);
                    pVarData = XdataReadWrite._barcodestr;
                    //List<string> parcelInf = new List<string>();
                    //parcelInf = XDataHelpers.ParcelInfoForObject(tr, objectId);
                    //if (parcelInf.Count > 0)
                    //    pVarData = parcelInf[0];
                    //else
                    //    pVarData = "";
                }
            }
            else
                pVarData = "";
        }

        // Set the property value, passes the specific object we
        // want to set the property value for
        void IDynamicProperty2.SetCurrentValueData(object pUnk, object varData)
        {
            // TODO: Save the value returned to you

            // Because we said the value type was a 32b int (VT_I4)
            //int myVal = (int)varData;
            //_barcodestr = varData.ToString();

            AcadObject obj = (AcadObject)pUnk;

            if (obj != null)
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                DocumentLock dl = doc.LockDocument(DocumentLockMode.ProtectedAutoWrite, null, null, true);
                Autodesk.AutoCAD.DatabaseServices.Database db = doc.Database;

                using (dl)
                {
//                    List<string> parcelInf = new List<string>();
                    ObjectId objectId = new ObjectId((IntPtr)obj.ObjectID);
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        DBObject dbobject = tr.GetObject(objectId, OpenMode.ForWrite);
                        XdataReadWrite._barcodestr = varData as string;
                        XdataReadWrite.SetXdata(dbobject);
                        tr.Commit(); // bunu yapmazsak de�i�ikliklikler i�lenmiyor.
                    }
                    //// Note k�sm� objeden okunuyor, SetParcelInfoOnObject'e parametre olarak tekrar ge�ilecek.
                    //parcelInf = XDataHelpers.ParcelInfoForObject(objId);

                    //if (parcelInf.Count > 1)
                    //{
                    //    //System.Windows.MessageBox.Show("parcelInf[1]:" + parcelInf[1]);
                    //    XDataHelpers.SetParcelInfoOnObject(objId, (string)varData, parcelInf[1]);
                    //}
                    //else
                    //    XDataHelpers.SetParcelInfoOnObject(objId, (string)varData, "");
                }
            }
        }

        // OPM passes its implementation of IDynamicPropertyNotify, you
        // cache it and call it to inform OPM your property has changed
        void IDynamicProperty2.Connect(object pSink)
        {
            m_pSink = (IDynamicPropertyNotify2)pSink;
        }

        void IDynamicProperty2.Disconnect()
        {
            m_pSink = null;
        }

        #endregion

        #region IAcPiPropertyDisplay methods

        public void GetCustomPropertyCtrl(
            object id, uint lcid, out string progId
        )
        {
            progId = "AcPEXCtl.AcPePropertyEditorText.16"; // di�er veri giri� tipleri i�in : ObjectARX 2017\inc-x64\acpexctl.h dosyas�na bak�n�z
        }
        public void GetPropertyWeight(object id, out int propertyWeight)
        {
            propertyWeight = 0;
        }
        public void GetPropertyIcon(object id, out object icon)
        {
            icon = null;
        }
        public void GetPropTextColor(object id, out uint textColor)
        {
            textColor = 0;
        }
        public void IsFullView(
            object id, out bool visible, out uint integralHeight
        )
        {
            visible = false;
            integralHeight = 1;
        }
        #endregion
        #region IAcPiCategorizeProperties Methods
        public void GetCategoryDescription(int categoryId, uint lcid, out string categoryDescription)
        {
            categoryDescription = "Ege GY Parsel3D Uygulamas�";
        }
        public void GetCategoryName(int categoryId, uint lcid, out string categoryName)
        {
            categoryName = "Parsel3D �zellikleri";
        }
        public void GetCategoryWeight(int categoryId, out int categoryWeight)
        {
            categoryWeight = 0;
        }
        public void GetCommandButtons(int categoryId, out object commandButtons)
        {
            commandButtons = null;
        }
        public void GetParentCategory(int categoryId, out int parentCategoryId)
        {
            parentCategoryId = 0;
        }
        public void GetUniqueID(out string value)
        {
            value = "parsel3d_properties";
        }
        public void MapPropertyToCategory(int dispatchId, out int categoryId)
        {
            categoryId = 1;
        }
        #endregion
    }
    public class NoteProperty : IDynamicProperty2, IAcPiCategorizeProperties//, IAcPiPropertyDisplay
    {
        private IDynamicPropertyNotify2 m_pSink = null;
        //private string _barcodestr="";

        #region IDynamicProperty2 methods
        // Unique property ID
        void IDynamicProperty2.GetGUID(out Guid propGUID)
        {
            propGUID = new Guid("479181EF-5E5D-42D1-B75A-06873E5FAE57");
    }

        // Property display name
        void IDynamicProperty2.GetDisplayName(out string szName)
        {
            szName = "Notlar";
        }

        // Show/Hide property in the OPM, for this object instance
        void IDynamicProperty2.IsPropertyEnabled(object pUnk, out int bEnabled)
        {
            bEnabled = 1;
        }

        // Is property showing but disabled
        void IDynamicProperty2.IsPropertyReadOnly(out int bReadonly)
        {
            bReadonly = 0;
        }

        // Get the property description string
        void IDynamicProperty2.GetDescription(out string szName)
        {
            szName = "Ba��ms�z b�l�m, kat, parsel vb. ile ilgili notlar, a��klamalar";
        }

        // OPM will typically display these in an edit field
        // optional: meta data representing property type name,
        // ex. ACAD_ANGLE
        void IDynamicProperty2.GetCurrentValueName(out string szName)
        {
            throw new System.NotImplementedException();
        }

        // What is the property type, ex. VT_R8
        void IDynamicProperty2.GetCurrentValueType(out ushort varType)
        {
            // The Property Inspector supports the following data
            // types for dynamic properties:
            //VT_I2(2), VT_I4(3), VT_R4(4), VT_R8(5),
            // VT_BSTR (8), VT_BOOL (11), and VT_USERDEFINED (29).  

            //varType = 3; // VT_I4
            varType = 8; // VT_BSTR
        }

        // Get the property value, passes the specific object
        // we need the property value for.
        void IDynamicProperty2.GetCurrentValueData(object pUnk, ref object pVarData)
        {
            AcadObject obj = (AcadObject)pUnk;

            if (obj != null)
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                using (Transaction tr = doc.TransactionManager.StartTransaction())
                {
                    ObjectId objectId = new ObjectId((IntPtr)obj.ObjectID);
                    DBObject dbobject = tr.GetObject(objectId, OpenMode.ForRead);
                    XdataReadWrite.GetXdata(dbobject);
                    pVarData = XdataReadWrite._notestr;
                    //List<string> parcelInf = new List<string>();
                    //parcelInf = XDataHelpers.ParcelInfoForObject(tr, objectId);
                    //if (parcelInf.Count > 1)
                    //    pVarData = parcelInf[1];
                    //else
                    //    pVarData = "";
                }
            }
            else
                pVarData = "";
        }

        // Set the property value, passes the specific object we
        // want to set the property value for
        void IDynamicProperty2.SetCurrentValueData(object pUnk, object varData)
        {
            // TODO: Save the value returned to you

            // Because we said the value type was a 32b int (VT_I4)
            //int myVal = (int)varData;
            //_barcodestr = varData.ToString();

            AcadObject obj = (AcadObject)pUnk;

            if (obj != null)
            {
                Document doc = Application.DocumentManager.MdiActiveDocument;
                DocumentLock dl = doc.LockDocument(DocumentLockMode.ProtectedAutoWrite, null, null, true);
                Autodesk.AutoCAD.DatabaseServices.Database db = doc.Database;

                using (dl)
                {
                    //List<string> parcelInf = new List<string>();
                    ObjectId objectId = new ObjectId((IntPtr)obj.ObjectID);
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        DBObject dbobject = tr.GetObject(objectId, OpenMode.ForWrite);
                        XdataReadWrite._notestr = varData as string;
                        XdataReadWrite.SetXdata(dbobject);
                        tr.Commit(); // bunu yapmazsak de�i�ikliklikler i�lenmiyor.
                    }

                    //// Note k�sm� objeden okunuyor, SetParcelInfoOnObject'e parametre olarak tekrar ge�ilecek.
                    //parcelInf = XDataHelpers.ParcelInfoForObject(objId);

                    //if (parcelInf.Count > 0)
                    //{
                    //    //System.Windows.MessageBox.Show("parcelInf[1]:" + parcelInf[1]);
                    //    XDataHelpers.SetParcelInfoOnObject(objId, parcelInf[0], (string)varData);
                    //}
                }
            }
        }

        // OPM passes its implementation of IDynamicPropertyNotify, you
        // cache it and call it to inform OPM your property has changed
        void IDynamicProperty2.Connect(object pSink)
        {
            m_pSink = (IDynamicPropertyNotify2)pSink;
        }

        void IDynamicProperty2.Disconnect()
        {
            m_pSink = null;
        }

        #endregion

        #region IAcPiPropertyDisplay methods

        public void GetCustomPropertyCtrl(
            object id, uint lcid, out string progId
        )
        {
            progId = "AcPEXCtl.AcPePropertyEditorText.16"; // di�er veri giri� tipleri i�in : ObjectARX 2017\inc-x64\acpexctl_i.c dosyas�na bak�n�z
        }
        public void GetPropertyWeight(object id, out int propertyWeight)
        {
            propertyWeight = 0;
        }
        public void GetPropertyIcon(object id, out object icon)
        {
            icon = null;
        }
        public void GetPropTextColor(object id, out uint textColor)
        {
            textColor = 0;
        }
        public void IsFullView(
            object id, out bool visible, out uint integralHeight
        )
        {
            visible = false;
            integralHeight = 1;
        }
        #endregion
        #region IAcPiCategorizeProperties Methods
        public void GetCategoryDescription(int categoryId, uint lcid, out string categoryDescription)
        {
            categoryDescription = "Ege GY Parsel3D Uygulamas�";
        }
        public void GetCategoryName(int categoryId, uint lcid, out string categoryName)
        {
            categoryName = "Parsel3D �zellikleri";
        }
        public void GetCategoryWeight(int categoryId, out int categoryWeight)
        {
            categoryWeight = 0;
        }
        public void GetCommandButtons(int categoryId, out object commandButtons)
        {
            commandButtons = null;
        }
        public void GetParentCategory(int categoryId, out int parentCategoryId)
        {
            parentCategoryId = 0;
        }
        public void GetUniqueID(out string value)
        {
            value = "parsel3d_properties";
        }
        public void MapPropertyToCategory(int dispatchId, out int categoryId)
        {
            categoryId = 1;
        }
        #endregion
    }

    #endregion


    #region Application Entry Point
    public class MyEntryPoint : IExtensionApplication {
        protected internal BarcodeProperty barcodeProp = null;
        protected internal NoteProperty noteProp = null;

        public void Initialize () {
            Assembly.LoadFrom("asdkOPMNetExt.dll");

            Dictionary classDict =SystemObjects.ClassDictionary;
            //RXClass lineDesc =(RXClass)classDict.At("AcDbEntity");
            RXClass polylineDesc = (RXClass)classDict.At("AcDbPolyline");
            RXClass solidDesc = (RXClass)classDict.At("AcDb3dSolid");
            barcodeProp = new BarcodeProperty();
            noteProp = new NoteProperty();

            // Add the Dynamic Property for LWPOLYLINE
            IPropertyManager2 pPropMan =(IPropertyManager2)xOPM.xGET_OPMPROPERTY_MANAGER(polylineDesc);
            pPropMan.AddProperty((object)barcodeProp);
            pPropMan.AddProperty((object)noteProp);

            // Add the Dynamic Property for 3DSOLID
            pPropMan = (IPropertyManager2)xOPM.xGET_OPMPROPERTY_MANAGER(solidDesc);
            pPropMan.AddProperty((object)barcodeProp);
            pPropMan.AddProperty((object)noteProp);
        }

        public void Terminate () {
            // Remove the Dynamic Property for LWPOLYLINE
            Dictionary classDict =SystemObjects.ClassDictionary;
            //RXClass lineDesc =(RXClass)classDict.At("AcDbEntity");
            RXClass polylineDesc =(RXClass)classDict.At("AcDbPolyline");
            RXClass solidDesc =(RXClass)classDict.At("AcDb3dSolid");
            IPropertyManager2 pPropMan =(IPropertyManager2)xOPM.xGET_OPMPROPERTY_MANAGER(polylineDesc);

            pPropMan.RemoveProperty((object)barcodeProp);
            pPropMan.RemoveProperty((object)noteProp);

            // Remove the Dynamic Property for LWPOLYLINE
            pPropMan = (IPropertyManager2)xOPM.xGET_OPMPROPERTY_MANAGER(solidDesc);

            pPropMan.RemoveProperty((object)barcodeProp);
            pPropMan.RemoveProperty((object)noteProp);
            barcodeProp = null;
        }
    }
    #endregion
}
