﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Runtime;
using System.Linq;

namespace Parsel3D
{
    class BooleanSolids
    {
        public class Commands
        {
            static public void UniteSolids()
            {
                var doc = Application.DocumentManager.MdiActiveDocument;

                var psr =
                  SelectSolids(
                    doc.Editor,
                    "\nSelect solid objects to unite"
                  );
                if (psr.Status != PromptStatus.OK)
                    return;

                if (psr.Value.Count > 1)
                {
                    BooleanSolids(
                      doc, psr.Value[0].ObjectId, AllButFirst(psr),
                      BooleanOperationType.BoolUnite
                    );
                }
            }
            static public Solid3d UniteSolidsWithCopy(PromptSelectionResult psr)
            {
                var doc = Application.DocumentManager.MdiActiveDocument;

                string copymode = (string)doc.GetLispSymbol("COPYMODE");
                doc.Editor.Command("copy", "p", "\n", "mode", "single", "0,0", "0,0");
                doc.SetLispSymbol("COPYMODE", copymode);
                Solid3d ret = null;
                if (psr.Value.Count > 1)
                {
                    ret = BooleanSolids(
                      doc, psr.Value[0].ObjectId, AllButFirst(psr),
                      BooleanOperationType.BoolUnite
                    );
                }
                return ret;
            }

            static public void IntersectSolids()
            {
                var doc = Application.DocumentManager.MdiActiveDocument;

                var psr =
                  SelectSolids(
                    doc.Editor,
                    "\nSelect solid objects to intersect"
                  );
                if (psr.Status != PromptStatus.OK)
                    return;

                if (psr.Value.Count > 1)
                {
                    BooleanSolids(
                      doc, psr.Value[0].ObjectId, AllButFirst(psr),
                      BooleanOperationType.BoolIntersect
                    );
                }
            }

            static public void SubtractSolids()
            {
                var doc = Application.DocumentManager.MdiActiveDocument;
                var ed = doc.Editor;

                var first = SelectSingleSolid(ed, "\nSelect primary solid");
                if (first == ObjectId.Null)
                    return;

                var psr = SelectSolids(ed, "\nSelect solids to subtract");
                if (psr.Status != PromptStatus.OK)
                    return;

                if (psr.Value.Count > 0)
                {
                    BooleanSolids(
                      doc, first, psr.Value.GetObjectIds(),
                      BooleanOperationType.BoolSubtract
                    );
                }
            }

            private static ObjectId SelectSingleSolid(
              Editor ed, string prompt
            )
            {
                var peo = new PromptEntityOptions(prompt);
                peo.SetRejectMessage("\nMust be a 3D solid");
                peo.AddAllowedClass(typeof(Solid3d), false);

                var per = ed.GetEntity(peo);
                if (per.Status != PromptStatus.OK)
                    return ObjectId.Null;

                return per.ObjectId;
            }

            private static ObjectId[] AllButFirst(PromptSelectionResult psr)
            {
                // Use LINQ to skip the first item in the IEnumerable
                // and then return the results as an ObjectId array

                return
                  psr.Value.Cast<SelectedObject>().Skip(1).
                    Select(o => { return o.ObjectId; }).ToArray();
            }

            private static PromptSelectionResult SelectSolids(
              Editor ed, string prompt
            )
            {
                // Set up our selection to only select 3D solids

                var pso = new PromptSelectionOptions();
                pso.MessageForAdding = prompt;

                var sf =
                  new SelectionFilter(
                    new TypedValue[]
                    {
                        new TypedValue((int)DxfCode.Start, "3DSOLID")
                    }
                  );

                return ed.GetSelection(pso, sf);
            }

            private static Solid3d BooleanSolids(
              Document doc, ObjectId first, ObjectId[] others,
              BooleanOperationType op
            )
            {
                var tr = doc.TransactionManager.StartTransaction();
                using (tr)
                {
                    var sol = tr.GetObject(first, OpenMode.ForWrite) as Solid3d;

                    if (sol != null)
                    {
                        foreach (ObjectId id in others)
                        {
                            var sol2 = tr.GetObject(id, OpenMode.ForWrite) as Solid3d;

                            if (sol2 != null)
                            {
                                sol.BooleanOperation(op, sol2);
                            }
                        }
                    }
                    tr.Commit();
                    return sol;
                }
            }

            public static Solid3d BooleanSolids(
              Transaction tr, ObjectId first, ObjectId[] others,
              BooleanOperationType op
            )
            {
                    var sol = tr.GetObject(first, OpenMode.ForWrite) as Solid3d;

                    if (sol != null)
                    {
                        foreach (ObjectId id in others)
                        {
                            var sol2 = tr.GetObject(id, OpenMode.ForWrite) as Solid3d;

                            if (sol2 != null)
                            {
                                sol.BooleanOperation(op, sol2);
                            }
                        }
                    }
                    return sol;
            }
            //public static void AcDbSubDMeshfromAcDb3dSolid()
            //{
            //    var doc = Application.DocumentManager.MdiActiveDocument;
            //    var ed = doc.Editor;

            //    var first = SelectSingleSolid(ed, "\nSelect solid");
            //    if (first == ObjectId.Null)
            //        return;
            //    var tr = doc.TransactionManager.StartTransaction();
            //    using (tr)
            //    {
            //        var sol = tr.GetObject(first, OpenMode.ForWrite) as Solid3d;
            //        if (sol != null)
            //        {
            //            FaceterSettings settings;
            //            Point3dCollection pts;

            //            AcArray faces;

            //            FaceData faceData;

            //            err = GetObjectMesh(ptrSolid, settings, pts, faces, faceData);

            //            if (faceData)

            //            {

            //                delete[] faceData->trueColors();

            //                delete[] faceData->materials();

            //                delete faceData;

            //            }
            //        }
            //        tr.Commit();
            //    }
            //}// AcDbSubDMeshfromAcDb3dSolid
        }
    }
}
