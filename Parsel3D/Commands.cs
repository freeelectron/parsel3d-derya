﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.GraphicsInterface;
using Autodesk.AutoCAD.Geometry;
using cadPolyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.AutoCAD.Windows;
using Autodesk.Windows;
using System.Windows.Media.Imaging;
using System.Drawing;
using Autodesk.AutoCAD.PlottingServices;
using Autodesk.AutoCAD.BoundaryRepresentation;
using System.IO;
using Region = Autodesk.AutoCAD.DatabaseServices.Region;
using System.Data.SQLite;
using System.Data.SqlClient;
using Parsel3D.V2.Forms;

[assembly: CommandClass(typeof(Parsel3D.Commands))]
[assembly: ExtensionApplication(typeof(Parsel3D.Initialization))]
[assembly: CommandClass(typeof(Parsel3D.EgeRibbonMenu))]

namespace Parsel3D
{
    public class EgeRibbonMenu
    {
        #region Ribbon Panel
        [CommandMethod("EgeRibbon")]
        public void EgeRibbon()
        {
            Autodesk.Windows.RibbonControl ribbonControl = Autodesk.Windows.ComponentManager.Ribbon;
            RibbonTab Tab = new RibbonTab
            {
                Title = "Ege Gayrımenkul",
                Id = "EGEGYRIBBON_TAB_ID"
            };

            //dynamic app = Application.AcadApplication;
            //// Iterate through all the menu groups
            //var mgs = app.MenuGroups;
            //foreach (var mg in mgs)
            //{
            //    // Iterate through a menu group's toolbars
            //    var tbs = mg.Toolbars;
            //    foreach (var tb in tbs)
            //    {
            //        if(tb.Id == Tab.Id) // tb.Id'de sıçıyor. öyle bir eleman yok hatası veriyor.
            //            return;
            //    }
            //}
            // ribbon zaten var mı diye bakılıyor.
            foreach (RibbonTab tab in ribbonControl.Tabs)
            {
                if (tab.Id == Tab.Id)
                    return;
            }
            ribbonControl.Tabs.Add(Tab);

            // create Ribbon panels
            Autodesk.Windows.RibbonPanelSource ParselPanelSource = new RibbonPanelSource
            {
                Title = "Parsel"
            };
            RibbonPanel ParselPanel = new RibbonPanel
            {
                Source = ParselPanelSource
            };

            Tab.Panels.Add(ParselPanel);

            RibbonButton ButtonParselBilgileri = new RibbonButton
            {
                Text = "Parsel\nBilgileri",
                ShowText = true,
                ShowImage = true,
                //            PanelParselButtonParselBilgileri.Image = Images.getBitmap(Parsel3d.Form);
                LargeImage = Images.getBitmap(Parsel3d.Form),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "BinaBilgi"
            };

            RibbonButton ButtonSayisallastir = new RibbonButton
            {
                Text = "Kat Planı\nSayısallaştırma",
                ShowText = true,
                ShowImage = true,
                //            ButtonSayisallastir.Image = Images.getBitmap(Parsel3d._3d_graph);
                LargeImage = Images.getBitmap(Parsel3d.plan_to_3d),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "BagSay"
            };

            //RibbonButton ButtonSayisallastir = new RibbonButton
            //{
            //    Text = "Kat Planı\nSayısallaştırma",
            //    ShowText = true,
            //    ShowImage = true,
            //    //            ButtonSayisallastir.Image = Images.getBitmap(Parsel3d._3d_graph);
            //    LargeImage = Images.getBitmap(Parsel3d.plan_to_3d),
            //    Orientation = System.Windows.Controls.Orientation.Vertical,
            //    Size = RibbonItemSize.Large,
            //    CommandHandler = new EgeRibbonCommandHandler(),
            //    CommandParameter = "KatPlanSay"
            //};

            //RibbonButton ButtonKatPlan3D = new RibbonButton
            //{
            //    Text = "Kat Planı\n3D Yap",
            //    ShowText = true,
            //    ShowImage = true,
            //    //            ButtonSayisallastir.Image = Images.getBitmap(Parsel3d._3d_graph);
            //    LargeImage = Images.getBitmap(Parsel3d.make3d),
            //    Orientation = System.Windows.Controls.Orientation.Vertical,
            //    Size = RibbonItemSize.Large,
            //    CommandHandler = new EgeRibbonCommandHandler(),
            //    CommandParameter = "KatPlan3D"
            //};

            RibbonButton ButtonExport = new RibbonButton
            {
                Text = "Bağ. Böl.\nExport",
                ShowText = true,
                ShowImage = true,
                //          ButtonExport.Image = Images.getBitmap(Parsel3d.Save_as);
                LargeImage = Images.getBitmap(Parsel3d.Save_as),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "BagExp"
            };

            RibbonButton ButtonSetElevation = new RibbonButton
            {
                Text = "Kotları\nAyarla",
                ShowText = true,
                ShowImage = true,
                //          ButtonExport.Image = Images.getBitmap(Parsel3d.Save_as);
                LargeImage = Images.getBitmap(Parsel3d.SeteElevation),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "SetElevation"
            };

            RibbonButton ButtonFaceExport = new RibbonButton
            {
                Text = "Face Export",
                ShowText = true,
                ShowImage = true,
                //          ButtonExport.Image = Images.getBitmap(Parsel3d.Save_as);
                LargeImage = Images.getBitmap(Parsel3d.face_export),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "FaceExport"
            };

            RibbonButton ButtonGorselAcKapa = new RibbonButton
            {
                Text = "Polyline Overrule\nAç Kapa",
                ShowText = true,
                ShowImage = true,
                //          ButtonGorselAcKapa.Image = Images.getBitmap(Parsel3d.Smooth_line);
                LargeImage = Images.getBitmap(Parsel3d.polyline_overrule),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "OvrAcKapa"
            };

            RibbonButton ButtonXdataRemove = new RibbonButton
            {
                Text = "XData Sil",
                ShowText = true,
                ShowImage = true,
                LargeImage = Images.getBitmap(Parsel3d.RemoveXData),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "XDataSil"
            };

            RibbonButton ButtonDataGoruntule = new RibbonButton
            {
                Text = "Data Görüntüle",
                ShowText = true,
                ShowImage = true,
                //          ButtonIncele.Image = Images.getBitmap(Parsel3d.Smooth_line);
                LargeImage = Images.getBitmap(Parsel3d.view_data),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "DataGor"
            };

            RibbonButton ButtonPlanExport = new RibbonButton
            {
                Text = "PlanExport\nPDF",
                ShowText = true,
                ShowImage = true,
                //          ButtonPlanExport.Image = Images.getBitmap(Parsel3d.Smooth_line);
                LargeImage = Images.getBitmap(Parsel3d.plan_to_pdf),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "PlanExport"
            };

            RibbonButton ButtonApiTest = new RibbonButton
            {
                Text = "ApiTest",
                ShowText = true,
                ShowImage = true,
                //          ButtonPlanExport.Image = Images.getBitmap(Parsel3d.Smooth_line);
                LargeImage = Images.getBitmap(Parsel3d.test),
                Orientation = System.Windows.Controls.Orientation.Vertical,
                Size = RibbonItemSize.Large,
                CommandHandler = new EgeRibbonCommandHandler(),
                CommandParameter = "ApiTest"
            };

            ParselPanelSource.Items.Add(ButtonParselBilgileri);
            ParselPanelSource.Items.Add(ButtonSayisallastir);
            ParselPanelSource.Items.Add(ButtonSetElevation);
            //            ParselPanelSource.Items.Add(ButtonKatPlan3D);
            ParselPanelSource.Items.Add(ButtonExport);
            ParselPanelSource.Items.Add(ButtonGorselAcKapa);
            ParselPanelSource.Items.Add(ButtonXdataRemove);
            ParselPanelSource.Items.Add(ButtonDataGoruntule);
            ParselPanelSource.Items.Add(ButtonPlanExport);
            ParselPanelSource.Items.Add(ButtonFaceExport);
            ParselPanelSource.Items.Add(ButtonApiTest);
            Tab.IsActive = true;
        }
        #endregion // Ribbon panel
        public class EgeRibbonCommandHandler : System.Windows.Input.ICommand
        {
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;

            public void Execute(object parameter)
            {
                if (parameter is RibbonCommandItem ribbonItem)
                {
                    Document dwg = Application.DocumentManager.MdiActiveDocument;

                    string cmdString = ((string)ribbonItem.CommandParameter).TrimEnd();
                    if (!cmdString.EndsWith(";"))
                    {
                        cmdString = cmdString + " ";
                    }

                    dwg.SendStringToExecute(cmdString, true, false, true);
                }
            }
        }

        public class Images
        {
            public static BitmapImage getBitmap(Bitmap image)
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                image.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = stream;
                bmp.EndInit();
                return bmp;
            }
        }
    } // Class EgeRibbonMenu

    public class Initialization : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        //static MyEntryPoint myEntryPoint = new MyEntryPoint();

        public void Initialize()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            ed.WriteMessage("Initializing - .");
            doc.SetLispSymbol("BINABILGILERIKONTROLEDILDI", "0");
            //doc.SetLispSymbol("POLYLINEDRAWOVERRULE", "1");
            doc.SendStringToExecute("OVERRULE1\n", true, false, false);
            doc.SendStringToExecute("EGERIBBON\n", true, false, false);
            //myEntryPoint.Initialize();
        } // Initialize

        public void Terminate()
        {
            Console.WriteLine("Parsel3D Temizlik...");
            //myEntryPoint.Terminate();
        }
    }

    public partial class Commands// : Autodesk.AutoCAD.Runtime.IExtensionApplication
    {
        static DataViewPalette dvp;

        public void Initialize()
        {
            DocumentCollection dm = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager;
            dm.DocumentCreated += new DocumentCollectionEventHandler(OnDocumentCreated);
            foreach (Document doc in dm)
            {
                //        doc.Editor.PointMonitor += new PointMonitorEventHandler(OnMonitorPoint);
            }
        }

        public void Terminate()
        {
            try
            {
                DocumentCollection dm = Application.DocumentManager;
                if (dm != null)
                {
                    Editor ed = dm.MdiActiveDocument.Editor;
                    //          ed.PointMonitor -= new PointMonitorEventHandler(OnMonitorPoint);
                }
            }
            catch (System.Exception)
            {
                // The editor may no longer
                // be available on unload
            }
        }

        private void OnDocumentCreated(
          object sender,
          DocumentCollectionEventArgs e
        )
        {
            //      e.Document.Editor.PointMonitor += new PointMonitorEventHandler(OnMonitorPoint);
        }

        //private void OnMonitorPoint(object sender, PointMonitorEventArgs e)
        //{
        //  FullSubentityPath[] paths = e.Context.GetPickedEntities();
        //  if (paths.Length <= 0)
        //  {
        //    tvp.SetObjectId(ObjectId.Null);
        //    return;
        //  };

        //  ObjectIdCollection idc = new ObjectIdCollection();
        //  foreach (FullSubentityPath path in paths)
        //  {
        //    // Just add the first ID in the list from each path
        //    ObjectId[] ids = path.GetObjectIds();
        //    idc.Add(ids[0]);
        //  }
        //  tvp.SetObjectIds(idc);
        //}


        //        private string _parcelinfo = "";
        private double _tabankotu = 0.0;
        private double _oncekitabankotu = 0.0;
        private double _katyuksekligi = 300.0;
        private double _oncekikatyuksekligi = 300.0;
        private int _bulunduguKat = 999;
        private Point3d _referansnoktasi;

        private ObjectIdCollection _idCollBagimsizBolumRegions = new ObjectIdCollection();
        private ObjectIdCollection _idCollKatRegions = new ObjectIdCollection();

        private List<cadPolyline> _polylines = new List<cadPolyline>();
        private List<cadPolyline> _floorPolylines = new List<cadPolyline>();
        private ParselBilgileri _parselBilgileri = new ParselBilgileri();
        private string _barcodeStr = "";

        //        private cadPolyline _floorPolyline = null;
        //private List<Tuple<string, Point3d>> _barcodeTexts = new List<Tuple<string, Point3d>>();
        private List<Tuple<string, Point3d>> _numberTexts = new List<Tuple<string, Point3d>>();
        private string _floortext = "";
       // private string _sayisallastirilmis_katlar = "";

        public void Overrule(bool enable)
        {
            // Regen to see the effect
            // (turn on/off Overruling and LWDISPLAY)

            DrawableOverrule.Overruling = enable;
            if (enable)
                Application.SetSystemVariable("LWDISPLAY", 1);
            else
                Application.SetSystemVariable("LWDISPLAY", 0);

            Document doc =
              Application.DocumentManager.MdiActiveDocument;
            doc.SendStringToExecute("REGEN3\n", true, false, false);
            doc.Editor.Regen();
        }

        [CommandMethod("PLANEXPORT")]
        public void PlanExportCommand()
        {
            var pe = new PlanExport();
            pe.PlanExportFunction();
            //var ppf = new PlanPlotForm();
            //ppf.ShowDialog();
        }

        [CommandMethod("ApiTest")]
        public void ApiTest()
        {
            MainForm projectInfoForm = new MainForm();
            projectInfoForm.Show();
        }

        [CommandMethod("DataGor")]
        public void ViewData()
        {
            if (dvp == null)
            {
                dvp = new DataViewPalette();
            }
            dvp.Show();
        }
        [CommandMethod("OVRACKAPA")]
        public void OverruleOnOff()
        {
            string polylineOverrule = (string)Application.DocumentManager.MdiActiveDocument.GetLispSymbol("POLYLINEDRAWOVERRULE");
            if (polylineOverrule == "0")
            {
                OverruleStart();
            }
            else
            {
                OverruleEnd();
            }
        }
        [CommandMethod("OVERRULE1")]
        public void OverruleStart()
        {
            ObjectOverrule.AddOverrule(
              RXClass.GetClass(typeof(Drawable)),
              DrawOverrule.theOverrule,
              true
            );
            Overrule(true);
            Application.DocumentManager.MdiActiveDocument.SetLispSymbol("POLYLINEDRAWOVERRULE", "1");
            Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\nOverrule Açıldı.");
        } // OverruleStart

        [CommandMethod("OVERRULE0")]
        public void OverruleEnd()
        {
            ObjectOverrule.RemoveOverrule(
              RXClass.GetClass(typeof(Drawable)),
              DrawOverrule.theOverrule
            );
            Overrule(false);
            Application.DocumentManager.MdiActiveDocument.SetLispSymbol("POLYLINEDRAWOVERRULE", "0");
            Application.DocumentManager.MdiActiveDocument.Editor.WriteMessage("\nOverrule Kapatıldı.");
        } // OverruleEnd

        [CommandMethod("XDATASIL")]
        public void RemoveAllXDataFromEntity_Command()
        {
            XDataHelpers.RemoveAllXDataFromEntities_Method();
        } // RemoveAllXDataFromEntities_Command

        [CommandMethod("SetElevation")]
        public void SetElevation()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptPointOptions opt = new PromptPointOptions("\nKotu belli olan noktayı gösteriniz : ")
            {
                AllowNone = false
            };
            PromptPointResult res = ed.GetPoint(opt);

            if (res.Status != PromptStatus.OK)
                return;
            var kotnok = new Point3d(res.Value.X, res.Value.Y, res.Value.Z);
            var ondulasyon = Ondulasyon.CalculateOndulation(kotnok);

            if(ondulasyon > -0.001)
            {
                System.Windows.MessageBox.Show("Nokta koordinatında bir hata olabilir.\nNokta İstanbul il sınırları içinde kalmalıdır.", "Ondülasyon hesaplanamadı!");
                return;
            }
            ed.WriteMessage($"\nBu noktadaki Ondülasyon={ondulasyon}");
            
            var tb = new ClassTapuBilgileri();
            tb = Utils.ReadFromDictionary();
            var elipsoidalkot = double.Parse(tb.BinaSifirKotu) + double.Parse(tb.Ondulasyon);
            PromptDoubleOptions pdo = new PromptDoubleOptions($"\nElipsoidal Kot? (m):")
            {
                DefaultValue = elipsoidalkot,
                UseDefaultValue = true
            };
            PromptDoubleResult pr = ed.GetDouble(pdo);
            if (pr.Status != PromptStatus.OK)
            {
                return;
            }
            double kot = pr.Value;

            PromptSelectionOptions pso = new PromptSelectionOptions
            {
                AllowDuplicates = false,
                MessageForAdding = "\nKotları ayarlanacak objeleri seçiniz: "
            };
            PromptSelectionResult promptSelectionResult = ed.GetSelection(pso);
            if (promptSelectionResult.Status != PromptStatus.OK)
                return;

            var osmode = Autodesk.AutoCAD.ApplicationServices.Core.Application.GetSystemVariable("OSMODE");
            Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("OSMODE", 0);
            ed.Command("_.MOVE", promptSelectionResult.Value, "", $"{kotnok.X},{kotnok.Y},{kotnok.Z}", $"{kotnok.X},{kotnok.Y},{kot}");
            Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("OSMODE", osmode);
        }

        private bool GetParcelObjects(Transaction tr)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;

            Utils.LayerlariOlustur();

            string tapuBilgileriKontrolu = (string)doc.GetLispSymbol("BINABILGILERIKONTROLEDILDI");
            if (tapuBilgileriKontrolu != "1")
            //if (false) // sayısallaştırma başlangıcında tapu bilgileri formu gösterme iptal edildi..
            {
                if (!Utils.BinaBilgileriFormuGoster())
                    return false;
            }
            // Parsel haline getirilecek objeler seçiliyor
            ObjectIdCollection selectionSetOfFloorPlan = Utils.SelectFloorPlanObjects();
            if (selectionSetOfFloorPlan.Count < 1) // hiç obje seçilmemişse..
                return false;

            PromptDoubleOptions pstro = new PromptDoubleOptions("\nTaban kotu (cm):");
            // Use the previous value, if if already called
            pstro.DefaultValue = _tabankotu + _katyuksekligi;
            pstro.UseDefaultValue = true;
            PromptDoubleResult pr = ed.GetDouble(pstro);
            // Return if something went wrong
            if (pr.Status == PromptStatus.OK)
            {
                _oncekitabankotu = _tabankotu;
                _tabankotu = pr.Value;
            }
            else
                return false;

            PromptDoubleOptions doubleOpt = new PromptDoubleOptions("\nKat yüksekliği (cm):");
            doubleOpt.DefaultValue = _katyuksekligi;
            doubleOpt.UseDefaultValue = true;
            doubleOpt.AllowNegative = false;
            doubleOpt.AllowZero = false;
            doubleOpt.AllowNone = true;
            PromptDoubleResult doubres = ed.GetDouble(doubleOpt);
            if (doubres.Status == PromptStatus.OK)
            {
                _oncekikatyuksekligi = _katyuksekligi;
                _katyuksekligi = doubres.Value;
            }
            else
                return false;

            // Set up the selection options
            PromptPointOptions opt = new PromptPointOptions("\nOrtak referans noktasını gösteriniz : ");
            opt.AllowNone = false;
            PromptPointResult res = ed.GetPoint(opt);

            if (res.Status != PromptStatus.OK)
                return false;
            _referansnoktasi = new Point3d(res.Value.X, res.Value.Y, 0.0);

            // önceki 'List'ler temizleniyor
            _polylines.Clear();
            //_barcodeTexts.Clear();
            _numberTexts.Clear();
            _floorPolylines.Clear();
            _floortext = null;
            // Use a transaction to edit our various objects
            // Loop through the selected objects
            foreach (ObjectId objectId in selectionSetOfFloorPlan)
            {
                DBObject obj = tr.GetObject(objectId, OpenMode.ForWrite);

                if (obj is cadPolyline)
                {
                    cadPolyline pl = obj as cadPolyline;

                    pl.Elevation = _tabankotu;
                    if (pl.Layer == Utils.LAYERADI_KAT)
                        _floorPolylines.Add(pl);
                    else
                        _polylines.Add(pl);
                }
                else if (obj is DBText)
                {
                    DBText tx = obj as DBText;
                    //yazıları kat ile aynı kota getiriyoruz çünkü ilerde hangi region'un içine girdiğini bulmak istiyoruz.
                    tx.Position = new Point3d(tx.Position.X, tx.Position.Y, _tabankotu);

                    if (tx.TextString.ToUpper().Contains("KAT"))
                        _floortext = tx.TextString;
                    else //if (tx.Height > 60) // yazı yüksekliği 60'tan büyük yazılar bağımsız bölüm numarası kabul ediliyor
                        _numberTexts.Add(new Tuple<string, Point3d>(tx.TextString, tx.Position));
                    // else
                    //     _barcodeTexts.Add(new Tuple<string, Point3d>(tx.TextString, tx.Position));
                }
                else if (obj is MText)
                {
                    MText tx = obj as MText;
                    //yazıları kat ile aynı kota getiriyoruz çünkü ilerde hangi region'un içine girdiğini bulmak istiyoruz.
                    tx.Location = new Point3d(tx.Location.X, tx.Location.Y, _tabankotu);

                    //System.Windows.MessageBox.Show("contents:" + tx.Contents + " text:" + tx.Text);
                    //if (tx.Contents.ToUpper().Contains("KAT"))
                    if (tx.Text.ToUpper().Contains("KAT"))
                    {
                        string[] spl = tx.Text.Split(';');
                        _floortext = spl.Length > 1 ? spl[1] : spl[0];
                        //System.Windows.MessageBox.Show(spl[0] + "+" + spl[1]);
                    }
                    else //if (tx.Height > 60) // yazı yüksekliği 60'tan büyük yazılar bağımsız bölüm numarası kabul ediliyor
                    {
                        _numberTexts.Add(new Tuple<string, Point3d>(tx.Contents, tx.Location));
                    }
                    //else
                    //    _barcodeTexts.Add(new Tuple<string, Point3d>(tx.Contents, tx.Location));
                }
            } // foreach
              //if (_floortext == "")
              //{
              //    System.Windows.MessageBox.Show("Kaçıncı kat olduğu anlaşılamadı! " +
              //        "\n\nKat yazıları aşağıdaki formata uymalıdır." +
              //        "\n\nÖrneğin:" +
              //        "\n2. Kat Planı\"" +
              //        "\n3. Kat" +
              //        "\n1. Bodrum Kat Planı" +
              //        "\n2. Bodrum kat" +
              //        "\nZemin Kat Planı" +
              //        "\ngibi."
              //        );
              //    return false;
              //}
              //else
              //{
            try
            {
                //System.Windows.MessageBox.Show("floortext:" + _floortext);
                if (_floortext.ToUpper().Contains("ZEM")) // zemin kat ise..
                {
                    _bulunduguKat = 0;
                }
                else
                {
                    _bulunduguKat = int.Parse(_floortext.Split('.')[0].TrimEnd('.'));
                    if (_floortext.ToUpper().Contains("BODRUM") && _bulunduguKat > 0)
                        _bulunduguKat *= -1;
                }
                ed.WriteMessage("\nKat Yazısı: " + _floortext + " -> Okunan değer: " + _bulunduguKat.ToString());
            }
            catch
            {
                System.Windows.MessageBox.Show("Kaçıncı kat olduğu anlaşılamadı! " +
                                    "\n\nKat yazıları aşağıdaki formata uymalıdır." +
                                    "\n\nÖrneğin:" +
                                    "\n2. Kat Planı\"" +
                                    "\n-3. Kat" +
                                    "\n1. Bodrum Kat Planı" +
                                    "\n2. Bodrum kat" +
                                    "\nZemin Kat Planı" +
                                    "\ngibi."
                                    );
                return false;
            }

            if (_floorPolylines.Count < 1)
            {
                System.Windows.MessageBox.Show("Kat'ı gösteren polyline(lar) bulunamadı!" +
                    "\n" + Utils.LAYERADI_KAT + " layer'ında olmalıdır.");
                return false;
            }

            //}

            //if (_polylines.Count != _numberTexts.Count)
            //{
            //    System.Windows.MessageBox.Show(_polylines.Count.ToString() +
            //        " adet polyline,\n" +
            //        _numberTexts.Count.ToString() + " adet numara yazısı var !");
            //    return false;
            //}
            return true;
        } // GetParcelObjects Method End

        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        private void CreateRegions()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;

            short delobj = (short)Application.GetSystemVariable("DELOBJ");
            Application.SetSystemVariable("DELOBJ", 0); // retain

            //Utils.SetVisualStyle("Conceptual");
            // Always dispose DocumentLock
            using (doc.LockDocument())
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                if (!GetParcelObjects(tr))
                {
                    _tabankotu = _oncekitabankotu;
                    _katyuksekligi = _oncekikatyuksekligi;
                    //return;
                }
                else
                { // aranamadmamamad
                    // No need to dispose a DBObject opened from a transaction
                    BlockTableRecord currentSpace =
                        (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                    // Dispose DBObjectCollection in case there're some objects left 
                    // for which no managed wrapper have been created
                    using (DBObjectCollection plineCollection1 = new DBObjectCollection())
                    {
                        // bağımsız bölüm ve kat polyline'ları birlikte işleniyor.
                        // (ayrı ayrı işlendiğinde extrude komutu ile solid oluştururken ayrık region'larda
                        // ayrı ayrı solid oluşturduğundan ve onlar da handle edilemediğinden bağımsız bölüm gibi işleniyor.

                        var allPolylines = new List<cadPolyline>[] { _polylines, _floorPolylines };
                        int i = 0; // kat ve bb collection'larını ayırt etmek için.
                        foreach (List<cadPolyline> plist in allPolylines)
                        {
                            foreach (cadPolyline pl1 in plist)
                            {
                                plineCollection1.Add(pl1);
                                using (DBObjectCollection regionCollection1 = Region.CreateFromCurves(plineCollection1))
                                {
                                    Region reg1 = regionCollection1[0] as Region;

                                    foreach (cadPolyline pl2 in plist)
                                    {
                                        if (pl1.ObjectId != pl2.ObjectId &&
                                            Utils.PolylineInPolyline(pl2, pl1) &&
                                            !Utils.IsPolylinesSame(pl2, pl1))
                                        {
                                            using (DBObjectCollection plineCollection2 = new DBObjectCollection())
                                            {
                                                plineCollection2.Add(pl2);
                                                using (DBObjectCollection regionCollection2 = Region.CreateFromCurves(plineCollection2))
                                                {
                                                    Region reg2 = regionCollection2[0] as Region;
                                                    reg1.BooleanOperation(BooleanOperationType.BoolSubtract, reg2);
                                                }
                                            }
                                        }
                                    } // foreach pl2
                                    reg1.Layer = pl1.Layer; // region current layer'da oluşuyor
                                    ObjectId reg1id = currentSpace.AppendEntity(reg1);
                                    if (i == 0)
                                        _idCollBagimsizBolumRegions.Add(reg1id);
                                    else
                                        _idCollKatRegions.Add(reg1id);

                                    tr.AddNewlyCreatedDBObject(reg1, true);
                                } // using regionCollection1
                                plineCollection1.Clear();
                            } // foreach pl1
                            i++;
                        }// foreach plist
                    }// using plineCollection1

                    tr.Commit();
                } // else
            }// using
            Application.SetSystemVariable("DELOBJ", delobj);
        } // CreateRgions Methos
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        private bool SetParselBilgileri(Transaction tr, Region bbReg, string numberStr)
        {
            _parselBilgileri.BagimsizBolumNo = numberStr;
            _parselBilgileri.BagimsizBolumTabanKotu_Mimari = _tabankotu / 100.0;
            _parselBilgileri.BagimsizBolumKatYuksekligi = _katyuksekligi / 100.0;
            _parselBilgileri.BagimsizBolumBulunduguKat = _bulunduguKat;
            _parselBilgileri.TapuBilgileri = Utils.ReadFromDictionary();

            _parselBilgileri.BagimsizBolumTabanKotu_DenizdenYukseklik =
            _parselBilgileri.BagimsizBolumTabanKotu_Mimari +
            Convert.ToDouble(_parselBilgileri.TapuBilgileri.BinaSifirKotu);
            _parselBilgileri.BarkodNo = _barcodeStr = Utils.BarkodOlustur(ref _parselBilgileri, Utils.BarkodTuru.BagimsizBolumBarkodu);// BarkodOlustur fonk içinde bağımsız türü de tespit ediliyor.
                                                                                                                                       //System.Windows.MessageBox.Show("tab.denizdenyuk:" + pb.BagimsizBolumTabanKotu_DenizdenYukseklik.ToString() + "binasifirkotu:" + pb.TapuBilgileri.BinaSifirKotu);
            var duplicatedObjectControl = Utils.SearchBarcodeDuplication(tr, ref _parselBilgileri);
            if (!duplicatedObjectControl)
            {
                return false;
            }
            _parselBilgileri.WriteToAutoCadObject(tr, bbReg);
            return true;
        } // SetParselBilgileri
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------

        //[CommandMethod("ptest")]
        public void ptest()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptSelectionOptions pso = new PromptSelectionOptions
            {
                //AllowDuplicates = false,
                SingleOnly = true,
                SinglePickInSpace = true,
                MessageForAdding = "\nSolid'i seçiniz (3DSOLID): "
            };
            TypedValue[] tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Start,"3DSOLID"),
                };

            SelectionFilter selectionfilter = new SelectionFilter(tvs);

            PromptSelectionResult selRes = doc.Editor.GetSelection(pso, selectionfilter);

            // If the user didn't make valid selection, we return

            if (selRes.Status != PromptStatus.OK)
                return;
            SelectionSet ssSolid = selRes.Value;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (SelectedObject o in ssSolid)
                {
                    Solid3d solid = tr.GetObject(o.ObjectId, OpenMode.ForRead) as Solid3d;
                    List<Point3dCollection> plist = Utils.GetFloorCoordinatesFromSolid3d(solid);
                    int i = 0, j = 0;
                    foreach (Point3dCollection pcoll in plist)
                    {
                        j = 0;
                        foreach (Point3d p in pcoll)
                        {
                            ed.WriteMessage("\n[" + i + "," + j + "] " + p.ToString());
                            j++;
                        }
                    }
                }
            }
        } // ptest method

        internal bool ParselBilgileriGirilmisMi(ParselBilgileri pb)
        {
            //System.Windows.MessageBox.Show("UNDO yaptığınız için bina bilgileri silindi.");
            return !string.IsNullOrEmpty(pb.TapuBilgileri.BinaSifirKotu);
        }

        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------------------
        [CommandMethod("BagSay")]
        public void KatPlaniSayisallastir()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            bool layerkontrol = Utils.LayerlariOlustur();
            if (!layerkontrol)
            {
                var page = new Autodesk.Windows.TaskDialog()
                {
                    MainIcon = Autodesk.Windows.TaskDialogIcon.Warning,
                    WindowTitle = "Uyarı",
                    MainInstruction = "Sayısallaştırma için gerekli bazı layerların önceden oluşturulmadığı görülüyor.",
                    ContentText = "Gerekli 'layer'lar oluşturuldu.\n",
                    UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                    FooterText = "Sayısallaştırmada kullanılacak LWPOLYLINE'lar ilgili 'layer'ında olmalıdır."
                    + $"\nKatlar \t\t\t\t: {Utils.LAYERADI_KATPLANI}"
                    + $"\nBağımsız Bölümler \t: {Utils.LAYERADI_BAGIMSIZBOLUM}"
                    + "\n\nLütfen kontrol edip gerekli düzeltmeleri yaptıktan sonra sayısallaştırmaya başlayınız.",

                    FooterIcon = Autodesk.Windows.TaskDialogIcon.Information,
                    //ExpandFooterArea = true,
                    //ExpandedText = "\nDubleks, tripleks vb. bağımsız bölümlerde barkod numarası aynı olmak zorundadır."
                    //+ "\nBağımsız bölüm geometrilerinin birleştirilebilmesi için bu gereklidir.",
                    Buttons =
                            {
                                new Autodesk.Windows.TaskDialogButton(1, "&Tamam"),
                                //new Autodesk.Windows.TaskDialogButton(2, "&Vazgeç"),
                            }
                };
                page.Show(Application.MainWindow.Handle);
            }

            _idCollBagimsizBolumRegions.Clear();
            _idCollKatRegions.Clear();
            _numberTexts.Clear();
//            _parselBilgileri = new ParselBilgileri();
            _parselBilgileri.TapuBilgileri = Utils.ReadFromDictionary();
            if (!ParselBilgileriGirilmisMi(_parselBilgileri))
            {
                //doc.SetLispSymbol("BINABILGILERIKONTROLEDILDI", "0");
                if (!Utils.BinaBilgileriFormuGoster())
                    return;
            }

            CreateRegions();

                using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                foreach (ObjectId id in _idCollBagimsizBolumRegions)
                {
                    var reg = tr.GetObject(id, OpenMode.ForWrite) as Region;
                    //// Create the sub-entity path to it
                    //var ids = new ObjectId[] { id };
                    //var path =
                    //  new FullSubentityPath(
                    //    ids, new SubentityId(SubentityType.Null, IntPtr.Zero)
                    //  );
                    using (Autodesk.AutoCAD.BoundaryRepresentation.Brep brepEnt = new Brep(reg))
                    {
                        // numara yazıları tek tek hangi region'ın içine giriyor ona bakılıyor.
                        foreach (Tuple<string, Point3d> tup in _numberTexts)
                        {
                            string numberStr = tup.Item1;
                            PointContainment pointCont = new PointContainment();
                            brepEnt.GetPointContainment(tup.Item2, out pointCont);
                            //ed.WriteMessage("\npointcont:" + pointCont.ToString() + "  x,y :" + tup.Item2.ToString());
                            if (pointCont == PointContainment.Inside ||
                                pointCont == PointContainment.OnBoundary)
                            {
                                _parselBilgileri.BagimsizBolumNotlar = ""; // bunu buraya koymayınca "Çok katlı b. bölüm" notu diğer regionlara sarkıyor.
                                if (!SetParselBilgileri(tr, reg, numberStr))
                                {
                                    _katyuksekligi = _oncekikatyuksekligi;
                                    _tabankotu = _oncekitabankotu;
                                    return;
                                }
                                //XDataHelpers.SetParcelInfoOnObject(tr, reg.ObjectId, _barcodeStr, numberStr);
                                break;
                            }
                        }// foreach tuple
                    } // using brepEnt
                }// foreach id
                tr.Commit();
            }
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                BlockTableRecord currentSpace =
                    (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);

                // bağımsız bölüm regionları kopyalanıp solid haline getiriliyor
                foreach (ObjectId id in _idCollBagimsizBolumRegions)
                {
                    using (var reg = tr.GetObject(id, OpenMode.ForWrite) as Region)
                    {
                        // Region 0,0 noktasına kopyalanıyor
                        Entity regEnt = Utils.CopyObject(reg as Entity, _referansnoktasi, new Point3d(0, 0, 0), Utils.LAYERADI_BAGIMSIZBOLUM);
                        // kopyalanan region "extrude" işlemiyle 3DSOLID haline getiriliyor.
                        using (Solid3d solid = new Solid3d())
                        {
                            solid.Extrude(regEnt as Region, _katyuksekligi, 0.0);
                            currentSpace.AppendEntity(solid);
                            tr.AddNewlyCreatedDBObject(solid, true);
                            _parselBilgileri.ReadFromAutoCadObject(reg);
                            _parselBilgileri.WriteToAutoCadObject(tr, solid);

                            //solid.Layer = layerName;
                        }
                    }
                }// foreach id

                // kat region'ları kopyalanıp solid haline getiriliyor
                foreach (ObjectId id in _idCollKatRegions)
                {
                    using (var reg = tr.GetObject(id, OpenMode.ForWrite) as Region)
                    {
                        // Region 0,0 noktasına kopyalanıyor
                        Entity regEnt = Utils.CopyObject(reg as Entity, _referansnoktasi, new Point3d(0, 0, 0), Utils.LAYERADI_KAT);
                        // kopyalanan region "extrude" işlemiyle 3DSOLID haline getiriliyor.
                        using (Solid3d solid = new Solid3d())
                        {
                            solid.Extrude(regEnt as Region, _katyuksekligi, 0.0);
                            currentSpace.AppendEntity(solid);
                            tr.AddNewlyCreatedDBObject(solid, true);
                            var katpb = _parselBilgileri;
                            katpb.BarkodNo = Utils.BarkodOlustur(ref katpb, Utils.BarkodTuru.KatBarkodu);
                            katpb.WriteToAutoCadObject(tr, solid);
                            //solid.Layer = layerName;
                        }
                    }
                }// foreach id
                tr.Commit();
            }// using tr
        } // KatPlanıSayisallastir Method

        //[CommandMethod("BagSay", CommandFlags.UsePickSet)]
        public void MakeParcel()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            _polylines.Clear();
            _floorPolylines.Clear();
            _numberTexts.Clear();
            ParselBilgileri pb = new ParselBilgileri();

            // Always dispose DocumentLock
            using (doc.LockDocument())

            // Always dispose Transaction
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                if (!GetParcelObjects(tr))
                    return;
                // numara yazıları tek tek hangi polyline'ın içine giriyor ona bakılıyor.
                foreach (Tuple<string, Point3d> tup in _numberTexts)
                {
                    string numberStr = tup.Item1;
                    //Utils.ShowMessage("--numberstr--" + numberStr);
                    //if (!GetBagimsizBolumRegion(tup.Item2.X, tup.Item2.Y))
                    //{
                    //    Utils.ShowMessage("Hata", "Bağımsız Bölüm Region oluşturulamadı", "");
                    //    return;
                    //}
                    //                    Utils.ShowMessage("--B--");
                    //
                    //
                    //                    //DBObject obj = tr.GetObject(ilgiliPoly.ObjectId, OpenMode.ForWrite);
                    //                    //cadPolyline poly = obj as cadPolyline;
                    //                    //Application.SetSystemVariable("CLAYER", Utils.LAYERADI_BAGIMSIZBOLUM);
                    //
                    //                    pb.BagimsizBolumNo = numberStr;
                    //                    pb.BagimsizBolumTabanKotu_Mimari = _tabankotu / 100.0;
                    //                    pb.BagimsizBolumKatYuksekligi = _katyuksekligi / 100.0;
                    //                    //System.Windows.MessageBox.Show("floortext:" + _floortext);
                    //                    if (_floortext.ToUpper().Contains("ZEM")) // zemin kat ise..
                    //                    {
                    //                        pb.BagimsizBolumBulunduguKat = 0;
                    //                    }
                    //                    else
                    //                    {
                    //                        pb.BagimsizBolumBulunduguKat = int.Parse(_floortext.Split('.')[0].TrimEnd('.'));
                    //                        if (_floortext.ToUpper().Contains("BODRUM") && pb.BagimsizBolumBulunduguKat > 0)
                    //                            pb.BagimsizBolumBulunduguKat *= -1;
                    //                    }
                    //                    pb.TapuBilgileri = Utils.ReadFromDictionary();
                    //                    pb.BagimsizBolumTabanKotu_DenizdenYukseklik =
                    //                    pb.BagimsizBolumTabanKotu_Mimari +
                    //                    Convert.ToDouble(pb.TapuBilgileri.BinaSifirKotu);
                    //                    pb.BarkodNo = barcodeStr = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.BagimsizBolumBarkodu);// BarkodOlustur fonk içinde bağımsız türü de tespit ediliyor.
                    //                    //System.Windows.MessageBox.Show("tab.denizdenyuk:" + pb.BagimsizBolumTabanKotu_DenizdenYukseklik.ToString() + "binasifirkotu:" + pb.TapuBilgileri.BinaSifirKotu);
                    //                    var duplicatedObjectId = Utils.SearchBarcodeDuplication(tr, barcodeStr);
                    //                    if (duplicatedObjectId != ObjectId.Null)
                    //                    {
                    //                        var page = new Autodesk.Windows.TaskDialog()
                    //                        {
                    //                            MainIcon = Autodesk.Windows.TaskDialogIcon.Warning,
                    //                            WindowTitle = "Uyarı",
                    //                            MainInstruction = string.Format("{0} ({1})\nBu barkoda sahip bir bağımsız bölüm zaten var",
                    //                            pb.BagimsizBolumNo, barcodeStr),
                    //                            ContentText = "Ne yapmak istersiniz?\n",
                    //                            UseCommandLinks = true, // button olarak mı satır olarak mı görünecek onu belirler.
                    //                            FooterText = "Not: Bağımsız bölüm barkod numaraları benzersiz (unique) olmalıdır. "
                    //                            + "\nDubleks, tripleks vb. bağımsız bölümlerde barkod numarası aynı olmak zorundadır."
                    //                            + "\nBağımsız bölüm geometrilerinin birleştirilebilmesi için bu gereklidir.",
                    //
                    //                            FooterIcon = Autodesk.Windows.TaskDialogIcon.Information,
                    //                            //ExpandFooterArea = true,
                    //                            //ExpandedText = "\nDubleks, tripleks vb. bağımsız bölümlerde barkod numarası aynı olmak zorundadır."
                    //                            //+ "\nBağımsız bölüm geometrilerinin birleştirilebilmesi için bu gereklidir.",
                    //                            Buttons =
                    //                                {
                    //                                    new Autodesk.Windows.TaskDialogButton(1, "&Devam et \nDubleks, tripleks vb. durumu. Export sırasında birleştirilir."),
                    //                                    new Autodesk.Windows.TaskDialogButton(2, "&Vazgeç"),
                    //                                }
                    //                        };
                    //                        var result = page.Show(Application.MainWindow.Handle);
                    //                        if (result != 1)
                    //                        {
                    //                            //break; // foreach
                    //                            return;
                    //                        }
                } // foreach


                //            Utils.ShowMessage("--C--");
                //            using (Transaction tr = db.TransactionManager.StartTransaction())
                //            {
                //
                //                //                XDataHelpers.SetParcelInfoOnObject(poly.ObjectId, barcodeStr, numberStr);
                //                //ViewData();
                //
                //                //              ObjectId bagimsizBolumSolidId = Utils.CreateSolidFromPolylines(listBagimsizBolumPolylines, _referansnoktasi, _katyuksekligi, Utils.LAYERADI_BAGIMSIZBOLUM, Utils.SolidTuru.BagimsizSolid);
                //
                //                //System.Windows.MessageBox.Show("--4--" + listBagimsizBolumPolylines.Count);
                //                //ObjectId Id = Utils.Create3DFloor(poly, _referansnoktasi, _katyuksekligi);
                //
                //                DBObject dbobj = tr.GetObject(_bagimsizBolumRegion.ObjectId, OpenMode.ForWrite);
                //                pb.WriteToAutoCadObject(tr, dbobj);
                //
                //                //XDataHelpers.SetParcelInfoOnObject(tr, tr.GetObject(Id,OpenMode.ForWrite), barcodeStr, numberStr);
                //
                //                //dvp.AddData(new Tuple<ObjectId, string>(Id, barcodeStr));
                //                // Bağımsız bölümler birleştirilip kata ait 3dSolid oluşturuluyor.
                //                //                ObjectId katSolidId = Utils.SolidleriBirlestir(floorObjectIdArray, Utils.LAYERADI_KAT).ObjectId;
                //                ObjectId katSolidId = Utils.CreateSolidFromPolylines(_floorPolylines, _referansnoktasi, _katyuksekligi, Utils.LAYERADI_KAT, Utils.SolidTuru.KatSolid);
                //                DBObject ddbobj = tr.GetObject(katSolidId, OpenMode.ForWrite);
                //                var katpb = pb;
                //                katpb.BarkodNo = Utils.BarkodOlustur(ref katpb, Utils.BarkodTuru.KatBarkodu);
                //                katpb.WriteToAutoCadObject(tr, ddbobj);
                //tr.Commit();
            } // using (tr)
        } // MakeParcel


        [CommandMethod("BinaBilgileriDuzenle")]
        public void BinaBilgileriDuzenle_method()
        {
        }

        [CommandMethod("BinaBilgi")]
        public void BinaBilgiCommand()
        {
            Utils.BinaBilgileriFormuGoster();
        }

        [CommandMethod("BagExp")] // Export to SQLite and Excel
        public void ExportToDatabase()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            if (!Utils.BinaBilgileriFormuGoster())
                return;

            Utils.LayerlariOlustur();

            PromptSelectionOptions pso = new PromptSelectionOptions
            {
                AllowDuplicates = false,
                MessageForAdding = "\nExcel'e aktarılacak 3dSolid'leri seçiniz: "
            };

            TypedValue[] tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Start,"3DSOLID"),
                    new TypedValue((int)DxfCode.ExtendedDataRegAppName,ParselBilgileri.regAppName)
                };

            SelectionFilter selectionfilter = new SelectionFilter(tvs);

            PromptSelectionResult selRes = doc.Editor.GetSelection(pso, selectionfilter);

            // If the user didn't make valid selection, we return
            if (selRes.Status != PromptStatus.OK)
                return;

            SelectionSet ssSolids = selRes.Value;

            // Parsel polyline'ının seçilmesi...
            pso = new PromptSelectionOptions
            {
                //AllowDuplicates = false,
                SingleOnly = true,
                SinglePickInSpace = true,
                MessageForAdding = "\nKadastral Parseli seçiniz (LWPOLYLINE): "
            };
            tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Start,"LWPOLYLINE"),
//                    new TypedValue((int)DxfCode.ExtendedDataRegAppName,ParselBilgileri.regAppName)
                };

            selectionfilter = new SelectionFilter(tvs);

            selRes = doc.Editor.GetSelection(pso, selectionfilter);

            // If the user didn't make valid selection, we return

            if (selRes.Status != PromptStatus.OK)
                return;
            SelectionSet ssParsel = selRes.Value;

            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.CheckFileExists = false;
            sfd.CheckPathExists = true;
            sfd.FileName = System.IO.Path.GetFileNameWithoutExtension(doc.Name) + ".xls"; ;
            sfd.Filter = "Excel Dosyaları (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            //sfd.Filter = "SQLite dosyaları (*.sqlite)|*.sqlite|Tüm Dosyalar (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;
            //sfd.Title = "Yeni SQLite Veritabanı Kayıt Dosyası";
            sfd.Title = "Yeni Excel Dosyası";
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string sqlfilename = sfd.FileName;
                //string excelfilename = System.IO.Path.GetFileNameWithoutExtension(doc.Name) + ".xlsx";
                string excelfilename = System.IO.Path.GetDirectoryName(sqlfilename) + "\\" + System.IO.Path.GetFileNameWithoutExtension(sqlfilename) + ".xlsx";

                List<Excel_BagimsizTabloSatiri> listExcelBagimsizTablosu = new List<Excel_BagimsizTabloSatiri>();
                //                List<Excel_BagimsizTabloSatiri> listExcelGirisTablosu = new List<Excel_BagimsizTabloSatiri>();
                //var listExcelOrtakAlanTablosu = new List<Excel_OrtakAlanTabloSatiri>();
                var listExcelOrtakAlanTablosu = new List<Excel_BagimsizTabloSatiri>();
                //var listExcelEklentiTablosu = new List<Excel_EklentiTabloSatiri>();
                var listExcelEklentiTablosu = new List<Excel_BagimsizTabloSatiri>();
                var listExcelKatlarTablosu = new List<Excel_KatlarTablosu>();
                var listExcelBinaTablosu = new List<Excel_BinaTablosu>();
                var listExcelAdaParselTablosu = new List<Excel_AdaParselTablosu>();

                var objectIdArrayKatlar = new List<ObjectId>();
                var objectIdArrayBagimsizBolumler = new List<ObjectId>();

                // Use a transaction to edit our various objects

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    foreach (SelectedObject o in ssSolids) // seçilen solidler katlar ve bağımsız bölümler olarak ayıklanıyor
                    {
                        var entity = (Entity)tr.GetObject(o.ObjectId, OpenMode.ForRead);
                        if (entity.Layer == Utils.LAYERADI_BINA) // bina solid'i oluşmuşsa siliyoruz çünkü yenisi yapılacak.
                        {
                            //entity.UpgradeOpen();
                            //entity.Erase();
                            continue;
                        }
                        //else if (entity.Layer == Utils.LAYERADI_KAT)
                        else if (entity.Layer.Contains("K"))
                        {
                            objectIdArrayKatlar.Add(entity.ObjectId);
                        }
                        else
                        {
                            objectIdArrayBagimsizBolumler.Add(entity.ObjectId);
                        }
                    } // foreach
                    tr.Commit();
                }

                ParselBilgileri pb = new ParselBilgileri();
                #region Bağımsız bölüm tablosunun oluşturulması
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    foreach (SelectedObject o in SelectionSet.FromObjectIds(objectIdArrayBagimsizBolumler.ToArray()))
                    {
                        DBObject obj = tr.GetObject(o.ObjectId, OpenMode.ForWrite);

                        if (obj is Solid3d)
                        {
                            if (obj.XData != null)
                            {
                                pb.ReadFromAutoCadObject(obj);
                                pb.TapuBilgileri = Utils.ReadFromDictionary(); // objeden okunan tapu(bina) bilgileri değil de en son BinaBilgileri formunda girilen değerler alınıyor.
                                pb.WriteToAutoCadObject(tr, obj); // objeye de en son tapu (bina)bilgileri yazılıyor.

                                Excel_BagimsizTabloSatiri excel_BagimsizTabloSatiri = new Excel_BagimsizTabloSatiri();
                                excel_BagimsizTabloSatiri.Oku(pb);
                                if (excel_BagimsizTabloSatiri.getBagimsizTuru() == "Ortak Alan")
                                {
                                    bool zatenvar = false;
                                    //foreach (Excel_BagimsizTabloSatiri ebts in listExcelOrtakAlanTablosu)
                                    //{
                                    //    if (ebts.BagimsizBolumBarkodNo == excel_BagimsizTabloSatiri.BagimsizBolumBarkodNo)
                                    //    {
                                    //        zatenvar = true;
                                    //        ebts.Birlestir(excel_BagimsizTabloSatiri);
                                    //    }
                                    //}
                                    if (!zatenvar)
                                        //listExcelOrtakAlanTablosu.Add(excel_BagimsizTabloSatiri as Excel_OrtakAlanTabloSatiri);
                                        listExcelOrtakAlanTablosu.Add(excel_BagimsizTabloSatiri);
                                }
                                else if (excel_BagimsizTabloSatiri.getBagimsizTuru() == "Eklenti")
                                {
                                    bool zatenvar = false;
                                    //foreach (Excel_BagimsizTabloSatiri ebts in listExcelEklentiTablosu)
                                    //{
                                    //    if (ebts.BagimsizBolumBarkodNo == excel_BagimsizTabloSatiri.BagimsizBolumBarkodNo)
                                    //    {
                                    //        zatenvar = true;
                                    //        ebts.Birlestir(excel_BagimsizTabloSatiri);
                                    //    }
                                    //}
                                    if (!zatenvar)
                                        //listExcelEklentiTablosu.Add(excel_BagimsizTabloSatiri as Excel_EklentiTabloSatiri);
                                        listExcelEklentiTablosu.Add(excel_BagimsizTabloSatiri);
                                }
                                else if (excel_BagimsizTabloSatiri.getBagimsizTuru() == "Giriş")
                                {
                                    bool zatenvar = false;
                                    //foreach (Excel_BagimsizTabloSatiri ebts in listExcelEklentiTablosu)
                                    //{
                                    //    if (ebts.BagimsizBolumBarkodNo == excel_BagimsizTabloSatiri.BagimsizBolumBarkodNo)
                                    //    {
                                    //        zatenvar = true;
                                    //        ebts.Birlestir(excel_BagimsizTabloSatiri);
                                    //    }
                                    //}
                                    if (!zatenvar)
                                        //listExcelGirisTablosu.Add(excel_BagimsizTabloSatiri); 
                                        // ilk önce girişler için ayrı tablo oluşturulup oraya giriş yapıldı.
                                        // Sonra vazgeçildi girişler ortak alan tablosuna yazılsın diye karar verildi. 29.11.2019
                                        //listExcelOrtakAlanTablosu.Add(excel_BagimsizTabloSatiri as Excel_OrtakAlanTabloSatiri);
                                        listExcelOrtakAlanTablosu.Add(excel_BagimsizTabloSatiri);
                                }
                                else // BagimsizTuru == "Bağımsız Bölüm"
                                {
                                    bool zatenvar = false;
                                    //foreach (Excel_BagimsizTabloSatiri ebts in listExcelBagimsizTablosu)
                                    //{
                                    //    if (ebts.BagimsizBolumBarkodNo == excel_BagimsizTabloSatiri.BagimsizBolumBarkodNo)
                                    //    {
                                    //        // dublekslerde, üstteki bağımsız bölümü alttaki bağımsız bölümün bulunduğu satıra eklemek gerekiyor. O yüzden kat kontrolü yapılıyor.
                                    //        zatenvar = true;
                                    //        //ebts.Notlar += (ebts.Notlar == "") ? "Dubleks" : ",Dubleks";
                                    //        var kat_bu = Math.Abs(Convert.ToInt32(ebts.KatNo));
                                    //        var kat_diger = Math.Abs(Convert.ToInt32(excel_BagimsizTabloSatiri.KatNo));
                                    //        if(kat_bu > kat_diger)
                                    //        {
                                    //            var new_ebts = new Excel_BagimsizTabloSatiri();
                                    //            new_ebts = excel_BagimsizTabloSatiri;
                                    //            new_ebts.Birlestir(ebts);
                                    //            ebts.CopyGeometryFromOther(new_ebts);
                                    //        }
                                    //        else
                                    //        {
                                    //            ebts.Birlestir(excel_BagimsizTabloSatiri);
                                    //        }
                                    //        //break;
                                    //    }
                                    //}
                                    if (!zatenvar)
                                        listExcelBagimsizTablosu.Add(excel_BagimsizTabloSatiri);
                                }
                            }
                            else
                                ed.WriteMessage("\nBağımsız bölüm Solid'inden XData okunamadı.");
                        }
                    } // foreach objectIdArrayBagimsizBolumler
                }
                #endregion

                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    #region KatlarTablosu'nun oluşturulması
                    if (objectIdArrayKatlar.Count > 0)
                    {
                        foreach (SelectedObject o in SelectionSet.FromObjectIds(objectIdArrayKatlar.ToArray()))
                        {
                            DBObject obj = tr.GetObject(o.ObjectId, OpenMode.ForWrite);
                            if (obj is Solid3d)
                            {
                                var katSolid = obj as Solid3d;
                                if (obj.XData != null)
                                {
                                    pb.ReadFromAutoCadObject(obj); // XData'dan verilen okunuyor.
                                    Excel_KatlarTablosu kattab = new Excel_KatlarTablosu();
                                    kattab.Oku(pb); // XData'can okunan bilgilerden excel tablosu için veri okunuyor.
                                    kattab.KatNo = pb.BagimsizBolumBulunduguKat.ToString();
                                    var katZeminKoordinatlari = pb.ZeminKoordinatlari;
                                    kattab.ZeminKoordinatlari_Grid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(katZeminKoordinatlari, 3);

                                    int dilort = Utils.GetDOB(pb.TapuBilgileri.ilceKodu);
                                    var listlistpcoll = new List<List<Point3dCollection>>(); // List<Point3dCollection>'daki (varsa) 2. Point3dCollection 'Boşluğu' ifade ediyor.
                                    foreach (var listPcoll in katZeminKoordinatlari)
                                    {
                                        var listenlemboylampcoll = GeoCalc.ConvertGridToLatLongListPColl(listPcoll, dilort);
                                        //listlistpcoll = new List<List<Point3dCollection>>() { listenlemboylampcoll };
                                        listlistpcoll.Add(listenlemboylampcoll);
                                    }

                                    kattab.ZeminKoordinatlari_EnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 8);


                                    Point3dCollection allpts = Utils.ReplaceZOfPoint3dCollection(Utils.GetAllCoordinatesFromSolid3d(katSolid), pb.BagimsizBolumTabanKotu_DenizdenYukseklik);
                                    listlistpcoll = new List<List<Point3dCollection>>();
                                    listlistpcoll.Add(new List<Point3dCollection>() { allpts });
                                    kattab._3DKoordinatlar_Grid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 3);

                                    var listgridpcoll = GeoCalc.ConvertGridToLatLongListPColl(new List<Point3dCollection>() { allpts }, dilort);
                                    listlistpcoll = new List<List<Point3dCollection>>(); // List<Point3dCollection>'daki (varsa) 2. Point3dCollection 'Boşluğu' ifade ediyor.
                                    listlistpcoll.Add(listgridpcoll);
                                    kattab._3DKoordinatlar_EnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 3);
                                    //kattab._3DKoordinatlar_Grid = Utils.ConvertToString3d(allpts, ' ');
                                    //kattab._3DKoordinatlar_EnlemBoylam =
                                    //    Utils.ConvertToStringEnlemBoylam3d(
                                    //        GeoCalc.ConvertGridToLatLong(allpts,
                                    //        Utils.GetDOB(pb.TapuBilgileri.ilceKodu)), ',');

                                    listExcelKatlarTablosu.Add(kattab);
                                }
                                else
                                    ed.WriteMessage("\nKat Solid'inden XData okunamadı.");
                            }
                        } // foreach
                    }// if
                    #endregion

                    #region BinaTablosu'nun oluşturulması
                    pb.BagimsizTuru = "Bina";
                    Excel_BinaTablosu bintab = new Excel_BinaTablosu();
                    bintab.Oku(pb);
                    if (objectIdArrayKatlar.Count > 0)
                    {
                        //System.Windows.MessageBox.Show("kat adeti" + objectIdArrayKatlar.Count.ToString());
                        ed.WriteMessage("\nKat adeti" + objectIdArrayKatlar.Count.ToString());
                        Solid3d binaSolid = Utils.KatSolidleriniBirlestir(objectIdArrayKatlar, Utils.LAYERADI_BINA);
                        pb.ReadFromAutoCadObject(binaSolid);

                        if (binaSolid != null)
                        {
                            //var binaZeminKoordinatlari = Utils.GetFloorCoordinatesFromSolid3d(binaSolid);
                            var binaZeminKoordinatlari = pb.ZeminKoordinatlari;
                            var binaYuksekligi = binaSolid.GeometricExtents.MaxPoint.Z - binaSolid.GeometricExtents.MinPoint.Z;
                            bintab.BinaCatiKotu = (pb.BagimsizBolumTabanKotu_DenizdenYukseklik + binaYuksekligi).ToString("0.000");
                            bintab.BinaTemelKotu = pb.BagimsizBolumTabanKotu_DenizdenYukseklik.ToString("0.000");

                            //Point3dCollection binaZeminKoordinatlari = new Point3dCollection();
                            //foreach (var pcoll in listpcoll)
                            //    foreach (Point3d p3d in pcoll)
                            //        binaZeminKoordinatlari.Add(p3d);

                            //Extents3d ex = Utils.GetExtents3dFromCoordinates(ref binaZeminKoordinatlari);
                            //Point3d zeminOrtaNoktasi = new Point3d(0.5 * (ex.MaxPoint.X + ex.MinPoint.X),                                                      0.5 * (ex.MaxPoint.Y + ex.MinPoint.Y), 0);


                            //bintab.BinaZeminOrtaNoktaVeDonukluk_Grid =
                            //    zeminOrtaNoktasi.ToString() + " " + pdr.ToString(); // radyan biriminde açı değeri. 
                            //                                                        //Converter.AngleToString(pdr.Value, AngularUnitFormat.Radians, 5);

                            //var gecici = new Point3dCollection();
                            //gecici.Add(zeminOrtaNoktasi);
                            //bintab.BinaZeminOrtaNoktaVeDonukluk_EnlemBoylam =
                            //Utils.ConvertToString3d(
                            //        GeoCalc.ConvertGridToLatLong(gecici,
                            //        Utils.GetDOB(pb.TapuBilgileri.ilceKodu)), ' '
                            //    ) + " " + pdr.ToString();

                            // bintab.BinaZeminKoordinatlari_Grid = Utils.ConvertToString3d(binaZeminKoordinatlari, ' ');

                            bintab.BinaZeminKoordinatlari_Grid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(binaZeminKoordinatlari, 3);

                            int dilort = Utils.GetDOB(pb.TapuBilgileri.ilceKodu);

                            var listlistpcoll = new List<List<Point3dCollection>>(); // List<Point3dCollection>'daki (varsa) 2. Point3dCollection 'Boşluğu' ifade ediyor.
                            foreach (var listPcoll in binaZeminKoordinatlari)
                            {
                                var listenlemboylampcoll = GeoCalc.ConvertGridToLatLongListPColl(listPcoll, dilort);
                                //listlistpcoll = new List<List<Point3dCollection>>() { listenlemboylampcoll };
                                listlistpcoll.Add(listenlemboylampcoll);
                            }

                            bintab.BinaZeminKoordinatlariEnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 8);

                            //bintab.BinaZeminKoordinatlariEnlemBoylam =
                            //    Utils.ConvertToStringEnlemBoylam2d(
                            //            GeoCalc.ConvertGridToLatLong(binaZeminKoordinatlari,
                            //            Utils.GetDOB(pb.TapuBilgileri.ilceKodu)), ','
                            //        );

                            //Point3dCollection allpts = Utils.GetAllCoordinatesFromSolid3d(binaSolid);
                            //bintab._3DKoordinatlar_Grid =
                            //    Utils.ConvertToString3d(allpts, ' ');
                            //bintab._3DKoordinatlar_EnlemBoylam =
                            //    Utils.ConvertToStringEnlemBoylam3d(
                            //        GeoCalc.ConvertGridToLatLong(allpts,
                            //        Utils.GetDOB(pb.TapuBilgileri.ilceKodu)), ',');
                            Point3dCollection allpts = Utils.ReplaceZOfPoint3dCollection(Utils.GetAllCoordinatesFromSolid3d(binaSolid), pb.BagimsizBolumTabanKotu_DenizdenYukseklik);
                            var listAllPts = new List<Point3dCollection>() { allpts };
                            listlistpcoll = new List<List<Point3dCollection>>(); // List<Point3dCollection>'daki (varsa) 2. Point3dCollection 'Boşluğu' ifade ediyor.
                            listlistpcoll.Add(listAllPts);
                            bintab._3DKoordinatlar_Grid = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 3);

                            var listpcoll = GeoCalc.ConvertGridToLatLongListPColl(listAllPts, dilort);
                            listlistpcoll = new List<List<Point3dCollection>>(); // List<Point3dCollection>'daki (varsa) 2. Point3dCollection 'Boşluğu' ifade ediyor.
                            listlistpcoll.Add(listpcoll);
                            bintab._3DKoordinatlar_EnlemBoylam = Utils.ConvertListOfListOf3dPointCollectionToWktMultiPolygon(listlistpcoll, 8);
                        }
                        else System.Windows.MessageBox.Show("almadı");
                    }
                    listExcelBinaTablosu.Add(bintab);
                    #endregion // BinaTablosu'nun oluşturulması
                    #region AdaParselTablosu'nun oluşturulması
                    Excel_AdaParselTablosu aptab = new Excel_AdaParselTablosu();
                    aptab.Oku(pb);
                    listExcelAdaParselTablosu.Add(aptab);

                    //cadPolyline parselPolyline = (cadPolyline)(tr.GetObject(ssParsel.OfType<SelectedObject>().First().ObjectId, OpenMode.ForRead).AcadObject);

                    if (ssParsel.Count > 0)
                    {
                        foreach (SelectedObject o in ssParsel)
                        {
                            DBObject obj = tr.GetObject(o.ObjectId, OpenMode.ForRead);
                            if (obj is cadPolyline)
                            {
                                var parselPolyline = obj as cadPolyline;
                                if (parselPolyline != null)
                                {

                                    var parselkosenoktalari = new Point3dCollection();
                                    // Use a for loop to get each vertex, one by one
                                    for (int i = 0; i < parselPolyline.NumberOfVertices; i++)
                                    {
                                        Point3d pt = parselPolyline.GetPoint3dAt(i);
                                        parselkosenoktalari.Add(pt);
                                    }
                                    //aptab.ParselKoordinatlari_Grid = Utils.ConvertToString3d(parselkosenoktalari, ' ');
                                    aptab.ParselKoordinatlari_Grid = "POLYGON((" + Utils.ConvertPoint3dCollectionToString(parselkosenoktalari, ' ', ',', 3) + "))";
                                    var DilimOrtaBoylami = Utils.GetDOB(pb.TapuBilgileri.ilceKodu);
                                    //aptab.ParselKoordinatlari_EnlemBoylam =
                                    //    Utils.ConvertToStringEnlemBoylam2d(
                                    //        GeoCalc.ConvertGridToLatLong(parselkosenoktalari,
                                    //        DilimOrtaBoylami), ',');
                                    aptab.ParselKoordinatlari_EnlemBoylam = "POLYGON((" +
                                        Utils.ConvertPoint3dCollectionToString(
                                            GeoCalc.ConvertGridToLatLong(parselkosenoktalari,
                                            DilimOrtaBoylami), ' ', ',', 8) +
                                             "))";
                                }
                            } // if
                        } // foreach
                    }// if 
                    else
                        System.Windows.MessageBox.Show("parsel :" + ssParsel.Count.ToString() + " adet");

                    #endregion // AdaParselTablosu'nun oluşturulması
                    tr.Commit();
                } // using tr

                #region SQLite kısmı iptal edildi
                /*
                SqliteHelper sqlh = new SqliteHelper();
                try
                {
                    sqlh.CreateParsel3DTables(sqlfilename);
                    sqlh.Write_ExcelBagimsizTablosuToDatabase("proje_bagimsiz_bolum", listExcelBagimsizTablosu);
                    //sqlh.Write_ExcelBagimsizTablosuToDatabase("proje_giris", listExcelGirisTablosu);
                    sqlh.Write_ExcelOrtakAlanTablosuToDatabase(listExcelOrtakAlanTablosu);
                    sqlh.Write_ExcelEklentiTablosuToDatabase(listExcelEklentiTablosu);
                    sqlh.Write_ExcelKatlarTablosuToDatabase(listExcelKatlarTablosu);
                    sqlh.Write_ExcelBinaTablosuToDatabase(listExcelBinaTablosu);
                    sqlh.Write_GeometryColumnsToDatabase();
                    sqlh.Write_RefSysColumnsToDatabase();
                    sqlh.Bitir();
                }
                catch (System.Exception ex)
                {
                    System.Windows.MessageBox.Show("sqlite dosyası oluşturmada hata oluştu..\r\nException: " + ex.Message);
                    return;
                }*/
                #endregion 

                //string xlFilePath = excelfilename;
                try
                {
                    System.Data.DataTable dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelBagimsizTablosu);
                    //System.Data.DataTable dt2 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelGirisTablosu);
                    System.Data.DataTable dt3 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelOrtakAlanTablosu);
                    System.Data.DataTable dt4 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelEklentiTablosu);
                    System.Data.DataTable dt5 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelBinaTablosu);
                    System.Data.DataTable dt6 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelAdaParselTablosu);
                    System.Data.DataTable dt7 = ExportToExcel.CreateExcelFile.ListToDataTable(listExcelKatlarTablosu);

                    dt1.TableName = "Bagimsiz_Bolumler";
                    //dt2.TableName = "Girisler";
                    dt3.TableName = "Ortak_Alanlar";
                    dt3.Columns["BagimsizBolumNo"].ColumnName = "OrtakAlanNo";
                    dt3.Columns["BagimsizBolumBarkodNo"].ColumnName = "OrtakAlanBarkodNo";
                    dt3.Columns.Remove("Notlar");

                    dt4.TableName = "Eklentiler";
                    dt4.Columns["BagimsizBolumNo"].ColumnName = "EklentiNo";
                    dt4.Columns["BagimsizBolumBarkodNo"].ColumnName = "EklentiBarkodNo";
                    dt4.Columns.Remove("Notlar");

                    dt5.TableName = "Bina";
                    dt6.TableName = "Ada_Parsel";
                    dt7.TableName = "Katlar";

                    System.Data.DataSet ds = new System.Data.DataSet();

                    ds.Tables.Add(dt1);
                    //ds.Tables.Add(dt2);
                    ds.Tables.Add(dt3);
                    ds.Tables.Add(dt4);
                    ds.Tables.Add(dt5);
                    ds.Tables.Add(dt6);
                    ds.Tables.Add(dt7);
                    ExportToExcel.CreateExcelFile.CreateExcelDocument(ds, excelfilename);

                    //ExportToExcel.CreateExcelFile.CreateExcelDocument<ParselBilgileri>(pil, xlFilePath);
                }
                catch (System.Exception ex)
                {
                    System.Windows.MessageBox.Show("Excel dosyası oluşturmada hata oluştu..\r\nException: " + ex.Message);
                    return;
                }

                System.Diagnostics.Process p = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo(excelfilename)
                };
                p.Start();
            } // if showdialog
        } // ExportToDatabase

        //[CommandMethod("Facexp__")] // Export face/material info
        public void ExportFace__()
        {
            Document activeDoc = Application.DocumentManager.MdiActiveDocument;
            Database db = activeDoc.Database;
            Editor ed = activeDoc.Editor;
            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a Solid3d : ");
            peo.SetRejectMessage("\nPlease select a Solid3d...");
            peo.AddAllowedClass(typeof(Solid3d), true);

            PromptEntityResult per = ed.GetEntity(peo);
            if (per.Status != PromptStatus.OK)
                return;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = tr.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;
                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                FullSubentityPath path =
                    new FullSubentityPath(ids,
                                new SubentityId(SubentityType.Null,
                                 IntPtr.Zero));
                using (Brep brep = new Brep(path))
                {
                    int facecount = 0;
                    foreach
                    (
                        Autodesk.AutoCAD.BoundaryRepresentation.Face
                        face in brep.Faces
                    )
                        try
                        {
                            facecount++;
                            Autodesk.AutoCAD.Geometry.Surface surface = face.Surface;
                            Interval[] interval = surface.GetEnvelope(); // intervalU ve intervalV değerleri

                            ed.WriteMessage("\nface.surface:" + face.Surface.ToString());
                            ObjectId materialId = solid3d.GetSubentityMaterial(face.SubentityPath.SubentId);
                            Material material = tr.GetObject(materialId, OpenMode.ForRead) as Material;
                            ed.WriteMessage("\n------------\n" + material.Name
                                          + "\n------------\n" + material.Bounds.ToString()
                                );

                            foreach (BoundaryLoop boundaryloop in face.Loops)
                            {
                                ed.WriteMessage("\n\nFACE:[{0}]", facecount);
                                LoopType loopType = boundaryloop.LoopType;
                                ed.WriteMessage("\nLooptype:{0}", loopType.ToString());
                                //LoopEdgeCollection edges = boundaryloop.Edges;
                                int edgecount = 0;
                                foreach (Edge edge in boundaryloop.Edges)
                                {
                                    edgecount++;
                                    ed.WriteMessage("\nEdge:[{0}] -> V1={1:0.00}, V2={2:0.00}", edgecount, edge.Vertex1.Point, edge.Vertex2.Point);
                                }
                            } // foreach BoundaryLoop
                        }
                        catch (System.Exception ex)
                        {
                            ed.WriteMessage("\nexception: [" + ex.Message + "]");
                        }
                } // using Brep0;                tr.Commit();
            }
        }
        public List<string> GetMaterialNames()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var names = new List<string>();
            var matColorNames = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                var matDict = tr.GetObject(db.MaterialDictionaryId, OpenMode.ForRead) as DBDictionary;
                ed.WriteMessage("\n -------Materials:------------ ");
                foreach (DBDictionaryEntry dbEntry in matDict)
                {
                    Material mat = tr.GetObject(dbEntry.Value, OpenMode.ForRead) as Material;
                    ed.WriteMessage("\n\t Material={0}", mat.Name);
                    names.Add(mat.Name);

                    MaterialColor Ambient = mat.Ambient;
                    var matColor = Ambient.Color;
                    if (matColor.ColorMethod == Autodesk.AutoCAD.Colors.ColorMethod.ByAci)
                        matColorNames[mat.Name] = "[Index]: " + matColor.ColorIndex.ToString();
                    else if (matColor.ColorMethod == Autodesk.AutoCAD.Colors.ColorMethod.ByColor)
                        matColorNames[mat.Name] = string.Format("[RGB]: {0},{1},{2}", matColor.Red, matColor.Green, matColor.Blue);
                }
            }
            return names;
        }

        //[CommandMethod("Facexp_")] // Export face/material info
        public void ExportFace_()
        {
            Document activeDoc = Application.DocumentManager.MdiActiveDocument;
            Database db = activeDoc.Database;
            Editor ed = activeDoc.Editor;
            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a Solid3d : ");
            peo.SetRejectMessage("\nPlease select a Solid3d...");
            peo.AddAllowedClass(typeof(Solid3d), true);
            PromptEntityResult per = ed.GetEntity(peo);
            if (per.Status != PromptStatus.OK)
                return;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = tr.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;
                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                FullSubentityPath path =
                    new FullSubentityPath(ids,
                                new SubentityId(SubentityType.Null,
                                 IntPtr.Zero));
                using (Brep brep = new Brep(path))
                {
                    foreach
                    (
                        Autodesk.AutoCAD.BoundaryRepresentation.Face
                        face in brep.Faces
                    )
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(face.SubentityPath.SubentId);
                            Material material = tr.GetObject(materialId, OpenMode.ForRead) as Material;
                            ed.WriteMessage("\n------------\n" + material.Name
                                          + "\n------------\n" + material.Bounds.ToString()
                                //                                face.Surface.
                                );
                        }
                        catch (System.Exception ex)
                        {
                            ed.WriteMessage(ex.Message);
                        }
                }
                tr.Commit();
            }
        }
        //[CommandMethod("Sube_")]
        public void SubEntExample_()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a 3D solid: ");
            peo.SetRejectMessage("\nInvalid selection...");
            peo.AddAllowedClass(typeof(Solid3d), true);

            PromptEntityResult per = ed.GetEntity(peo);

            if (per.Status != PromptStatus.OK)
                return;

            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid = Tx.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;

                ObjectId[] ids = new ObjectId[] { per.ObjectId };

                FullSubentityPath path = new FullSubentityPath(ids, new SubentityId(SubentityType.Null, IntPtr.Zero));

                List<SubentityId> subEntIds = new List<SubentityId>();

                using (Brep brep = new Brep(solid))
                {
                    int i = 0;
                    Point3dCollection pts = new Point3dCollection();
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge edge in brep.Edges)
                    {
                        //ed.WriteMessage("\nedge,i = {0} xy={1},{2} xy={1},{2}", i, edge.Vertex1.Point.X, edge.Vertex1.Point.Y,
                        //    edge.Vertex2.Point.X, edge.Vertex2.Point.Y);
                        // ed.WriteMessage("\nedge: i = {0} xy=[{1}],[{2}]", i, edge.Vertex1.Point, edge.Vertex2.Point);
                        //if (!pts.Contains(edge.Vertex1.Point))
                        {
                            pts.Add(edge.Vertex1.Point);
                            ed.WriteMessage("\nVertex1: i = {0} xy={1:0.00}", i, edge.Vertex1.Point);
                        }
                        //if (!pts.Contains(edge.Vertex2.Point))
                        {
                            pts.Add(edge.Vertex2.Point);
                            ed.WriteMessage("\nvertex2: i = {0} xy={1:0.00}", i, edge.Vertex2.Point);
                        }
                        i++;
                    }
                    int facecount = 0;
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                    {
                        ed.WriteMessage("\nface no:" + facecount++.ToString());
                        foreach (BoundaryLoop boundaryloop in face.Loops)
                        {
                            ed.WriteMessage("\n\nFACE:[{0}]", facecount);
                            LoopType loopType = boundaryloop.LoopType;
                            ed.WriteMessage("\nLooptype:{0}", loopType.ToString());
                            //LoopEdgeCollection edges = boundaryloop.Edges;
                            int edgecount = 0;
                            foreach (Edge edge in boundaryloop.Edges)
                            {
                                edgecount++;
                                ed.WriteMessage("\nEdge:[{0}] -> V1={1:0.00}, V2={2:0.00}", edgecount, edge.Vertex1.Point, edge.Vertex2.Point);
                            }
                            int vertexcount = 0;
                            foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                            {
                                vertexcount++;
                                ed.WriteMessage("\nVertex{0}: [{1:0.00}]", vertexcount, vertex.Point);
                            }

                            string matName = "xxx";
                            try
                            {
                                ObjectId materialId = brep.Solid.GetSubentityMaterial(face.SubentityPath.SubentId);
                                Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                                matName = material.Name;
                            }
                            catch (System.Exception ex)
                            {
                                matName = "----Yok-----(Exception:" + ex.ToString() + ")";
                            }
                            ed.WriteMessage("\nMaterial Name:==============>{0}<================", matName);
                        } // foreach BoundaryLoop
                    }
                } // using new Brep(solid)

                //using (Brep brep = new Brep(path))
                //{
                //    int facecount = 0;
                //    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                //    {
                //        ed.WriteMessage("\n\n++++++++++++++++++++++++++++face no:{0}++++++++++++++++++++++++++++++++",facecount++);

                //        ObjectId materialId = solid.GetSubentityMaterial(face.SubentityPath.SubentId);
                //        Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                //        ed.WriteMessage("\n------------ " + material.Name
                //                        + " ------------" + material.Bounds.ToString()
                //            );
                //        foreach (BoundaryLoop boundaryloop in face.Loops)
                //        {
                //            ed.WriteMessage("\n\nFACE:[{0}]", facecount);
                //            LoopType loopType = boundaryloop.LoopType;
                //            ed.WriteMessage("\nLooptype:{0}", loopType.ToString());
                //            //LoopEdgeCollection edges = boundaryloop.Edges;
                //            int edgecount = 0;
                //            foreach (Edge edge in boundaryloop.Edges)
                //            {
                //                edgecount++;
                //                ed.WriteMessage("\nEdge:[{0}] -> V1={1:0.00}, V2={2:0.00}", edgecount, edge.Vertex1.Point, edge.Vertex2.Point);
                //            }
                //            int vertexcount = 0;
                //            foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                //            {
                //                vertexcount++;
                //                ed.WriteMessage("\nVertex{0}: [{1:0.00}]", vertexcount, vertex.Point);
                //            }
                //        } // foreach BoundaryLoop
                //    }
                //} // using new Brep(path)
                Tx.Commit();
            }
        } // SubentExample_
        //[CommandMethod("Sube")]
        public void SubEntExample()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a 3D solid: ");
            peo.SetRejectMessage("\nInvalid selection...");
            peo.AddAllowedClass(typeof(Solid3d), true);

            PromptEntityResult per = ed.GetEntity(peo);

            if (per.Status != PromptStatus.OK)
                return;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = tr.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;
                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                FullSubentityPath path =
                    new FullSubentityPath(ids,
                                new SubentityId(SubentityType.Null,
                                 IntPtr.Zero));
                using (Brep brep = new Brep(path))
                {
                    foreach
                    (
                        Autodesk.AutoCAD.BoundaryRepresentation.Face
                        face in brep.Faces
                    )
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(face.SubentityPath.SubentId);
                            Material material = tr.GetObject(materialId, OpenMode.ForRead) as Material;
                            ed.WriteMessage("\n------------\n" + material.Name
                                          + "\n------------\n" + material.Bounds.ToString()
                                //                                face.Surface.
                                );
                        }
                        catch (System.Exception ex)
                        {
                            ed.WriteMessage(ex.Message);
                        }
                }
                tr.Commit();
            } //using Transaction tr
            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid = Tx.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;

                ObjectId[] ids = new ObjectId[] { per.ObjectId };

                FullSubentityPath path = new FullSubentityPath(ids, new SubentityId(SubentityType.Null, IntPtr.Zero));

                List<SubentityId> subEntIds = new List<SubentityId>();

                using (Brep brep = new Brep(solid))
                {
                    int i = 0;
                    Point3dCollection pts = new Point3dCollection();
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Edge edge in brep.Edges)
                    {
                        //ed.WriteMessage("\nedge,i = {0} xy={1},{2} xy={1},{2}", i, edge.Vertex1.Point.X, edge.Vertex1.Point.Y,
                        //    edge.Vertex2.Point.X, edge.Vertex2.Point.Y);
                        // ed.WriteMessage("\nedge: i = {0} xy=[{1}],[{2}]", i, edge.Vertex1.Point, edge.Vertex2.Point);
                        //if (!pts.Contains(edge.Vertex1.Point))
                        {
                            pts.Add(edge.Vertex1.Point);
                            ed.WriteMessage("\nVertex1: i = {0} xy={1:0.00}", i, edge.Vertex1.Point);
                        }
                        //if (!pts.Contains(edge.Vertex2.Point))
                        {
                            pts.Add(edge.Vertex2.Point);
                            ed.WriteMessage("\nvertex2: i = {0} xy={1:0.00}", i, edge.Vertex2.Point);
                        }
                        i++;
                    }
                    int facecount = 0;
                    foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                    {
                        ed.WriteMessage("\nface no:" + facecount++.ToString());
                        foreach (BoundaryLoop boundaryloop in face.Loops)
                        {
                            ed.WriteMessage("\n\nFACE:[{0}]", facecount);
                            LoopType loopType = boundaryloop.LoopType;
                            ed.WriteMessage("\nLooptype:{0}", loopType.ToString());
                            //LoopEdgeCollection edges = boundaryloop.Edges;
                            int edgecount = 0;
                            foreach (Edge edge in boundaryloop.Edges)
                            {
                                edgecount++;
                                ed.WriteMessage("\nEdge:[{0}] -> V1={1:0.00}, V2={2:0.00}", edgecount, edge.Vertex1.Point, edge.Vertex2.Point);
                            }
                            int vertexcount = 0;
                            foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                            {
                                vertexcount++;
                                ed.WriteMessage("\nVertex{0}: [{1:0.00}]", vertexcount, vertex.Point);
                            }

                            //string matName = "xxx";
                            //try
                            //{
                            //    ObjectId materialId = brep.Solid.GetSubentityMaterial(face.SubentityPath.SubentId);
                            //    Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                            //    matName = material.Name;
                            //}
                            //catch (System.Exception ex)
                            //{
                            //    matName = "----Yok-----";
                            //}
                            //ed.WriteMessage("\nMaterial Name:==============>{0}<================", matName);
                        } // foreach BoundaryLoop
                    }
                } // using new Brep(solid)
                Tx.Commit();
            }
        } // SubentExample

        [CommandMethod("MatTest")]
        public void MatTest()
        {
            MaterialTest.Commands.AddMaterialToLibrary("material deneme1", "D:\\Data\\ege\\faceexport\\tas.jpg", 5, 6, 2, 0.5, 90);
            MaterialTest.Commands.Test();
        }
        public string GetSolidBarkodId(ObjectId solidId)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                DBObject obj = tr.GetObject(solidId, OpenMode.ForWrite);
                string ret = "";
                if (obj is Solid3d)
                {
                    var katSolid = obj as Solid3d;
                    if (obj.XData != null)
                    {
                        var pb = new ParselBilgileri();
                        pb.ReadFromAutoCadObject(obj); // XData'dan verilen okunuyor.
                        ret = pb.BarkodNo;
                    }
                }
                return ret;
            }
        }

        public List<string> GetSolidMaterialNames(ObjectId solidId)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var materialNameList = new List<string>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                Solid3d solid3d = tr.GetObject(solidId, OpenMode.ForWrite) as Solid3d;
                ObjectId[] ids = new ObjectId[] { solid3d.ObjectId };
                FullSubentityPath path =
                    new FullSubentityPath(ids,
                                new SubentityId(SubentityType.Null,
                                 IntPtr.Zero));
                using (Brep brep = new Brep(path))
                {
                    foreach
                    (
                        Autodesk.AutoCAD.BoundaryRepresentation.Face
                        face in brep.Faces
                    )
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(face.SubentityPath.SubentId);
                            Material material = tr.GetObject(materialId, OpenMode.ForRead) as Material;
                            //                            ed.WriteMessage("\n------------\n" + material.Name + "\n------------\n" + material.Bounds.ToString());
                            materialNameList.Add(material.Name);
                        }
                        catch (System.Exception ex)
                        {
                            materialNameList.Add("-Yok-" + ex.ToString()); // <Yok>
                        }
                }
                tr.Commit();
            } //using Transaction tr
            return materialNameList;
        }// GetSolidMaterialNames
        public List<AutoCadMaterialRawProperties> GetDrawingMaterialTable()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var autoCadMaterialPropertiesList = new List<AutoCadMaterialRawProperties>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                var matDict = tr.GetObject(db.MaterialDictionaryId, OpenMode.ForRead) as DBDictionary;
                foreach (DBDictionaryEntry dbEntry in matDict)
                {
                    Material mat = tr.GetObject(dbEntry.Value, OpenMode.ForRead) as Material;
                    var amp = new AutoCadMaterialRawProperties();
                    amp.ReadFromTypedValues(mat);
                    autoCadMaterialPropertiesList.Add(amp);
                }
                tr.Commit();
            } //using Transaction tr
            return autoCadMaterialPropertiesList;
        }// GetDrawingMaterialTable

        public List<AutoCadMaterialExportProperties> GetMaterialExportPropertiesTable()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var autoCadMaterialExportPropertiesList = new List<AutoCadMaterialExportProperties>();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                var matDict = tr.GetObject(db.MaterialDictionaryId, OpenMode.ForRead) as DBDictionary;
                foreach (DBDictionaryEntry dbEntry in matDict)
                {
                    Material mat = tr.GetObject(dbEntry.Value, OpenMode.ForRead) as Material;
                    if (mat != null)
                    {
                        var amexp = new AutoCadMaterialExportProperties();
                        amexp.ReadFromMaterial(mat);
                        amexp.SourceFileRelativePath = Utils.MakeRelativePath(doc.Name, amexp.SourceFileFullPath);
                        autoCadMaterialExportPropertiesList.Add(amexp);
                    }
                }
                tr.Commit();
            } //using Transaction tr
            return autoCadMaterialExportPropertiesList;
        }// GetDrawingMaterialTable

        [CommandMethod("FaceExport")]
        public void FaceExport()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a 3D solid: ");
            peo.SetRejectMessage("\nInvalid selection...");
            peo.AddAllowedClass(typeof(Solid3d), true);

            PromptEntityResult per = ed.GetEntity(peo);

            if (per.Status != PromptStatus.OK)
                return;
            var solidId = per.ObjectId;
            var materialNameList = GetSolidMaterialNames(solidId);
            //var faceCoordinates = Utils.GetSolidFaceCoordinates(solidId);
            var faceInfos = Utils.GetSolidFaceMaterialInfoList(solidId);

            var binaBarkod = GetSolidBarkodId(solidId);

            System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
            sfd.FileName = System.IO.Path.GetFileNameWithoutExtension(doc.Name) + "_FaceExport.xlsx"; ;
            sfd.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            sfd.FilterIndex = 1;
            sfd.RestoreDirectory = true;
            sfd.OverwritePrompt = true;

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var tb = new ClassTapuBilgileri();
                if (!Utils.BinaBilgileriFormuGoster())
                    return;
                tb = Utils.ReadFromDictionary();
                var listYuzeyTablosu = new List<Excel_YuzeyTablosu>();

                for (int i = 0; i < faceInfos.Count; i++)
                {
                    var faceInfo = faceInfos[i];
                    var facecoordi = faceInfo.points;
                    facecoordi.Add(facecoordi[0]); // ilk koordinat sona da ekleniyor.

                    var yuzeytab = new Excel_YuzeyTablosu();
                    var latlongs = GeoCalc.ConvertGridToLatLong(facecoordi, Utils.GetDOB(tb.ilceKodu));

                    yuzeytab.Oku(tb);
                    yuzeytab.Koordinatlar_EnlemBoylam = Utils.ConvertEnlemBoylam3dCollectionToWktString(latlongs, ' ');
                    yuzeytab.Koordinatlar_Grid = Utils.Convert3dPointCollectionToWktPolygon(facecoordi, ' ');
                    yuzeytab.MaterialName = materialNameList[i];
                    yuzeytab.Face_Number = faceInfo.faceNumber.ToString();

                    string prec = "0.00000";

                    yuzeytab.UOffset = faceInfo.materialMappingParameters.UOffset.ToString(prec);

                    yuzeytab.VOffset = faceInfo.materialMappingParameters.VOffset.ToString(prec);
                    yuzeytab.UScale = faceInfo.materialMappingParameters.UScale.ToString(prec);
                    yuzeytab.VScale = faceInfo.materialMappingParameters.VScale.ToString(prec);
                    yuzeytab.Rotation = faceInfo.materialMappingParameters.Rotation.ToString(prec);
                    yuzeytab.AutoTransformMethodOfDiffuseMapMapper = faceInfo.materialMappingParameters.AutoTransformMethodOfDiffuseMapMapper.ToString();

                    Matrix3d m3d = faceInfo.transformationMatrix;

                    yuzeytab.mat00 = m3d[0, 0].ToString(prec);
                    yuzeytab.mat10 = m3d[1, 0].ToString(prec);
                    yuzeytab.mat20 = m3d[2, 0].ToString(prec);
                    yuzeytab.mat30 = m3d[3, 0].ToString(prec);

                    yuzeytab.mat01 = m3d[0, 1].ToString(prec);
                    yuzeytab.mat11 = m3d[1, 1].ToString(prec);
                    yuzeytab.mat21 = m3d[2, 1].ToString(prec);
                    yuzeytab.mat31 = m3d[3, 1].ToString(prec);

                    yuzeytab.mat02 = m3d[0, 2].ToString(prec);
                    yuzeytab.mat12 = m3d[1, 2].ToString(prec);
                    yuzeytab.mat22 = m3d[2, 2].ToString(prec);
                    yuzeytab.mat32 = m3d[3, 2].ToString(prec);

                    yuzeytab.mat03 = m3d[0, 3].ToString(prec);
                    yuzeytab.mat13 = m3d[1, 3].ToString(prec);
                    yuzeytab.mat23 = m3d[2, 3].ToString(prec);
                    yuzeytab.mat33 = m3d[3, 3].ToString(prec);

                    //ed.WriteMessage("\n----------------Face[{0}]Material:{1}-----------------", i, materialNameList[i]);
                    //for (int j = 0; j < latlongs.Count; j++)
                    //{
                    //    ed.WriteMessage("\nv[{0}]:{1}", j, faceCoordinates[i][j]);
                    //}
                    listYuzeyTablosu.Add(yuzeytab);
                }

                string xlFilePath = sfd.FileName;
                //var mattable = GetDrawingMaterialTable();
                var mattable = GetMaterialExportPropertiesTable();
                //System.Windows.MessageBox.Show("\nMATERIAL TABLE:\n" + mattable.ToString());

                try
                {
                    System.Data.DataTable dt1 = ExportToExcel.CreateExcelFile.ListToDataTable(listYuzeyTablosu);
                    dt1.TableName = "Yüzey Tablosu";
                    System.Data.DataTable dt2 = ExportToExcel.CreateExcelFile.ListToDataTable(mattable);
                    dt2.TableName = "Material Tablosu";
                    System.Data.DataSet ds = new System.Data.DataSet();

                    ds.Tables.Add(dt1);
                    ds.Tables.Add(dt2);
                    ExportToExcel.CreateExcelFile.CreateExcelDocument(ds, xlFilePath);
                }
                catch (System.Exception ex)
                {
                    System.Windows.MessageBox.Show("Excel dosyası oluşturmada hata oluştu..\r\nException: " + ex.Message);
                    return;
                }
                System.Diagnostics.Process p = new System.Diagnostics.Process
                {
                    StartInfo = new System.Diagnostics.ProcessStartInfo(xlFilePath)
                };
                p.Start();
            }
        } // FaceExport

        ////////////////////////////////////////////////////////////////////////////////
        //Use: Retrieves entity vertices, edges, faces using AssocPersSubentityIdPE
        //
        ////////////////////////////////////////////////////////////////////////////////
        [CommandMethod("SubEntityPE")]
        public void SubEntityPE()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            PromptEntityOptions peo = new PromptEntityOptions(
                "\nSelect an Entity: ");
            PromptEntityResult per = ed.GetEntity(peo);
            if (per.Status != PromptStatus.OK)
                return;
            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Entity entity = Tx.GetObject(per.ObjectId, OpenMode.ForRead)
                    as Entity;
                ObjectId[] entId = new ObjectId[] { entity.ObjectId };
                IntPtr pSubentityIdPE = entity.QueryX(
                    AssocPersSubentityIdPE.GetClass(
                    typeof(AssocPersSubentityIdPE)));
                if (pSubentityIdPE == IntPtr.Zero)
                    //Entity doesn't support the subentityPE
                    return;
                AssocPersSubentityIdPE subentityIdPE =
                    AssocPersSubentityIdPE.Create(pSubentityIdPE, false)
                        as AssocPersSubentityIdPE;
                SubentityId[] vertexIds = subentityIdPE.GetAllSubentities(
                    entity,
                    SubentityType.Vertex);
                ed.WriteMessage("\n- Vertex Subentities: ");
                foreach (SubentityId subentId in vertexIds)
                {
                    FullSubentityPath path = new FullSubentityPath(entId, subentId);
                    DBPoint vertex = entity.GetSubentity(path) as DBPoint;
                    if (vertex != null)
                    {
                        ed.WriteMessage(
                            "\n . Vertex: [{0}, {1}, {2}]",
                            vertex.Position.X,
                            vertex.Position.Y,
                            vertex.Position.Z);
                        vertex.Dispose();
                    }
                }
                SubentityId[] edgeIds = subentityIdPE.GetAllSubentities(entity, SubentityType.Edge);
                ed.WriteMessage("\n- Edge Subentities: ");
                foreach (SubentityId subentId in edgeIds)
                {
                    FullSubentityPath path = new FullSubentityPath(entId, subentId);
                    Entity edgeEntity = entity.GetSubentity(path);
                    if (edgeEntity != null)
                    {
                        ed.WriteMessage("\n . " + edgeEntity.ToString());
                        edgeEntity.Dispose();
                    }
                }
                SubentityId[] faceIds = subentityIdPE.GetAllSubentities(entity, SubentityType.Face);
                ed.WriteMessage("\n- Face Subentities: ");
                Solid3d solid3d = entity as Solid3d;
                int i = 0;
                foreach (SubentityId subentId in faceIds)
                {
                    i++;
                    FullSubentityPath path = new FullSubentityPath(entId, subentId);
                    Entity faceEntity = entity.GetSubentity(path);

                    if (faceEntity != null)
                    {
                        ed.WriteMessage("\n . " + faceEntity.ToString());
                        // faceEntity.Dispose();
                    }

                    if (faceEntity != null)
                    {
                        string matName = "xxx";
                        try
                        {
                            ObjectId materialId = solid3d.GetSubentityMaterial(path.SubentId);

                            Material material = Tx.GetObject(materialId, OpenMode.ForRead) as Material;
                            matName = material.Name;

                            Mapper mapper = solid3d.GetSubentityMaterialMapper(path.SubentId);
                            Matrix3d matris = mapper.Transform;
                            Utils.TransformationParameters trparam = Utils.ReadFromTransformationMatrix(matris);
                            trparam.AutoTransformMethodOfDiffuseMapMapper = (int)mapper.AutoTransform;

                            ed.WriteMessage("\n-------------------Transformation parameters:---------------------" +
                               "\nSubentId IndexPtr : " + subentId.IndexPtr +
                               "\nUOffset:" + trparam.UOffset.ToString("0.000") +
                               "\nVOffset:" + trparam.VOffset.ToString("0.000") +
                               "\nUScale:" + trparam.UScale.ToString("0.000") +
                               "\nVScale:" + trparam.VScale.ToString("0.000") +
                               "\nRotation:" + trparam.Rotation.ToString("0.000") +
                               "\nMaterial" + matName +
                               "\n----------------------------------------------------------------------------------\n");
                        }
                        catch (System.Exception ex)
                        {
                            matName = "----Yok-----" + ex.ToString();
                        }
                        //short colIDx = solid3d.GetSubentityColor(subentId).ColorIndex;
                        short colIDx = 1;

                        if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Surface))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Surface sEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Surface;
                            ed.WriteMessage(
                            "\n *** Surface ***",
                            "\n SubentId IndexPtr : {0}" +
                            "\n Face Perimeter : {1}" +
                            "\n Face Area : {2}" +
                            "\n Color Index : {3}" +
                            "\n Material : {4}",

                            subentId.IndexPtr,
                            sEnt.Perimeter,
                            sEnt.GetArea(),
                            colIDx,
                            matName

                            //face.GetPerimeterLength()
                            //face.GetArea().ToString()
                            //brp.Solid.GetSubentityColor(fSubEntpath.SubentId).ColorIndex
                            //sEnt.ColorIndex.ToString()
                            );
                        }

                        else if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Region))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Region rEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Region;
                            ed.WriteMessage(
                            "\n *** Region ***" +
                            "\n SubentId IndexPtr : {0}" +
                            "\n Face Perimeter : {1}" +
                            "\n Face Area : {2}" +
                            "\n Color Index : {3}" +
                            "\n Material : {4}",

                            subentId.IndexPtr,
                            rEnt.Perimeter,
                            rEnt.Area,
                            colIDx,
                            matName
                            //face.GetPerimeterLength()
                            //face.GetArea().ToString()
                            //brp.Solid.GetSubentityColor(fSubEntpath.SubentId).ColorIndex
                            //rEnt.ColorIndex.ToString()
                            );
                            using (Brep brep = new Brep(rEnt))
                            //using (Brep brep = new Brep(path))
                            {
                                foreach (Autodesk.AutoCAD.BoundaryRepresentation.Face face in brep.Faces)
                                {
                                    ed.WriteMessage("\nface.SubentityPath.SubentId.IndexPtr:" + face.ToString());
                                    foreach (BoundaryLoop boundaryloop in face.Loops)
                                    {
                                        foreach (Autodesk.AutoCAD.BoundaryRepresentation.Vertex vertex in boundaryloop.Vertices)
                                        {
                                            //sfmi.points.Add(vertex.Point);
                                            ed.WriteMessage("\nvertex(" + i.ToString() + "):" +
                                                vertex.Point.X.ToString("0.000") + "," +
                                                vertex.Point.Y.ToString("0.000") + "," +
                                                vertex.Point.Z.ToString("0.000")
                                                );
                                        }
                                    } // foreach BoundaryLoop
                                }
                            } // using new Brep(rEnt)

                        }
                        else if (faceEntity.GetType() == typeof(Autodesk.AutoCAD.DatabaseServices.Face))
                        {
                            Autodesk.AutoCAD.DatabaseServices.Face rEnt = faceEntity as Autodesk.AutoCAD.DatabaseServices.Face;
                            ed.WriteMessage(
                            "\n *** Face ***",
                            "\n SubentId IndexPtr : {0}" +
                            "\n MaterialMapper : {1}" +
                            "\n Vertex(0) : {2}" +
                            "\n Material : {3}",

                            subentId.IndexPtr,
                            rEnt.MaterialMapper.ToString(),
                            rEnt.GetVertexAt(0).ToString(),
                            colIDx,
                            matName
                            );
                        }
                        //ed.WriteMessage("\n . " + faceEntity.ToString());
                        faceEntity.Dispose();
                    }
                }

            }
        }
        //[CommandMethod("grip")]
        public void getgrip()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptEntityOptions peo = new PromptEntityOptions("\nSelect a 3D solid: ");
            peo.SetRejectMessage("\nInvalid selection...");
            peo.AddAllowedClass(typeof(Solid3d), true);

            PromptEntityResult per = ed.GetEntity(peo);

            if (per.Status != PromptStatus.OK)
                return;
            // Always dispose DocumentLock
            using (doc.LockDocument())
            // Always dispose Transaction
            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                Solid3d solid = Tx.GetObject(per.ObjectId, OpenMode.ForWrite) as Solid3d;
                Point3dCollection pts = new Point3dCollection();
                for (int i = 0; i < 20; i++)
                    pts.Add(new Point3d(0, 0, 0));

                ed.WriteMessage("\naaaaaa");

                pts = GripInfo.RetrieveGripInfo(solid);
                ed.WriteMessage("\nbbbb");
                for (int i = 0; i < pts.Count; i++)
                {
                    ed.WriteMessage("\nPt[{0}] = {1}", i, pts[i]);
                }
                Tx.Commit();
            }
        }

        //[CommandMethod("parseltest1")]
        public static void Parsel3DTest1_Method()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            try
            {
                PromptEntityResult prEntRes = ed.GetEntity("\nParseli seçiniz:");
                if (prEntRes.Status == PromptStatus.OK)
                {
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        //Entity ent = (Entity)tr.GetObject(prEntRes.ObjectId, OpenMode.ForRead);
                        DBObject obj = tr.GetObject(prEntRes.ObjectId, OpenMode.ForWrite);
                        if (obj.XData != null)
                        {
                            ParselBilgileri pi = new ParselBilgileri();
                            pi.TapuBilgileri.ilceKodu = "çankaya";
                            pi.BarkodNo = "123";
                            pi.WriteToAutoCadObject(tr, obj);
                            Utils.WriteToDictionary(pi.TapuBilgileri);
                        }
                        else
                            ed.WriteMessage("\nNo XData to write.");
                        tr.Commit();
                    }
                }
            }
            catch (System.Exception ex)
            {
                ed.WriteMessage(ex.ToString());
            }
        }
        //[CommandMethod("parseltest2")]
        public static void Parsel3DTest2_Method()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            try
            {
                PromptEntityResult prEntRes = ed.GetEntity("\nParseli seçiniz:");
                if (prEntRes.Status == PromptStatus.OK)
                {
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        //Entity ent = (Entity)tr.GetObject(prEntRes.ObjectId, OpenMode.ForRead);
                        DBObject obj = tr.GetObject(prEntRes.ObjectId, OpenMode.ForRead);
                        if (obj.XData != null)
                        {
                            ParselBilgileri ps = new ParselBilgileri();
                            ps.ReadFromAutoCadObject(obj);
                            //    System.Windows.MessageBox.Show(
                            //        ps.BarKodNo + "-"
                            //        + ps.il + "-"
                            //        + ps.ilce + "-"
                            //        + ps.AdaNo + "-"
                            //        + ps.ParselNo + "-"
                            //        + ps.SiteAdi + "-"
                            //        + ps.BlokAdi + "-"
                            //        + ps.ParselCinsi.ToString() + "-"
                            //        + ps.BulunduguKat.ToString() + "-"
                            //        + ps.TabanKotu.ToString() + "-"
                            //        + ps.KatYuksekligi.ToString() + "-"
                            //        + ps.BagimsizBolumNo + "-"
                            //        + ps.Notlar
                            //        );
                        }
                        else
                            ed.WriteMessage("\nNo XData to read.");
                        tr.Commit();
                    }
                }
            }
            catch (System.Exception ex)
            {
                ed.WriteMessage(ex.ToString());
            }
        } // parseltest2

        [CommandMethod("parseltest3")]
        public static void Parsel3DTest3_Method()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            try
            {
                PromptEntityResult prEntRes = ed.GetEntity("\nParseli seçiniz:");
                if (prEntRes.Status == PromptStatus.OK)
                {
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        Solid3d sol = tr.GetObject(prEntRes.ObjectId, OpenMode.ForRead) as Solid3d;
                        List<Point3dCollection> plist = Utils.GetFloorCoordinatesFromSolid3d(sol);
                        ed.WriteMessage("\n------------------" + plist.Count.ToString() + " adet");
                        int i = 0;
                        foreach (Point3dCollection pcoll in plist)
                        {
                            ed.WriteMessage("\n------------------------------ [" + ++i + "]");
                            foreach (Point3d pt in pcoll)
                                ed.WriteMessage("\n" + pt.ToString() + ",");
                        }
                        tr.Commit();
                    }
                }
            }
            catch (System.Exception ex)
            {
                ed.WriteMessage(ex.ToString());
            }
        } // parseltest3

        //[CommandMethod("parseltest4")]
        public static void Parsel3DTest4_Method()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptSelectionOptions pso = new PromptSelectionOptions();
            pso.AllowDuplicates = false;
            pso.MessageForAdding = "\nExcel'e aktarılacak 3dSolid'leri seçiniz: ";

            TypedValue[] tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Start,"3DSOLID"),
                };

            SelectionFilter selectionfilter = new SelectionFilter(tvs);

            PromptSelectionResult selRes = doc.Editor.GetSelection(pso, selectionfilter);

            // If the user didn't make valid selection, we return

            if (selRes.Status != PromptStatus.OK)
                return;

            string copymode = (string)doc.GetLispSymbol("COPYMODE");
            ed.Command("copy", "p", "\n", "mode", "single", "0,0", "0,0");
            doc.SetLispSymbol("COPYMODE", copymode);
            //doc.SendStringToExecute("copy p  0,0 1000,1000  ", true, false, true);

        } // parseltest4

        //[CommandMethod("parseltest5")]
        public static void Parsel3DTest5_Method()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            PromptSelectionOptions pso = new PromptSelectionOptions();
            pso.AllowDuplicates = false;
            pso.MessageForAdding = "\nPolyfaceMesh yapılacak polyline'ı seçiniz: ";

            TypedValue[] tvs =
                new TypedValue[] {
                    new TypedValue((int)DxfCode.Start,"LWPOLYLINE"),
                };
            SelectionFilter selectionfilter = new SelectionFilter(tvs);
            PromptSelectionResult selRes = doc.Editor.GetSelection(pso, selectionfilter);

            // If the user didn't make valid selection, we return

            if (selRes.Status != PromptStatus.OK)
                return;

            // Set up the selection options
            PromptPointOptions opt = new PromptPointOptions("\nOrtak referans noktasını gösteriniz : ");
            opt.AllowNone = false;
            PromptPointResult res = ed.GetPoint(opt);
            if (res.Status != PromptStatus.OK)
                return;

            SelectionSet ss = selRes.Value;

            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                foreach (SelectedObject o in ss) // seçilen solidler katlar ve bağımsız bölümler olarak ayıklanıyor
                {
                    var entity = (Autodesk.AutoCAD.DatabaseServices.Polyline)tr.GetObject(o.ObjectId, OpenMode.ForRead);
                    Utils.Create3DFloor(entity, res.Value, 300);
                } // foreach
                tr.Commit();
            }
        }
        //[CommandMethod("TFS")] // http://through-the-interface.typepad.com/through_the_interface/2007/08/creating-an-aut.html
        static public void TableFromSpreadsheet()
        {
            // Hardcoding the string
            // Could also select for it
            const string dlName =
              "Import table from Excel demo";
            Document doc =
              Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            OpenFileDialog ofd =
              new OpenFileDialog(
                "Select Excel spreadsheet to link",
                null,
                "xls; xlsx",
                "ExcelFileToLink",
                OpenFileDialog.OpenFileDialogFlags.
                  DoNotTransferRemoteFiles
              );
            System.Windows.Forms.DialogResult dr =
              ofd.ShowDialog();
            if (dr != System.Windows.Forms.DialogResult.OK)
                return;
            ed.WriteMessage(
              "\nFile selected was \"{0}\".",
              ofd.Filename
            );
            PromptPointResult ppr =
              ed.GetPoint(
                "\nEnter table insertion point: "
              );
            if (ppr.Status != PromptStatus.OK)
                return;
            // Remove the Data Link, if it exists already
            DataLinkManager dlm = db.DataLinkManager;
            ObjectId dlId = dlm.GetDataLink(dlName);
            if (dlId != ObjectId.Null)
            {
                dlm.RemoveDataLink(dlId);
            }
            // Create and add the Data Link
            DataLink dl = new DataLink();
            dl.DataAdapterId = "AcExcel";
            dl.Name = dlName;
            dl.Description =
              "Excel fun with Through the Interface";
            dl.ConnectionString = ofd.Filename;
            dl.DataLinkOption =
              DataLinkOption.PersistCache;
            dl.UpdateOption |=
              (int)UpdateOption.AllowSourceUpdate;
            dlId = dlm.AddDataLink(dl);
            Transaction tr =
              doc.TransactionManager.StartTransaction();
            using (tr)
            {
                tr.AddNewlyCreatedDBObject(dl, true);
                BlockTable bt =
                  (BlockTable)tr.GetObject(
                    db.BlockTableId,
                    OpenMode.ForRead
                  );
                Table tb = new Table();
                tb.TableStyle = db.Tablestyle;
                tb.Position = ppr.Value;
                //tb.SetDataLink(0, 0, dlId, true);
                tb.Cells[0, 0].DataLink = dlId;
                tb.GenerateLayout();
                BlockTableRecord btr =
                  (BlockTableRecord)tr.GetObject(
                    db.CurrentSpaceId,
                    OpenMode.ForWrite
                  );
                btr.AppendEntity(tb);
                tr.AddNewlyCreatedDBObject(tb, true);
                tr.Commit();
            }
            // Force a regen to display the table
            ed.Regen();
        }
        [CommandMethod("MakeTexturePathsRelative")]
        public void MakeTexturePathsRelative()
        {
            string[] ChannelFlagStr = { "None", "UseDiffuse", "UseSpecular", "UseReflection", "UseOpacity", "UseBump", "UseRefraction", "UseAll" };

            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                var matDict = tr.GetObject(db.MaterialDictionaryId, OpenMode.ForWrite) as DBDictionary;
                foreach (DBDictionaryEntry dbEntry in matDict)
                {
                    Material mat = tr.GetObject(dbEntry.Value, OpenMode.ForWrite) as Material;
                    if (mat != null)
                    {
                        MaterialDiffuseComponent mdc = mat.Diffuse;
                        MaterialMap map = mdc.Map;
                        var cf = (int)mat.ChannelFlags;
                        int cfi =
                            (cf == 63 ? 7 :
                             cf == 32 ? 6 :
                             cf == 16 ? 5 :
                             cf == 8 ? 4 :
                             cf == 4 ? 3 :
                             cf == 2 ? 2 :
                             cf == 1 ? 1 : 0);
                        var channelFlags = ChannelFlagStr[(int)cfi];

                        if (map == null && channelFlags == "UseBump")
                        {
                            map = mat.Bump;
                        }

                        if (map != null)
                        {
                            Source source = map.Source;

                            Mapper mapper = map.Mapper;
                            if (mapper != null)
                            {
                                if (source == Autodesk.AutoCAD.GraphicsInterface.Source.File)
                                {
                                    MaterialMap newMap = new MaterialMap();
                                    newMap = map.Clone() as MaterialMap;

                                    var materialTexture = new MaterialTexture();
                                    materialTexture = map.Texture.Clone() as MaterialTexture;
                                    ImageFileTexture tex = materialTexture as ImageFileTexture;
                                    var sourceFileFullPath = tex.SourceFileName;
                                    tex.SourceFileName = Path.GetFileName(sourceFileFullPath);
                                    System.Windows.MessageBox.Show("tex.SourceFileName(orj):\n\n " + sourceFileFullPath + "\n\nKısa:\n\n" + tex.SourceFileName);
                                    //map.Texture = materialTexture; // Property Read-Only olduğundan sıçıyor.
                                }
                            }
                        }
                    }
                }
                tr.Commit();
            } //using Transaction tr
        }
        public void MakeTexturePathsRelative_()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            string dwgFilePath = null;
            // dwgFilePath = @"D:\Temp\Test.dwg";
            //using (Database db = new Database(false, true))
            using (Database db = doc.Database)
            {
                //db.ReadDwgFile(dwgFilePath,
                //    FileOpenMode.OpenForReadAndWriteNoShare,
                //    false, "");
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    DBDictionary nod = tr.GetObject(
                        db.NamedObjectsDictionaryId,
                        OpenMode.ForRead) as DBDictionary;
                    //ObjectId imageDictId = nod.GetAt("ACAD_IMAGE_DICT");
                    ObjectId imageDictId = RasterImageDef.GetImageDictionary(db);

                    DBDictionary imageDict = tr.GetObject(imageDictId, OpenMode.ForRead) as DBDictionary;

                    foreach (DBDictionaryEntry dbDictEntry in imageDict)
                    {
                        RasterImageDef rasterImageDef = tr.GetObject(
                            dbDictEntry.Value,
                            OpenMode.ForWrite) as RasterImageDef;
                        ed.WriteMessage("{0} Old SourcefileName : {1}",
                        Environment.NewLine, rasterImageDef.SourceFileName);
                        try
                        {
                            if (File.Exists(rasterImageDef.SourceFileName))
                            {
                                dynamic dwgPathUri
                                        = new Uri(dwgFilePath);
                                dynamic rasterImagePathUri
                                    = new Uri(rasterImageDef.SourceFileName);
                                // Make the raster image path  
                                // relative to the drawing path 
                                dynamic relativeRasterPathUri
                                    = dwgPathUri.MakeRelativeUri
                                            (rasterImagePathUri);
                                // Set the source path as relative 
                                rasterImageDef.SourceFileName
                                = Uri.UnescapeDataString(
                                    relativeRasterPathUri.ToString());
                                ed.WriteMessage(
                                    "{0} Image path changed to : {1}",
                                    Environment.NewLine,
                                    rasterImageDef.SourceFileName);
                                // Reload for AutoCAD to  
                                // resolve active path 
                                rasterImageDef.Load();
                                // Check if we found it 
                                ed.WriteMessage("{0} Image found at : {1}",
                                Environment.NewLine,
                                rasterImageDef.ActiveFileName);
                            }
                        }
                        catch (UriFormatException)
                        {
                            // Will ignore this. 
                            // If the raster image path is already relative 
                            // we might catch this exception 
                        }
                    }
                    tr.Commit();
                }
                //db.SaveAs(db.OriginalFileName,
                //        true,
                //        db.OriginalFileVersion,
                //        db.SecurityParameters);
            }
        }
    }// class commands
} // namespace
