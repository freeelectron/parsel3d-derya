﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Interop;
using Autodesk.AutoCAD.Interop.Common;
using Autodesk.AutoCAD.PlottingServices;
using System;
using System.Collections.Specialized;
using System.Windows.Forms;
using acadApp = Autodesk.AutoCAD.ApplicationServices.Application;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Threading.Tasks;

namespace Parsel3D
{
    public partial class PlanPlotForm : Form
    {

        public PlotSettingsValidator plotSettingsValidator1 { get; private set; }
        public PlotSettingsValidator plotSettingsValidator2 { get; private set; }
        public PlotSettings plotSettings1 { get; private set; }
        public PlotSettings plotSettings2 { get; private set; }
        public PromptEntityResult promptEntityResult { get; private set; }

        public string plotFilePath { get; set; }
        public bool backgroundplot { get; set; }
        public Extents2d window { get; private set; }
        public bool cizimalanibelirlendi { get; private set; }
        public string devicename1 { get; private set; }
        public string devicename2 { get; private set; }
        public string medianame1 { get; private set; }
        public string medianame2 { get; private set; }
        public string plotstylename1 { get; private set; }
        public string plotstylename2 { get; private set; }
        public StringCollection devicelist { get; private set; }
        public string dosyaAdiOnEk { get; private set; }
        public string dosyaAdiSonEk { get; private set; }


        public string katlartext { get; private set; }

        public PlanPlotForm()
        {
            cizimalanibelirlendi = false;
            InitializeComponent();
            InitDialog();
        }

        private void LoadValues()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;

            plotFilePath = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_PATH) ??
                System.IO.Path.GetDirectoryName(doc.Name);

            backgroundplot = ((string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_BACKGROUNDPLOT) == "1") ? true : false;

            dosyaAdiOnEk = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_ONEK) ??
                System.IO.Path.GetFileNameWithoutExtension(doc.Name);

            dosyaAdiSonEk = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_SONEK) ?? PlanPlotConstants.DEFAULT_SON_EK;
            devicename1 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_DEVICENAME1) ?? PlanPlotConstants.DEFAULT_DEVICE_NAME1;
            devicename2 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_DEVICENAME2) ?? PlanPlotConstants.DEFAULT_DEVICE_NAME2;
            medianame1 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_MEDIANAME1) ?? PlanPlotConstants.DEFAULT_MEDIA_NAME1;
            medianame2 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_MEDIANAME2) ?? PlanPlotConstants.DEFAULT_MEDIA_NAME2;
            plotstylename1 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_PLOTSTYLE1) ?? PlanPlotConstants.DEFAULT_PLOTSTYLE_NAME1;
            plotstylename2 = (string)doc.GetLispSymbol(PlanPlotConstants.PLANEXPORT_PLOTSTYLE2) ?? PlanPlotConstants.DEFAULT_PLOTSTYLE_NAME2;

            textboxKlasor.Text = plotFilePath;
            textBoxOnEk.Text = dosyaAdiOnEk;
            textBoxSonEk.Text = dosyaAdiSonEk;

            cbDeviceList1.SelectedIndex = cbDeviceList1.Items.IndexOf(devicename1);
            cbMediaName1.SelectedIndex = cbMediaName1.Items.IndexOf(medianame1);
            cbPlotStyle1.SelectedIndex = cbPlotStyle1.Items.IndexOf(plotstylename1);

            cbDeviceList2.SelectedIndex = cbDeviceList2.Items.IndexOf(devicename2);
            cbMediaName2.SelectedIndex = cbMediaName2.Items.IndexOf(medianame2);
            cbPlotStyle2.SelectedIndex = cbPlotStyle2.Items.IndexOf(plotstylename2);

            checkBoxArkaplanda.Checked = backgroundplot;
            window = new Extents2d(0, 0, 100, 100);
        }
        private void SaveValues()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;

            devicename1 = cbDeviceList1.Text;
            devicename2 = cbDeviceList2.Text;
            medianame1 = cbMediaName1.Text;
            medianame2 = cbMediaName2.Text;
            plotstylename1 = cbPlotStyle1.Text;
            plotstylename2 = cbPlotStyle2.Text;
            katlartext = textboxKatlar.Text;
            backgroundplot = checkBoxArkaplanda.Checked;
            plotFilePath = textboxKlasor.Text;
            dosyaAdiOnEk = textBoxOnEk.Text;
            dosyaAdiSonEk = textBoxSonEk.Text;

            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_PATH, textboxKlasor.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_BACKGROUNDPLOT, checkBoxArkaplanda.Checked ? "1" : "0");
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_ONEK, textBoxOnEk.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_SONEK, textBoxSonEk.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_DEVICENAME1, cbDeviceList1.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_DEVICENAME2, cbDeviceList2.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_MEDIANAME1, cbMediaName1.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_MEDIANAME2, cbMediaName2.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_PLOTSTYLE1, cbPlotStyle1.Text);
            doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_PLOTSTYLE2, cbPlotStyle2.Text);
        }
        private void InitDialog()
        {
            LoadValues();
        }
        private void InitDialog_()
        {
            Document doc = acadApp.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            window = new Extents2d(0, 0, 100, 100);
            backgroundplot = false;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {

                BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForRead);
                Layout lo = (Layout)tr.GetObject(btr.LayoutId, OpenMode.ForRead);
                // We need a PlotInfo object
                // linked to the layout
                PlotInfo pi = new PlotInfo
                {
                    Layout = btr.LayoutId
                };
                // We need a PlotSettings object
                // based on the layout settings
                // which we then customize

                //using (PlotSettings plotSettings1 = new PlotSettings(lo.ModelType)) // plotsettings1'i gloabl yaptık
                //{
                    plotSettings1 = new PlotSettings(lo.ModelType);
                    plotSettings1.CopyFrom(lo);
                    // The PlotSettingsValidator helps
                    // create a valid PlotSettings object
                    plotSettingsValidator1 = PlotSettingsValidator.Current;

                    // Let's first select the device
                    devicelist = plotSettingsValidator1.GetPlotDeviceList();

                    cbDeviceList1.DataSource = devicelist;
                    cbDeviceList2.DataSource = devicelist;
                    LoadValues();
                    cbDeviceList1.SelectedItem = devicename1;
                    cbDeviceList2.SelectedItem = devicename2;
                    //cbDeviceList.SelectedIndex = cbDeviceList.Items.IndexOf(devicename);
                    //System.Windows.MessageBox.Show("devlist:" + devlist.ToString() + "devname:" + devname);
                    //                PlotSettings ps = new PlotSettings(true);
                    plotSettingsValidator1.SetPlotWindowArea(plotSettings1, window);
                    plotSettingsValidator2.SetPlotWindowArea(plotSettings2, window);

                    plotSettingsValidator1.SetPlotType(
                      plotSettings1,
                    Autodesk.AutoCAD.DatabaseServices.PlotType.Window
                    //Autodesk.AutoCAD.DatabaseServices.PlotType.Extents
                    );
                    plotSettingsValidator2.SetPlotType(
                    plotSettings2,
                    Autodesk.AutoCAD.DatabaseServices.PlotType.Window); //Autodesk.AutoCAD.DatabaseServices.PlotType.Extents);
                    plotSettingsValidator1.SetUseStandardScale(plotSettings1, true);
                    plotSettingsValidator2.SetUseStandardScale(plotSettings2, true);
                    plotSettingsValidator1.SetStdScaleType(plotSettings1, StdScaleType.ScaleToFit);
                    plotSettingsValidator2.SetStdScaleType(plotSettings2, StdScaleType.ScaleToFit);
                    plotSettingsValidator1.SetPlotCentered(plotSettings1, true);
                    plotSettingsValidator2.SetPlotCentered(plotSettings2, true);

                    // We should refresh the lists,
                    // in case setting the device impacts
                    // the available media
                    plotSettingsValidator1.SetPlotConfigurationName(plotSettings1, devicename1, null);
                    plotSettingsValidator2.SetPlotConfigurationName(plotSettings2, devicename2, null);
                    plotSettingsValidator1.RefreshLists(plotSettings1);
                    plotSettingsValidator2.RefreshLists(plotSettings2);
                    StringCollection medlist1 = plotSettingsValidator1.GetCanonicalMediaNameList(plotSettings1);
                    cbMediaName1.DataSource = medlist1;
                    cbMediaName1.SelectedIndex = cbMediaName1.Items.IndexOf(medianame1);

                    StringCollection medlist2 = plotSettingsValidator2.GetCanonicalMediaNameList(plotSettings2);
                    cbMediaName2.DataSource = medlist2;
                    cbMediaName2.SelectedIndex = cbMediaName2.Items.IndexOf(medianame2);
                    //cbKagit.SelectedText = "ISO_A4_(297.00_x_210.00_MM)";

                    StringCollection pstyles = plotSettingsValidator1.GetPlotStyleSheetList();
                    cbPlotStyle1.DataSource = pstyles;
                    cbPlotStyle1.SelectedItem = plotstylename1;

                    pstyles = plotSettingsValidator2.GetPlotStyleSheetList();
                    cbPlotStyle2.DataSource = pstyles;
                    cbPlotStyle2.SelectedItem = plotstylename2;


                //cbPlotStyle.SelectedIndex = cbMediaName.Items.IndexOf(plotstylename);
                //} // using plotsettings
            } // using tr
        }// InitDialog

        private void buttonGozat_Click(object sender, EventArgs e)
        {
            //var fb = new FolderBrowserDialog();
            CommonOpenFileDialog fb = new CommonOpenFileDialog();
            fb.IsFolderPicker = true;

            var dr = fb.ShowDialog();
            if (dr == CommonFileDialogResult.Ok)
            {
                //PlotDosyaKlasoru = fb.SelectedPath;
                plotFilePath = fb.FileName;
                textboxKlasor.Text = plotFilePath;
            }
        }

        private void buttonTamam_Click(object sender, EventArgs e)
        {
            if (textboxKlasor.Text == "")
            {
                System.Windows.MessageBox.Show("Klasör seçimini boş geçmeyiniz.", "Hata!");
                textboxKlasor.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            if (textboxKatlar.Text == "")
            {
                System.Windows.MessageBox.Show("Hangi kat olduğunu yazsanız iyi olurdu.", "Hata!");
                textboxKatlar.Focus();
                this.DialogResult = DialogResult.None;
                return;
            }
            if (!cizimalanibelirlendi)
            {
                System.Windows.MessageBox.Show("Çizim Alanını belirleyiniz.", "Hata!");
                this.DialogResult = DialogResult.None;
                return;
            }

            //plotSettingsValidator1.SetCanonicalMediaName(plotSettings1, cbMediaName1.Text);
            //plotSettingsValidator1.SetCurrentStyleSheet(plotSettings1, cbPlotStyle1.Text);

            SaveValues();
        }

        private void Pickbutton_Click(object sender, EventArgs e)
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.
            Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;
            using (EditorUserInteraction UI = ed.StartUserInteraction(this))
            {
                PromptPointOptions opt = new PromptPointOptions("\nKat planının bir köşesini gösteriniz :");
                opt.AllowNone = false;
                PromptPointResult res = ed.GetPoint(opt);
                if (res.Status == PromptStatus.OK)
                {
                    var first = new Autodesk.AutoCAD.Geometry.Point2d(res.Value.X, res.Value.Y);
                    var pco = new PromptCornerOptions("\nKat planının diğer köşesini gösteriniz :",
                        new Autodesk.AutoCAD.Geometry.Point3d(first.X, first.Y,0));
                    res = ed.GetCorner(pco);
                    if (res.Status == PromptStatus.OK)
                    {
                        var second = new Autodesk.AutoCAD.Geometry.Point2d(res.Value.X, res.Value.Y);
                        // verilen pencereyi solalt, sağüst formuna getirmek gerekiyor. Yoksa çıktı boş geliyor.
                        window = new Extents2d(
                            Math.Min(first.X, second.X), // sol alt X
                            Math.Min(first.Y, second.Y), // sol alt Y
                            Math.Max(first.X, second.X), // sağ üst X
                            Math.Max(first.Y, second.Y)); // sağ üst Y
                        cizimalanibelirlendi = true;
                        //plotSettingsValidator1.SetPlotWindowArea(plotSettings1, window);
                    }
                }
            }
        }

        private void buttonPreview_Click(object sender, EventArgs e)
        {
            if (!cizimalanibelirlendi)
            {
                System.Windows.MessageBox.Show("Önce çizim alanını belirleyiniz");
                return;
            }
            var ed = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            using (EditorUserInteraction UI = ed.StartUserInteraction(this))
            {
                AcadApplication app = (AcadApplication)Autodesk.AutoCAD.ApplicationServices.Application.AcadApplication;
                app.ActiveDocument.Plot.DisplayPlotPreview(AcPreviewMode.acFullPreview);
            }
        }

        private void cbDeviceList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // We should refresh the lists,
            // in case setting the device impacts
            // the available media
            //devicename1 = cbDeviceList1.Text;
            //plotSettingsValidator1.SetPlotConfigurationName(
            //  plotSettings1,
            //  devicename1,
            //  null
            //);
            //plotSettingsValidator1.RefreshLists(plotSettings1);
            //StringCollection medlist = plotSettingsValidator1.GetCanonicalMediaNameList(plotSettings1);
            //cbMediaName1.DataSource = medlist;
            ////var sel = cbMediaName.Items.IndexOf(PlanPlotConstants.DEFAULT_MEDIA_NAME);
            //var sel = cbMediaName1.Items.IndexOf(medianame1 ?? PlanPlotConstants.DEFAULT_MEDIA_NAME1);
            //if (sel == -1)
            //    sel = 0;
            //cbMediaName1.SelectedIndex = sel;
            //medianame1 = cbMediaName1.Text;
        }

        private void cbDeviceList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // We should refresh the lists,
            // in case setting the device impacts
            // the available media
            //devicename2 = cbDeviceList2.Text;
            //plotSettingsValidator2.SetPlotConfigurationName(
            //  plotSettings2,
            //  devicename2,
            //  null
            //);
            //plotSettingsValidator2.RefreshLists(plotSettings2);
            //StringCollection medlist = plotSettingsValidator2.GetCanonicalMediaNameList(plotSettings2);
            //cbMediaName2.DataSource = medlist;
            ////var sel = cbMediaName.Items.IndexOf(PlanPlotConstants.DEFAULT_MEDIA_NAME);
            //var sel = cbMediaName2.Items.IndexOf(medianame2 ?? PlanPlotConstants.DEFAULT_MEDIA_NAME2);
            //if (sel == -1)
            //    sel = 0;
            //cbMediaName2.SelectedIndex = sel;
            //medianame2 = cbMediaName2.Text;
        }

        private void buttonCizimdenOku_Click(object sender, EventArgs e)
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;
            using (EditorUserInteraction UI = ed.StartUserInteraction(this))
            {
                var peo = new PromptEntityOptions("\nKatları belirten yazıyı seçiniz (TEXT/MTEXT) :");
                peo.SetRejectMessage("\nText ya da MText olmalıdır!");
                peo.AddAllowedClass(typeof(DBText), false);
                peo.AddAllowedClass(typeof(MText), false);

                promptEntityResult = ed.GetEntity(peo);
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    if (promptEntityResult.Status == PromptStatus.OK)
                    {
                        string kattext = null;
                        kattext = Utils.getStringFromDBTextOrMText(tr, promptEntityResult.ObjectId);
                        if (kattext == null)
                        {
                            System.Windows.MessageBox.Show("Kat yazısı okunamadı!");
                            return;
                        }
                        else
                        {
                            textboxKatlar.Text = PlanExport.KatlarTextTemizle(kattext);
                        }
                    }
                }
            }
        }
    }
    public static class PlanPlotConstants
    {
        public const string DEFAULT_MEDIA_NAME1 = "ISO_A4_(297.00_x_210.00_MM)";
        public const string DEFAULT_MEDIA_NAME2 = "Super_XGA_(1280.00_x_1024.00_Pixels)";
        public const string DEFAULT_DEVICE_NAME1 = "DWG To PDF.pc3";
        public const string DEFAULT_DEVICE_NAME2 = "PublishToWeb PNG.pc3";
        public const string DEFAULT_PLOTSTYLE_NAME1 = "acad.ctb";
        public const string DEFAULT_PLOTSTYLE_NAME2 = "acad.ctb";

        public const string DEFAULT_SON_EK = "Kat_Planı";
        public const string PLANEXPORT_PATH = "PLANEXPORT_PATH";
        public const string PLANEXPORT_ONEK = "PLANEXPORT_ONEK";
        public const string PLANEXPORT_SONEK = "PLANEXPORT_SONEK";
        public const string PLANEXPORT_DEVICENAME1 = "PLANEXPORT_DEVICENAME1";
        public const string PLANEXPORT_DEVICENAME2 = "PLANEXPORT_DEVICENAME2";
        public const string PLANEXPORT_MEDIANAME1 = "PLANEXPORT_MEDIANAME1";
        public const string PLANEXPORT_MEDIANAME2 = "PLANEXPORT_MEDIANAME2";
        public const string PLANEXPORT_PLOTSTYLE1 = "PLANEXPORT_PLOTSTYLE1";
        public const string PLANEXPORT_PLOTSTYLE2 = "PLANEXPORT_PLOTSTYLE2";
        public const string PLANEXPORT_BACKGROUNDPLOT = "PLANEXPORT_BACKGROUNDPLOT";
    }
}
