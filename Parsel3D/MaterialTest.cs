﻿

using System;

using Autodesk.AutoCAD.DatabaseServices;

using acApp = Autodesk.AutoCAD.ApplicationServices.Application;

using Autodesk.AutoCAD.GraphicsInterface;

using Autodesk.AutoCAD.ApplicationServices;

using Autodesk.AutoCAD.Runtime;

using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Colors;

namespace MaterialTest

{

    public class Commands

    {

        public static void AddMaterialToLibrary(

          String sMaterialName, String sTextureMapPath, double uOffset, double vOffset, double uScale, double vScale, double rotationDeg)

        {

            Document doc = acApp.DocumentManager.MdiActiveDocument;

            using (

              Transaction acTrans =

                doc.TransactionManager.StartTransaction())

            {

                // Get the material library

                DBDictionary matLib =

                  (DBDictionary)acTrans.GetObject(

                    doc.Database.MaterialDictionaryId, OpenMode.ForRead);



                // If this material does not exist

                if (matLib.Contains(sMaterialName) == false)

                {

                    // Create the texture map image

                    ImageFileTexture tex = new ImageFileTexture();

                    tex.SourceFileName = sTextureMapPath;



                    // Create the material map

                    //double uScale = 15, vScale = 20;
                    //double uOffset = 25, vOffset = 30;
                    double rot = rotationDeg * Math.PI / 180.0;
                    Matrix3d mx = new Matrix3d(new double[]{
              uScale*Math.Cos(rot), -uScale*Math.Sin(rot), 0, uScale * uOffset,
              vScale*Math.Sin(rot),  vScale*Math.Cos(rot), 0, vScale * vOffset,
              0, 0, 1, 0,
              0, 0, 0, 1});



                    Mapper mapper =

                      new Mapper(

                        Projection.Cylinder, Tiling.Tile, Tiling.Tile,

                        AutoTransform.None, mx);



                    MaterialMap map =

                      new MaterialMap(Source.File, tex, 1.0, mapper);



                    // Set the opacity and refraction maps

                    MaterialDiffuseComponent mdc =

                      new MaterialDiffuseComponent(new MaterialColor(), map);

                    MaterialRefractionComponent mrc =

                      new MaterialRefractionComponent(2.0, map);



                    // Create a new material

                    Material mat = new Material();

                    mat.Name = sMaterialName;

                    mat.Diffuse = mdc;

                    mat.Refraction = mrc;

                    mat.Mode = Mode.Realistic;

                    mat.Reflectivity = 1.0;

                    mat.IlluminationModel = IlluminationModel.BlinnShader;



                    // Add it to the library

                    matLib.UpgradeOpen();

                    matLib.SetAt(sMaterialName, mat);

                    acTrans.AddNewlyCreatedDBObject(mat, true);

                    acTrans.Commit();

                }

            }

        }

        public static void CreateNewMaterial()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database m_db = doc.Database;
            Editor ed = doc.Editor;

            Mapper mapper1 = new Mapper();
            MaterialTexture mttr = new MaterialTexture();

            MaterialMap map = new MaterialMap(Source.File, mttr, 1, mapper1);

            EntityColor eclr = new EntityColor(150, 150, 150);
            MaterialColor mc = new MaterialColor(Method.Override, 1, eclr);
            MaterialDiffuseComponent mdc = new MaterialDiffuseComponent(mc, map);
            MaterialSpecularComponent mck = new MaterialSpecularComponent(mc, map, 0.5);
            MaterialOpacityComponent moc = new MaterialOpacityComponent(1, map);
            MaterialRefractionComponent mrfr = new MaterialRefractionComponent(2, map);

            Material Mat = new Material
            {
                Name = "MaterialName",
                Description = "New Material",
                Diffuse = mdc,
                Specular = mck,
                Refraction = mrfr,
                Reflectivity = 1,
                Reflection = map,
                Opacity = moc,
                Ambient = mc,
                Bump = map,
                SelfIllumination = 1
            };

            Transaction tr = Application.DocumentManager.MdiActiveDocument.TransactionManager.StartTransaction();
            using (tr)
            {
                using (DBDictionary dict = (DBDictionary)tr.GetObject(m_db.MaterialDictionaryId, OpenMode.ForWrite))
                {
                    ObjectId idm = dict.SetAt(Mat.Name, Mat);
                    tr.AddNewlyCreatedDBObject(Mat, true);
                }
                tr.Commit();
            }
        }

        //[CommandMethod("MatTest")]

        public static void Test()

        {

            AddMaterialToLibrary(

              //"MyMaterial", @"C:\MaterialTest\MaterialTest\test.png");
              "MyMaterial", @"D:\Data\ege\faceexport\murat\test_2\300x200.png", 25, 30, 15, 20, 45);

        }

    }

}
