﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.PlottingServices;
using cadPolyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Parsel3D
{
    public partial class PlanExport
    {
        //[System.Runtime.InteropServices.DllImport("acad.exe",
        //          CallingConvention = System.Runtime.InteropServices.CallingConvention.Cdecl,
        //          EntryPoint = "acedTrans")
        //]
        //public static extern int acedTrans(
        //  double[] point,
        //  IntPtr fromRb,
        //  IntPtr toRb,
        //  int disp,
        //  double[] result
        //);
        //private List<Extents3d> listBagimsizBolumKoseleri;
        private List<ObjectId> listPolyId;

        public void Initialize()
        {
            listPolyId = new List<ObjectId>();
        }

        static public void PublisherDSD()
        {
            try
            {
                DsdEntryCollection collection = new DsdEntryCollection();
                DsdEntry entry;

                entry = new DsdEntry();
                entry.Layout = "Layout1";
                entry.DwgName = "c:\\Temp\\Drawing1.dwg";
                entry.Nps = "Setup1";
                entry.Title = "Sheet1";
                collection.Add(entry);
                entry = new DsdEntry();
                entry.Layout = "Layout1";
                entry.DwgName = "c:\\Temp\\Drawing2.dwg";
                entry.Nps = "Setup1";
                entry.Title = "Sheet2";
                collection.Add(entry);
                DsdData dsd = new DsdData();
                dsd.SetDsdEntryCollection(collection);
                dsd.ProjectPath = "c:\\Temp\\";
                dsd.LogFilePath = "c:\\Temp\\logdwf.log";
                dsd.SheetType = SheetType.MultiDwf;
                dsd.NoOfCopies = 1;
                dsd.DestinationName = "c:\\Temp\\PublisherTest.dwf";
                dsd.SheetSetName = "PublisherSet";
                dsd.WriteDsd("c:\\Temp\\publisher.dsd");
                int nbSheets = collection.Count;
                using (PlotProgressDialog progressDlg =
                    new PlotProgressDialog(false, nbSheets, true))
                {
                    progressDlg.set_PlotMsgString(
                        PlotMessageIndex.DialogTitle,
                        "Plot API Progress");
                    progressDlg.set_PlotMsgString(
                        PlotMessageIndex.CancelJobButtonMessage,
                        "Cancel Job");
                    progressDlg.set_PlotMsgString(
                        PlotMessageIndex.CancelSheetButtonMessage,
                        "Cancel Sheet");
                    progressDlg.set_PlotMsgString(
                        PlotMessageIndex.SheetSetProgressCaption,
                        "Job Progress");
                    progressDlg.set_PlotMsgString(
                        PlotMessageIndex.SheetProgressCaption,
                        "Sheet Progress");
                    progressDlg.UpperPlotProgressRange = 100;
                    progressDlg.LowerPlotProgressRange = 0;
                    progressDlg.UpperSheetProgressRange = 100;
                    progressDlg.LowerSheetProgressRange = 0;
                    progressDlg.IsVisible = true;
                    Autodesk.AutoCAD.Publishing.Publisher publisher =
                        Application.Publisher;
                    Autodesk.AutoCAD.PlottingServices.PlotConfigManager.
                        SetCurrentConfig("DWF6 ePlot.pc3");
                    publisher.PublishDsd(
                        "c:\\Temp\\publisher.dsd", progressDlg);
                }
            }
            catch (Autodesk.AutoCAD.Runtime.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
        public void PlanPlot_()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            var fb = new System.Windows.Forms.FolderBrowserDialog();
            fb.Description = "Resim dosyalarının saklanacağı klasörü gösteriniz :";
            var dr = fb.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                var peo = new PromptEntityOptions("\nKatları belirten yazıyı seçiniz (TEXT/MTEXT) :");
                peo.SetRejectMessage("\nText ya da MText olmalıdır!");
                peo.AddAllowedClass(typeof(DBText), false);
                peo.AddAllowedClass(typeof(MText), false);

                var per = ed.GetEntity(peo);
                if (per.Status != PromptStatus.OK)
                    return;

                var ppo = new PromptPointOptions("\nResmi alınacak bölgenin ilk köşesini gösteriniz :");
                var ppr = ed.GetPoint(ppo);
                if (ppr.Status != PromptStatus.OK)
                    return;
                var first = String.Format("{0},{1}", ppr.Value.X, ppr.Value.Y); ;
                var pco = new PromptCornerOptions("\nDiğer köşeyi gösteriniz :", ppr.Value);
                ppr = ed.GetCorner(pco);
                if (ppr.Status != PromptStatus.OK)
                    return;
                var second = String.Format("{0},{1}", ppr.Value.X, ppr.Value.Y);
                //*
                Application.DocumentManager.MdiActiveDocument.SetLispSymbol("FILEDIA", "0");
                //doc.SendStringToExecute("copy p  0,0 1000,1000  ", true, false, true);
                ed.Command("-plot",
                    "Yes", // Detailed plot configuration? [Yes/No] <No>:
                    "Model", // Enter a layout name or [?] <Model>:
                    "PublishToWeb PNG.pc3", // Enter an output device name or [?] <None>:
                    "Sun Hi-Res (1600.00 x 1280.00 Pixels)", // Enter paper size or [?] <Sun Hi-Res (1600.00 x 1280.00 Pixels)>:
                    "Landscape", // Enter drawing orientation [Portrait/Landscape] <Portrait>:
                    "No", // Plot upside down? [Yes/No] <No>:
                    "Window", // Enter plot area [Display/Extents/Limits/View/Window] <Display>:
                    first, // Enter lower left corner of window <0.000000,0.000000>:
                    second, // Enter upper right corner of window <0.000000,0.000000>:
                    "Fit", //Enter plot scale (Plotted pixels=Drawing Units) or [Fit] <Fit>:
                    "Center", // Enter plot offset (x,y) or [Center] <0.00,0.00>:
                    "Yes", // Plot with plot styles? [Yes/No] <Yes>:
                    "acad.ctb", // Enter plot style table name or [?] (enter . for none) <>:
                    "Yes", // Plot with lineweights? [Yes/No] <Yes>:
                    "As Displayed", // Enter shade plot setting [As displayed/legacy Wireframe/legacy Hidden/Visual styles/Rendered] <As displayed>:
                    "Realistic", // Enter an option [Wireframe/Hidden/Realistic/Conceptual/Shaded/shade with Edges/shades of Grey/SKetchy/X-ray/Other] <Realistic>:
                    "d:\\sil\\siiii.png", // buraya dosya adı string değişkeni gelecek..
                    "Yes", // Save changes to page setup ? Or set shade plot quality?[Yes / No / Quality] < N >:
                    "Yes" // Proceed with plot [Yes/No] <Y>:
                    );
                Application.DocumentManager.MdiActiveDocument.SetLispSymbol("FILEDIA", "1");
            }
        } // PlanPlot (iptal)

        // örnek katstr: "A BLOK 9.,10.,11.,12.,13.,14.,15. ve 16. KAT PLANI   ÖLÇEK: 1/100"
        // sonuc -> "-1,-2,9,10,11,12,13,14,15,16"

        public static string KatlarTextTemizle(string katlartext)
        {
            var katstring = katlartext.ToUpper();
            var ind = katstring.IndexOf("PLAN");
            if (ind != -1)
                katstring = katstring.Substring(0, ind); // PLAN yazısına kadar olan kısım alınıyor. Sonrası atılıyor.

            katstring = katstring.Replace("VE", ",");
            katstring = katstring.Replace(".", ",");

            if (katstring.Contains("ZEM"))
            {
                //var zemin = new string[] { "0" };
                return "0";
            }
            int carpan = 1;
            if (katstring.ToUpper().Contains("BODRUM"))
                carpan = -1;
            var newstr = Utils.RemoveNonNumberDigitsAndCharacters(katstring);
            //            System.Windows.MessageBox.Show("RemoveNonNumberDigitsAndCharacterskatstring:" + newstr);
            char[] separator = { ',' };
            string[] strArr = newstr.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            string sonuc = "";
            for(int i = 0; i < strArr.Length; i++)
            {
                if (carpan == -1)
                    sonuc = sonuc + ",-" + strArr[i];
                else
                    sonuc = sonuc + "," + strArr[i];
            }
            return sonuc.TrimStart(',');
        }

        public int[] Katlar(string katstr)
        {
            char[] separator = { ',', ' ' };
            //int[] sonuc = katstr.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray(); // tek satırda iş bitirilebilir
            int[] sonuc = katstr.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray(); // tek satırda iş bitirilebilir

//            string[] strArr = katstr.Split(separator, StringSplitOptions.RemoveEmptyEntries);
//            int[] sonuc = Array.ConvertAll(strArr, int.Parse);

            return sonuc;
        } // Katlar...

        public void PlotWindow_eski1(string filepath, Point3d first, Point3d second)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            //// Transform from UCS to DCS
            //ResultBuffer rbFrom =
            //  new ResultBuffer(new TypedValue(5003, 1)),
            //            rbTo =
            //  new ResultBuffer(new TypedValue(5003, 2));

            //double[] firres = new double[] { 0, 0, 0 };
            //double[] secres = new double[] { 0, 0, 0 };

            //// Transform the first point...
            //Utils.acedTrans(
            //  first.ToArray(),
            //  rbFrom.UnmanagedObject,
            //  rbTo.UnmanagedObject,
            //  0,
            //  firres
            //);
            //// ... and the second
            //Utils.acedTrans(
            //  second.ToArray(),
            //  rbFrom.UnmanagedObject,
            //  rbTo.UnmanagedObject,
            //  0,
            //  secres
            //);
            //// We can safely drop the Z-coord at this stage
            //Extents2d window =
            //  new Extents2d(
            //    firres[0],
            //    firres[1],
            //    secres[0],
            //    secres[1]
            //  );

            //var window = new Extents2d(first.X, first.Y, second.X, second.Y);
            // verilen pencereyi solalt, sağüst formuna getirmek gerekiyor. Yoksa çıktı boş geliyor.
            var window = new Extents2d( 
                Math.Min(first.X,second.X), // sol alt X
                Math.Min(first.Y, second.Y), // sol alt Y
                Math.Max(first.X, second.X), // sağ üst X
                Math.Max(first.Y, second.Y)); // sağ üst Y
            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                //System.Windows.MessageBox.Show("Çıktısı alınan: (" + filepath + ")");

                // We'll be plotting the current layout
                BlockTableRecord btr = (BlockTableRecord)tr.GetObject(
                    db.CurrentSpaceId,
                    OpenMode.ForRead
                  );
                Layout lo = (Layout)tr.GetObject(
                    btr.LayoutId,
                    OpenMode.ForRead
                  );
                // We need a PlotInfo object
                // linked to the layout
                PlotInfo pi = new PlotInfo
                {
                    Layout = btr.LayoutId
                };
                // We need a PlotSettings object
                // based on the layout settings
                // which we then customize
                PlotSettings ps = new PlotSettings(lo.ModelType);
                ps.CopyFrom(lo);
                // The PlotSettingsValidator helps
                // create a valid PlotSettings object
                PlotSettingsValidator psv = PlotSettingsValidator.Current;
                // We'll plot the extents, centered and
                // scaled to fit
                psv.SetPlotWindowArea(ps, window);
                psv.SetPlotType(
                  ps,
                Autodesk.AutoCAD.DatabaseServices.PlotType.Window
                );
                psv.SetUseStandardScale(ps, true);
                psv.SetStdScaleType(ps, StdScaleType.ScaleToFit);
                psv.SetPlotCentered(ps, true);
                // We'll use the standard DWF PC3, as
                // for today we're just plotting to file
                psv.SetPlotConfigurationName(
                  ps,
                   //"PublishToWeb PNG.pc3",
                   //"DWF6 ePlot.pc3",
                   "DWG To PDF.pc3",
                   "ANSI_A_(8.50_x_11.00_Inches)"
                //"ISO A4 (297.00 x 210.00 MM)"
                //"Sun Hi-Res (1600.00 x 1280.00 Pixels)"
                );

                // Set the pen settings
                var ssl = psv.GetPlotStyleSheetList();
                if (ssl.Contains("Grayscale.ctb"))
                {
                    psv.SetCurrentStyleSheet(ps, "Grayscale.ctb");
                }

                // We need to link the PlotInfo to the
                // PlotSettings and then validate it
                pi.OverrideSettings = ps;
                PlotInfoValidator piv =
                  new PlotInfoValidator
                  {
                      MediaMatchingPolicy = MatchingPolicy.MatchEnabled
                  };
                piv.Validate(pi);
                object bgp = Application.GetSystemVariable("BACKGROUNDPLOT");
                try
                {
                    Application.SetSystemVariable("BACKGROUNDPLOT", 0);
                    // A PlotEngine does the actual plotting
                    // (can also create one for Preview)
                    if (PlotFactory.ProcessPlotState ==
                        ProcessPlotState.NotPlotting)
                    {
                        PlotEngine pe = PlotFactory.CreatePublishEngine();
                        using (pe)
                        {
                            // Create a Progress Dialog to provide info
                            // and allow thej user to cancel
                            PlotProgressDialog ppd = new PlotProgressDialog(false, 3, true);
                            using (ppd)
                            {
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.DialogTitle,
                                  "Plot işlemi yürütülen:" + filepath
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.CancelJobButtonMessage,
                                  "İşlemi Kes"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.CancelSheetButtonMessage,
                                  "Çıktıyı Kes"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.SheetSetProgressCaption,
                                  "Sheet Set Progress"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.SheetProgressCaption,
                                  "Sheet Progress"
                                );
                                ppd.LowerPlotProgressRange = 0;
                                ppd.UpperPlotProgressRange = 100;
                                ppd.PlotProgressPos = 0;
                                // Let's start the plot, at last
                                ppd.OnBeginPlot();
                                ppd.IsVisible = true;
                                pe.BeginPlot(ppd, null);
                                // We'll be plotting a single document
                                pe.BeginDocument(
                                  pi,
                                  doc.Name,
                                  null,
                                  1,
                                  true, // Let's plot to file
                                  filepath//"d:\\sil\\test-output"
                                );
                                // Which contains a single sheet
                                ppd.OnBeginSheet();
                                ppd.LowerSheetProgressRange = 0;
                                ppd.UpperSheetProgressRange = 100;
                                ppd.SheetProgressPos = 0;
                                PlotPageInfo ppi = new PlotPageInfo();
                                pe.BeginPage(
                                  ppi,
                                  pi,
                                  true,
                                  null
                                );
                                pe.BeginGenerateGraphics(null);
                                pe.EndGenerateGraphics(null);
                                // Finish the sheet
                                pe.EndPage(null);
                                ppd.SheetProgressPos = 100;
                                ppd.OnEndSheet();
                                // Finish the document
                                pe.EndDocument(null);
                                // And finish the plot
                                ppd.PlotProgressPos = 100;
                                ppd.OnEndPlot();
                                pe.EndPlot(null);
                            } // using ppd
                        } // using pe
                    }
                    else
                    {
                        ed.WriteMessage(
                          "\nŞu an başka bir Plot işlemi yürütülüyor."
                        );
                    }
                }// try
                catch (System.Exception exn)
                {
                    ed.WriteMessage("\nHata Oluştu: {0}\n{1}", exn.Message, exn.StackTrace);
                    throw;
                }
                finally
                {
                    Application.SetSystemVariable("BACKGROUNDPLOT", bgp);
                }
            } // using tr
        } // PlotWindow_eski
        public void PlotWindow(PlotSettings plotsettings, string filepath, Extents2d window)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            //// Transform from UCS to DCS
            //ResultBuffer rbFrom =
            //  new ResultBuffer(new TypedValue(5003, 1)),
            //            rbTo =
            //  new ResultBuffer(new TypedValue(5003, 2));

            //double[] firres = new double[] { 0, 0, 0 };
            //double[] secres = new double[] { 0, 0, 0 };

            //// Transform the first point...
            //Utils.acedTrans(
            //  first.ToArray(),
            //  rbFrom.UnmanagedObject,
            //  rbTo.UnmanagedObject,
            //  0,
            //  firres
            //);
            //// ... and the second
            //Utils.acedTrans(
            //  second.ToArray(),
            //  rbFrom.UnmanagedObject,
            //  rbTo.UnmanagedObject,
            //  0,
            //  secres
            //);
            //// We can safely drop the Z-coord at this stage
            //Extents2d window =
            //  new Extents2d(
            //    firres[0],
            //    firres[1],
            //    secres[0],
            //    secres[1]
            //  );

            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                //System.Windows.MessageBox.Show("Çıktısı alınan: (" + filepath + ")");
                var psv = PlotSettingsValidator.Current;
                psv.SetPlotWindowArea(plotsettings, window);

                // We'll be plotting the current layout
                BlockTableRecord btr = (BlockTableRecord)tr.GetObject(
                    db.CurrentSpaceId,
                    OpenMode.ForRead
                  );
                //Layout lo = (Layout)tr.GetObject(
                //    btr.LayoutId,
                //    OpenMode.ForRead
                //  );
                // We need a PlotInfo object
                // linked to the layout
                PlotInfo pi = new PlotInfo
                {
                    Layout = btr.LayoutId
                };
                // We need a PlotSettings object
                // based on the layout settings
                // which we then customize
                // We need to link the PlotInfo to the
                // PlotSettings and then validate it
                pi.OverrideSettings = plotsettings;
                PlotInfoValidator piv =
                  new PlotInfoValidator
                  {
                      MediaMatchingPolicy = MatchingPolicy.MatchEnabled
                  };
                piv.Validate(pi);
                object bgp = Application.GetSystemVariable("BACKGROUNDPLOT");
                try
                {
                    Application.SetSystemVariable("BACKGROUNDPLOT", 0);
                    // A PlotEngine does the actual plotting
                    // (can also create one for Preview)
                    if (PlotFactory.ProcessPlotState ==
                        ProcessPlotState.NotPlotting)
                    {
                        PlotEngine pe = PlotFactory.CreatePublishEngine();
                        using (pe)
                        {
                            // Create a Progress Dialog to provide info
                            // and allow thej user to cancel
                            PlotProgressDialog ppd = new PlotProgressDialog(false, 3, true);
                            using (ppd)
                            {
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.DialogTitle,
                                  "Plot işlemi yürütülen:" + filepath
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.CancelJobButtonMessage,
                                  "İşlemi Kes"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.CancelSheetButtonMessage,
                                  "Çıktıyı Kes"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.SheetSetProgressCaption,
                                  "Sheet Set Progress"
                                );
                                ppd.set_PlotMsgString(
                                  PlotMessageIndex.SheetProgressCaption,
                                  "Sheet Progress"
                                );
                                ppd.LowerPlotProgressRange = 0;
                                ppd.UpperPlotProgressRange = 100;
                                ppd.PlotProgressPos = 0;
                                // Let's start the plot, at last
                                ppd.OnBeginPlot();
                                ppd.IsVisible = true;
                                pe.BeginPlot(ppd, null);
                                // We'll be plotting a single document
                                pe.BeginDocument(
                                  pi,
                                  doc.Name,
                                  null,
                                  1,
                                  true, // Let's plot to file
                                  filepath//"d:\\sil\\test-output"
                                );
                                // Which contains a single sheet
                                ppd.OnBeginSheet();
                                ppd.LowerSheetProgressRange = 0;
                                ppd.UpperSheetProgressRange = 100;
                                ppd.SheetProgressPos = 0;
                                PlotPageInfo ppi = new PlotPageInfo();
                                pe.BeginPage(
                                  ppi,
                                  pi,
                                  true,
                                  null
                                );
                                pe.BeginGenerateGraphics(null);
                                pe.EndGenerateGraphics(null);
                                // Finish the sheet
                                pe.EndPage(null);
                                ppd.SheetProgressPos = 100;
                                ppd.OnEndSheet();
                                // Finish the document
                                pe.EndDocument(null);
                                // And finish the plot
                                ppd.PlotProgressPos = 100;
                                ppd.OnEndPlot();
                                pe.EndPlot(null);
                            } // using ppd
                        } // using pe
                    }
                    else
                    {
                        ed.WriteMessage(
                          "\nŞu an başka bir Plot işlemi yürütülüyor."
                        );
                    }
                }// try
                catch (System.Exception exn)
                {
                    ed.WriteMessage("\nHata Oluştu: {0}\n{1}", exn.Message, exn.StackTrace);
                    throw;
                }
                finally
                {
                    Application.SetSystemVariable("BACKGROUNDPLOT", bgp);
                }
            } // using tr
        } // PlotWindow

        public void PlanExportFunction()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            string katbarkodu, katFileName;

            //string tapuBilgileriKontrolu = (string)doc.GetLispSymbol("BINABILGILERIKONTROLEDILDI");
            //if (tapuBilgileriKontrolu != "1")
            //{
            //    if (!Utils.BinaBilgileriFormuGoster())
            //        return;
            //}
            //doc.SetLispSymbol("BINABILGILERIKONTROLEDILDI", "1");

            var planplotform = new PlanPlotForm();
            var dr = planplotform.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                var tb = Utils.ReadFromDictionary();
                var pb = new ParselBilgileri
                {
                    TapuBilgileri = tb
                };
                using (doc.LockDocument())
                {
                    pb.BagimsizBolumBulunduguKat = Int32.Parse(planplotform.katlartext);
                    katbarkodu = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.KatBarkodu);
                    if (katbarkodu == "HATALIBARKOD")
                    {
                        var result = System.Windows.MessageBox.Show("Barkod oluştururken hata oluştu. Lütfen Ada/Parsel bilgilerini kontrol ediniz.\nDevam ederseniz barkod numarası boş olarak gelecek", "Dikkat!", System.Windows.MessageBoxButton.OKCancel);
                        if (result == System.Windows.MessageBoxResult.OK)
                            katbarkodu = "";
                        else return;
                    }
                    katFileName = planplotform.plotFilePath + "\\" + planplotform.dosyaAdiOnEk + "_" + katbarkodu + "_" + planplotform.dosyaAdiSonEk;

                    PlotWindowAlternative(katFileName, planplotform.devicename1, planplotform.medianame1, planplotform.plotstylename1, planplotform.window, planplotform.backgroundplot);
                    PlotWindowAlternative(katFileName, planplotform.devicename2, planplotform.medianame2, planplotform.plotstylename2, planplotform.window, planplotform.backgroundplot);

                    WBlockOut(katFileName, planplotform.window);
                }
            }
        }

        private void WBlockOut(string katFileName, Extents2d window)
        {
            Document doc = Autodesk.AutoCAD.ApplicationServices.Core.Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            try
            {
                Application.SetSystemVariable("FILEDIA", 0);
                ed.Command("-wblock", katFileName, "\n", "0,0", "c", window.MinPoint, window.MaxPoint, "");
                ed.Command("OOPS");
            }// try
            catch (System.Exception exn)
            {
                ed.WriteMessage("\nHata Oluştu: {0}\n{1}", exn.Message, exn.StackTrace);
                throw;
            }
            finally
            {
                Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("FILEDIA", 1);
            }
        }

        public void PlanExportFunction_()
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            Initialize();

            //string tapuBilgileriKontrolu = (string)doc.GetLispSymbol("BINABILGILERIKONTROLEDILDI");
            //if (tapuBilgileriKontrolu != "1")
            //{
            //    if (!Utils.BinaBilgileriFormuGoster())
            //        return;
            //}
            //doc.SetLispSymbol("BINABILGILERIKONTROLEDILDI", "1");

            var planplotform = new PlanPlotForm();
            var dr = planplotform.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                //var ppo = new PromptPointOptions("\nResmi alınacak bölgenin ilk köşesini gösteriniz :");
                //var ppr = ed.GetPoint(ppo);
                //if (ppr.Status != PromptStatus.OK)
                //    return;
                ////var first = String.Format("{0},{1}", ppr.Value.X, ppr.Value.Y); ;
                //var first = ppr.Value;
                //var pco = new PromptCornerOptions("\nDiğer köşeyi gösteriniz :", ppr.Value);
                //ppr = ed.GetCorner(pco);
                //if (ppr.Status != PromptStatus.OK)
                //    return;
                ////var second = String.Format("{0},{1}", ppr.Value.X, ppr.Value.Y);
                //var second = ppr.Value;

                var tb = Utils.ReadFromDictionary();
                var pb = new ParselBilgileri
                {
                    TapuBilgileri = tb
                };
                doc.SetLispSymbol(PlanPlotConstants.PLANEXPORT_PATH, planplotform.plotFilePath);

                SortedList<int, ObjectId> listBagimsizBolumNo = ProcessPlanArea(planplotform.window);

                using (doc.LockDocument())
                {
                    using (Transaction tr = db.TransactionManager.StartTransaction())
                    {
                        int[] katlar = Katlar(planplotform.katlartext);
                        string katbarkodu, katFileName;

                        // ilk çıktı olduğu şekliyle alınıyor
                        pb.BagimsizBolumBulunduguKat = katlar.First();
                        //katbarkodu = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.KatBarkodu);
                        //katFileName = planplotform.PlotDosyaKlasoru + "\\" + katbarkodu;
                        //PlotWindow(planplotform.ps, katFileName, planplotform.window);
                        //PlotPDF(katFileName, planplotform.devicename, planplotform.medianame, planplotform.plotstylename, planplotform.window, planplotform.onizleme);

                        int i = 1;
                        var artim = 0;
                        var bbTextLocation = new Point3d();
                        string bbTextString = "";

                        // diğer çıktılar için bb numara yazıları kat'ına göre ayarlanıp öyle çıktıya gönderiliyor.
                        //foreach (int kat in katlar.Skip(1)) // katlar, ilki hariç tek tek ele alınıyor.
                        //bool ilk = true;
                        foreach (int kat in katlar)
                        {
                            // ed.Regen(); // gereksiz..faydasız..

                            // her bir bb yazısı kat numarasına göre arttırılarak çıktıya yollanıyor.
                            foreach (KeyValuePair<int,ObjectId> listObj in listBagimsizBolumNo)
                            {
                                var entity = (Entity)tr.GetObject(listObj.Value, OpenMode.ForWrite);

                                if (entity is DBText)
                                {
                                    var dbt = entity as DBText;
                                    dbt.TextString = String.Format("{0}", Convert.ToInt32(dbt.TextString) + artim);
                                    bbTextString = dbt.TextString;
                                    bbTextLocation = dbt.Position;
                                }
                                else if (entity is MText)
                                {
                                    var mt = entity as MText;
                                    var str = mt.Contents;
                                    //System.Windows.MessageBox.Show("mt0:(" + mt.Contents + ")");
                                    mt.Contents = str.Replace(mt.Text, String.Format("{0}", Convert.ToInt32(mt.Text) + artim));
                                    bbTextString = mt.Text;
                                    bbTextLocation = mt.Location;
                                    //System.Windows.MessageBox.Show("mt1:(" + mt.Contents + ")");
                                }
                                if(bbTextString=="")
                                {
                                    System.Windows.MessageBox.Show("cort");
                                    break;
                                }
                                pb.BagimsizBolumNo = bbTextString;
                                var bbbarkodu = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.BagimsizBolumBarkodu);
                                var bbFileName = planplotform.plotFilePath + "\\" + bbbarkodu;
                                PlotBagimsizBolum(planplotform.plotSettings1, tr, bbTextLocation, bbFileName, planplotform.backgroundplot); // bağımsız bölüm çıktıya yollanıyor.
                            } // foreach

                            pb.BagimsizBolumBulunduguKat = kat;
                            katbarkodu = Utils.BarkodOlustur(ref pb, Utils.BarkodTuru.KatBarkodu);
                            katFileName = planplotform.plotFilePath + "\\" + planplotform.dosyaAdiOnEk + "_" + katbarkodu + "_" + planplotform.dosyaAdiSonEk;


                            //PlotWindow(planplotform.ps, katFileName, planplotform.window); // Komple kat planı çıktıya yollanıyor.
                            //PlotPDF(katFileName, planplotform.devicename, planplotform.medianame, planplotform.plotstylename, planplotform.window, planplotform.onizleme);
                            PlotWindowAlternative(katFileName, planplotform.devicename1, planplotform.medianame1, planplotform.plotstylename1, planplotform.window, planplotform.backgroundplot);


                            if (katlar.Length > 1) // bu kontrolü yapmazsak tek katlarda sıkıntı oluyor
                                artim = listBagimsizBolumNo.Count * (katlar[i] - katlar[i - 1]);

                            if (i < (katlar.Length - 1))
                                i++;
                        } // foreach
                        ed.UpdateScreen();
                        // tr.Commit();
                    } // using tr
                } // using doc
            } // if dr ==

        } // PlanExportFunction
        public void PlotBagimsizBolum(PlotSettings ps, Transaction tr, Point3d bbtextlocation, string fileName, bool onizleme)
        {
            foreach(ObjectId id in listPolyId)
            {
                var entity = (Entity)tr.GetObject(id, OpenMode.ForRead);
                if (entity is cadPolyline)
                {
                    var polyent = entity as cadPolyline;
                    // koordinatı verilen bb no yazısının hangi polyline içine girdiği bulunuyor.
                    if (Utils.PointInPoly(bbtextlocation.X, bbtextlocation.Y, polyent))
                    {
                        var ext = polyent.GeometricExtents;
                        //PlotWindow(ps, fileName, new Extents2d(ext.MinPoint.X, ext.MinPoint.Y, ext.MaxPoint.X, ext.MaxPoint.Y));
                        PlotPDF(fileName, ps.PlotConfigurationName, ps.CanonicalMediaName,ps.CurrentStyleSheet, new Extents2d(ext.MinPoint.X, ext.MinPoint.Y, ext.MaxPoint.X, ext.MaxPoint.Y), onizleme);
                        break;
                    }
                }
            }
        }
        public SortedList<int, ObjectId> ProcessPlanArea(Extents2d win)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            var textlist = new SortedList<int, ObjectId>();

            // Always dispose DocumentLock
            using (doc.LockDocument())
            {
                // Always dispose Transaction
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    TypedValue[] tvs =
                        new TypedValue[] {
                            new TypedValue((int)DxfCode.Operator,"<and"),
                            new TypedValue((int)DxfCode.LayerName,Utils.LAYERADI_KATPLANI),
                            new TypedValue((int)DxfCode.Operator,"<or"),
                            new TypedValue((int)DxfCode.Start,"LWPOLYLINE"),
                            new TypedValue((int)DxfCode.Start,"TEXT"),
                            new TypedValue((int)DxfCode.Start,"MTEXT"),
                            new TypedValue((int)DxfCode.Operator,"or>"),
                            new TypedValue((int)DxfCode.Operator,"and>")
                        };

                    SelectionFilter selectionfilter = new SelectionFilter(tvs);
                    var corner1 = new Point3d(win.MinPoint.X, win.MinPoint.Y, 0.0);
                    var corner2 = new Point3d(win.MaxPoint.X, win.MaxPoint.Y, 0.0);

                    PromptSelectionResult psr = doc.Editor.SelectCrossingWindow(
                        corner1, corner2,
                        selectionfilter);

                    if (psr.Status != PromptStatus.OK)
                        return textlist;
                    SelectionSet ss = psr.Value;
                    //ObjectId[] oidArray = psr.Value.Cast<SelectedObject>().Select(o => { return o.ObjectId; }).ToArray();

                    int num;
                    string str = "0";
                    int i = 0;

                    foreach (SelectedObject o in ss) // seçilen 
                    {
                        DBObject entity = tr.GetObject(o.ObjectId, OpenMode.ForRead);
                        if (entity is cadPolyline)
                        {
                            var pl = entity as cadPolyline;
                        if (pl != null)
                                listPolyId.Add(pl.ObjectId);
                            else
                                System.Windows.MessageBox.Show("sıkıntı var-1- " + (i).ToString());
                        //else
                        //    System.Windows.MessageBox.Show("amanın");
                        //System.Windows.MessageBox.Show("neresiiiiiiiiiiiiiiii? " + (i).ToString());
                    }
                    else if(entity is DBText || entity is MText)
                        {
                            if (entity is DBText)
                            {
                                var dbt = entity as DBText;
                                str = dbt.TextString;
                            }
                            else if (entity is MText)
                            {
                                var mt = entity as MText;
                                str = mt.Text;
                            }
                            // ortak alanlar pas geçiliyor 
                            // çünkü her bir katın plotu sırasında numara yazıları
                            // kat numarasına göre otomatik arttırılıyor.
                            // artış sayısı da bağımsız bölüm numara adetinden hesaplanıyor.
                            // yani : artım = bbsayısı*(şimdiki_kat_no - önceki_katno)
                            if (!str.Contains("+") &&
                                !str.Contains("-"))
                            {
                                //System.Windows.MessageBox.Show("-aaa-- " + i.ToString() + "(" + str + ")");
                                num = Convert.ToInt32(str);
                                textlist.Add(num, o.ObjectId);
                                //System.Windows.MessageBox.Show("-ccc-- ");
                            }
                        }
                        else
                            System.Windows.MessageBox.Show("-offff-- ");

                            i++;
                    }
                   // tr.Commit();
                } // using tr
            }// using (doc.LockDocument())
//            if(textlist != null)  System.Windows.MessageBox.Show(textlist.ToString());
            return textlist;
        } // ProcessPlanArea
    } // class PlanExport
}