﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using System.Collections.Generic;
using System.Linq;

namespace Parsel3D
{
    public class XDataHelpers
    {
        //const string regAppName = "EGEGY_PARSEL3D";

        // Get the XData for a particular object
        // and return the "parcel info" if it exists
        // res[0] => barcode string
        // res[1] => note
        public static List<string> ParcelInfoForObject(DBObject obj)
        {
            List<string> res = new List<string>();
            ResultBuffer rb = obj.XData;
            if (rb != null)
            {
                bool foundStart = false;

                foreach (TypedValue tv in rb)
                {
                    if (tv.TypeCode == (int)DxfCode.ExtendedDataRegAppName &&
                        tv.Value.ToString() == ParselBilgileri.regAppName)
                    {
                        foundStart = true;
                        res.Add(tv.Value.ToString());
                    }
                    else
                    {
                        if (foundStart == true)
                        {
                            res.Add(tv.Value.ToString());
                        }
                    }
                }
                rb.Dispose();
            }
            return res;
        }
        public static List<string> ParcelInfoForObject(ObjectId objectId)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Autodesk.AutoCAD.DatabaseServices.Database db = doc.Database;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                return ParcelInfoForObject(tr, objectId);
            }
        }
        public static List<string> ParcelInfoForObject(Transaction tr, ObjectId objectId)
        {
            DBObject obj = tr.GetObject(objectId, OpenMode.ForRead);
            return ParcelInfoForObject(obj);
        }

        // Set the "ParselBilgileri (Id etc.) in the XData of a particular object
        public static void SetParcelInfoOnObject(ObjectId objectId, string barcode, string note)
        {
            //Database db = objectId.OriginalDatabase;//  obj.Database;
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Autodesk.AutoCAD.DatabaseServices.Database db = doc.Database;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                SetParcelInfoOnObject(tr, objectId, barcode, note);
            }
        }
        public static void SetParcelInfoOnObject(
          Transaction tr, ObjectId objectId, string barcode, string note)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Autodesk.AutoCAD.DatabaseServices.Database db = doc.Database;

            // Make sure the application is registered
            // (we could separate this out to be called
            // only once for a set of operations)
            RegAppTable rat =
              (RegAppTable)tr.GetObject(
                db.RegAppTableId,
                OpenMode.ForRead);
            if (!rat.Has(ParselBilgileri.regAppName))
            {
                rat.UpgradeOpen(); // burada eLockViolation hatası veriyor ..
                RegAppTableRecord ratr = new RegAppTableRecord();
                ratr.Name = ParselBilgileri.regAppName;
                rat.Add(ratr);
                tr.AddNewlyCreatedDBObject(ratr, true);
            }

            // Create the XData and set it on the object

            ResultBuffer rb =
              new ResultBuffer(
                new TypedValue(
                  (int)DxfCode.ExtendedDataRegAppName, ParselBilgileri.regAppName
                ),
                new TypedValue(
                  (int)DxfCode.ExtendedDataAsciiString, barcode
                ),
                new TypedValue(
                  (int)DxfCode.ExtendedDataAsciiString, note
                )
              );
            DBObject obj = tr.GetObject(objectId, OpenMode.ForWrite);
            obj.XData = rb;
            tr.Commit();
            rb.Dispose();
        }
        //[CommandMethod("RemoveAllXDataFromEntities")]
        public static void RemoveAllXDataFromEntity(ObjectId objectId)
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            try
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    Entity ent = (Entity)tr.GetObject(objectId, OpenMode.ForRead);
                    if (ent.XData != null)
                    {
                        System.Collections.Generic.IEnumerable<string> appNames =
                            from TypedValue tv in ent.XData.AsArray()
                            where tv.TypeCode == 1001
                            select tv.Value.ToString();

                        ent.UpgradeOpen();
                        foreach (string appName in appNames)
                        {
                            ent.XData = new ResultBuffer(new TypedValue(1001, appName));
                        }
                        ed.WriteMessage("\nEge GY XData bilgisi silindi.");
                    }
                    else
                        ed.WriteMessage("\nSilinecek XData yok.");
                    tr.Commit();
                }
            }
            catch (System.Exception ex)
            {
                ed.WriteMessage(ex.ToString());
            }
        } // RemoveAllXDataFromEntity

        public static void RemoveAllXDataFromEntities_Method()
        {
            Database db = HostApplicationServices.WorkingDatabase;
            Editor ed = Application.DocumentManager.MdiActiveDocument.Editor;
            PromptSelectionResult prSelRes = Select_EgeGY_Entities(ed, "\nEge GY XData Bilgisinin silineceği objeleri seçiniz (Objede XData Bilgisi yoksa seçilmez) :");

            if (prSelRes.Status == PromptStatus.OK)
            {
                foreach (SelectedObject o in prSelRes.Value)
                {
                    RemoveAllXDataFromEntity(o.ObjectId);
                }
            }
        } // RemoveAllXDataFromEntities_Method

        public static PromptSelectionResult Select_EgeGY_Entities(Editor ed, string prompt)
        {
            // Set up our selection to only select has regAppName
            var pso = new PromptSelectionOptions();
            pso.MessageForAdding = prompt;

            var sf =
              new SelectionFilter(
                new TypedValue[]
                {
//                    new TypedValue((int)DxfCode.Start,"LWPOLYLINE"),
                    new TypedValue((int)DxfCode.ExtendedDataRegAppName,ParselBilgileri.regAppName)
                }
              );
            return ed.GetSelection(pso, sf);
        } // Select_EgeGY_Entities
    }
}