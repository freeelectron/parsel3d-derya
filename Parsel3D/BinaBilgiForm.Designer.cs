﻿using System.Collections.Generic;

namespace Parsel3D
{
    partial class BinaBilgiForm
    {
        private List<List<string>> MahalleKodlariList_Eski;
        private List<List<string>> MahalleKodlariTapuList;
        private List<List<string>> MahalleKodlariIdariList;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxIlceKodu = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAdaNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxParselNo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBinaNo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxBinaBarkodNo = new System.Windows.Forms.TextBox();
            this.textBoxBinaKirmiziKotu = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxSiteAdi = new System.Windows.Forms.TextBox();
            this.comboBoxMahalleKoduTapu = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBlokAdi = new System.Windows.Forms.TextBox();
            this.textBoxDaskNo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxNotlar = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxDaskKod = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBoxMahalleKoduIdari = new System.Windows.Forms.ComboBox();
            this.labelOndulasyon = new System.Windows.Forms.Label();
            this.textBoxOndulasyon = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxElipsoidal = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonOndulasyon = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxProjeId = new System.Windows.Forms.TextBox();
            this.textBoxOtelAdi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxIlceKodu
            // 
            this.comboBoxIlceKodu.AutoCompleteCustomSource.AddRange(new string[] {
            "Adana",
            "Adıyaman",
            "Afyonkarahisar",
            "Ağrı",
            "Aksaray",
            "Amasya",
            "Ankara",
            "Antalya",
            "Ardahan",
            "Artvin",
            "Aydın",
            "Balıkesir",
            "Bartın",
            "Batman",
            "Bayburt",
            "Bilecik",
            "Bingöl",
            "Bitlis",
            "Bolu",
            "Burdur",
            "Bursa",
            "Çanakkale",
            "Çankırı",
            "Çorum",
            "Denizli",
            "Diyarbakır",
            "Düzce",
            "Edirne",
            "Elazığ",
            "Erzincan",
            "Erzurum",
            "Eskişehir",
            "Gaziantep",
            "Giresun",
            "Gümüşhane",
            "Hakkari",
            "Hatay",
            "Iğdır",
            "Isparta",
            "İstanbul",
            "İzmir",
            "Kahramanmaraş",
            "Karabük",
            "Karaman",
            "Kars",
            "Kastamonu",
            "Kayseri",
            "Kırıkkale",
            "Kırklareli",
            "Kırşehir",
            "Kilis",
            "Kocaeli",
            "Konya",
            "Kütahya",
            "Malatya",
            "Manisa",
            "Mardin",
            "Mersin",
            "Muğla",
            "Muş",
            "Nevşehir",
            "Niğde",
            "Ordu",
            "Osmaniye",
            "Rize",
            "Sakarya",
            "Samsun",
            "Siirt",
            "Sinop",
            "Sivas",
            "Şanlıurfa",
            "Şırnak",
            "Tekirdağ",
            "Tokat",
            "Trabzon",
            "Tunceli",
            "Uşak",
            "Van",
            "Yalova",
            "Yozgat",
            "Zonguldak"});
            this.comboBoxIlceKodu.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxIlceKodu.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.comboBoxIlceKodu.DropDownWidth = 250;
            this.comboBoxIlceKodu.FormattingEnabled = true;
            this.comboBoxIlceKodu.Items.AddRange(new object[] {
            "01 Adana-Seyhan (1104)",
            "01 Adana-Ceyhan (1219)",
            "01 Adana-Feke (1329)",
            "01 Adana-Karaisalı (1437)",
            "01 Adana-Karataş (1443)",
            "01 Adana-Kozan (1486)",
            "01 Adana-Pozantı (1580)",
            "01 Adana-Saimbeyli (1588)",
            "01 Adana-Tufanbeyli (1687)",
            "01 Adana-Yumurtalık (1734)",
            "01 Adana-Yüreğir (1748)",
            "01 Adana-Aladağ (1757)",
            "01 Adana-İmamoğlu (1806)",
            "01 Adana-Sarıçam (2032)",
            "01 Adana-Çukurova (2033)",
            "02 Adıyaman-Merkez (1105)",
            "02 Adıyaman-Besni (1182)",
            "02 Adıyaman-Çelikhan (1246)",
            "02 Adıyaman-Gerger (1347)",
            "02 Adıyaman-Gölbaşı (1354)",
            "02 Adıyaman-Kahta (1425)",
            "02 Adıyaman-Samsat (1592)",
            "02 Adıyaman-Sincik (1985)",
            "02 Adıyaman-Tut (1989)",
            "03 Afyonkarahisar-Merkez (1108)",
            "03 Afyonkarahisar-Bolvadin (1200)",
            "03 Afyonkarahisar-Çay (1239)",
            "03 Afyonkarahisar-Dazkırı (1267)",
            "03 Afyonkarahisar-Dinar (1281)",
            "03 Afyonkarahisar-Emirdağ (1306)",
            "03 Afyonkarahisar-İhsaniye (1404)",
            "03 Afyonkarahisar-Sandıklı (1594)",
            "03 Afyonkarahisar-Sinanpaşa (1626)",
            "03 Afyonkarahisar-Sultandağı (1639)",
            "03 Afyonkarahisar-Şuhut (1664)",
            "03 Afyonkarahisar-Başmakçı (1771)",
            "03 Afyonkarahisar-Bayat (1773)",
            "03 Afyonkarahisar-İscehisar (1809)",
            "03 Afyonkarahisar-Çobanlar (1906)",
            "03 Afyonkarahisar-Evciler (1923)",
            "03 Afyonkarahisar-Hocalar (1944)",
            "03 Afyonkarahisar-Kızılören (1961)",
            "04 Ağrı-Merkez (1111)",
            "04 Ağrı-Diyadin (1283)",
            "04 Ağrı-Doğubayazıt (1287)",
            "04 Ağrı-Eleşkirt (1301)",
            "04 Ağrı-Hamur (1379)",
            "04 Ağrı-Patnos (1568)",
            "04 Ağrı-Taşlıçay (1667)",
            "04 Ağrı-Tutak (1691)",
            "05 Amasya-Merkez (1134)",
            "05 Amasya-Göynücek (1363)",
            "05 Amasya-Gümüşhacıköy (1368)",
            "05 Amasya-Merzifon (1524)",
            "05 Amasya-Suluova (1641)",
            "05 Amasya-Taşova (1668)",
            "05 Amasya-Hamamözü (1938)",
            "06 Ankara-Altındağ (1130)",
            "06 Ankara-Ayaş (1157)",
            "06 Ankara-Bala (1167)",
            "06 Ankara-Beypazarı (1187)",
            "06 Ankara-Çamlıdere (1227)",
            "06 Ankara-Çankaya (1231)",
            "06 Ankara-Çubuk (1260)",
            "06 Ankara-Elmadağ (1302)",
            "06 Ankara-Güdül (1365)",
            "06 Ankara-Haymana (1387)",
            "06 Ankara-Kalecik (1427)",
            "06 Ankara-Kızılcahamam (1473)",
            "06 Ankara-Nallıhan (1539)",
            "06 Ankara-Polatlı (1578)",
            "06 Ankara-Şereflikoçhisar (1658)",
            "06 Ankara-Yenimahalle (1723)",
            "06 Ankara-Gölbaşı (1744)",
            "06 Ankara-Keçiören (1745)",
            "06 Ankara-Mamak (1746)",
            "06 Ankara-Sincan (1747)",
            "06 Ankara-Kazan (1815)",
            "06 Ankara-Akyurt (1872)",
            "06 Ankara-Etimesgut (1922)",
            "06 Ankara-Evren (1924)",
            "06 Ankara-Pursaklar (2034)",
            "07 Antalya-Akseki (1121)",
            "07 Antalya-Alanya (1126)",
            "07 Antalya-Merkez (1138)",
            "07 Antalya-Elmalı (1303)",
            "07 Antalya-Finike (1333)",
            "07 Antalya-Gazipaşa (1337)",
            "07 Antalya-Gündoğmuş (1370)",
            "07 Antalya-Kaş (1451)",
            "07 Antalya-Korkuteli (1483)",
            "07 Antalya-Kumluca (1492)",
            "07 Antalya-Manavgat (1512)",
            "07 Antalya-Serik (1616)",
            "07 Antalya-Demre (1811)",
            "07 Antalya-İbradı (1946)",
            "07 Antalya-Kemer (1959)",
            "07 Antalya-Aksu (2035)",
            "07 Antalya-Döşemealtı (2036)",
            "07 Antalya-Kepez (2037)",
            "07 Antalya-Konyaaltı (2038)",
            "07 Antalya-Muratpaşa (2039)",
            "08 Artvin-Ardanuç (1145)",
            "08 Artvin-Arhavi (1147)",
            "08 Artvin-Merkez (1152)",
            "08 Artvin-Borçka (1202)",
            "08 Artvin-Hopa (1395)",
            "08 Artvin-Şavşat (1653)",
            "08 Artvin-Yusufeli (1736)",
            "08 Artvin-Murgul (1828)",
            "09 Aydın-Merkez (1159)",
            "09 Aydın-Bozdoğan (1206)",
            "09 Aydın-Çine (1256)",
            "09 Aydın-Germencik (1348)",
            "09 Aydın-Karacasu (1435)",
            "09 Aydın-Koçarlı (1479)",
            "09 Aydın-Kuşadası (1497)",
            "09 Aydın-Kuyucak (1498)",
            "09 Aydın-Nazilli (1542)",
            "09 Aydın-Söke (1637)",
            "09 Aydın-Sultanhisar (1640)",
            "09 Aydın-Yenipazar (1724)",
            "09 Aydın-Buharkent (1781)",
            "09 Aydın-İncirliova (1807)",
            "09 Aydın-Karpuzlu (1957)",
            "09 Aydın-Köşk (1968)",
            "09 Aydın-Didim (2000)",
            "09 Aydın-Efeler (2076)",
            "10 Balıkesir-Ayvalık (1161)",
            "10 Balıkesir-Merkez (1168)",
            "10 Balıkesir-Balya (1169)",
            "10 Balıkesir-Bandırma (1171)",
            "10 Balıkesir-Bigadiç (1191)",
            "10 Balıkesir-Burhaniye (1216)",
            "10 Balıkesir-Dursunbey (1291)",
            "10 Balıkesir-Edremit (1294)",
            "10 Balıkesir-Erdek (1310)",
            "10 Balıkesir-Gönen (1360)",
            "10 Balıkesir-Havran (1384)",
            "10 Balıkesir-İvrindi (1418)",
            "10 Balıkesir-Kepsut (1462)",
            "10 Balıkesir-Manyas (1514)",
            "10 Balıkesir-Savaştepe (1608)",
            "10 Balıkesir-Sındırgı (1619)",
            "10 Balıkesir-Susurluk (1644)",
            "10 Balıkesir-Marmara (1824)",
            "10 Balıkesir-Gömeç (1928)",
            "10 Balıkesir-Altıeylül (2077)",
            "10 Balıkesir-Karesi (2078)",
            "11 Bilecik-Merkez (1192)",
            "11 Bilecik-Bozüyük (1210)",
            "11 Bilecik-Gölpazarı (1359)",
            "11 Bilecik-Osmaneli (1559)",
            "11 Bilecik-Pazaryeri (1571)",
            "11 Bilecik-Söğüt (1636)",
            "11 Bilecik-Yenipazar (1857)",
            "11 Bilecik-İnhisar (1948)",
            "12 Bingöl-Merkez (1193)",
            "12 Bingöl-Genç (1344)",
            "12 Bingöl-Karlıova (1446)",
            "12 Bingöl-Kiğı (1475)",
            "12 Bingöl-Solhan (1633)",
            "12 Bingöl-Adaklı (1750)",
            "12 Bingöl-Yayladere (1855)",
            "12 Bingöl-Yedisu (1996)",
            "13 Bitlis-Adilcevaz (1106)",
            "13 Bitlis-Ahlat (1112)",
            "13 Bitlis-Merkez (1196)",
            "13 Bitlis-Hizan (1394)",
            "13 Bitlis-Mutki (1537)",
            "13 Bitlis-Tatvan (1669)",
            "13 Bitlis-Güroymak (1798)",
            "14 Bolu-Merkez (1199)",
            "14 Bolu-Gerede (1346)",
            "14 Bolu-Göynük (1364)",
            "14 Bolu-Kıbrıscık (1466)",
            "14 Bolu-Mengen (1522)",
            "14 Bolu-Mudurnu (1531)",
            "14 Bolu-Seben (1610)",
            "14 Bolu-Dörtdivan (1916)",
            "14 Bolu-Yeniçağa (1997)",
            "15 Burdur-Ağlasun (1109)",
            "15 Burdur-Bucak (1211)",
            "15 Burdur-Merkez (1215)",
            "15 Burdur-Gölhisar (1357)",
            "15 Burdur-Tefenni (1672)",
            "15 Burdur-Yeşilova (1728)",
            "15 Burdur-Karamanlı (1813)",
            "15 Burdur-Kemer (1816)",
            "15 Burdur-Altınyayla (1874)",
            "15 Burdur-Çavdır (1899)",
            "15 Burdur-Çeltikçi (1903)",
            "16 Bursa-Gemlik (1343)",
            "16 Bursa-İnegöl (1411)",
            "16 Bursa-İznik (1420)",
            "16 Bursa-Karacabey (1434)",
            "16 Bursa-Keles (1457)",
            "16 Bursa-Mudanya (1530)",
            "16 Bursa-Mustafakemalpaşa (1535)",
            "16 Bursa-Orhaneli (1553)",
            "16 Bursa-Orhangazi (1554)",
            "16 Bursa-Yenişehir (1725)",
            "16 Bursa-Büyükorhan (1783)",
            "16 Bursa-Harmancık (1799)",
            "16 Bursa-Nilüfer (1829)",
            "16 Bursa-Osmangazi (1832)",
            "16 Bursa-Yıldırım (1859)",
            "16 Bursa-Gürsu (1935)",
            "16 Bursa-Kestel (1960)",
            "17 Çanakkale-Ayvacık (1160)",
            "17 Çanakkale-Bayramiç (1180)",
            "17 Çanakkale-Biga (1190)",
            "17 Çanakkale-Bozcaada (1205)",
            "17 Çanakkale-Çan (1229)",
            "17 Çanakkale-Merkez (1230)",
            "17 Çanakkale-Eceabat (1293)",
            "17 Çanakkale-Ezine (1326)",
            "17 Çanakkale-Gelibolu (1340)",
            "17 Çanakkale-Gökçeada (1408)",
            "17 Çanakkale-Lapseki (1503)",
            "17 Çanakkale-Yenice (1722)",
            "18 Çankırı-Merkez (1232)",
            "18 Çankırı-Çerkeş (1248)",
            "18 Çankırı-Eldivan (1300)",
            "18 Çankırı-Ilgaz (1399)",
            "18 Çankırı-Kurşunlu (1494)",
            "18 Çankırı-Orta (1555)",
            "18 Çankırı-Şabanözü (1649)",
            "18 Çankırı-Yapraklı (1718)",
            "18 Çankırı-Atkaracalar (1765)",
            "18 Çankırı-Kızılırmak (1817)",
            "18 Çankırı-Bayramören (1885)",
            "18 Çankırı-Korgun (1963)",
            "19 Çorum-Alaca (1124)",
            "19 Çorum-Bayat (1177)",
            "19 Çorum-Merkez (1259)",
            "19 Çorum-İskilip (1414)",
            "19 Çorum-Kargı (1445)",
            "19 Çorum-Mecitözü (1520)",
            "19 Çorum-Ortaköy (1556)",
            "19 Çorum-Osmancık (1558)",
            "19 Çorum-Sungurlu (1642)",
            "19 Çorum-Boğazkale (1778)",
            "19 Çorum-Uğurludağ (1850)",
            "19 Çorum-Dodurga (1911)",
            "19 Çorum-Laçin (1972)",
            "19 Çorum-Oğuzlar (1976)",
            "20 Denizli-Acıpayam (1102)",
            "20 Denizli-Buldan (1214)",
            "20 Denizli-Çal (1224)",
            "20 Denizli-Çameli (1226)",
            "20 Denizli-Çardak (1233)",
            "20 Denizli-Çivril (1257)",
            "20 Denizli-Merkez (1271)",
            "20 Denizli-Güney (1371)",
            "20 Denizli-Kale (1426)",
            "20 Denizli-Sarayköy (1597)",
            "20 Denizli-Tavas (1670)",
            "20 Denizli-Babadağ (1769)",
            "20 Denizli-Bekilli (1774)",
            "20 Denizli-Honaz (1803)",
            "20 Denizli-Serinhisar (1840)",
            "20 Denizli-Pamukkale (1871)",
            "20 Denizli-Baklan (1881)",
            "20 Denizli-Beyağaç (1888)",
            "20 Denizli-Bozkurt (1889)",
            "20 Denizli-Merkezefendi (2079)",
            "21 Diyarbakır-Bismil (1195)",
            "21 Diyarbakır-Çermik (1249)",
            "21 Diyarbakır-Çınar (1253)",
            "21 Diyarbakır-Çüngüş (1263)",
            "21 Diyarbakır-Dicle (1278)",
            "21 Diyarbakır-Merkez (1284)",
            "21 Diyarbakır-Ergani (1315)",
            "21 Diyarbakır-Hani (1381)",
            "21 Diyarbakır-Hazro (1389)",
            "21 Diyarbakır-Kulp (1490)",
            "21 Diyarbakır-Lice (1504)",
            "21 Diyarbakır-Silvan (1624)",
            "21 Diyarbakır-Eğil (1791)",
            "21 Diyarbakır-Kocaköy (1962)",
            "21 Diyarbakır-Bağlar (2040)",
            "21 Diyarbakır-Kayapınar (2041)",
            "21 Diyarbakır-Sur (2042)",
            "21 Diyarbakır-Yenişehir (2043)",
            "22 Edirne-Merkez (1295)",
            "22 Edirne-Enez (1307)",
            "22 Edirne-Havsa (1385)",
            "22 Edirne-İpsala (1412)",
            "22 Edirne-Keşan (1464)",
            "22 Edirne-Lalapaşa (1502)",
            "22 Edirne-Meriç (1523)",
            "22 Edirne-Uzunköprü (1705)",
            "22 Edirne-Süloğlu (1988)",
            "23 Elazığ-Ağın (1110)",
            "23 Elazığ-Baskil (1173)",
            "23 Elazığ-Merkez (1298)",
            "23 Elazığ-Karakoçan (1438)",
            "23 Elazığ-Keban (1455)",
            "23 Elazığ-Maden (1506)",
            "23 Elazığ-Palu (1566)",
            "23 Elazığ-Sivrice (1631)",
            "23 Elazığ-Arıcak (1762)",
            "23 Elazığ-Kovancılar (1820)",
            "23 Elazığ-Alacakaya (1873)",
            "24 Erzincan-Çayırlı (1243)",
            "24 Erzincan-Merkez (1318)",
            "24 Erzincan-İliç (1406)",
            "24 Erzincan-Kemah (1459)",
            "24 Erzincan-Kemaliye (1460)",
            "24 Erzincan-Refahiye (1583)",
            "24 Erzincan-Tercan (1675)",
            "24 Erzincan-Üzümlü (1853)",
            "24 Erzincan-Otlukbeli (1977)",
            "25 Erzurum-Aşkale (1153)",
            "25 Erzurum-Çat (1235)",
            "25 Erzurum-Merkez (1319)",
            "25 Erzurum-Hınıs (1392)",
            "25 Erzurum-Horasan (1396)",
            "25 Erzurum-İspir (1416)",
            "25 Erzurum-Karayazı (1444)",
            "25 Erzurum-Narman (1540)",
            "25 Erzurum-Oltu (1550)",
            "25 Erzurum-Olur (1551)",
            "25 Erzurum-Pasinler (1567)",
            "25 Erzurum-Şenkaya (1657)",
            "25 Erzurum-Tekman (1674)",
            "25 Erzurum-Tortum (1683)",
            "25 Erzurum-Karaçoban (1812)",
            "25 Erzurum-Uzundere (1851)",
            "25 Erzurum-Pazaryolu (1865)",
            "25 Erzurum-Aziziye (1945)",
            "25 Erzurum-Köprüköy (1967)",
            "25 Erzurum-Palandöken (2044)",
            "25 Erzurum-Yakutiye (2045)",
            "26 Eskişehir-Çifteler (1255)",
            "26 Eskişehir-Merkez (1322)",
            "26 Eskişehir-Mahmudiye (1508)",
            "26 Eskişehir-Mihalıççık (1527)",
            "26 Eskişehir-Sarıcakaya (1599)",
            "26 Eskişehir-Seyitgazi (1618)",
            "26 Eskişehir-Sivrihisar (1632)",
            "26 Eskişehir-Alpu (1759)",
            "26 Eskişehir-Beylikova (1777)",
            "26 Eskişehir-İnönü (1808)",
            "26 Eskişehir-Günyüzü (1934)",
            "26 Eskişehir-Han (1939)",
            "26 Eskişehir-Mihalgazi (1973)",
            "26 Eskişehir-Odunpazarı (2046)",
            "26 Eskişehir-Tepebaşı (2047)",
            "27 Gaziantep-Araban (1139)",
            "27 Gaziantep-İslahiye (1415)",
            "27 Gaziantep-Nizip (1546)",
            "27 Gaziantep-Oğuzeli (1549)",
            "27 Gaziantep-Yavuzeli (1720)",
            "27 Gaziantep-Şahinbey (1841)",
            "27 Gaziantep-Şehitkamil (1844)",
            "27 Gaziantep-Karkamış (1956)",
            "27 Gaziantep-Nurdağı (1974)",
            "28 Giresun-Alucra (1133)",
            "28 Giresun-Bulancak (1212)",
            "28 Giresun-Dereli (1272)",
            "28 Giresun-Espiye (1320)",
            "28 Giresun-Eynesil (1324)",
            "28 Giresun-Merkez (1352)",
            "28 Giresun-Görele (1361)",
            "28 Giresun-Keşap (1465)",
            "28 Giresun-Şebinkarahisar (1654)",
            "28 Giresun-Tirebolu (1678)",
            "28 Giresun-Piraziz (1837)",
            "28 Giresun-Yağlıdere (1854)",
            "28 Giresun-Çamoluk (1893)",
            "28 Giresun-Çanakçı (1894)",
            "28 Giresun-Doğankent (1912)",
            "28 Giresun-Güce (1930)",
            "29 Gümüşhane-Merkez (1369)",
            "29 Gümüşhane-Kelkit (1458)",
            "29 Gümüşhane-Şiran (1660)",
            "29 Gümüşhane-Torul (1684)",
            "29 Gümüşhane-Köse (1822)",
            "29 Gümüşhane-Kürtün (1971)",
            "30 Hakkari-Çukurca (1261)",
            "30 Hakkari-Merkez (1377)",
            "30 Hakkari-Şemdinli (1656)",
            "30 Hakkari-Yüksekova (1737)",
            "31 Hatay-Altınözü (1131)",
            "31 Hatay-Dörtyol (1289)",
            "31 Hatay-Hassa (1382)",
            "31 Hatay-Merkez (1383)",
            "31 Hatay-İskenderun (1413)",
            "31 Hatay-Kırıkhan (1468)",
            "31 Hatay-Reyhanlı (1585)",
            "31 Hatay-Samandağ (1591)",
            "31 Hatay-Yayladağı (1721)",
            "31 Hatay-Erzin (1792)",
            "31 Hatay-Belen (1887)",
            "31 Hatay-Kumlu (1970)",
            "31 Hatay-Antakya (2080)",
            "31 Hatay-Arsuz (2081)",
            "31 Hatay-Defne (2082)",
            "31 Hatay-Payas (2083)",
            "32 Isparta-Atabey (1154)",
            "32 Isparta-Eğirdir (1297)",
            "32 Isparta-Gelendost (1341)",
            "32 Isparta-Merkez (1401)",
            "32 Isparta-Keçiborlu (1456)",
            "32 Isparta-Senirkent (1615)",
            "32 Isparta-Sütçüler (1648)",
            "32 Isparta-Şarkikaraağaç (1651)",
            "32 Isparta-Uluborlu (1699)",
            "32 Isparta-Yalvaç (1717)",
            "32 Isparta-Aksu (1755)",
            "32 Isparta-Gönen (1929)",
            "32 Isparta-Yenişarbademli (2001)",
            "33 Mersin-Anamur (1135)",
            "33 Mersin-Erdemli (1311)",
            "33 Mersin-Gülnar (1366)",
            "33 Mersin-Merkez (1402)",
            "33 Mersin-Mut (1536)",
            "33 Mersin-Silifke (1621)",
            "33 Mersin-Tarsus (1665)",
            "33 Mersin-Aydıncık (1766)",
            "33 Mersin-Bozyazı (1779)",
            "33 Mersin-Çamlıyayla (1892)",
            "33 Mersin-Akdeniz (2064)",
            "33 Mersin-Mezitli (2065)",
            "33 Mersin-Toroslar (2066)",
            "33 Mersin-Yenişehir (2067)",
            "34 İstanbul-Adalar (504)",
            "34 İstanbul-Arnavutköy (6132)",
            "34 İstanbul-Ataşehir (6133)",
            "34 İstanbul-Avcılar (505)",
            "34 İstanbul-Bağcılar (506)",
            "34 İstanbul-Bahçelievler (507)",
            "34 İstanbul-Bakırköy (508)",
            "34 İstanbul-Başakşehir (6134)",
            "34 İstanbul-Bayrampaşa (509)",
            "34 İstanbul-Beşiktaş (510)",
            "34 İstanbul-Beykoz (511)",
            "34 İstanbul-Beylikdüzü (6135)",
            "34 İstanbul-Beyoğlu (512)",
            "34 İstanbul-Büyükçekmece (513)",
            "34 İstanbul-Çatalca (514)",
            "34 İstanbul-Çekmeköy (6137)",
            "34 İstanbul-Esenler (516)",
            "34 İstanbul-Esenyurt (6136)",
            "34 İstanbul-Eyüpsultan (517)",
            "34 İstanbul-Fatih (518)",
            "34 İstanbul-Gaziosmanpaşa (519)",
            "34 İstanbul-Güngören (520)",
            "34 İstanbul-Kadıköy (521)",
            "34 İstanbul-Kağıthane (522)",
            "34 İstanbul-Kartal (523)",
            "34 İstanbul-Küçükçekmece (524)",
            "34 İstanbul-Maltepe (525)",
            "34 İstanbul-Pendik (526)",
            "34 İstanbul-Sancaktepe (6138)",
            "34 İstanbul-Sarıyer (527)",
            "34 İstanbul-Silivri (528)",
            "34 İstanbul-Sultanbeyli (529)",
            "34 İstanbul-Sultangazi (6139)",
            "34 İstanbul-Şile (530)",
            "34 İstanbul-Şişli (531)",
            "34 İstanbul-Tuzla (532)",
            "34 İstanbul-Ümraniye (533)",
            "34 İstanbul-Üsküdar (534)",
            "34 İstanbul-Zeytinburnu (535)",
            "35 İzmir-Aliağa (1128)",
            "35 İzmir-Bayındır (1178)",
            "35 İzmir-Bergama (1181)",
            "35 İzmir-Bornova (1203)",
            "35 İzmir-Çeşme (1251)",
            "35 İzmir-Dikili (1280)",
            "35 İzmir-Foça (1334)",
            "35 İzmir-Karaburun (1432)",
            "35 İzmir-Karşıyaka (1448)",
            "35 İzmir-Kemalpaşa (1461)",
            "35 İzmir-Kınık (1467)",
            "35 İzmir-Kiraz (1477)",
            "35 İzmir-Menemen (1521)",
            "35 İzmir-Ödemiş (1563)",
            "35 İzmir-Seferihisar (1611)",
            "35 İzmir-Selçuk (1612)",
            "35 İzmir-Tire (1677)",
            "35 İzmir-Torbalı (1682)",
            "35 İzmir-Urla (1703)",
            "35 İzmir-Beydağ (1776)",
            "35 İzmir-Buca (1780)",
            "35 İzmir-Konak (1819)",
            "35 İzmir-Menderes (1826)",
            "35 İzmir-Balçova (2006)",
            "35 İzmir-Çiğli (2007)",
            "35 İzmir-Gaziemir (2009)",
            "35 İzmir-Narlıdere (2013)",
            "35 İzmir-Güzelbahçe (2018)",
            "35 İzmir-Bayraklı (2056)",
            "35 İzmir-Karabağlar (2057)",
            "36 Kars-Arpaçay (1149)",
            "36 Kars-Digor (1279)",
            "36 Kars-Kağızman (1424)",
            "36 Kars-Kars Merkez (1447)",
            "36 Kars-Sarıkamış (1601)",
            "36 Kars-Selim (1614)",
            "36 Kars-Susuz (1645)",
            "36 Kars-Akyaka (1756)",
            "37 Kastamonu-Abana (1101)",
            "37 Kastamonu-Araç (1140)",
            "37 Kastamonu-Azdavay (1162)",
            "37 Kastamonu-Bozkurt (1208)",
            "37 Kastamonu-Cide (1221)",
            "37 Kastamonu-Çatalzeytin (1238)",
            "37 Kastamonu-Daday (1264)",
            "37 Kastamonu-Devrekani (1277)",
            "37 Kastamonu-İnebolu (1410)",
            "37 Kastamonu-Kastamonu (1450)",
            "37 Kastamonu-Küre (1499)",
            "37 Kastamonu-Taşköprü (1666)",
            "37 Kastamonu-Tosya (1685)",
            "37 Kastamonu-İhsangazi (1805)",
            "37 Kastamonu-Pınarbaşı (1836)",
            "37 Kastamonu-Şenpazar (1845)",
            "37 Kastamonu-Ağlı (1867)",
            "37 Kastamonu-Doğanyurt (1915)",
            "37 Kastamonu-Hanönü (1940)",
            "37 Kastamonu-Seydiler (1984)",
            "38 Kayseri-Bünyan (1218)",
            "38 Kayseri-Develi (1275)",
            "38 Kayseri-Felahiye (1330)",
            "38 Kayseri-İncesu (1409)",
            "38 Kayseri-Pınarbaşı (1576)",
            "38 Kayseri-Sarıoğlan (1603)",
            "38 Kayseri-Sarız (1605)",
            "38 Kayseri-Tomarza (1680)",
            "38 Kayseri-Yahyalı (1715)",
            "38 Kayseri-Yeşilhisar (1727)",
            "38 Kayseri-Akkışla (1752)",
            "38 Kayseri-Talas (1846)",
            "38 Kayseri-Kocasinan (1863)",
            "38 Kayseri-Melikgazi (1864)",
            "38 Kayseri-Hacılar (1936)",
            "38 Kayseri-Özvatan (1978)",
            "39 Kırklareli-Babaeski (1163)",
            "39 Kırklareli-Demirköy (1270)",
            "39 Kırklareli-Merkez (1471)",
            "39 Kırklareli-Kofçaz (1480)",
            "39 Kırklareli-Lüleburgaz (1505)",
            "39 Kırklareli-Pehlivanköy (1572)",
            "39 Kırklareli-Pınarhisar (1577)",
            "39 Kırklareli-Vize (1714)",
            "40 Kırşehir-Çiçekdağı (1254)",
            "40 Kırşehir-Kaman (1429)",
            "40 Kırşehir-Merkez (1472)",
            "40 Kırşehir-Mucur (1529)",
            "40 Kırşehir-Akpınar (1754)",
            "40 Kırşehir-Akçakent (1869)",
            "40 Kırşehir-Boztepe (1890)",
            "41 Kocaeli-Gebze (1338)",
            "41 Kocaeli-Gölcük (1355)",
            "41 Kocaeli-Kandıra (1430)",
            "41 Kocaeli-Karamürsel (1440)",
            "41 Kocaeli-Merkez (1478)",
            "41 Kocaeli-Körfez (1821)",
            "41 Kocaeli-Derince (2030)",
            "41 Kocaeli-Başiskele (2058)",
            "41 Kocaeli-Çayırova (2059)",
            "41 Kocaeli-Darıca (2060)",
            "41 Kocaeli-Dilovası (2061)",
            "41 Kocaeli-İzmit (2062)",
            "41 Kocaeli-Kartepe (2063)",
            "42 Konya-Akşehir (1122)",
            "42 Konya-Beyşehir (1188)",
            "42 Konya-Bozkır (1207)",
            "42 Konya-Cihanbeyli (1222)",
            "42 Konya-Çumra (1262)",
            "42 Konya-Doğanhisar (1285)",
            "42 Konya-Ereğli (1312)",
            "42 Konya-Hadim (1375)",
            "42 Konya-Ilgın (1400)",
            "42 Konya-Kadınhanı (1422)",
            "42 Konya-Karapınar (1441)",
            "42 Konya-Kulu (1491)",
            "42 Konya-Sarayönü (1598)",
            "42 Konya-Seydişehir (1617)",
            "42 Konya-Yunak (1735)",
            "42 Konya-Akören (1753)",
            "42 Konya-Altınekin (1760)",
            "42 Konya-Derebucak (1789)",
            "42 Konya-Hüyük (1804)",
            "42 Konya-Karatay (1814)",
            "42 Konya-Meram (1827)",
            "42 Konya-Selçuklu (1839)",
            "42 Konya-Taşkent (1848)",
            "42 Konya-Ahırlı (1868)",
            "42 Konya-Çeltik (1902)",
            "42 Konya-Derbent (1907)",
            "42 Konya-Emirgazi (1920)",
            "42 Konya-Güneysınır (1933)",
            "42 Konya-Halkapınar (1937)",
            "42 Konya-Tuzlukçu (1990)",
            "42 Konya-Yalıhüyük (1994)",
            "43 Kütahya-Altıntaş (1132)",
            "43 Kütahya-Domaniç (1288)",
            "43 Kütahya-Emet (1304)",
            "43 Kütahya-Gediz (1339)",
            "43 Kütahya-Merkez (1500)",
            "43 Kütahya-Simav (1625)",
            "43 Kütahya-Tavşanlı (1671)",
            "43 Kütahya-Aslanapa (1764)",
            "43 Kütahya-Dumlupınar (1790)",
            "43 Kütahya-Hisarcık (1802)",
            "43 Kütahya-Şaphane (1843)",
            "43 Kütahya-Çavdarhisar (1898)",
            "43 Kütahya-Pazarlar (1979)",
            "44 Malatya-Akçadağ (1114)",
            "44 Malatya-Arapgir (1143)",
            "44 Malatya-Arguvan (1148)",
            "44 Malatya-Darende (1265)",
            "44 Malatya-Doğanşehir (1286)",
            "44 Malatya-Hekimhan (1390)",
            "44 Malatya-Merkez (1509)",
            "44 Malatya-Pütürge (1582)",
            "44 Malatya-Yeşilyurt (1729)",
            "44 Malatya-Battalgazi (1772)",
            "44 Malatya-Doğanyol (1914)",
            "44 Malatya-Kale (1953)",
            "44 Malatya-Kuluncak (1969)",
            "44 Malatya-Yazıhan (1995)",
            "45 Manisa-Akhisar (1118)",
            "45 Manisa-Alaşehir (1127)",
            "45 Manisa-Demirci (1269)",
            "45 Manisa-Gördes (1362)",
            "45 Manisa-Kırkağaç (1470)",
            "45 Manisa-Kula (1489)",
            "45 Manisa-Merkez (1513)",
            "45 Manisa-Salihli (1590)",
            "45 Manisa-Sarıgöl (1600)",
            "45 Manisa-Saruhanlı (1606)",
            "45 Manisa-Selendi (1613)",
            "45 Manisa-Soma (1634)",
            "45 Manisa-Turgutlu (1689)",
            "45 Manisa-Ahmetli (1751)",
            "45 Manisa-Gölmarmara (1793)",
            "45 Manisa-Köprübaşı (1965)",
            "45 Manisa-Şehzadeler (2086)",
            "45 Manisa-Yunusemre (2087)",
            "46 Kahramanmaraş-Afşin (1107)",
            "46 Kahramanmaraş-Andırın (1136)",
            "46 Kahramanmaraş-Elbistan (1299)",
            "46 Kahramanmaraş-Göksun (1353)",
            "46 Kahramanmaraş-Merkez (1515)",
            "46 Kahramanmaraş-Pazarcık (1570)",
            "46 Kahramanmaraş-Türkoğlu (1694)",
            "46 Kahramanmaraş-Çağlayancerit (1785)",
            "46 Kahramanmaraş-Ekinözü (1919)",
            "46 Kahramanmaraş-Nurhak (1975)",
            "46 Kahramanmaraş-Dulkadiroğlu (2084)",
            "46 Kahramanmaraş-Onikişubat (2085)",
            "47 Mardin-Derik (1273)",
            "47 Mardin-Kızıltepe (1474)",
            "47 Mardin-Merkez (1516)",
            "47 Mardin-Mazıdağı (1519)",
            "47 Mardin-Midyat (1526)",
            "47 Mardin-Nusaybin (1547)",
            "47 Mardin-Ömerli (1564)",
            "47 Mardin-Savur (1609)",
            "47 Mardin-Dargeçit (1787)",
            "47 Mardin-Yeşilli (2002)",
            "47 Mardin-Artuklu (2088)",
            "48 Muğla-Bodrum (1197)",
            "48 Muğla-Datça (1266)",
            "48 Muğla-Fethiye (1331)",
            "48 Muğla-Köyceğiz (1488)",
            "48 Muğla-Marmaris (1517)",
            "48 Muğla-Milas (1528)",
            "48 Muğla-Merkez (1532)",
            "48 Muğla-Ula (1695)",
            "48 Muğla-Yatağan (1719)",
            "48 Muğla-Dalaman (1742)",
            "48 Muğla-Ortaca (1831)",
            "48 Muğla-Kavaklıdere (1958)",
            "48 Muğla-Menteşe (2089)",
            "48 Muğla-Seydikemer (2090)",
            "49 Muş-Bulanık (1213)",
            "49 Muş-Malazgirt (1510)",
            "49 Muş-Merkez (1534)",
            "49 Muş-Varto (1711)",
            "49 Muş-Hasköy (1801)",
            "49 Muş-Korkut (1964)",
            "50 Nevşehir-Avanos (1155)",
            "50 Nevşehir-Derinkuyu (1274)",
            "50 Nevşehir-Gülşehir (1367)",
            "50 Nevşehir-Hacıbektaş (1374)",
            "50 Nevşehir-Kozaklı (1485)",
            "50 Nevşehir-Merkez (1543)",
            "50 Nevşehir-Ürgüp (1707)",
            "50 Nevşehir-Acıgöl (1749)",
            "51 Niğde-Bor (1201)",
            "51 Niğde-Çamardı (1225)",
            "51 Niğde-Merkez (1544)",
            "51 Niğde-Ulukışla (1700)",
            "51 Niğde-Altunhisar (1876)",
            "51 Niğde-Çiftlik (1904)",
            "52 Ordu-Akkuş (1119)",
            "52 Ordu-Aybastı (1158)",
            "52 Ordu-Fatsa (1328)",
            "52 Ordu-Gölköy (1358)",
            "52 Ordu-Korgan (1482)",
            "52 Ordu-Kumru (1493)",
            "52 Ordu-Mesudiye (1525)",
            "52 Ordu-Merkez (1552)",
            "52 Ordu-Perşembe (1573)",
            "52 Ordu-Ulubey (1696)",
            "52 Ordu-Ünye (1706)",
            "52 Ordu-Gülyalı (1795)",
            "52 Ordu-Gürgentepe (1797)",
            "52 Ordu-Çamaş (1891)",
            "52 Ordu-Çatalpınar (1897)",
            "52 Ordu-Çaybaşı (1900)",
            "52 Ordu-İkizce (1947)",
            "52 Ordu-Kabadüz (1950)",
            "52 Ordu-Kabataş (1951)",
            "52 Ordu-Altınordu (2103)",
            "53 Rize-Ardeşen (1146)",
            "53 Rize-Çamlıhemşin (1228)",
            "53 Rize-Çayeli (1241)",
            "53 Rize-Fındıklı (1332)",
            "53 Rize-İkizdere (1405)",
            "53 Rize-Kalkandere (1428)",
            "53 Rize-Pazar (1569)",
            "53 Rize-Merkez (1586)",
            "53 Rize-Güneysu (1796)",
            "53 Rize-Derepazarı (1908)",
            "53 Rize-Hemşin (1943)",
            "53 Rize-İyidere (1949)",
            "54 Sakarya-Akyazı (1123)",
            "54 Sakarya-Geyve (1351)",
            "54 Sakarya-Hendek (1391)",
            "54 Sakarya-Karasu (1442)",
            "54 Sakarya-Kaynarca (1453)",
            "54 Sakarya-Merkez (1589)",
            "54 Sakarya-Sapanca (1595)",
            "54 Sakarya-Kocaali (1818)",
            "54 Sakarya-Pamukova (1833)",
            "54 Sakarya-Taraklı (1847)",
            "54 Sakarya-Ferizli (1925)",
            "54 Sakarya-Karapürçek (1955)",
            "54 Sakarya-Söğütlü (1986)",
            "54 Sakarya-Adapazarı (2068)",
            "54 Sakarya-Arifiye (2069)",
            "54 Sakarya-Erenler (2070)",
            "54 Sakarya-Serdivan (2071)",
            "55 Samsun-Alaçam (1125)",
            "55 Samsun-Bafra (1164)",
            "55 Samsun-Çarşamba (1234)",
            "55 Samsun-Havza (1386)",
            "55 Samsun-Kavak (1452)",
            "55 Samsun-Ladik (1501)",
            "55 Samsun-Merkez (1593)",
            "55 Samsun-Terme (1676)",
            "55 Samsun-Vezirköprü (1712)",
            "55 Samsun-Asarcık (1763)",
            "55 Samsun-19 Mayıs (1830)",
            "55 Samsun-Salıpazarı (1838)",
            "55 Samsun-Tekkeköy (1849)",
            "55 Samsun-Ayvacık (1879)",
            "55 Samsun-Yakakent (1993)",
            "55 Samsun-Atakum (2072)",
            "55 Samsun-Canik (2073)",
            "55 Samsun-İlkadım (2074)",
            "56 Siirt-Baykan (1179)",
            "56 Siirt-Eruh (1317)",
            "56 Siirt-Kurtalan (1495)",
            "56 Siirt-Pervari (1575)",
            "56 Siirt-Merkez (1620)",
            "56 Siirt-Şirvan (1662)",
            "56 Siirt-Tillo (1878)",
            "57 Sinop-Ayancık (1156)",
            "57 Sinop-Boyabat (1204)",
            "57 Sinop-Durağan (1290)",
            "57 Sinop-Erfelek (1314)",
            "57 Sinop-Gerze (1349)",
            "57 Sinop-Merkez (1627)",
            "57 Sinop-Türkeli (1693)",
            "57 Sinop-Dikmen (1910)",
            "57 Sinop-Saraydüzü (1981)",
            "58 Sivas-Divriği (1282)",
            "58 Sivas-Gemerek (1342)",
            "58 Sivas-Gürün (1373)",
            "58 Sivas-Hafik (1376)",
            "58 Sivas-İmranlı (1407)",
            "58 Sivas-Kangal (1431)",
            "58 Sivas-Koyulhisar (1484)",
            "58 Sivas-Merkez (1628)",
            "58 Sivas-Suşehri (1646)",
            "58 Sivas-Şarkışla (1650)",
            "58 Sivas-Yıldızeli (1731)",
            "58 Sivas-Zara (1738)",
            "58 Sivas-Akıncılar (1870)",
            "58 Sivas-Altınyayla (1875)",
            "58 Sivas-Doğanşar (1913)",
            "58 Sivas-Gölova (1927)",
            "58 Sivas-Ulaş (1991)",
            "59 Tekirdağ-Çerkezköy (1250)",
            "59 Tekirdağ-Çorlu (1258)",
            "59 Tekirdağ-Hayrabolu (1388)",
            "59 Tekirdağ-Malkara (1511)",
            "59 Tekirdağ-Muratlı (1538)",
            "59 Tekirdağ-Saray (1596)",
            "59 Tekirdağ-Şarköy (1652)",
            "59 Tekirdağ-Merkez (1673)",
            "59 Tekirdağ-Marmaraereğlisi (1825)",
            "59 Tekirdağ-Ergene (2094)",
            "59 Tekirdağ-Kapaklı (2095)",
            "59 Tekirdağ-Süleymanpaşa (2096)",
            "60 Tokat-Almus (1129)",
            "60 Tokat-Artova (1151)",
            "60 Tokat-Erbaa (1308)",
            "60 Tokat-Niksar (1545)",
            "60 Tokat-Reşadiye (1584)",
            "60 Tokat-Merkez (1679)",
            "60 Tokat-Turhal (1690)",
            "60 Tokat-Zile (1740)",
            "60 Tokat-Pazar (1834)",
            "60 Tokat-Yeşilyurt (1858)",
            "60 Tokat-Başçiftlik (1883)",
            "60 Tokat-Sulusaray (1987)",
            "61 Trabzon-Akçaabat (1113)",
            "61 Trabzon-Araklı (1141)",
            "61 Trabzon-Arsin (1150)",
            "61 Trabzon-Çaykara (1244)",
            "61 Trabzon-Maçka (1507)",
            "61 Trabzon-Of (1548)",
            "61 Trabzon-Sürmene (1647)",
            "61 Trabzon-Tonya (1681)",
            "61 Trabzon-Merkez (1686)",
            "61 Trabzon-Vakfıkebir (1709)",
            "61 Trabzon-Yomra (1732)",
            "61 Trabzon-Beşikdüzü (1775)",
            "61 Trabzon-Şalpazarı (1842)",
            "61 Trabzon-Çarşıbaşı (1896)",
            "61 Trabzon-Dernekpazarı (1909)",
            "61 Trabzon-Düzköy (1917)",
            "61 Trabzon-Hayrat (1942)",
            "61 Trabzon-Köprübaşı (1966)",
            "61 Trabzon-Ortahisar (2097)",
            "62 Tunceli-Çemişgezek (1247)",
            "62 Tunceli-Hozat (1397)",
            "62 Tunceli-Mazgirt (1518)",
            "62 Tunceli-Nazımiye (1541)",
            "62 Tunceli-Ovacık (1562)",
            "62 Tunceli-Pertek (1574)",
            "62 Tunceli-Pülümür (1581)",
            "62 Tunceli-Merkez (1688)",
            "63 Şanlıurfa-Akçakale (1115)",
            "63 Şanlıurfa-Birecik (1194)",
            "63 Şanlıurfa-Bozova (1209)",
            "63 Şanlıurfa-Ceylanpınar (1220)",
            "63 Şanlıurfa-Halfeti (1378)",
            "63 Şanlıurfa-Hilvan (1393)",
            "63 Şanlıurfa-Siverek (1630)",
            "63 Şanlıurfa-Suruç (1643)",
            "63 Şanlıurfa-Merkez (1702)",
            "63 Şanlıurfa-Viranşehir (1713)",
            "63 Şanlıurfa-Harran (1800)",
            "63 Şanlıurfa-Eyyübiye (2091)",
            "63 Şanlıurfa-Haliliye (2092)",
            "63 Şanlıurfa-Karaköprü (2093)",
            "64 Uşak-Banaz (1170)",
            "64 Uşak-Eşme (1323)",
            "64 Uşak-Karahallı (1436)",
            "64 Uşak-Sivaslı (1629)",
            "64 Uşak-Ulubey (1697)",
            "64 Uşak-Merkez (1704)",
            "65 Van-Başkale (1175)",
            "65 Van-Çatak (1236)",
            "65 Van-Erciş (1309)",
            "65 Van-Gevaş (1350)",
            "65 Van-Gürpınar (1372)",
            "65 Van-Muradiye (1533)",
            "65 Van-Özalp (1565)",
            "65 Van-Merkez (1710)",
            "65 Van-Bahçesaray (1770)",
            "65 Van-Çaldıran (1786)",
            "65 Van-Edremit (1918)",
            "65 Van-Saray (1980)",
            "65 Van-İpekyolu (2098)",
            "65 Van-Tuşba (2099)",
            "66 Yozgat-Akdağmadeni (1117)",
            "66 Yozgat-Boğazlıyan (1198)",
            "66 Yozgat-Çayıralan (1242)",
            "66 Yozgat-Çekerek (1245)",
            "66 Yozgat-Sarıkaya (1602)",
            "66 Yozgat-Sorgun (1635)",
            "66 Yozgat-Şefaatli (1655)",
            "66 Yozgat-Yerköy (1726)",
            "66 Yozgat-Merkez (1733)",
            "66 Yozgat-Aydıncık (1877)",
            "66 Yozgat-Çandır (1895)",
            "66 Yozgat-Kadışehri (1952)",
            "66 Yozgat-Saraykent (1982)",
            "66 Yozgat-Yenifakılı (1998)",
            "67 Zonguldak-Çaycuma (1240)",
            "67 Zonguldak-Devrek (1276)",
            "67 Zonguldak-Ereğli (1313)",
            "67 Zonguldak-Merkez (1741)",
            "67 Zonguldak-Alaplı (1758)",
            "67 Zonguldak-Gökçebey (1926)",
            "67 Zonguldak-Kilimli (2100)",
            "67 Zonguldak-Kozlu (2101)",
            "68 Aksaray-Merkez (1120)",
            "68 Aksaray-Ortaköy (1557)",
            "68 Aksaray-Ağaçören (1860)",
            "68 Aksaray-Güzelyurt (1861)",
            "68 Aksaray-Sarıyahşi (1866)",
            "68 Aksaray-Eskil (1921)",
            "68 Aksaray-Gülağaç (1932)",
            "69 Bayburt-Merkez (1176)",
            "69 Bayburt-Aydıntepe (1767)",
            "69 Bayburt-Demirözü (1788)",
            "70 Karaman-Ermenek (1316)",
            "70 Karaman-Merkez (1439)",
            "70 Karaman-Ayrancı (1768)",
            "70 Karaman-Kazımkarabekir (1862)",
            "70 Karaman-Başyayla (1884)",
            "70 Karaman-Sarıveliler (1983)",
            "71 Kırıkkale-Delice (1268)",
            "71 Kırıkkale-Keskin (1463)",
            "71 Kırıkkale-Merkez (1469)",
            "71 Kırıkkale-Sulakyurt (1638)",
            "71 Kırıkkale-Bahşili (1880)",
            "71 Kırıkkale-Balışeyh (1882)",
            "71 Kırıkkale-Çelebi (1901)",
            "71 Kırıkkale-Karakeçili (1954)",
            "71 Kırıkkale-Yahşihan (1992)",
            "72 Batman-Merkez (1174)",
            "72 Batman-Beşiri (1184)",
            "72 Batman-Gercüş (1345)",
            "72 Batman-Kozluk (1487)",
            "72 Batman-Sason (1607)",
            "72 Batman-Hasankeyf (1941)",
            "73 Şırnak-Beytüşşebap (1189)",
            "73 Şırnak-Cizre (1223)",
            "73 Şırnak-İdil (1403)",
            "73 Şırnak-Silopi (1623)",
            "73 Şırnak-Merkez (1661)",
            "73 Şırnak-Uludere (1698)",
            "73 Şırnak-Güçlükonak (1931)",
            "74 Bartın-Merkez (1172)",
            "74 Bartın-Kurucaşile (1496)",
            "74 Bartın-Ulus (1701)",
            "74 Bartın-Amasra (1761)",
            "75 Ardahan-Merkez (1144)",
            "75 Ardahan-Çıldır (1252)",
            "75 Ardahan-Göle (1356)",
            "75 Ardahan-Hanak (1380)",
            "75 Ardahan-Posof (1579)",
            "75 Ardahan-Damal (2008)",
            "76 Iğdır-Aralık (1142)",
            "76 Iğdır-Merkez (1398)",
            "76 Iğdır-Tuzluca (1692)",
            "76 Iğdır-Karakoyunlu (2011)",
            "77 Yalova-Merkez (1716)",
            "77 Yalova-Altınova (2019)",
            "77 Yalova-Armutlu (2020)",
            "77 Yalova-Çınarcık (2021)",
            "77 Yalova-Çiftlikköy (2022)",
            "77 Yalova-Termal (2026)",
            "78 Karabük-Eflani (1296)",
            "78 Karabük-Eskipazar (1321)",
            "78 Karabük-Merkez (1433)",
            "78 Karabük-Ovacık (1561)",
            "78 Karabük-Safranbolu (1587)",
            "78 Karabük-Yenice (1856)",
            "79 Kilis-Merkez (1476)",
            "79 Kilis-Elbeyli (2023)",
            "79 Kilis-Musabeyli (2024)",
            "79 Kilis-Polateli (2025)",
            "80 Osmaniye-Bahçe (1165)",
            "80 Osmaniye-Kadirli (1423)",
            "80 Osmaniye-Merkez (1560)",
            "80 Osmaniye-Düziçi (1743)",
            "80 Osmaniye-Hasanbeyli (2027)",
            "80 Osmaniye-Sumbas (2028)",
            "80 Osmaniye-Toprakkale (2029)",
            "81 Düzce-Akçakoca (1116)",
            "81 Düzce-Merkez (1292)",
            "81 Düzce-Yığılca (1730)",
            "81 Düzce-Cumayeri (1784)",
            "81 Düzce-Gölyaka (1794)",
            "81 Düzce-Çilimli (1905)",
            "81 Düzce-Gümüşova (2017)",
            "81 Düzce-Kaynaşlı (2031)"});
            this.comboBoxIlceKodu.Location = new System.Drawing.Point(192, 63);
            this.comboBoxIlceKodu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxIlceKodu.Name = "comboBoxIlceKodu";
            this.comboBoxIlceKodu.Size = new System.Drawing.Size(269, 24);
            this.comboBoxIlceKodu.TabIndex = 0;
            this.comboBoxIlceKodu.SelectedIndexChanged += new System.EventHandler(this.comboBoxIlceKodu_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 66);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "İlçe Kodu :";
            // 
            // textBoxAdaNo
            // 
            this.textBoxAdaNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxAdaNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxAdaNo.Location = new System.Drawing.Point(192, 160);
            this.textBoxAdaNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxAdaNo.Name = "textBoxAdaNo";
            this.textBoxAdaNo.Size = new System.Drawing.Size(269, 22);
            this.textBoxAdaNo.TabIndex = 2;
            this.textBoxAdaNo.Tag = "";
            this.textBoxAdaNo.TextChanged += new System.EventHandler(this.textBoxAdaNo_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 164);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Ada No :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(100, 196);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Parsel No :";
            // 
            // textBoxParselNo
            // 
            this.textBoxParselNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxParselNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxParselNo.Location = new System.Drawing.Point(192, 192);
            this.textBoxParselNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxParselNo.Name = "textBoxParselNo";
            this.textBoxParselNo.Size = new System.Drawing.Size(269, 22);
            this.textBoxParselNo.TabIndex = 3;
            this.textBoxParselNo.TextChanged += new System.EventHandler(this.textBoxParselNo_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(112, 292);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Bina No :";
            // 
            // textBoxBinaNo
            // 
            this.textBoxBinaNo.Location = new System.Drawing.Point(193, 288);
            this.textBoxBinaNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxBinaNo.Name = "textBoxBinaNo";
            this.textBoxBinaNo.Size = new System.Drawing.Size(269, 22);
            this.textBoxBinaNo.TabIndex = 5;
            this.textBoxBinaNo.TextChanged += new System.EventHandler(this.textBoxBinaNo_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 103);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Mahalle Kodu (Tapu):";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(61, 418);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Bina Barkod No :";
            // 
            // textBoxBinaBarkodNo
            // 
            this.textBoxBinaBarkodNo.Location = new System.Drawing.Point(193, 415);
            this.textBoxBinaBarkodNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxBinaBarkodNo.Name = "textBoxBinaBarkodNo";
            this.textBoxBinaBarkodNo.Size = new System.Drawing.Size(269, 22);
            this.textBoxBinaBarkodNo.TabIndex = 6;
            // 
            // textBoxBinaKirmiziKotu
            // 
            this.textBoxBinaKirmiziKotu.ForeColor = System.Drawing.Color.Red;
            this.textBoxBinaKirmiziKotu.Location = new System.Drawing.Point(192, 486);
            this.textBoxBinaKirmiziKotu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxBinaKirmiziKotu.MaxLength = 10;
            this.textBoxBinaKirmiziKotu.Name = "textBoxBinaKirmiziKotu";
            this.textBoxBinaKirmiziKotu.Size = new System.Drawing.Size(91, 22);
            this.textBoxBinaKirmiziKotu.TabIndex = 7;
            this.textBoxBinaKirmiziKotu.TextChanged += new System.EventHandler(this.textBoxBinaKirmiziKotu_TextChanged);
            this.textBoxBinaKirmiziKotu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBinaKirmiziKotu_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(113, 228);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 17);
            this.label8.TabIndex = 4;
            this.label8.Text = "Site Adı :";
            // 
            // textBoxSiteAdi
            // 
            this.textBoxSiteAdi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxSiteAdi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxSiteAdi.Location = new System.Drawing.Point(192, 224);
            this.textBoxSiteAdi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxSiteAdi.Name = "textBoxSiteAdi";
            this.textBoxSiteAdi.Size = new System.Drawing.Size(269, 22);
            this.textBoxSiteAdi.TabIndex = 4;
            this.textBoxSiteAdi.TextChanged += new System.EventHandler(this.textBoxParselNo_TextChanged);
            // 
            // comboBoxMahalleKoduTapu
            // 
            this.comboBoxMahalleKoduTapu.DropDownWidth = 250;
            this.comboBoxMahalleKoduTapu.FormattingEnabled = true;
            this.comboBoxMahalleKoduTapu.Items.AddRange(new object[] {
            "kodlari",
            "mahalle"});
            this.comboBoxMahalleKoduTapu.Location = new System.Drawing.Point(192, 96);
            this.comboBoxMahalleKoduTapu.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxMahalleKoduTapu.Name = "comboBoxMahalleKoduTapu";
            this.comboBoxMahalleKoduTapu.Size = new System.Drawing.Size(269, 24);
            this.comboBoxMahalleKoduTapu.Sorted = true;
            this.comboBoxMahalleKoduTapu.TabIndex = 0;
            this.comboBoxMahalleKoduTapu.TextChanged += new System.EventHandler(this.comboBoxMahalleKoduTapu_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(112, 322);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 17);
            this.label9.TabIndex = 5;
            this.label9.Text = "Blok Adı :";
            // 
            // textBlokAdi
            // 
            this.textBlokAdi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBlokAdi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBlokAdi.Location = new System.Drawing.Point(193, 319);
            this.textBlokAdi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBlokAdi.Name = "textBlokAdi";
            this.textBlokAdi.Size = new System.Drawing.Size(269, 22);
            this.textBlokAdi.TabIndex = 5;
            this.textBlokAdi.TextChanged += new System.EventHandler(this.textBoxBinaNo_TextChanged);
            // 
            // textBoxDaskNo
            // 
            this.textBoxDaskNo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxDaskNo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxDaskNo.Location = new System.Drawing.Point(193, 383);
            this.textBoxDaskNo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxDaskNo.Name = "textBoxDaskNo";
            this.textBoxDaskNo.Size = new System.Drawing.Size(269, 22);
            this.textBoxDaskNo.TabIndex = 12;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(53, 354);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 17);
            this.label11.TabIndex = 13;
            this.label11.Text = "DASK Bina Kodu :";
            // 
            // OK
            // 
            this.OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OK.Location = new System.Drawing.Point(360, 700);
            this.OK.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(100, 28);
            this.OK.TabIndex = 8;
            this.OK.Text = "&Tamam";
            this.OK.UseVisualStyleBackColor = true;
            // 
            // Cancel
            // 
            this.Cancel.AccessibleName = "Cancel";
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(191, 700);
            this.Cancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(100, 28);
            this.Cancel.TabIndex = 9;
            this.Cancel.Text = "&Vazgeç";
            this.Cancel.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(25, 30);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 34);
            this.label7.TabIndex = 7;
            this.label7.Text = "Bina Kırmızı Kotu (m) :\r\n(Mimari ±0.00 Kotu)";
            // 
            // textBoxNotlar
            // 
            this.textBoxNotlar.Location = new System.Drawing.Point(101, 596);
            this.textBoxNotlar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxNotlar.Multiline = true;
            this.textBoxNotlar.Name = "textBoxNotlar";
            this.textBoxNotlar.Size = new System.Drawing.Size(359, 88);
            this.textBoxNotlar.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(40, 599);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 17);
            this.label12.TabIndex = 6;
            this.label12.Text = "Notlar :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            // 
            // textBoxDaskKod
            // 
            this.textBoxDaskKod.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxDaskKod.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxDaskKod.Location = new System.Drawing.Point(193, 351);
            this.textBoxDaskKod.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxDaskKod.Name = "textBoxDaskKod";
            this.textBoxDaskKod.Size = new System.Drawing.Size(269, 22);
            this.textBoxDaskKod.TabIndex = 15;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(69, 386);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(107, 17);
            this.label13.TabIndex = 16;
            this.label13.Text = "DASK Bina No :";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(37, 130);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 17);
            this.label14.TabIndex = 18;
            this.label14.Text = "Mahalle Kodu (İdari):";
            // 
            // comboBoxMahalleKoduIdari
            // 
            this.comboBoxMahalleKoduIdari.DropDownWidth = 250;
            this.comboBoxMahalleKoduIdari.FormattingEnabled = true;
            this.comboBoxMahalleKoduIdari.Items.AddRange(new object[] {
            "kodlari",
            "mahalle"});
            this.comboBoxMahalleKoduIdari.Location = new System.Drawing.Point(191, 128);
            this.comboBoxMahalleKoduIdari.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBoxMahalleKoduIdari.Name = "comboBoxMahalleKoduIdari";
            this.comboBoxMahalleKoduIdari.Size = new System.Drawing.Size(269, 24);
            this.comboBoxMahalleKoduIdari.Sorted = true;
            this.comboBoxMahalleKoduIdari.TabIndex = 17;
            this.comboBoxMahalleKoduIdari.TextChanged += new System.EventHandler(this.comboBoxMahalleKoduIdari_TextChanged);
            // 
            // labelOndulasyon
            // 
            this.labelOndulasyon.AutoSize = true;
            this.labelOndulasyon.Location = new System.Drawing.Point(53, 92);
            this.labelOndulasyon.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOndulasyon.Name = "labelOndulasyon";
            this.labelOndulasyon.Size = new System.Drawing.Size(116, 17);
            this.labelOndulasyon.TabIndex = 7;
            this.labelOndulasyon.Text = "Ondülasyon (N) :";
            // 
            // textBoxOndulasyon
            // 
            this.textBoxOndulasyon.Location = new System.Drawing.Point(184, 90);
            this.textBoxOndulasyon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxOndulasyon.Name = "textBoxOndulasyon";
            this.textBoxOndulasyon.Size = new System.Drawing.Size(91, 22);
            this.textBoxOndulasyon.TabIndex = 7;
            this.textBoxOndulasyon.TextChanged += new System.EventHandler(this.textBoxOndulasyon_TextChanged);
            this.textBoxOndulasyon.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBinaKirmiziKotu_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Blue;
            this.label15.Location = new System.Drawing.Point(296, 16);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(142, 17);
            this.label15.TabIndex = 7;
            this.label15.Text = "Elipsoidal (h = H + N)";
            // 
            // textBoxElipsoidal
            // 
            this.textBoxElipsoidal.ForeColor = System.Drawing.Color.Blue;
            this.textBoxElipsoidal.Location = new System.Drawing.Point(299, 37);
            this.textBoxElipsoidal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxElipsoidal.Name = "textBoxElipsoidal";
            this.textBoxElipsoidal.ReadOnly = true;
            this.textBoxElipsoidal.Size = new System.Drawing.Size(139, 22);
            this.textBoxElipsoidal.TabIndex = 7;
            this.textBoxElipsoidal.TextChanged += new System.EventHandler(this.textBoxBinaKirmiziKotu_TextChanged);
            this.textBoxElipsoidal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBinaKirmiziKotu_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonOndulasyon);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBoxElipsoidal);
            this.groupBox3.Controls.Add(this.labelOndulasyon);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.textBoxOndulasyon);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Location = new System.Drawing.Point(9, 449);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(451, 127);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Kot Ayarları";
            // 
            // buttonOndulasyon
            // 
            this.buttonOndulasyon.Location = new System.Drawing.Point(283, 86);
            this.buttonOndulasyon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonOndulasyon.Name = "buttonOndulasyon";
            this.buttonOndulasyon.Size = new System.Drawing.Size(133, 28);
            this.buttonOndulasyon.TabIndex = 20;
            this.buttonOndulasyon.Text = "Otomatik <";
            this.buttonOndulasyon.UseVisualStyleBackColor = true;
            this.buttonOndulasyon.Click += new System.EventHandler(this.buttonOndulasyon_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(181, 16);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 17);
            this.label16.TabIndex = 20;
            this.label16.Text = "Ortometrik (H)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(109, 16);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 17);
            this.label17.TabIndex = 6;
            this.label17.Text = "Proje ID :";
            // 
            // textBoxProjeId
            // 
            this.textBoxProjeId.Location = new System.Drawing.Point(191, 14);
            this.textBoxProjeId.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxProjeId.Name = "textBoxProjeId";
            this.textBoxProjeId.Size = new System.Drawing.Size(269, 22);
            this.textBoxProjeId.TabIndex = 6;
            // 
            // textBoxOtelAdi
            // 
            this.textBoxOtelAdi.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.textBoxOtelAdi.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.textBoxOtelAdi.Location = new System.Drawing.Point(192, 256);
            this.textBoxOtelAdi.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOtelAdi.Name = "textBoxOtelAdi";
            this.textBoxOtelAdi.Size = new System.Drawing.Size(269, 22);
            this.textBoxOtelAdi.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(113, 260);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "Otel Adı :";
            // 
            // BinaBilgiForm
            // 
            this.AcceptButton = this.OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel;
            this.ClientSize = new System.Drawing.Size(473, 782);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.comboBoxMahalleKoduIdari);
            this.Controls.Add(this.textBoxDaskKod);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxNotlar);
            this.Controls.Add(this.textBoxDaskNo);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxOtelAdi);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxBinaKirmiziKotu);
            this.Controls.Add(this.textBoxProjeId);
            this.Controls.Add(this.textBoxBinaBarkodNo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBlokAdi);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxBinaNo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxSiteAdi);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxParselNo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxAdaNo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxMahalleKoduTapu);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxIlceKodu);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BinaBilgiForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Bina Bilgileri";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ParselBilgiForm_FormClosing);
            this.Load += new System.EventHandler(this.ExportForm_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public ClassTapuBilgileri tb;
        private System.Windows.Forms.ComboBox comboBoxIlceKodu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAdaNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxParselNo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBinaNo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxBinaBarkodNo;
        private System.Windows.Forms.TextBox textBoxBinaKirmiziKotu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxSiteAdi;
        private System.Windows.Forms.ComboBox comboBoxMahalleKoduTapu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBlokAdi;
        private System.Windows.Forms.TextBox textBoxDaskNo;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.Button OK;
        public System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxNotlar;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxDaskKod;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxMahalleKoduIdari;
        private System.Windows.Forms.Label labelOndulasyon;
        private System.Windows.Forms.TextBox textBoxOndulasyon;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxElipsoidal;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxProjeId;
        private System.Windows.Forms.Button buttonOndulasyon;
        private System.Windows.Forms.TextBox textBoxOtelAdi;
        private System.Windows.Forms.Label label10;
    }
}