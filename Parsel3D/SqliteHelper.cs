﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.IO;
using System.Reflection;

namespace Parsel3D
{
    class SqliteHelper
    {
        public enum BagimsizTuru { BagimsizBolum, OrtakAlan, Eklenti };
        private SQLiteConnection conn;

        /// <summary>
        /// Provides a method to load spatialite into a given connection
        /// https://github.com/brandonxiang/SpatialiteSharp/blob/master/SpatialiteSharp/SpatialiteLoader.cs
        /// </summary>
        public class SpatialiteLoader
        {
            private static readonly object Lock = new object();

            private static bool _haveSetPath;

            /// <summary>
            /// Loads mod_spatialite.dll on the given connection
            /// </summary>
            public static void Load(SQLiteConnection conn)
            {
                lock (Lock)
                {
                    //Need to work out where the file is and add it to the path so it can load all the other dlls too
                    if (!_haveSetPath)
                    {
                        //var dllPath = AppDomain.CurrentDomain.BaseDirectory;
                        //var spatialitePath = Path.Combine(dllPath, Environment.Is64BitProcess ? "x64" : "x86", "spatialite") + ";";
                        //var paths = Environment.GetEnvironmentVariable("PATH");

                        //Environment.SetEnvironmentVariable("PATH", spatialitePath + paths);
                        var spatialitePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), (Environment.Is64BitProcess ? "x64" : "x86"), "spatialite");

                        Environment.SetEnvironmentVariable("PATH", spatialitePath + ";" + Environment.GetEnvironmentVariable("PATH"));

                        _haveSetPath = true;
                    }
                }

                conn.LoadExtension("mod_spatialite.dll");
            }
        }

        public void Bitir()
        {
            conn.Close();
        }

        public bool CreateParsel3DTables(string dbFilePath)
        {
            SQLiteConnection.CreateFile(dbFilePath);    //        Yoksa oluşturacaktır.
            //conn = new SQLiteConnection("Data Source=" + dbFilePath + ";Version=3;");
            conn = new SQLiteConnection("Data Source=" + dbFilePath + ";Version=3;UseUTF8Encoding=True;");
            conn.Open();

            //InitSpatialite();

            string sql = GetBagimsizBolumCreateTableString("proje_bagimsiz_bolum");
            SQLiteCommand command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            //sql = GetBagimsizBolumCreateTableString("proje_giris");
            //command = new SQLiteCommand(sql, conn);
            //command.ExecuteNonQuery();

            sql = GetProjeBilgileriCreateTableString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetProjeBinaTablosuCreateTableString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetProjeEklentiTablosuCreateTableString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetProjeOrtakAlanTablosuCreateTableString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetProjeKatlarTablosuCreateString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetGeometryColumnsTableCreateString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            sql = GetSpatialRefSysTableCreateString();
            command = new SQLiteCommand(sql, conn);
            command.ExecuteNonQuery();

            //conn.Close();
            return true;
        }
        public bool InitSpatialite()
        {
            SpatialiteLoader.Load(conn);
            //conn.EnableExtensions(true);
            //conn.LoadExtension("mod_spatialite", "sqlite3_modspatialite_init");// module name without filename extension, entry function name


            string query = "SELECT InitSpatialMetaData(1)"; // without 1 as param will take minutes.
            using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
            {
                sqlitecommand.ExecuteNonQuery();
            }
            return true;
        }
        public void Write_ExcelBagimsizTablosuToDatabase(string tabloadi, List<Excel_BagimsizTabloSatiri> ebtsList)
        {
            string query =
                    "insert into " +
                    tabloadi + 
                    //"proje_bagimsiz_bolum (" +
                    " (" +
                    "fid," +
                    "geometry," +
                    "ilcekodu_kisa," +
                    "mahallekodu_kisa," +
                    "ada," +
                    "parsel," +
                    "binano," +
                    "blokadi," +
                    "bagimsizbolumbarkodno," +
                    "katno," +
                    "bagimsizturu," +
                    "tabankotu_mimari," +
                    "tabankotu_denizdenyukseklik," +
                    "katyuksekligi," +
                    //"proje_id," +
                    //"alan_hesaplanan," +
                    //"bina_gid," +
                    //"iptal," +
                    //"kata_gore_azimuth," +
                    //"ikinciel," +
                    "fonksiyon_id," +
                    //"ta_ntlk_id," +
                    //"nt_aln," +
                    //"brt_aln," +
                    //"ta_dr_tp," +
                    //"ars_py," +
                    //"ars_pyd," +
                    "kt_pln_tp," +
                    "bagimsizbolumno" + // virgüle dikkat
                    //"satilma_durumu," +
                    //"ta_manzara_tipi" +

                    ") values (" +

                    "@fid," +
                    "@geometry," +
                    "@ilcekodu_kisa," +
                    "@mahallekodu_kisa," +
                    "@ada," +
                    "@parsel," +
                    "@binano," +
                    "@blokadi," +
                    "@bagimsizbolumbarkodno," +
                    "@katno," +
                    "@bagimsizturu," +
                    "@tabankotu_mimari," +
                    "@tabankotu_denizdenyukseklik," +
                    "@katyuksekligi," +
                    //"@proje_id," +
                    //"@alan_hesaplanan," +
                    //"@bina_gid," +
                    //"@iptal," +
                    //"@kata_gore_azimuth," +
                    //"@ikinciel," +
                    "@fonksiyon_id," +
                    //"@ta_ntlk_id," +
                    //"@nt_aln," +
                    //"@brt_aln," +
                    //"@ta_dr_tp," +
                    //"@ars_py," +
                    //"@ars_pyd," +
                    "@kt_pln_tp," +
                    "@bagimsizbolumno" + // virgüle dikkat
                    //"@satilma_durumu," +
                    //"@ta_manzara_tipi" +
                    ")";

            int i = 0;
            foreach (Excel_BagimsizTabloSatiri ebts in ebtsList)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@fid", DbType.String).Value = i.ToString();
                    sqlitecommand.Parameters.Add("@geometry", DbType.String).Value = ebts.ZeminKoordinatlariEnlemBoylam;
                    sqlitecommand.Parameters.Add("@ilcekodu_kisa", DbType.String).Value = ebts.ilceKodu_Kisa;
                    sqlitecommand.Parameters.Add("@mahallekodu_kisa", DbType.String).Value = ebts.TapuMahalleKodu;
                    sqlitecommand.Parameters.Add("@ada", DbType.String).Value = ebts.Ada;
                    sqlitecommand.Parameters.Add("@parsel", DbType.String).Value = ebts.Parsel;
                    sqlitecommand.Parameters.Add("@binano", DbType.String).Value = ebts.BinaNo;
                    sqlitecommand.Parameters.Add("@blokadi", DbType.String).Value = ebts.BlokAdi;
                    sqlitecommand.Parameters.Add("@bagimsizbolumbarkodno", DbType.String).Value = ebts.getBarkodNo();
                    sqlitecommand.Parameters.Add("@katno", DbType.String).Value = ebts.KatNo;
                    sqlitecommand.Parameters.Add("@bagimsizturu", DbType.String).Value = ebts.getBagimsizTuru();
                    sqlitecommand.Parameters.Add("@tabankotu_mimari", DbType.String).Value = ebts.TabanKotu_Mimari;
                    sqlitecommand.Parameters.Add("@tabankotu_denizdenyukseklik", DbType.String).Value = ebts.TabanKotu_DenizdenYukseklik;
                    sqlitecommand.Parameters.Add("@katyuksekligi", DbType.String).Value = ebts.KatYuksekligi;
                    //sqlitecommand.Parameters.Add("@proje_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@alan_hesaplanan", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@bina_gid", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@iptal", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@kata_gore_azimuth", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ikinciel", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@fonksiyon_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ta_ntlk_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@nt_aln", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@brt_aln", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ta_dr_tp", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ars_py", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ars_pyd", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@kt_pln_tp", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@bagimsizbolumno", DbType.String).Value = ebts.getBolumNo();
                    //sqlitecommand.Parameters.Add("@satilma_durumu", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@ta_manzara_tipi", DbType.String).Value = "";
                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_ExcelOrtakAlanTablosuToDatabase(List<Excel_BagimsizTabloSatiri> ebtsList)
        {
            string query =
                    "insert into " +
                    "proje_ortak_alan (" +
                    "fid," +
                    "geometry," +
                    "ilcekodu_kisa," +
                    "mahallekodu_kisa," +
                    "ada," +
                    "parsel," +
                    "binano," +
                    "blokadi," +
                    "ortak_alan_no," +
                    "ortak_alan_barkod_no," +
                    "katno," +
                    "bagimsizturu," +
                    "tabankotu_mimari," +
                    "tabankotu_denizdenyukseklik," +
                    "katyuksekligi" + // virgüle dikkat
                    //"proje_id," +
                    //"alan_hesaplanan," +
                    //"bina_gid," +
                    //"iptal," +
                    //"kata_gore_azimuth," +
                    //"fonksiyon_id" +

                    ") values (" +

                    "@fid," +
                    "@geometry," +
                    "@ilcekodu_kisa," +
                    "@mahallekodu_kisa," +
                    "@ada," +
                    "@parsel," +
                    "@binano," +
                    "@blokadi," +
                    "@ortak_alan_no," +
                    "@ortak_alan_barkod_no," +
                    "@katno," +
                    "@bagimsizturu," +
                    "@tabankotu_mimari," +
                    "@tabankotu_denizdenyukseklik," +
                    "@katyuksekligi" + // virgüle dikkat

                    //"@proje_id," +
                    //"@alan_hesaplanan," +
                    //"@bina_gid," +
                    //"@iptal," +
                    //"@kata_gore_azimuth," +
                    //"@fonksiyon_id" +
                    ")";

            int i = 0;
            foreach (Excel_BagimsizTabloSatiri ebts in ebtsList)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@fid", DbType.String).Value = i.ToString();
                    sqlitecommand.Parameters.Add("@geometry", DbType.String).Value = ebts.ZeminKoordinatlariEnlemBoylam;
                    sqlitecommand.Parameters.Add("@ilcekodu_kisa", DbType.String).Value = ebts.ilceKodu_Kisa;
                    sqlitecommand.Parameters.Add("@mahallekodu_kisa", DbType.String).Value = ebts.TapuMahalleKodu;
                    sqlitecommand.Parameters.Add("@ada", DbType.String).Value = ebts.Ada;
                    sqlitecommand.Parameters.Add("@parsel", DbType.String).Value = ebts.Parsel;
                    sqlitecommand.Parameters.Add("@binano", DbType.String).Value = ebts.BinaNo;
                    sqlitecommand.Parameters.Add("@blokadi", DbType.String).Value = ebts.BlokAdi;
                    sqlitecommand.Parameters.Add("@ortak_alan_no", DbType.String).Value = ebts.getBolumNo();
                    sqlitecommand.Parameters.Add("@ortak_alan_barkod_no", DbType.String).Value = ebts.getBarkodNo();
                    sqlitecommand.Parameters.Add("@katno", DbType.String).Value = ebts.KatNo;
                    sqlitecommand.Parameters.Add("@bagimsizturu", DbType.String).Value = ebts.getBagimsizTuru();
                    sqlitecommand.Parameters.Add("@tabankotu_mimari", DbType.String).Value = ebts.TabanKotu_Mimari;
                    sqlitecommand.Parameters.Add("@tabankotu_denizdenyukseklik", DbType.String).Value = ebts.TabanKotu_DenizdenYukseklik;
                    sqlitecommand.Parameters.Add("@katyuksekligi", DbType.String).Value = ebts.KatYuksekligi;
                    //sqlitecommand.Parameters.Add("@proje_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@alan_hesaplanan", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@bina_gid", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@iptal", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@kata_gore_azimuth", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@fonksiyon_id", DbType.String).Value = "";
                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_ExcelEklentiTablosuToDatabase(List<Excel_BagimsizTabloSatiri> ebtsList)
        {
            string query =
                    "insert into " +
                    "proje_eklenti (" +
                    "fid," +
                    "geometry," +
                    "ilcekodu_kisa," +
                    "mahallekodu_kisa," +
                    "ada," +
                    "parsel," +
                    "binano," +
                    "blokadi," +
                    "eklenti_no," +
                    "eklenti_barkod_no," +
                    "katno," +
                    "tabankotu_mimari," +
                    "tabankotu_denizdenyukseklik," +
                    "katyuksekligi," +
                    //"proje_id," +
                    //"alan_hesaplanan," +
                    //"bina_gid," +
                    //"iptal," +
                    //"fonksiyon_id," +
                    //"nt_aln," +
                    //"brt_aln," +
                    "bagimsizbolum_no" +
                    ") values(" +

                    "@fid," +
                    "@geometry," +
                    "@ilcekodu_kisa," +
                    "@mahallekodu_kisa," +
                    "@ada," +
                    "@parsel," +
                    "@binano," +
                    "@blokadi," +
                    "@eklenti_no," +
                    "@eklenti_barkod_no," +
                    "@katno," +
                    "@tabankotu_mimari," +
                    "@tabankotu_denizdenyukseklik," +
                    "@katyuksekligi," +
                    //"@proje_id," +
                    //"@alan_hesaplanan," +
                    //"@bina_gid," +
                    //"@iptal," +
                    //"@fonksiyon_id," +
                    //"@nt_aln," +
                    //"@brt_aln," +
                    "@bagimsizbolum_no" +
                    ")";

            int i = 0;
            foreach (Excel_BagimsizTabloSatiri ebts in ebtsList)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@fid", DbType.String).Value = i.ToString();
                    sqlitecommand.Parameters.Add("@geometry", DbType.String).Value = ebts.ZeminKoordinatlariEnlemBoylam;
                    sqlitecommand.Parameters.Add("@ilcekodu_kisa", DbType.String).Value = ebts.ilceKodu_Kisa;
                    sqlitecommand.Parameters.Add("@mahallekodu_kisa", DbType.String).Value = ebts.TapuMahalleKodu;
                    sqlitecommand.Parameters.Add("@ada", DbType.String).Value = ebts.Ada;
                    sqlitecommand.Parameters.Add("@parsel", DbType.String).Value = ebts.Parsel;
                    sqlitecommand.Parameters.Add("@binano", DbType.String).Value = ebts.BinaNo;
                    sqlitecommand.Parameters.Add("@blokadi", DbType.String).Value = ebts.BlokAdi;
                    sqlitecommand.Parameters.Add("@eklenti_no", DbType.String).Value = ebts.getBolumNo();
                    sqlitecommand.Parameters.Add("@eklenti_barkod_no", DbType.String).Value = ebts.getBarkodNo();
                    sqlitecommand.Parameters.Add("@katno", DbType.String).Value = ebts.KatNo;
                    sqlitecommand.Parameters.Add("@tabankotu_mimari", DbType.String).Value = ebts.TabanKotu_Mimari;
                    sqlitecommand.Parameters.Add("@tabankotu_denizdenyukseklik", DbType.String).Value = ebts.TabanKotu_DenizdenYukseklik;
                    sqlitecommand.Parameters.Add("@katyuksekligi", DbType.String).Value = ebts.KatYuksekligi;
                    //sqlitecommand.Parameters.Add("@proje_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@alan_hesaplanan", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@bina_gid", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@iptal", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@fonksiyon_id", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@nt_aln", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@brt_aln", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@bagimsizbolum_no", DbType.String).Value = ebts.getBolumNo();
                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_ExcelBinaTablosuToDatabase(List<Excel_BinaTablosu> listExcelBinaTablosu)
        {
            string query =
                    "insert into proje_bina (" +
                    "fid," +
                    "geometry," +
                    "bina_no," +
                    "blok_adi," +
                    "bina_yukse," +
                    "bina_zemin," +
                    "parsel_gid," +
                    "bina_barko," +
                    "proje_id," +
                    "bina_cati," +
                    "bina_temel," +
                    "dask_bina_kodu," +
                    "dask_bina_no," +
                    "tekil_bina_no," +
                    "iptal," +
                    "ikinciel," +
                    "fonksiyon_id" +

                    ") values (" +

                    "@fid," +
                    "@geometry," +
                    "@bina_no," +
                    "@blok_adi," +
                    "@bina_yukse," +
                    "@bina_zemin," +
                    "@parsel_gid," +
                    "@bina_barko," +
                    "@proje_id," +
                    "@bina_cati," +
                    "@bina_temel," +
                    "@dask_bina_kodu," +
                    "@dask_bina_no," +
                    "@tekil_bina_no," +
                    "@iptal," +
                    "@ikinciel," +
                    "@fonksiyon_id" +
                    ")";

            int i = 0;
            foreach (Excel_BinaTablosu ebt in listExcelBinaTablosu)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@fid", DbType.String).Value = i.ToString();
                    sqlitecommand.Parameters.Add("@geometry", DbType.String).Value = ebt.BinaZeminKoordinatlariEnlemBoylam;
                    sqlitecommand.Parameters.Add("@bina_no", DbType.String).Value = ebt.BinaNo;
                    sqlitecommand.Parameters.Add("@blok_adi", DbType.String).Value = ebt.BlokAdi;
                    //sqlitecommand.Parameters.Add("@bina_yukse", DbType.String).Value = ebt.BinaYuksekligi;
                    sqlitecommand.Parameters.Add("@bina_zemin", DbType.String).Value = ebt.BinaKirmiziKotu; // bina_zemin kotu?
                    sqlitecommand.Parameters.Add("@parsel_gid", DbType.String).Value = ebt.Parsel;
                    sqlitecommand.Parameters.Add("@bina_barko", DbType.String).Value = ebt.BinaBarkodNo;
                    sqlitecommand.Parameters.Add("@proje_id", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@bina_cati", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@bina_temel", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@dask_bina_kodu", DbType.String).Value = "";
                    //sqlitecommand.Parameters.Add("@dask_bina_no", DbType.String).Value = ebt.BinaDaskNo;
                    sqlitecommand.Parameters.Add("@tekil_bina_no", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@iptal", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@ikinciel", DbType.String).Value = "";
                    sqlitecommand.Parameters.Add("@fonksiyon_id", DbType.String).Value = "";

                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_ExcelKatlarTablosuToDatabase(List<Excel_KatlarTablosu> listExcelKatlarTablosu)
        {
            string query =
                    "insert into proje_katlar (" +
                    "fid," +
                    "geometry," +
                    "ilcekodu_kisa," +
                    "mahallekodu_kisa," +
                    "ada," +
                    "parsel," +
                    "bina_no," +
                    "blok_adi," +
                    "kat_no," +
                    "kat_barkod," +
                    "taban_kotu," +
                    "taban_yuks," +
                    "kat_standa" +

                    ") values (" +

                    "@fid," +
                    "@geometry," +
                    "@ilcekodu_kisa," +
                    "@mahallekodu_kisa," +
                    "@ada," +
                    "@parsel," +
                    "@bina_no," +
                    "@blok_adi," +
                    "@kat_no," +
                    "@kat_barkod," +
                    "@taban_kotu," +
                    "@taban_yuks," +
                    "@kat_standa" +
                    ")";

            int i = 0;
            foreach (Excel_KatlarTablosu ekt in listExcelKatlarTablosu)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@fid", DbType.String).Value = i.ToString();
                    sqlitecommand.Parameters.Add("@geometry", DbType.String).Value = ekt.ZeminKoordinatlari_EnlemBoylam;
                    sqlitecommand.Parameters.Add("@ilcekodu_kisa", DbType.String).Value = ekt.ilceKodu_Kisa;
                    sqlitecommand.Parameters.Add("@mahallekodu_kisa", DbType.String).Value = ekt.TapuMahalleKodu;
                    sqlitecommand.Parameters.Add("@ada", DbType.String).Value = ekt.Ada;
                    sqlitecommand.Parameters.Add("@parsel", DbType.String).Value = ekt.Parsel; // bina_zemin kotu?
                    sqlitecommand.Parameters.Add("@bina_no", DbType.String).Value = ekt.BinaNo;
                    sqlitecommand.Parameters.Add("@blok_adi", DbType.String).Value = ekt.BlokAdi;
                    sqlitecommand.Parameters.Add("@kat_no", DbType.String).Value = ekt.KatNo;
                    sqlitecommand.Parameters.Add("@kat_barkod", DbType.String).Value = ekt.KatBarkodNo;
                    sqlitecommand.Parameters.Add("@taban_kotu", DbType.String).Value = ekt.TabanKotu_Mimari;
                    sqlitecommand.Parameters.Add("@taban_yuks", DbType.String).Value = ekt.TabanKotu_DenizdenYukseklik;
                    sqlitecommand.Parameters.Add("@kat_standa", DbType.String).Value = ekt.KatYuksekligi;

                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_GeometryColumnsToDatabase()
        {
            string query =
                    "insert into geometry_columns (" +
                    "f_table_name," +
                    "f_geometry_column," +
                    "geometry_type," +
                    "coord_dimension," +
                    "srid," +
                    "geometry_format" +

                    ") values (" +

                    "@f_table_name," +
                    "@f_geometry_column," +
                    "@geometry_type," +
                    "@coord_dimension," +
                    "@srid," +
                    "@geometry_format" +
                    ")";

            int i = 0;
            //string[] tablenames = { "proje_bagimsiz_bolum", "proje_giris", "proje_eklenti", "proje_ortak_alan", "proje_katlar" };
            string[] tablenames = { "proje_bagimsiz_bolum", "proje_eklenti", "proje_ortak_alan", "proje_katlar" };


            foreach (string name in tablenames)
            {
                using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
                {
                    sqlitecommand.Parameters.Add("@f_table_name", DbType.String).Value = name;
                    sqlitecommand.Parameters.Add("@f_geometry_column", DbType.String).Value = "geometry";
                    sqlitecommand.Parameters.Add("@geometry_type", DbType.String).Value = "3";
                    // http://www.gaia-gis.it/gaia-sins/libspatialite-4.1.0-RC1/spatialite-sql-4.1.0.html
                    //    geom_type has to be one of the followings:
                    //    1 POINT
                    //    2 LINESTRING
                    //    3 POLYGON
                    //    4 MULTIPOINT
                    //    5 MULTILINESTRING
                    //    6 MULTIPOLYGON
                    //    7 GEOMETRYCOLLECTION
                    //
                    //    dimension may be 2, 3 or 4, accordingly to OGR / FDO specs
                    //    geometry_format has to be one of the followings:
                    //
                    //    'WBT'
                    //    'WKT'
                    //    'FGF'

                    sqlitecommand.Parameters.Add("@coord_dimension", DbType.String).Value = "3";
                    sqlitecommand.Parameters.Add("@srid", DbType.String).Value = "4326";
                    sqlitecommand.Parameters.Add("@geometry_format", DbType.String).Value = "WKT";

                    sqlitecommand.ExecuteNonQuery();
                } // using sqlitecommand
                i++;
            } // foreach
        }
        public void Write_RefSysColumnsToDatabase()
        {
            string query =
                    "insert into spatial_ref_sys (" +
                    "srid," +
                    "auth_name," +
                    "auth_srid," +
                    "srtext" +

                    ") values (" +

                    "@srid," +
                    "@auth_name," +
                    "@auth_srid," +
                    "@srtext" +
                    ")";

            using (SQLiteCommand sqlitecommand = new SQLiteCommand(query, conn))
            {
                sqlitecommand.Parameters.Add("@srid", DbType.String).Value = "4326";
                sqlitecommand.Parameters.Add("@auth_name", DbType.String).Value = "EPSG";
                sqlitecommand.Parameters.Add("@auth_srid", DbType.String).Value = "4326";
                sqlitecommand.Parameters.Add("@srtext", DbType.String).Value =
                    "GEOGCS[\"WGS 84\", DATUM[\"WGS_1984\", SPHEROID[\"WGS 84\", 6378137, 298.257223563, AUTHORITY[\"EPSG\", \"7030\"]], " +
                    "AUTHORITY[\"EPSG\", \"6326\"]], PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], UNIT[\"degree\", " +
                    "0.0174532925199433, AUTHORITY[\"EPSG\", \"9122\"]], AXIS[\"Latitude\", NORTH], AXIS[\"Longitude\", EAST], " +
                    "AUTHORITY[\"EPSG\", \"4326\"]]";

                sqlitecommand.ExecuteNonQuery();

                sqlitecommand.Parameters.Add("@srid", DbType.String).Value = "5254";
                sqlitecommand.Parameters.Add("@auth_name", DbType.String).Value = "EPSG";
                sqlitecommand.Parameters.Add("@auth_srid", DbType.String).Value = "5254";
                sqlitecommand.Parameters.Add("@srtext", DbType.String).Value =
                    "PROJCS[\"TUREF / TM30\", GEOGCS[\"TUREF\", DATUM[\"Turkish_National_Reference_Frame\", " +
                    "SPHEROID[\"GRS 1980\", 6378137, 298.257222101, AUTHORITY[\"EPSG\", \"7019\"]], " +
                    "TOWGS84[0, 0, 0, 0, 0, 0, 0], AUTHORITY[\"EPSG\", \"1057\"]], " +
                    "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
                    "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9122\"]], " +
                    "AXIS[\"Latitude\", NORTH], AXIS[\"Longitude\", EAST], AUTHORITY[\"EPSG\", \"5252\"]], " +
                    "PROJECTION[\"Transverse_Mercator\"], PARAMETER[\"latitude_of_origin\", 0], " +
                    "PARAMETER[\"central_meridian\", 30], PARAMETER[\"scale_factor\", 1], " +
                    "PARAMETER[\"false_easting\", 500000], PARAMETER[\"false_northing\", 0], " +
                    "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]], AXIS[\"X\", NORTH], AXIS[\"Y\", EAST], AUTHORITY[\"EPSG\", \"5254\"]]";
                sqlitecommand.ExecuteNonQuery();
            } // using sqlitecommand
        }
        public void Dene(string dbFilePath)
        {
            sqldenemesatiri sqlds = new sqldenemesatiri { WKT = "WKTdene", fid = "123", ilcekodu_kisa = "456" };
            List<sqldenemesatiri> listsqldenemeTablosu = new List<sqldenemesatiri>();
            listsqldenemeTablosu.Add(sqlds);

            //string connectionString = @"Data Source = MyServerName/Instance; Integrated Security=true; Initial Catalog=YourDatabase";
            string connectionString = "Data Source = " + dbFilePath;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection))
                {
                    System.Data.DataTable myDataTable = ExportToExcel.CreateExcelFile.ListToDataTable(listsqldenemeTablosu);
                    myDataTable.TableName = "deneme_tablosu";

                    foreach (DataColumn c in myDataTable.Columns)
                        bulkCopy.ColumnMappings.Add(c.ColumnName, c.ColumnName);

                    bulkCopy.DestinationTableName = myDataTable.TableName;
                    try
                    {
                        bulkCopy.WriteToServer(myDataTable);
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message);
                    }
                }
            }
        }
        //public void DataSetToSqlExport(string dbFilePath, List<sqldenemesatiri> listsqldenemeTablosu)
        //{
        //    //SQLiteConnection.CreateFile(dbFilePath);    //        Yoksa oluşturacaktır.
        //    //SQLiteConnection conn = new SQLiteConnection("Data Source=" + dbFilePath + ";Version=3;");
        //    //conn.Open();

        //    const string sql = "select * from deneme_tablosu;";
        //    var da = new SQLiteDataAdapter(sql, conn);
        //    conn.Close();
        //}
        private string GetBagimsizBolumCreateTableString(string tabloadi)
        {
            string sqlstr =
            //"CREATE TABLE \"proje_bagimsiz_bolum\"(" +
            "CREATE TABLE \"" + tabloadi + "\"(" +
                "\"fid\"    INTEGER," +
                "\"geometry\"   TEXT," +
                "\"ilcekodu_kisa\" INTEGER," +
                "\"mahallekodu_kisa\"  INTEGER," +
                "\"ada\"   INTEGER," +
                "\"parsel\"    INTEGER," +
                "\"binano\"    INTEGER," +
                "\"blokadi\"   TEXT," +
                "\"bagimsizbolumbarkodno\" TEXT," +
                "\"katno\" INTEGER," +
                "\"bagimsizturu\"  TEXT," +
                "\"tabankotu_mimari\"  REAL," +
                "\"tabankotu_denizdenyukseklik\"   REAL," +
                "\"katyuksekligi\" REAL," +
                "\"proje_id\"  INTEGER," +
                "\"alan_hesaplanan\"   TEXT," +
                "\"bina_gid\"  INTEGER," +
                "\"iptal\" INTEGER," +
                "\"kata_gore_azimuth\" TEXT," +
                "\"ikinciel\"  BOOLEAN," +
                "\"fonksiyon_id\"  INTEGER," +
                "\"ta_ntlk_id\"    INTEGER," +
                "\"nt_aln\"    REAL," +
                "\"brt_aln\"   REAL," +
                "\"ta_dr_tp\"  INTEGER," +
                "\"ars_py\"    INTEGER," +
                "\"ars_pyd\"   INTEGER," +
                "\"kt_pln_tp\" TEXT," +
                "\"bagimsizbolumno\"   INTEGER," +
                "\"satilma_durumu\"    BOOLEAN," +
                "\"ta_manzara_tipi\"   INTEGER)";
            return sqlstr;
        }
        private string GetProjeBilgileriCreateTableString()
        {
            string sqlstr =
            "CREATE TABLE \"proje_bilgileri\"(" +
                "\"fid\"    INTEGER," +
                "\"proje_adi\" TEXT," +
                "\"firma_id\"  INTEGER," +
                "\"onizleme_resim\"    TEXT," +
                "\"aciklama\"  TEXT," +
                "\"yapim_yili\"    TEXT," +
                "\"teslim_tarihi\" TEXT," +
                "\"mimar_adi\" TEXT," +
                "\"konut_sayisi\"  TEXT," +
                "\"ofis_sayisi\"   TEXT," +
                "\"magaza_sayisi\" TEXT)";
            return sqlstr;
        }
        private string GetProjeBinaTablosuCreateTableString()
        {
            string sqlstr =
                "CREATE TABLE \"proje_bina\"(" +
                    "\"fid\"   INTEGER," +
                    "\"geometry\"   TEXT," +
                    "\"bina_no\"   INTEGER," +
                    "\"blok_adi\"  TEXT," +
                    "\"bina_yukse\"    TEXT," +
                    "\"bina_zemin\"    TEXT," +
                    "\"parsel_gid\"    INTEGER," +
                    "\"bina_barko\"    TEXT," +
                    "\"proje_id\"  INTEGER," +
                    "\"bina_cati\" TEXT," +
                    "\"bina_temel\"    TEXT," +
                    "\"dask_bina_kodu\"    TEXT," +
                    "\"dask_bina_no\"  TEXT," +
                    "\"tekil_bina_no\" TEXT," +
                    "\"iptal\" INTEGER," +
                    "\"ikinciel\"  INTEGER," +
                    "\"fonksiyon_id\"  INTEGER)";
            return sqlstr;
        }
        private string GetProjeEklentiTablosuCreateTableString()
        {
            string sqlstr =
            "CREATE TABLE \"proje_eklenti\"(" +
                "\"fid\"    INTEGER," +
                "\"geometry\"   TEXT," +
                "\"ilcekodu_kisa\" INTEGER," +
                "\"mahallekodu_kisa\"  INTEGER," +
                "\"ada\"   INTEGER," +
                "\"parsel\"    INTEGER," +
                "\"binano\"    INTEGER," +
                "\"blokadi\"   TEXT," +
                "\"eklenti_no\"    TEXT," +
                "\"eklenti_barkod_no\" TEXT," +
                "\"katno\" INTEGER," +
                "\"tabankotu_mimari\"  REAL," +
                "\"tabankotu_denizdenyukseklik\"   REAL," +
                "\"katyuksekligi\" REAL," +
                "\"proje_id\"  INTEGER," +
                "\"alan_hesaplanan\"   TEXT," +
                "\"bina_gid\"  INTEGER," +
                "\"iptal\" BOOLEAN," +
                "\"fonksiyon_id\"  INTEGER," +
                "\"nt_aln\"    REAL," +
                "\"brt_aln\"   REAL," +
                "\"bagimsizbolum_no\"  INTEGER)";
            return sqlstr;
        }
        private string GetProjeOrtakAlanTablosuCreateTableString()
        {
            string sqlstr =
            "CREATE TABLE \"proje_ortak_alan\"(" +
                "\"fid\"    INTEGER," +
                "\"geometry\"   TEXT," +
                "\"ilcekodu_kisa\" INTEGER," +
                "\"mahallekodu_kisa\"  INTEGER," +
                "\"ada\"   INTEGER," +
                "\"parsel\"    INTEGER," +
                "\"binano\"    INTEGER," +
                "\"blokadi\"   TEXT," +
                "\"ortak_alan_no\" TEXT," +
                "\"ortak_alan_barkod_no\"  TEXT," +
                "\"katno\" INTEGER," +
                "\"bagimsizturu\"  TEXT," +
                "\"tabankotu_mimari\"  REAL," +
                "\"tabankotu_denizdenyukseklik\"   REAL," +
                "\"katyuksekligi\" REAL," +
                "\"proje_id\"  INTEGER," +
                "\"alan_hesaplanan\"   TEXT," +
                "\"bina_gid\"  INTEGER," +
                "\"iptal\" BOOLEAN," +
                "\"kata_gore_azimuth\" TEXT," +
                "\"fonksiyon_id\"  INTEGER)";
            return sqlstr;
        }
        private string GetProjeKatlarTablosuCreateString()
        {
            string sqlstr =
            "CREATE TABLE \"proje_katlar\"(" +
                "\"fid\"    INTEGER," +
                "\"geometry\"   TEXT," +
                "\"ilcekodu_kisa\" INTEGER," +
                "\"mahallekodu_kisa\"  INTEGER," +
                "\"ada\"   INTEGER," +
                "\"parsel\"    INTEGER," +
                "\"bina_no\"    INTEGER," +
                "\"blok_adi\"   TEXT," +
                "\"kat_no\"   INTEGER," +
                "\"kat_barkod\"  TEXT," +
                "\"taban_kotu\"  REAL," +
                "\"taban_yuks\"   REAL," +
                "\"kat_standa\" REAL" +
                ")";
            return sqlstr;
        }
        private string GetGeometryColumnsTableCreateString()
        {
            string sqlstr =
            "CREATE TABLE \"geometry_columns\"(" +
            "\"f_table_name\" VARCHAR," +
            "\"f_geometry_column\" VARCHAR," +
            "\"geometry_type\" INTEGER," +
            "\"coord_dimension\" INTEGER," +
            "\"srid\" INTEGER," +
            "\"geometry_format\" VARCHAR)";
            return sqlstr;
        }
        private string GetSpatialRefSysTableCreateString()
        {
            string sqlstr = "CREATE TABLE \"spatial_ref_sys\"(" + 
                "\"srid\" INTEGER UNIQUE," +
                "\"auth_name\" TEXT," +
                "\"auth_srid\" TEXT," +
                "\"srtext\" TEXT)";
            return sqlstr;
        }
        //private string GetSqliteSequenceTableCreateString() // reserved table name hatası veriyor. gerek yok eklemeye
        //{
        //    string sqlstr = "CREATE TABLE sqlite_sequence(name,seq)";
        //    return sqlstr;
        //}
    }
}