// https://forums.autodesk.com/t5/net/how-to-add-new-attibutedefinition-newly-c-object-arx/td-p/3534210
        [CommandMethod("blkonfly")]
        public static void CreateBlockMark()
        {
            // Get the current document and database
            Document doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;

            Database db = doc.Database;

            Editor ed = doc.Editor;
            // Start a transaction
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                try
                {
                    BlockTable bt = (BlockTable)tr.GetObject(db.BlockTableId, OpenMode.ForWrite);
                    string blkName = "BLDG_MARK";
                    if (bt.Has(blkName))
                    {
                        ed.WriteMessage("\nBlock \"BLDG_MARK\" already exist.");
                        return;
                    }
                    BlockTableRecord btr = (BlockTableRecord)tr.GetObject(db.CurrentSpaceId, OpenMode.ForWrite);
                    Point3d inspt = new Point3d(0, 0, 0);
                    BlockTableRecord newBtr = new BlockTableRecord();
                    bt.Add(newBtr);
                    newBtr.Name = blkName;
                    newBtr.Origin = inspt;
                    newBtr.BlockScaling = BlockScaling.Uniform;
                    newBtr.Units = UnitsValue.Inches;
                    newBtr.Explodable = true;
                    tr.AddNewlyCreatedDBObject(newBtr, true);

                    Polyline pline = new Polyline();

                    Point2dCollection pts = new Point2dCollection()
                    { new Point2d(2.454, 3.4631), 
                        new Point2d(1.991, 2.908), 
                        new Point2d(1.991, 0), 
                        new Point2d(-1.991, 0),
                    new Point2d (-1.991,  2.908),
                    new Point2d (-1.991,  2.908),
                    new Point2d (-2.454,  3.4631),
                    new Point2d (0, 5.0096)
                    };
                    int i = 0;
                    foreach (Point2d pt in pts)
                    {
                        pline.AddVertexAt(i, pt, 0, 0, 0);
                        i += 1;
                    }
                    pline.Closed = true;
                    pline.Layer = "0";
                    pline.LinetypeId = db.ContinuousLinetype;
                    pline.ColorIndex = 0;
                    pline.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 0);
                    newBtr.AppendEntity(pline);
                    tr.AddNewlyCreatedDBObject(pline, true);

                    AttributeDefinition attr = new AttributeDefinition();
                    attr.Layer = "0";
                    attr.LinetypeId = db.ContinuousLinetype;
                    attr.ColorIndex = 0;
                    attr.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 0);
                    attr.Tag = "BLDG_NUMBER";
                    attr.Prompt = "Building number:";
                    attr.TextString = "900";
                    attr.Preset=false;
                    
                    //attr.TextStyle = db.Textstyle;//<--   A2009
                    attr.TextStyleId = db.Textstyle;//<--   A2010
                    attr.Height = 1.0;
                    attr.Position = new Point3d(0, 1.0574, 0);
                    attr.Justify = AttachmentPoint.MiddleCenter;
                    attr.AlignmentPoint = new Point3d(0, 1.0574, 0);
                    attr.LockPositionInBlock = true;
                    attr.AdjustAlignment(db);
                    newBtr.AppendEntity(attr);
                    tr.AddNewlyCreatedDBObject(attr, true);


                    AttributeDefinition attr2 = new AttributeDefinition();
                    attr2.Layer = "0";
                    attr2.LinetypeId = db.ContinuousLinetype;
                    attr2.ColorIndex = 0;
                    attr2.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 0);
                    attr2.Tag = "BLDG_CODE";
                    attr2.Prompt = "Building code:";
                    attr2.TextString = "DM1000";
                    attr2.Preset = false;
                    
                    //attr.TextStyle = db.Textstyle;//<--   A2009
                    attr2.TextStyleId = db.Textstyle;//<--   A2010
                    attr2.Height = 1.0;
                    attr2.Position = new Point3d (0, 6.0678, 0);                   
                    attr2.Justify = AttachmentPoint.MiddleCenter;
                    attr2.AlignmentPoint = new Point3d(0, 6.0678, 0);
                    attr2.LockPositionInBlock = true;
                    attr2.AdjustAlignment(db);
                    newBtr.AppendEntity(attr2);
                    tr.AddNewlyCreatedDBObject(attr2, true);

                    AttributeDefinition attr3 = new AttributeDefinition();
                    attr3.Layer = "0";
                    attr3.LinetypeId = db.ContinuousLinetype;
                    attr3.ColorIndex = 0;
                    attr3.Color = Autodesk.AutoCAD.Colors.Color.FromColorIndex(ColorMethod.ByAci, 0);
                    attr3.Tag =  "BLDG_MARK";
                    attr3.Prompt =  "Building mark:";
                    attr3.TextString = "M";
                    //attr.Verifiable = true;
                    attr.Preset = false;
                    //attr.TextStyle = db.Textstyle;//<--   A2009
                    attr3.TextStyleId = db.Textstyle;//<--   A2010
                    attr3.Height = 1.0;
                    attr3.Position = new Point3d (2.7165, 4.5163, 0);
                    attr3.Justify = AttachmentPoint.MiddleCenter;
                    attr3.AlignmentPoint = new Point3d (2.7165, 4.5163, 0);
                    attr3.LockPositionInBlock = true;
                    attr3.AdjustAlignment(db);
                    newBtr.AppendEntity(attr3);
                    tr.AddNewlyCreatedDBObject(attr3, true);

                    tr.Commit();

                }
                catch (System.Exception ex)
                {
                    ed.WriteMessage(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }